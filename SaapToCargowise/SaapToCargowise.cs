﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace SaapToCargowise
{
    public class ConversionException : System.Exception
    {
        public ConversionException()
        {

        }
        public ConversionException(String message) : base(message)
        {

        }
        public ConversionException(String message, Exception innerException) : base(message, innerException)
        { }
    }
    public class ConvertToCW
    {
        public String DoConvert(String fileName, String senderid, String recipientid, String outputpath)
        {
            string xmlPath = Path.GetDirectoryName(fileName);
            Article articleFile = new Article();
            if (Directory.Exists(xmlPath))
            {
                XmlSerializer xmlSaap = new XmlSerializer(typeof(Article));
                StreamReader sr = new StreamReader(fileName);
                string xmlSource = sr.ReadToEnd();
                sr.Close();
                sr.Dispose();
                try
                {
                    using (StringReader reader = new StringReader(xmlSource))
                    {
                        articleFile = (Article)xmlSaap.Deserialize(reader);
                        reader.Close();
                        reader.Dispose();
                    }
                }
                catch (Exception Ex)
                {

                    return "Invalid XSD. Checking Next";
                }

                UniversalInterchange ui = new UniversalInterchange();
                ui.Header.RecipientID = recipientid;
                ui.Header.SenderID = senderid;
                UniversalInterchangeBody bodyData = new UniversalInterchangeBody();

                NativeProduct nativeProduct = new NativeProduct();

                nativeProduct.Action = Action.INSERT;
                nativeProduct.IsWarehouseProduct = true;
                nativeProduct.PartNum = articleFile.PartNum;
                
                String cwXML = Path.Combine(outputpath, senderid + articleFile.PartNum + ".xml");
                Stream outPutSaap = File.Open(cwXML, FileMode.Create);
                StringWriter writer = new StringWriter();
                XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));



            }


            //            Stream outputSilk = File.Open(cwXML, FileMode.Create);
            //            StringWriter writer = new StringWriter();
            //            XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
            //            xSer.Serialize(outputSilk, interchange);
            //            outputSilk.Flush();
            //            outputSilk.Close();
            //        }
            //        catch (Exception ex)
            //        {
            //            throw new ConversionException(ex.Message);
            //        }
            //        sr.Close();
            //    }
            return "Conversion Complete";
            //}
            //else
            //{
            //    throw new ConversionException("XML Path not found");
            //}
        }
    }
}
