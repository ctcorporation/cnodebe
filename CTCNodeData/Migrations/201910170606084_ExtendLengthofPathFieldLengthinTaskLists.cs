namespace CTCNodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExtendLengthofPathFieldLengthinTaskLists : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TaskLists", "TL_Path", c => c.String(maxLength: 150));
            AlterColumn("dbo.TaskLists", "TL_ReceivedFolder", c => c.String(maxLength: 150));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TaskLists", "TL_ReceivedFolder", c => c.String(maxLength: 50));
            AlterColumn("dbo.TaskLists", "TL_Path", c => c.String(maxLength: 50));
        }
    }
}
