namespace CTCNodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMaxLengthToSubject : DbMigration
    {
        public override void Up()
        {
            //RenameTable(name: "dbo.TODOes", newName: "TODO");
            AlterColumn("dbo.TaskLists", "TL_Subject", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TaskLists", "TL_Subject", c => c.String(maxLength: 100));
            //RenameTable(name: "dbo.TODO", newName: "TODOes");
        }
    }
}
