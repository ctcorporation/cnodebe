﻿namespace CTCNodeData.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddMappingMasterFields : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MappingMasterFields",
                c => new
                {
                    MM_ID = c.Guid(nullable: false, defaultValueSql: "newid()"),
                    MM_FieldName = c.String(maxLength: 50),
                    MM_MO = c.Guid(nullable: false),
                })
                .PrimaryKey(t => t.MM_ID);

        }

        public override void Down()
        {
            DropTable("dbo.MappingMasterFields");
        }
    }
}
