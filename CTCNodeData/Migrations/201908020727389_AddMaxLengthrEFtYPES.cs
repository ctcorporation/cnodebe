namespace CTCNodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMaxLengthrEFtYPES : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Transactions", "T_REF1TYPE", c => c.String(maxLength: 20));
            AlterColumn("dbo.Transactions", "T_REF2TYPE", c => c.String(maxLength: 20));
            AlterColumn("dbo.Transactions", "T_REF3TYPE", c => c.String(maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transactions", "T_REF3TYPE", c => c.String());
            AlterColumn("dbo.Transactions", "T_REF2TYPE", c => c.String());
            AlterColumn("dbo.Transactions", "T_REF1TYPE", c => c.String());
        }
    }
}
