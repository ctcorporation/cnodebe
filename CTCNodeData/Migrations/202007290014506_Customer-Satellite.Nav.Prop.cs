﻿namespace CTCNodeData.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CustomerSatelliteNavProp : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Customer", "HeartBeat_HB_ID", c => c.Guid());
            //  AddColumn("dbo.PROFILE", "P_RENAMEFILE", c => c.Boolean(nullable: false));
            //   AddColumn("dbo.vw_CustomerProfile", "P_RENAMEFILE", c => c.Boolean(nullable: false));
            CreateIndex("dbo.Customer", "HeartBeat_HB_ID");
            AddForeignKey("dbo.Customer", "HeartBeat_HB_ID", "dbo.HEARTBEAT", "HB_ID");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Customer", "HeartBeat_HB_ID", "dbo.HEARTBEAT");
            DropIndex("dbo.Customer", new[] { "HeartBeat_HB_ID" });
            DropColumn("dbo.vw_CustomerProfile", "P_RENAMEFILE");
            DropColumn("dbo.PROFILE", "P_RENAMEFILE");
            DropColumn("dbo.Customer", "HeartBeat_HB_ID");
        }
    }
}
