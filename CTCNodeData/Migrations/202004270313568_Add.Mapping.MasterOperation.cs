﻿namespace CTCNodeData.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddMappingMasterOperation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MappingMasterOperations",
                c => new
                {
                    MO_ID = c.Guid(nullable: false, defaultValueSql: "newid()"),
                    MO_OperationName = c.String(maxLength: 50),
                    MO_Description = c.String(),
                })
                .PrimaryKey(t => t.MO_ID);

        }

        public override void Down()
        {
            DropTable("dbo.MappingMasterOperations");
        }
    }
}
