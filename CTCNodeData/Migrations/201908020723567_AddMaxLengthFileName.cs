namespace CTCNodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMaxLengthFileName : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Transactions", "T_FILENAME", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transactions", "T_FILENAME", c => c.String());
        }
    }
}
