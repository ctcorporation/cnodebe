namespace CTCNodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TodoTableAdded : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.APILOGs",
            //    c => new
            //        {
            //            F_TRACKINGID = c.Guid(nullable: false),
            //            F_FILENAME = c.String(nullable: false, maxLength: 80),
            //            F_SENDERID = c.String(nullable: false, maxLength: 15),
            //            F_CLIENTID = c.String(maxLength: 20),
            //            F_APPCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            F_SUBJECT = c.String(maxLength: 50),
            //            F_CWFILENAME = c.String(maxLength: 80),
            //            F_DATERECEIVED = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.F_TRACKINGID);
            
            //CreateTable(
            //    "dbo.Archives",
            //    c => new
            //        {
            //            AL_ID = c.Guid(nullable: false),
            //            AL_TL = c.Guid(nullable: false),
            //            AL_Path = c.String(maxLength: 50),
            //            AL_ArchiveName = c.String(maxLength: 20),
            //            AL_DateArchived = c.DateTime(nullable: false),
            //            AL_FileName = c.String(maxLength: 50),
            //        })
            //    .PrimaryKey(t => t.AL_ID);
            
            //CreateTable(
            //    "dbo.Cargowise_Enums",
            //    c => new
            //        {
            //            CW_ID = c.Guid(nullable: false),
            //            CW_ENUMTYPE = c.String(maxLength: 20, fixedLength: true, unicode: false),
            //            CW_ENUM = c.String(maxLength: 10, fixedLength: true),
            //            CW_MAPVALUE = c.String(maxLength: 20, unicode: false),
            //        })
            //    .PrimaryKey(t => t.CW_ID);
            
            //CreateTable(
            //    "dbo.CONTACTS",
            //    c => new
            //        {
            //            S_ID = c.Guid(nullable: false),
            //            S_CONTACTNAME = c.String(maxLength: 50, unicode: false),
            //            S_EMAILADDRESS = c.String(unicode: false),
            //            S_C = c.Guid(),
            //            S_ALERTS = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //        })
            //    .PrimaryKey(t => t.S_ID);
            
            //CreateTable(
            //    "dbo.Customer",
            //    c => new
            //        {
            //            C_ID = c.Guid(nullable: false),
            //            C_NAME = c.String(maxLength: 50, unicode: false),
            //            C_IS_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_ON_HOLD = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_PATH = c.String(maxLength: 80, unicode: false),
            //            C_FTP_CLIENT = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_CODE = c.String(maxLength: 15, unicode: false),
            //            C_TRIAL = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_TRIALSTART = c.DateTime(),
            //            C_TRIALEND = c.DateTime(),
            //            C_SHORTNAME = c.String(maxLength: 10, fixedLength: true),
            //            C_SATELLITE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_INVOICED = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //        })
            //    .PrimaryKey(t => t.C_ID);
            
            //CreateTable(
            //    "dbo.HEARTBEAT",
            //    c => new
            //        {
            //            HB_ID = c.Guid(nullable: false),
            //            HB_C = c.Guid(),
            //            HB_NAME = c.String(maxLength: 50, unicode: false),
            //            HB_PATH = c.String(maxLength: 200, unicode: false),
            //            HB_ISMONITORED = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            HB_PID = c.Int(),
            //            HB_LASTOPERATION = c.String(maxLength: 50, unicode: false),
            //            HB_LASTCHECKIN = c.DateTime(),
            //            HB_OPENED = c.DateTime(),
            //            HB_LASTOPERATIONPARAMS = c.String(maxLength: 100),
            //        })
            //    .PrimaryKey(t => t.HB_ID);
            
            //CreateTable(
            //    "dbo.PROFILE",
            //    c => new
            //        {
            //            P_ID = c.Guid(nullable: false),
            //            P_C = c.Guid(),
            //            P_REASONCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_SERVER = c.String(unicode: false),
            //            P_USERNAME = c.String(unicode: false),
            //            P_PASSWORD = c.String(unicode: false),
            //            P_DELIVERY = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_PORT = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //            P_DESCRIPTION = c.String(unicode: false),
            //            P_PATH = c.String(maxLength: 100, unicode: false),
            //            P_DIRECTION = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_LIBNAME = c.String(maxLength: 50, unicode: false),
            //            P_MESSAGETYPE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_RECIPIENTID = c.String(maxLength: 15, unicode: false),
            //            P_MSGTYPE = c.String(maxLength: 20, fixedLength: true, unicode: false),
            //            P_CHARGEABLE = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_BILLTO = c.String(maxLength: 15, fixedLength: true, unicode: false),
            //            P_SENDERID = c.String(maxLength: 15, unicode: false),
            //            P_DTS = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_EMAILADDRESS = c.String(maxLength: 100, unicode: false),
            //            P_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_SSL = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_SENDEREMAIL = c.String(maxLength: 100, unicode: false),
            //            P_SUBJECT = c.String(maxLength: 100, unicode: false),
            //            P_FILETYPE = c.String(maxLength: 7, fixedLength: true, unicode: false),
            //            P_MESSAGEDESCR = c.String(maxLength: 20, unicode: false),
            //            P_EVENTCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_CUSTOMERCOMPANYNAME = c.String(maxLength: 50, unicode: false),
            //            P_GROUPCHARGES = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_XSD = c.String(maxLength: 50, unicode: false),
            //            P_PARAMLIST = c.String(unicode: false),
            //            P_METHOD = c.String(maxLength: 50, unicode: false),
            //            P_NOTIFY = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_FORWARDWITHFLAGS = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_CWSUBJECT = c.String(maxLength: 80),
            //            P_CWFILENAME = c.String(maxLength: 80),
            //            P_CWAPPCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //        })
            //    .PrimaryKey(t => t.P_ID);
            
            //CreateTable(
            //    "dbo.TaskLists",
            //    c => new
            //        {
            //            TL_ID = c.Guid(nullable: false),
            //            TL_OrginalFileName = c.String(maxLength: 100),
            //            TL_FileName = c.String(maxLength: 50),
            //            TL_ReceiveDate = c.DateTime(),
            //            TL_Processed = c.Boolean(nullable: false),
            //            TL_Path = c.String(maxLength: 50),
            //            TL_Subject = c.String(maxLength: 100),
            //            TL_SenderID = c.String(maxLength: 50),
            //            TL_RecipientID = c.String(maxLength: 50),
            //            TL_ReceivedFolder = c.String(maxLength: 50),
            //            TL_ReceiverProcess = c.String(maxLength: 10),
            //            TL_ProcessDate = c.DateTime(),
            //            TL_P = c.Guid(),
            //            TL_CWFileName = c.String(maxLength: 80),
            //            TL_FromAddress = c.String(maxLength: 50),
            //            TL_ToAddress = c.String(maxLength: 50),
            //        })
            //    .PrimaryKey(t => t.TL_ID);
            
            //CreateTable(
            //    "dbo.TODOes",
            //    c => new
            //        {
            //            L_ID = c.Guid(nullable: false, identity: true),
            //            L_P = c.Guid(),
            //            L_FILENAME = c.String(),
            //            L_LASTRESULT = c.String(),
            //            L_DATE = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.L_ID);
            
            //CreateTable(
            //    "dbo.vw_CustomerProfile",
            //    c => new
            //        {
            //            C_IS_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_ON_HOLD = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_FTP_CLIENT = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_ID = c.Guid(nullable: false),
            //            C_ID = c.Guid(nullable: false),
            //            P_DIRECTION = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_DTS = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_NOTIFY = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_NAME = c.String(maxLength: 50, unicode: false),
            //            C_CODE = c.String(maxLength: 15, unicode: false),
            //            C_PATH = c.String(maxLength: 80, unicode: false),
            //            P_C = c.Guid(),
            //            P_REASONCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_SERVER = c.String(unicode: false),
            //            P_USERNAME = c.String(unicode: false),
            //            P_PASSWORD = c.String(unicode: false),
            //            P_DESCRIPTION = c.String(unicode: false),
            //            P_PORT = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //            P_DELIVERY = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_PATH = c.String(maxLength: 100, unicode: false),
            //            P_XSD = c.String(maxLength: 50, unicode: false),
            //            P_LIBNAME = c.String(maxLength: 50, unicode: false),
            //            P_RECIPIENTID = c.String(maxLength: 15, unicode: false),
            //            P_SENDERID = c.String(maxLength: 15, unicode: false),
            //            P_BILLTO = c.String(maxLength: 15, fixedLength: true, unicode: false),
            //            P_CHARGEABLE = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_MSGTYPE = c.String(maxLength: 20, fixedLength: true, unicode: false),
            //            P_MESSAGETYPE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_FILETYPE = c.String(maxLength: 7, fixedLength: true, unicode: false),
            //            P_EMAILADDRESS = c.String(maxLength: 100, unicode: false),
            //            P_SSL = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_SENDEREMAIL = c.String(maxLength: 100, unicode: false),
            //            P_SUBJECT = c.String(maxLength: 100, unicode: false),
            //            P_MESSAGEDESCR = c.String(maxLength: 20, unicode: false),
            //            P_EVENTCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_CUSTOMERCOMPANYNAME = c.String(maxLength: 50, unicode: false),
            //            C_SHORTNAME = c.String(maxLength: 10, fixedLength: true),
            //            P_METHOD = c.String(maxLength: 50, unicode: false),
            //            P_PARAMLIST = c.String(unicode: false),
            //            P_FORWARDWITHFLAGS = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_CWSUBJECT = c.String(maxLength: 80),
            //            P_CWFILENAME = c.String(maxLength: 80),
            //            P_CWAPPCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //        })
            //    .PrimaryKey(t => new { t.C_IS_ACTIVE, t.C_ON_HOLD, t.C_FTP_CLIENT, t.P_ID, t.C_ID, t.P_DIRECTION, t.P_DTS, t.P_ACTIVE, t.P_NOTIFY });
            
        }
        
        public override void Down()
        {
            //DropTable("dbo.vw_CustomerProfile");
            //DropTable("dbo.TODOes");
            //DropTable("dbo.TaskLists");
            //DropTable("dbo.PROFILE");
            //DropTable("dbo.HEARTBEAT");
            //DropTable("dbo.Customer");
            //DropTable("dbo.CONTACTS");
            //DropTable("dbo.Cargowise_Enums");
            //DropTable("dbo.Archives");
            //DropTable("dbo.APILOGs");
        }
    }
}
