namespace CTCNodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMaxLengthTransactionReferences : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Transactions", "T_REF1", c => c.String(maxLength: 200));
            AlterColumn("dbo.Transactions", "T_REF2", c => c.String(maxLength: 200));
            AlterColumn("dbo.Transactions", "T_REF3", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transactions", "T_REF3", c => c.String());
            AlterColumn("dbo.Transactions", "T_REF2", c => c.String());
            AlterColumn("dbo.Transactions", "T_REF1", c => c.String());
        }
    }
}
