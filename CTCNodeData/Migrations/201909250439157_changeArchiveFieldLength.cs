namespace CTCNodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeArchiveFieldLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TaskLists", "TL_ArchiveName", c => c.String(maxLength: 200));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TaskLists", "TL_ArchiveName", c => c.String(maxLength: 50));
        }
    }
}
