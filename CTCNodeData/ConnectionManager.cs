﻿using NodeResources;
using System.Configuration;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;

namespace CTCNodeData
{
    public class ConnectionManager : IConnectionManager
    {
        public string ConnString { get; set; }
        public string ServerName { get; set; }
        public string DatabaseName { get; set; }
        public string User { get; set; }
        public string Password { get; set; }

        public ConnectionManager()
        {
            ConnectionStringSettings _settings = ConfigurationManager.ConnectionStrings["CNODEEntities"];
            ConnString = _settings.ConnectionString.ToString();
        }

        public ConnectionManager(string server, string database, string userName, string password)
        {
            Password = password;
            DatabaseName = database;
            ServerName = server;
            User = userName;
            ConnString = BuildConnectionString(ServerName, DatabaseName, User, Password);
        }
        //public ConnectionManager(string server, string database, string userName, string password)
        //{
        //    ConnString = BuildEntityConnectionString(BuildConnectionString(server, database, userName, password));
        //}

        public string BuildConnectionString(string server, string database, string user, string password)
        {
            var sqlBuilder = new SqlConnectionStringBuilder
            {
                DataSource = server,
                InitialCatalog = database,
                UserID = user,
                Password = password

            };
            return sqlBuilder.ToString();

        }

        public string BuildEntityConnectionString(string sqlString)
        {
            EntityConnectionStringBuilder esb = new EntityConnectionStringBuilder();
            esb.Metadata = "res://*/Models.CTCNodeModel.csdl|res://*/Models.CTCNodeModel.ssdl|res://*/Models.CTCNodeModel.msl";
            esb.Provider = "System.Data.SqlClient";
            esb.ProviderConnectionString = sqlString;
            return esb.ToString();

        }
    }
}
