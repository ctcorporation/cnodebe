namespace CTCNodeData.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class CONTACTS
    {
        [Key]
        public Guid S_ID { get; set; }

        [StringLength(50)]
        public string S_CONTACTNAME { get; set; }

        public string S_EMAILADDRESS { get; set; }

        public Guid? S_C { get; set; }

        [StringLength(1)]
        public string S_ALERTS { get; set; }
    }
}
