namespace CTCNodeData.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class vw_HeartBeat
    {
        [StringLength(50)]
        public string C_NAME { get; set; }

        [StringLength(50)]
        public string HB_NAME { get; set; }

        [StringLength(50)]
        public string HB_LASTOPERATION { get; set; }

        public DateTime? HB_OPENED { get; set; }

        public bool? HAS_SATELLITE { get; set; }

        public bool? IS_MONITORED { get; set; }

        [Key]
        public Guid HB_ID { get; set; }

        public int? HB_PID { get; set; }

        public DateTime? HB_LASTCHECKIN { get; set; }
    }
}
