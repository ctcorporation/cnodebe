﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CTCNodeData.Models
{
    public class MappingMasterOperation
    {
        [Key]
        public Guid MO_ID { get; set; }

        [MaxLength(50)]
        [Display(Name = "Map Operation")]
        public string MO_OperationName { get; set; }

        [Display(Name = "Operation Description")]
        public string MO_Description { get; set; }
    }
}
