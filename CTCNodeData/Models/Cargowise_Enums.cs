namespace CTCNodeData.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Cargowise_Enums
    {
        [Key]
        public Guid CW_ID { get; set; }

        [StringLength(20)]
        public string CW_ENUMTYPE { get; set; }

        [StringLength(10)]
        public string CW_ENUM { get; set; }

        [StringLength(20)]
        public string CW_MAPVALUE { get; set; }
    }
}
