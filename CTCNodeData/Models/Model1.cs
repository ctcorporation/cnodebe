namespace CTCNodeData.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<vw_HeartBeat> VW_HEARTBEAT { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<vw_HeartBeat>()
                .Property(e => e.C_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_HeartBeat>()
                .Property(e => e.HB_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_HeartBeat>()
                .Property(e => e.HB_LASTOPERATION)
                .IsUnicode(false);
        }
    }
}
