namespace CTCNodeData.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class TaskList
    {
        [Key]
        public Guid TL_ID { get; set; }

        [StringLength(100)]
        public string TL_OrginalFileName { get; set; }

        [StringLength(50)]
        public string TL_FileName { get; set; }

        public DateTime? TL_ReceiveDate { get; set; }

        public bool TL_Processed { get; set; }

        [StringLength(150)]
        public string TL_Path { get; set; }

        [StringLength(200)]
        public string TL_Subject { get; set; }

        [StringLength(50)]
        public string TL_SenderID { get; set; }

        [StringLength(50)]
        public string TL_RecipientID { get; set; }

        [StringLength(150)]
        public string TL_ReceivedFolder { get; set; }

        [StringLength(10)]
        public string TL_ReceiverProcess { get; set; }

        public DateTime? TL_ProcessDate { get; set; }

        [ForeignKey("Profile")]
        public Guid? TL_P { get; set; }

        public PROFILE Profile { get; set; }

        [StringLength(80)]
        public string TL_CWFileName { get; set; }

        [StringLength(50)]
        public string TL_FromAddress { get; set; }

        [StringLength(50)]
        public string TL_ToAddress { get; set; }

        [StringLength(200)]
        public string TL_ArchiveName { get; set; }
    }
}
