namespace CTCNodeData.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Archives
    {
        [Key]
        public Guid AL_ID { get; set; }

        public Guid AL_TL { get; set; }

        [StringLength(50)]
        public string AL_Path { get; set; }

        [StringLength(20)]
        public string AL_ArchiveName { get; set; }

        public DateTime AL_DateArchived { get; set; }

        [StringLength(50)]
        public string AL_FileName { get; set; }
    }
}
