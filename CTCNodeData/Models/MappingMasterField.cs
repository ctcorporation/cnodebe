﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CTCNodeData.Models
{
    public class MappingMasterField
    {
        [Key]
        public Guid MM_ID { get; set; }

        [MaxLength(50)]
        [Display(Name = "Field Name")]
        public string MM_FieldName { get; set; }

        public Guid MM_MO { get; set; }

    }
}
