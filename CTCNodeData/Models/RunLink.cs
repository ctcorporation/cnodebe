namespace CTCNodeData.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("RunLink")]
    public partial class RunLink
    {
        [Key]
        public Guid RL_ID { get; set; }

        [ForeignKey("RunTransportLegPickup")]
        public Guid? RL_TP { get; set; }
        public virtual TransportLeg RunTransportLegPickup { get; set; }

        [ForeignKey("RunTransportLegDelivery")]
        public Guid? RL_TD { get; set; }
        public virtual TransportLeg RunTransportLegDelivery { get; set; }

        [ForeignKey("RunLinkRunSheet")]
        public Guid? RL_RS { get; set; }

        public virtual RunSheet RunLinkRunSheet { get; set; }

        [StringLength(10)]
        public string RL_STATUS { get; set; }

        [StringLength(255)]
        public string RL_NOTES { get; set; }



        public int? RL_SEQUENCE { get; set; }
    }
}
