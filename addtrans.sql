-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE AddTransaction
	-- Add the parameters for the stored procedure here
	@T_C		UniqueIdentifier,
	@T_P		UniqueIdentifier = NULL,
	@T_FILENAME	VarChar(100),
	@T_TRIAL	Char(1) = 'N',
	@T_DATETIME	DateTime

AS
BEGIN
	Insert into Transactions (T_C, T_P, T_DATETIME, T_FILENAME, T_TRIAL)
	values (@T_C, @T_P, @T_DATETIME, @T_FILENAME, @T_TRIAL)



END
GO
