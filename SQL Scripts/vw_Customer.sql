USE [CNODE]
GO

/****** Object:  View [dbo].[vw_Customer]    Script Date: 17/05/2019 8:40:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_Customer]
AS
SELECT        dbo.Customer.C_ID, dbo.Customer.C_NAME, dbo.Customer.C_CODE, CAST(CASE WHEN C_IS_ACTIVE = 'Y' THEN 1 ELSE 0 END AS BIT) AS ACTIVE, CAST(CASE WHEN C_ON_HOLD = 'Y' THEN 1 ELSE 0 END AS BIT) 
                         AS ONHOLD, CAST(CASE WHEN C_SATELLITE = 'Y' THEN 1 ELSE 0 END AS BIT) AS SATELLITE, dbo.Customer.C_PATH, dbo.Customer.C_SHORTNAME, dbo.HEARTBEAT.HB_ID, dbo.HEARTBEAT.HB_NAME, 
                         dbo.HEARTBEAT.HB_PATH, CAST(CASE WHEN dbo.HEARTBEAT.HB_ISMONITORED = 'Y' THEN 1 ELSE 0 END AS BIT) AS ISMONITORED, CAST(CASE WHEN C_INVOICED = 'Y' THEN 1 ELSE 0 END AS bit) AS INVOICED, 
                         dbo.HEARTBEAT.HB_PID
FROM            dbo.Customer LEFT OUTER JOIN
                         dbo.HEARTBEAT ON dbo.Customer.C_ID = dbo.HEARTBEAT.HB_C
GO


