﻿using Microsoft.Win32.SafeHandles;
using OpenPop.Pop3;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net.Mail;
using System.Runtime.InteropServices;

namespace NodeResources
{
    public class MailModule : IDisposable
    {

        bool disposed = false;
        // Instantiate a SafeHandle instance.
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                handle.Dispose();
                // Free any other managed objects here.
                //
            }

            disposed = true;
        }

        public MailModule(IMailServerSettings _mailServer)
        {
            this.MailServerSettings = _mailServer;
        }


        public MailModule(IMailServerSettings serverSettings, string alertsTo, string customReceive)
        {
            this.MailServerSettings = serverSettings;
            this.AlertsTo = AlertsTo;
            this.CustomReceive = customReceive;
        }
        public string AlertsTo { get; set; }
        public string CustomReceive { get; set; }

        public IMailServerSettings MailServerSettings { get; set; }

        public List<IMsgSummary> GetMail()
        {
            int messageCount = 0;
            string fromMail = "";
            string subject = "";
            List<IMsgSummary> allMessages = new List<IMsgSummary>();
            Pop3Client client = new Pop3Client();
            try
            {
                client.Connect(MailServerSettings.Server, MailServerSettings.Port, MailServerSettings.IsSecure);
                if (client.Connected)
                {
                    client.Authenticate(MailServerSettings.UserName, MailServerSettings.Password);
                }
                messageCount = client.GetMessageCount();
                if (messageCount > 0)
                {
                    for (int i = messageCount; i > 0; i--)
                    {
                        if (client.GetMessage(i) != null)
                        {
                            OpenPop.Mime.Message msg = client.GetMessage(i);
                            MsgSummary newSumm = new MsgSummary();
                            try
                            {
                                if (msg.Headers.From.MailAddress.Address != null && msg.Headers.From.MailAddress.Address != "")
                                {
                                    fromMail = msg.Headers.From.MailAddress.Address;
                                }
                                if (msg.Headers.Subject != null && msg.Headers.Subject != "")
                                {
                                    subject = msg.Headers.Subject;
                                }
                                List<IMsgAtt> att = new List<IMsgAtt>();
                                foreach (OpenPop.Mime.MessagePart attach in msg.FindAllAttachments())
                                {
                                    string file_name_attach = attach.FileName;
                                    FileInfo fi = new FileInfo(Path.Combine(CustomReceive, file_name_attach));
                                    att.Add(new MsgAtt() { AttFilename = fi });
                                    attach.Save(fi);
                                }
                                newSumm.MsgFile = att;
                                newSumm.MsgFrom = fromMail;
                                newSumm.MsgTo = msg.Headers.To[0].MailAddress.ToString();
                                newSumm.MsgSubject = subject;
                                newSumm.MsgStatus = "Ok";
                                client.DeleteMessage(i);
                            }
                            catch (Exception exAtt)
                            {
                                newSumm.MsgStatus = exAtt.Message;

                            }
                            finally
                            {
                                allMessages.Add(newSumm);
                            }
                        }

                    }
                }
            }

            catch (Exception ex)
            {
                MsgSummary newSumm = new MsgSummary();
                newSumm.MsgStatus = ex.Message;
                allMessages.Add(newSumm);
            }
            finally
            {
                try
                {
                    client.Disconnect();
                }
                catch (Exception)
                {

                }
            }
            return allMessages;

        }

        public string CheckMail()
        {
            string mailStatus = "";
            //MailClient client = new MailClient("Checking");
            //MailServer server = new MailServer(hostname,username,password,ServerProtocol.Pop3);
            //server.SSLConnection = useSsl;
            //server.Port = port;
            Pop3Client client = new Pop3Client();
            try
            {
                //client.Connect(server);

                client.Connect(MailServerSettings.Server, MailServerSettings.Port, MailServerSettings.IsSecure);
                if (client.Connected)
                {
                    mailStatus += "Server " + MailServerSettings.Server + ":" + MailServerSettings.Port.ToString() + ", Connected. Ok." + Environment.NewLine;
                    try
                    {

                        client.Authenticate(MailServerSettings.UserName, MailServerSettings.Password);
                        mailStatus += "Username and Password details are Correct. Mail Connection Connected. Ok." + Environment.NewLine;
                    }
                    catch (Exception)
                    {
                        mailStatus += "Username and/or Password are incorrect. Please confirm the credentials" + Environment.NewLine;
                    }


                }
                else
                {
                    mailStatus += "Warning. Server " + MailServerSettings.Server + ":" + MailServerSettings.Port.ToString() + ", Not Connected. " + Environment.NewLine + "Please check server, port, SSL settings";
                }
            }
            catch (Exception ex)
            {
                mailStatus = ex.Message;
            }
            finally
            {
                try
                {
                    client.Disconnect();
                    mailStatus += "Server disconnected." + Environment.NewLine;
                }
                catch (Exception)
                {

                }
            }
            return mailStatus;


        }

        static bool mailSent = false;

        public static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            string token = (string)e.UserState;

            if (e.Cancelled)
            {
                Console.WriteLine("[{0}] Send canceled.", token);
            }
            if (e.Error != null)
            {
                Console.WriteLine("[{0}] {1}", token, e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message sent.");
            }
            mailSent = true;
        }

        public string SendMsg(string file, string to, string mySubject, string msg)
        {
            var myMsg = new MailMessage();
            myMsg.To.Add(new System.Net.Mail.MailAddress(to));
            if (file != "")
            {
                try
                {
                    if (File.Exists(file))
                    {
                        myMsg.Attachments.Add(new System.Net.Mail.Attachment(file));

                    }
                }
                catch (Exception ex)
                {
                    SendMsg("", AlertsTo, "Error Sending Alert Email", "Error Message is: " + ex.Message + Environment.NewLine + "Original Message to send was: " + Environment.NewLine + msg);
                }
            }
            myMsg.From = new System.Net.Mail.MailAddress(MailServerSettings.Email, "CTC Adapter");
            myMsg.Subject = mySubject;
            myMsg.Body = msg;
            SmtpClient myMail = new SmtpClient
            {
                Host = MailServerSettings.Server,
                Port = Convert.ToInt16(MailServerSettings.Port),
                //myMail.EnableSsl = ssl;
                Credentials = new System.Net.NetworkCredential(MailServerSettings.UserName, MailServerSettings.Password),
                DeliveryMethod = SmtpDeliveryMethod.Network
            };
            myMail.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
            //String userState = "Send from CTCNODE";
            try
            {
                myMail.Send(myMsg);
                myMsg.Dispose();
                myMail.Dispose();

                return "Message Sent OK";
            }
            catch (Exception ex)
            {
                myMsg.Dispose();
                myMail.Dispose();
                return "Message failed to send. Error was: " + ex.Message;
            }
        }


    }
}
