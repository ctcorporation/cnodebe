﻿namespace NodeResources
{
    public interface ITransactionResult
    {
        string Reference1
        {
            get;
            set;
        }
        string Reference2
        {
            get;
            set;
        }
        string Reference3
        {
            get;
            set;
        }
        RefType Ref1Type
        {
            get;
            set;
        }
        RefType Ref2Type
        {
            get;
            set;
        }
        RefType Ref3Type
        {
            get;
            set;
        }
    }
}
