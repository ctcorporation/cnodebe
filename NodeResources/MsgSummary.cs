﻿using System.Collections.Generic;
using System.IO;

namespace NodeResources
{
    public class MsgAtt : IMsgAtt
    {
        public FileInfo AttFilename
        {
            get;
            set;
        }
    }
    public class MsgSummary : IMsgSummary
    {
        public string MsgFrom
        {
            get;
            set;
        }
        public string MsgSubject
        {
            get;
            set;
        }
        public List<IMsgAtt> MsgFile
        {
            get;
            set;
        }
        public string MsgStatus
        {
            get;
            set;
        }

        public string MsgTo
        {
            get;
            set;
        }

    }
}
