﻿using System.Collections.Generic;

namespace NodeResources
{
    public interface IMsgSummary
    {
        List<IMsgAtt> MsgFile { get; set; }
        string MsgFrom { get; set; }
        string MsgStatus { get; set; }
        string MsgSubject { get; set; }
        string MsgTo { get; set; }
    }
}