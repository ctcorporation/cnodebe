﻿using System.IO;

namespace NodeResources
{
    public interface IMsgAtt
    {
        FileInfo AttFilename { get; set; }
    }
}