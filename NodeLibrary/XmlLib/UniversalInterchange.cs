﻿using System.Xml.Serialization;
namespace NodeLibrary.XmlLib.Native
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Universal/2011/11")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.cargowise.com/Schemas/Universal/2011/11", IsNullable = false)]
    public partial class UniversalInterchange
    {

        private UniversalInterchangeHeader headerField;

        private UniversalInterchangeBody bodyField;

        private string versionField;

        /// <remarks/>
        public UniversalInterchangeHeader Header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        public UniversalInterchangeBody Body
        {
            get
            {
                return this.bodyField;
            }
            set
            {
                this.bodyField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute(DataType = "token")]
        public string version
        {
            get
            {
                return this.versionField;
            }
            set
            {
                this.versionField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Universal/2011/11")]
    public partial class UniversalInterchangeHeader
    {

        private string senderIDField;

        private string recipientIDField;

        private UniversalInterchangeHeaderAcknowledgement acknowledgementField;

        /// <remarks/>
        public string SenderID
        {
            get
            {
                return this.senderIDField;
            }
            set
            {
                this.senderIDField = value;
            }
        }

        /// <remarks/>
        public string RecipientID
        {
            get
            {
                return this.recipientIDField;
            }
            set
            {
                this.recipientIDField = value;
            }
        }

        /// <remarks/>
        public UniversalInterchangeHeaderAcknowledgement Acknowledgement
        {
            get
            {
                return this.acknowledgementField;
            }
            set
            {
                this.acknowledgementField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Universal/2011/11")]
    public partial class UniversalInterchangeHeaderAcknowledgement
    {

        private UniversalInterchangeHeaderAcknowledgementRequired requiredField;

        private UniversalInterchangeHeaderAcknowledgementChannel channelField;

        private string recipientIDField;

        private UniversalInterchangeHeaderAcknowledgementContext[] contextCollectionField;

        /// <remarks/>
        public UniversalInterchangeHeaderAcknowledgementRequired Required
        {
            get
            {
                return this.requiredField;
            }
            set
            {
                this.requiredField = value;
            }
        }

        /// <remarks/>
        public UniversalInterchangeHeaderAcknowledgementChannel Channel
        {
            get
            {
                return this.channelField;
            }
            set
            {
                this.channelField = value;
            }
        }

        /// <remarks/>
        public string RecipientID
        {
            get
            {
                return this.recipientIDField;
            }
            set
            {
                this.recipientIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Context", IsNullable = false)]
        public UniversalInterchangeHeaderAcknowledgementContext[] ContextCollection
        {
            get
            {
                return this.contextCollectionField;
            }
            set
            {
                this.contextCollectionField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Universal/2011/11")]
    public enum UniversalInterchangeHeaderAcknowledgementRequired
    {

        /// <remarks/>
        OnAll,

        /// <remarks/>
        OnError,

        /// <remarks/>
        OnSuccess,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Universal/2011/11")]
    public enum UniversalInterchangeHeaderAcknowledgementChannel
    {

        /// <remarks/>
        eHub,

        /// <remarks/>
        eAdaptor,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Universal/2011/11")]
    public partial class UniversalInterchangeHeaderAcknowledgementContext
    {

        private string typeField;

        private string valueField;

        /// <remarks/>
        public string Type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        /// <remarks/>
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                this.valueField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.cargowise.com/Schemas/Universal/2011/11")]
    public partial class UniversalInterchangeBody
    {

        private System.Xml.XmlElement[] anyField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAnyElementAttribute()]
        public System.Xml.XmlElement[] Any
        {
            get
            {
                return this.anyField;
            }
            set
            {
                this.anyField = value;
            }
        }
    }
}
