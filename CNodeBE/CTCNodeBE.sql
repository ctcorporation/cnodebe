USE [CNODE]
GO
/****** Object:  StoredProcedure [dbo].[Add_Customer]    Script Date: 04/03/2017 10:47:29 ******/
DROP PROCEDURE [dbo].[Add_Customer]
GO
/****** Object:  StoredProcedure [dbo].[Add_Profile]    Script Date: 04/03/2017 10:47:29 ******/
DROP PROCEDURE [dbo].[Add_Profile]
GO
/****** Object:  StoredProcedure [dbo].[Add_Todo]    Script Date: 04/03/2017 10:47:29 ******/
DROP PROCEDURE [dbo].[Add_Todo]
GO
/****** Object:  StoredProcedure [dbo].[AddTransaction]    Script Date: 04/03/2017 10:47:29 ******/
DROP PROCEDURE [dbo].[AddTransaction]
GO
/****** Object:  StoredProcedure [dbo].[Edit_Customer]    Script Date: 04/03/2017 10:47:29 ******/
DROP PROCEDURE [dbo].[Edit_Customer]
GO
/****** Object:  StoredProcedure [dbo].[Edit_Profile]    Script Date: 04/03/2017 10:47:29 ******/
DROP PROCEDURE [dbo].[Edit_Profile]
GO
/****** Object:  View [dbo].[vw_CustomerProfile]    Script Date: 04/03/2017 10:47:29 ******/
DROP VIEW [dbo].[vw_CustomerProfile]
GO
/****** Object:  View [dbo].[vw_Transaction]    Script Date: 04/03/2017 10:47:29 ******/
DROP VIEW [dbo].[vw_Transaction]
GO
/****** Object:  Table [dbo].[PROFILE]    Script Date: 04/03/2017 10:47:28 ******/
ALTER TABLE [dbo].[PROFILE] DROP CONSTRAINT [DF_PROFILE_P_ID]
GO
ALTER TABLE [dbo].[PROFILE] DROP CONSTRAINT [DF_PROFILE_P_RECEIVEONLY]
GO
DROP TABLE [dbo].[PROFILE]
GO
/****** Object:  Table [dbo].[TODO]    Script Date: 04/03/2017 10:47:29 ******/
ALTER TABLE [dbo].[TODO] DROP CONSTRAINT [DF_TODO_L_ID]
GO
DROP TABLE [dbo].[TODO]
GO
/****** Object:  Table [dbo].[Transactions]    Script Date: 04/03/2017 10:47:29 ******/
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [DF_Transaction_T_ID]
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [DF_Transaction_T_TRIAL]
GO
ALTER TABLE [dbo].[Transactions] DROP CONSTRAINT [DF_Transaction_T_INVOICED]
GO
DROP TABLE [dbo].[Transactions]
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 04/03/2017 10:47:28 ******/
ALTER TABLE [dbo].[Contact] DROP CONSTRAINT [DF_Contact_CT_Contactid]
GO
ALTER TABLE [dbo].[Contact] DROP CONSTRAINT [DF_Contact_CT_MainContact]
GO
DROP TABLE [dbo].[Contact]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 04/03/2017 10:47:28 ******/
ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [DF_Customer_C_ID]
GO
ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [DF_Customer_C_IS_ACTIVE]
GO
ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [DF_Customer_C_ON_HOLD]
GO
ALTER TABLE [dbo].[Customer] DROP CONSTRAINT [DF_Customer_C_FTP_CLIENT]
GO
DROP TABLE [dbo].[Customer]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 04/03/2017 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer](
	[C_ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Customer_C_ID]  DEFAULT (newid()),
	[C_NAME] [varchar](50) NULL,
	[C_CODE] [varchar](9) NULL,
	[C_IS_ACTIVE] [char](1) NOT NULL CONSTRAINT [DF_Customer_C_IS_ACTIVE]  DEFAULT ('N'),
	[C_ON_HOLD] [char](1) NOT NULL CONSTRAINT [DF_Customer_C_ON_HOLD]  DEFAULT ('N'),
	[C_PATH] [varchar](50) NULL,
	[C_FTP_CLIENT] [char](1) NOT NULL CONSTRAINT [DF_Customer_C_FTP_CLIENT]  DEFAULT ('N'),
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[C_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 04/03/2017 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Contact](
	[CT_Contactid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Contact_CT_Contactid]  DEFAULT (newid()),
	[CT_FirstName] [varchar](50) NULL,
	[CT_LastName] [varchar](50) NULL,
	[CT_DirectPH] [char](20) NULL,
	[CT_Mobile] [char](20) NULL,
	[CT_AD] [uniqueidentifier] NULL,
	[CT_Skype] [varchar](50) NULL,
	[CT_MainContact] [char](1) NOT NULL CONSTRAINT [DF_Contact_CT_MainContact]  DEFAULT ('N'),
	[CT_CO] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Transactions]    Script Date: 04/03/2017 10:47:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Transactions](
	[T_ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_Transaction_T_ID]  DEFAULT (newid()),
	[T_C] [uniqueidentifier] NULL,
	[T_P] [uniqueidentifier] NULL,
	[T_DATETIME] [datetime] NULL,
	[T_FILENAME] [varchar](100) NULL,
	[T_TRIAL] [char](1) NOT NULL CONSTRAINT [DF_Transaction_T_TRIAL]  DEFAULT ('N'),
	[T_INVOICED] [char](1) NOT NULL CONSTRAINT [DF_Transaction_T_INVOICED]  DEFAULT ('N'),
	[T_INVOICEDATE] [datetime] NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[T_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TODO]    Script Date: 04/03/2017 10:47:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TODO](
	[L_ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_TODO_L_ID]  DEFAULT (newid()),
	[L_P] [uniqueidentifier] NULL,
	[L_FILENAME] [varchar](max) NULL,
	[L_LASTRESULT] [varchar](max) NULL,
	[L_DATE] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PROFILE]    Script Date: 04/03/2017 10:47:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PROFILE](
	[P_ID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_PROFILE_P_ID]  DEFAULT (newid()),
	[P_C] [uniqueidentifier] NULL,
	[P_REASONCODE] [char](3) NULL,
	[P_SERVER] [varchar](max) NULL,
	[P_USERNAME] [varchar](max) NULL,
	[P_PASSWORD] [varchar](max) NULL,
	[P_DELIVERY] [char](1) NULL CONSTRAINT [DF_PROFILE_P_RECEIVEONLY]  DEFAULT ('N'),
	[P_PORT] [char](10) NULL,
	[P_DESCRIPTION] [varchar](max) NULL,
	[P_RECIPIENTID] [varchar](10) NULL,
 CONSTRAINT [PK_PROFILE] PRIMARY KEY CLUSTERED 
(
	[P_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[vw_Transaction]    Script Date: 04/03/2017 10:47:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Transaction]
AS
SELECT     dbo.Customer.C_ID, dbo.Customer.C_NAME, dbo.Customer.C_CODE, dbo.Customer.C_IS_ACTIVE, dbo.Customer.C_ON_HOLD, dbo.Customer.C_PATH, dbo.Customer.C_FTP_CLIENT, 
                      dbo.Transactions.T_DATETIME, dbo.Transactions.T_FILENAME, dbo.Transactions.T_TRIAL, dbo.Transactions.T_INVOICED, dbo.Transactions.T_INVOICEDATE, dbo.PROFILE.P_DESCRIPTION, 
                      dbo.PROFILE.P_REASONCODE, dbo.PROFILE.P_RECIPIENTID
FROM         dbo.Customer INNER JOIN
                      dbo.Transactions ON dbo.Customer.C_ID = dbo.Transactions.T_C LEFT OUTER JOIN
                      dbo.PROFILE ON dbo.Transactions.T_P = dbo.PROFILE.P_ID
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Customer"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 221
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PROFILE"
            Begin Extent = 
               Top = 8
               Left = 672
               Bottom = 244
               Right = 849
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Transactions"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 126
               Right = 415
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Transaction'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Transaction'
GO
/****** Object:  View [dbo].[vw_CustomerProfile]    Script Date: 04/03/2017 10:47:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_CustomerProfile]
AS
SELECT     dbo.Customer.C_NAME, dbo.Customer.C_CODE, dbo.Customer.C_IS_ACTIVE, dbo.Customer.C_ON_HOLD, dbo.Customer.C_PATH, dbo.Customer.C_FTP_CLIENT, dbo.PROFILE.P_ID, 
                      dbo.PROFILE.P_C, dbo.PROFILE.P_REASONCODE, dbo.PROFILE.P_SERVER, dbo.PROFILE.P_USERNAME, dbo.PROFILE.P_PASSWORD, dbo.Customer.C_ID, dbo.PROFILE.P_RECIPIENTID, 
                      dbo.PROFILE.P_DESCRIPTION, dbo.PROFILE.P_PORT, dbo.PROFILE.P_DELIVERY
FROM         dbo.Customer LEFT OUTER JOIN
                      dbo.PROFILE ON dbo.Customer.C_ID = dbo.PROFILE.P_C
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Customer"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 213
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PROFILE"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 207
               Right = 423
            End
            DisplayFlags = 280
            TopColumn = 1
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_CustomerProfile'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_CustomerProfile'
GO
/****** Object:  StoredProcedure [dbo].[Edit_Profile]    Script Date: 04/03/2017 10:47:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andy Duggan
-- Create date: 08/03/2016
-- Description:	Add or modify Customer Profile entries
-- =============================================
CREATE PROCEDURE [dbo].[Edit_Profile]
	@P_C			UniqueIdentifier,
	@P_ID			UniqueIdentifier,
	@P_REASONCODE	Char(3)=Null,
	@P_SERVER		Varchar(50)=Null,
	@P_USERNAME		Varchar(50)=Null,
	@P_PASSWORD		Varchar(50)=Null,
	@P_DELIVERY		Char(1)="R",  --R = Receive  C = CTC Adapter  F= FTP
	@P_PORT			Char(10)=Null,
	@P_DESCRIPTION	VarChar(100)=Null,
	@P_RECIPIENTID	VarChar(10),
	@R_ID			UniqueIdentifier OUTPUT,
	@R_ACTION		Char(1) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    Declare @ID UniqueIdentifier
    set @ID = NEWID()
    
    
		Update PROFILE SET P_REASONCODE = @P_REASONCODE, P_SERVER=@P_SERVER, P_USERNAME=@P_USERNAME, P_PASSWORD=@P_PASSWORD, P_DELIVERY=@P_DELIVERY, P_PORT=@P_PORT, 
		P_DESCRIPTION=@P_DESCRIPTION, P_RECIPIENTID=@P_RECIPIENTID
		where P_ID = @P_ID
    	SET @R_ID = @P_ID
    	SET @R_ACTION='U'
	
END
GO
/****** Object:  StoredProcedure [dbo].[Edit_Customer]    Script Date: 04/03/2017 10:47:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andy Duggan
-- Create date: 07/03/2017
-- Description:	Create New Customer - Will check if Exists
-- =============================================
CREATE PROCEDURE [dbo].[Edit_Customer]
	@C_ID		UniqueIdentifier,
	@C_CODE		varchar(9),
	@C_IS_ACTIVE char(1),
	@C_NAME		varchar(50),
	@C_ON_HOLD	char(1),
	@C_PATH		varchar(50),
	@C_FTP_CLIENT char(1),
	@R_ID		Uniqueidentifier OUTPUT,
	@R_ACTION	Char(1) OUTPUT
	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	
			Update Customer Set C_NAME = @C_NAME, C_CODE = @C_CODE, C_IS_ACTIVE =@C_IS_ACTIVE, C_ON_HOLD=@C_ON_HOLD, C_PATH=@C_PATH, C_FTP_CLIENT=@C_FTP_CLIENT where C_ID = @C_ID	
			Set @R_ACTION='U'
			Set @R_ID = @C_ID
	
END
GO
/****** Object:  StoredProcedure [dbo].[AddTransaction]    Script Date: 04/03/2017 10:47:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[AddTransaction]
	-- Add the parameters for the stored procedure here
	@T_C		UniqueIdentifier,
	@T_P		UniqueIdentifier = NULL,
	@T_FILENAME	VarChar(100),
	@T_TRIAL	Char(1) = 'N',
	@T_DATETIME	DateTime

AS
BEGIN
If NOT EXISTS (SELECT T_ID FROM Transactions WHERE T_FILENAME=@T_FILENAME)
BEGIN
	Insert into Transactions (T_C, T_P, T_DATETIME, T_FILENAME, T_TRIAL)
	values (@T_C, @T_P, @T_DATETIME, @T_FILENAME, @T_TRIAL)
END



END
GO
/****** Object:  StoredProcedure [dbo].[Add_Todo]    Script Date: 04/03/2017 10:47:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Add_Todo]
	-- Add the parameters for the stored procedure here
	@P_ID			UniqueIdentifier,
	@FILENAME		VarChar(MAX),
	@LASTRESULT		VarChar(MAX)=NULL,
	@LASTDATE		Datetime=Null,
	@R_ID			UniqueIdentifier OUTPUT,
	@R_STATUS		Char(1) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @ID		UniqueIdentifier
	
    -- Insert statements for procedure here
    SET @ID = (SELECT CASE 
    		WHEN EXISTS (Select L_ID from TODO where L_P = @P_ID and L_FILENAME = @FILENAME)
			THEN (SELECT L_ID from TODO where L_P = @P_ID and L_FILENAME = @FILENAME)
			ELSE (NULL)
			End AS ID
			FROM TODO)
			
	If @ID IS NOT NULL
	BEGIN
			UPDATE TODO SET L_DATE=@LASTDATE, L_LASTRESULT=@LASTRESULT where L_ID=@ID
			SET @R_STATUS='A'
	END
	ELSE
	BEGIN
			SET @ID = NEWID()
			INSERT INTO TODO (L_ID, L_P, L_FILENAME, L_DATE,L_LASTRESULT) values 
			(@ID, @P_ID, @FILENAME, @LASTDATE, 'Added to TODO List')
			SET @R_STATUS='U'
	END
	SET @R_ID=@ID
				
END
GO
/****** Object:  StoredProcedure [dbo].[Add_Profile]    Script Date: 04/03/2017 10:47:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andy Duggan
-- Create date: 08/03/2016
-- Description:	Add or modify Customer Profile entries
-- =============================================
CREATE PROCEDURE [dbo].[Add_Profile]
	@P_C			UniqueIdentifier,
	@P_REASONCODE	Char(3)=Null,
	@P_SERVER		Varchar(50)=Null,
	@P_USERNAME		Varchar(50)=Null,
	@P_PASSWORD		Varchar(50)=Null,
	@P_DELIVERY		Char(1)="R",  --R = Receive  C = CTC Adapter  F= FTP
	@P_PORT			Char(10)=Null,
	@P_DESCRIPTION	VarChar(100)=Null,
	@P_RECIPIENTID	VarChar(10), 
	@R_ID			UniqueIdentifier OUTPUT,
	@R_ACTION		Char(1) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    Declare @ID UniqueIdentifier
    set @ID = NEWID()
    
    If exists(Select P_ID from PROFILE WHERE P_RECIPIENTID= @P_RECIPIENTID and P_REASONCODE = @P_REASONCODE)
    Begin
		Select @ID = P_ID from PROFILE WHERE P_RECIPIENTID= @P_RECIPIENTID and P_REASONCODE = @P_REASONCODE
		Update PROFILE SET P_REASONCODE = @P_REASONCODE, P_SERVER=@P_SERVER, P_USERNAME=@P_USERNAME, P_PASSWORD=@P_PASSWORD, P_DELIVERY=@P_DELIVERY, P_PORT=@P_PORT, 
		P_DESCRIPTION=@P_DESCRIPTION, P_RECIPIENTID=@P_RECIPIENTID where P_RECIPIENTID= @P_RECIPIENTID and P_REASONCODE = @P_REASONCODE
		SET @R_ACTION='U'
    End
    Else
    Begin
		Insert into PROFILE (P_ID, P_C, P_REASONCODE, P_SERVER, P_USERNAME, P_PASSWORD, P_DELIVERY, P_PORT, P_DESCRIPTION, P_RECIPIENTID) 
		Values (@ID, @P_C, @P_REASONCODE, @P_SERVER, @P_USERNAME, @P_PASSWORD, @P_DELIVERY, @P_PORT, @P_DESCRIPTION, @P_RECIPIENTID)
		Set @R_ACTION='A'
    End
	set @R_ID=@ID	
	
END
GO
/****** Object:  StoredProcedure [dbo].[Add_Customer]    Script Date: 04/03/2017 10:47:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Andy Duggan
-- Create date: 07/03/2017
-- Description:	Create New Customer - Will check if Exists
-- =============================================
CREATE PROCEDURE [dbo].[Add_Customer]
	
	@C_CODE		varchar(9),
	@C_IS_ACTIVE char(1),
	@C_NAME		varchar(50),
	@C_ON_HOLD	char(1),
	@C_PATH		varchar(50),
	@C_FTP_CLIENT char(1), 
	@R_ID		Uniqueidentifier OUTPUT,
	@R_ACTION	Char(1) OUTPUT
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @ID		UniqueIdentifier
	Set @ID = NEWID()	
	If exists(Select C_ID from Customer where C_CODE= @C_CODE)
	Begin
			Select @ID = C_ID from Customer where C_CODE = @C_CODE
			Update Customer Set C_NAME = @C_NAME, C_IS_ACTIVE =@C_IS_ACTIVE, C_ON_HOLD=@C_ON_HOLD, C_PATH=@C_PATH, C_FTP_CLIENT=@C_FTP_CLIENT where C_ID = @ID
			Set @R_ACTION='U'
	End
	else
	Begin
		Insert into Customer (C_ID, C_NAME, C_CODE,C_IS_ACTIVE, C_ON_HOLD, C_PATH, C_FTP_CLIENT) Values (@ID, @C_NAME, @C_CODE, @C_IS_ACTIVE, @C_ON_HOLD, @C_PATH, @C_FTP_CLIENT)
		Set @R_ACTION ='A'
	End
    -- Insert statements for procedure here
	Set @R_ID = @ID
	
END
GO
