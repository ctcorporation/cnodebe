﻿namespace CTC_Adapter
{
    partial class CargowiseXML
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbClose = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.XMLFoldertoScan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OutputTemplate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.edLoc = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.edLog = new System.Windows.Forms.TextBox();
            this.xpath = new System.Windows.Forms.TextBox();
            this.xval = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.ds = new System.Data.DataSet();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            this.SuspendLayout();
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Location = new System.Drawing.Point(573, 418);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(75, 23);
            this.bbClose.TabIndex = 0;
            this.bbClose.Text = "&Close";
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.XMLFoldertoScan,
            this.OutputTemplate});
            this.dataGridView1.Location = new System.Drawing.Point(28, 13);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(612, 122);
            this.dataGridView1.TabIndex = 1;
            // 
            // XMLFoldertoScan
            // 
            this.XMLFoldertoScan.HeaderText = "XML Folder Location";
            this.XMLFoldertoScan.Name = "XMLFoldertoScan";
            // 
            // OutputTemplate
            // 
            this.OutputTemplate.HeaderText = "Output Template";
            this.OutputTemplate.Name = "OutputTemplate";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 152);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Folder to Scan";
            // 
            // edLoc
            // 
            this.edLoc.Location = new System.Drawing.Point(28, 169);
            this.edLoc.Name = "edLoc";
            this.edLoc.Size = new System.Drawing.Size(351, 20);
            this.edLoc.TabIndex = 3;
            this.edLoc.Text = "D:\\One Drive\\OneDrive\\Documents\\CTC\\EMOTrans\\XML\\Old-Shipment.xml";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(573, 389);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // edLog
            // 
            this.edLog.Location = new System.Drawing.Point(12, 295);
            this.edLog.Multiline = true;
            this.edLog.Name = "edLog";
            this.edLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.edLog.Size = new System.Drawing.Size(521, 146);
            this.edLog.TabIndex = 5;
            // 
            // xpath
            // 
            this.xpath.Location = new System.Drawing.Point(29, 206);
            this.xpath.Name = "xpath";
            this.xpath.Size = new System.Drawing.Size(309, 20);
            this.xpath.TabIndex = 6;
            // 
            // xval
            // 
            this.xval.Location = new System.Drawing.Point(28, 232);
            this.xval.Multiline = true;
            this.xval.Name = "xval";
            this.xval.Size = new System.Drawing.Size(505, 57);
            this.xval.TabIndex = 7;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(503, 204);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 8;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ds
            // 
            this.ds.DataSetName = "NewDataSet";
            // 
            // CargowiseXML
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(660, 453);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.xval);
            this.Controls.Add(this.xpath);
            this.Controls.Add(this.edLog);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.edLoc);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.bbClose);
            this.Name = "CargowiseXML";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cargowise XML Operations";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn XMLFoldertoScan;
        private System.Windows.Forms.DataGridViewTextBoxColumn OutputTemplate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edLoc;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox edLog;
        private System.Windows.Forms.TextBox xpath;
        private System.Windows.Forms.TextBox xval;
        private System.Windows.Forms.Button button2;
        private System.Data.DataSet ds;
    }
}