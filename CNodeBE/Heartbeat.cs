﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace CNodeBE
{
    class Heartbeat
    {

        public static void RegisterHeartBeat(string custCode, string operation, ParameterInfo[] parameters)
        {
            SqlConnection sqlConn = new SqlConnection { ConnectionString = Globals.connString() };
            SqlCommand addHeartbeat = new SqlCommand
            {
                CommandType = System.Data.CommandType.StoredProcedure,
                CommandText = "AddHeartBeat",
                Connection = sqlConn
            };

            Process currentProcess = Process.GetCurrentProcess();
            int pid = currentProcess.Id;

            addHeartbeat.Parameters.AddWithValue("@APPNAME", Assembly.GetExecutingAssembly().GetName().Name);
            addHeartbeat.Parameters.AddWithValue("@PATH", Path.GetDirectoryName(Application.ExecutablePath));
            addHeartbeat.Parameters.AddWithValue("@PID", pid);
            // addHeartbeat.Parameters.AddWithValue("@CUSTID", NodeResources.GetCustomer(custCode));
            addHeartbeat.Parameters.AddWithValue("@CHECKIN", DateTime.Now);
            if (operation == "Starting")
            {
                addHeartbeat.Parameters.AddWithValue("@STARTING", 1);
                addHeartbeat.Parameters.AddWithValue("@OPENED", DateTime.Now);

            }
            if (operation == "Stopping")
            {
                addHeartbeat.Parameters.AddWithValue("@STARTING", 1);
                addHeartbeat.Parameters.AddWithValue("@CHECKIN", null);
                addHeartbeat.Parameters.AddWithValue("@OPENED", null);
            }
            
            addHeartbeat.Parameters.AddWithValue("@LASTOP", operation);
            string pList = string.Empty;
            if (parameters != null)
            {
                for (int i = 0; i < parameters.Length; i++)
                {
                    pList += parameters[i].Name + ": " + parameters[i].ToString();
                }
            }

            addHeartbeat.Parameters.AddWithValue("@PARAMS", pList);
            if (sqlConn.State == System.Data.ConnectionState.Open)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            addHeartbeat.ExecuteNonQuery();

        }


    }
}
