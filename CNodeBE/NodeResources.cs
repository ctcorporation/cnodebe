﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace CNodeBE
{
    class NodeResources
    {


        public static DataTable ConvertCSVtoDataTable(string strFilePath)
        {
            StreamReader sr = new StreamReader(strFilePath);
            string[] headers = sr.ReadLine().Split(',');
            DataTable dt = new DataTable();
            foreach (string header in headers)
            {
                dt.Columns.Add(header);
            }
            while (!sr.EndOfStream)
            {
                string[] rows = Regex.Split(sr.ReadLine(), ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
                if (headers.Length == rows.Length)
                {
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < headers.Length; i++)
                    {
                        dr[i] = rows[i];
                    }
                    dt.Rows.Add(dr);
                }
            }
            sr.Close();

            return dt;
        }

        public static void AddLabel(Label lb, string value)
        {
            if (lb.InvokeRequired)
            {
                lb.BeginInvoke(new System.Action(delegate { AddLabel(lb, value); }));
                return;
            }
            lb.Text = value;
        }

        public static void AddText(RichTextBox tb, string value)
        {
            if (tb.InvokeRequired)
            {
                tb.BeginInvoke(new System.Action(delegate { AddText(tb, value); }));
                return;
            }
            tb.AppendText(value + Environment.NewLine);
            tb.ScrollToCaret();
        }

        public static void AddText(RichTextBox rtb, string value, Color color)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddRTBText(rtb, value, color); }));
                return;
            }
           // string[] str = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            rtb.DeselectAll();
            rtb.SelectionColor = color;
            rtb.AppendText(value + Environment.NewLine);
            rtb.SelectionFont = new Font(rtb.SelectionFont, FontStyle.Regular);
            rtb.SelectionColor = Color.Black;
            rtb.ScrollToCaret();

        }
        public static void AddText(RichTextBox rtb, string value, Color color, FontStyle fontStyle)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddRTBText(rtb, value, color); }));
                return;
            }
            string[] str = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);

            //rtb.AppendText(value);

            Font currentFont = rtb.SelectionFont;
            rtb.DeselectAll();
            rtb.SelectionColor = color;
            rtb.AppendText(value + Environment.NewLine);
            rtb.SelectionFont = new Font(rtb.SelectionFont, fontStyle);
            rtb.DeselectAll();

            rtb.SelectionColor = Color.Black;
            rtb.SelectionFont = currentFont;
            rtb.ScrollToCaret();

        }

        public static XDocument DocumentToXDocumentReader(XmlDocument doc)
        {
            return XDocument.Load(new XmlNodeReader(doc));
        }

        public static Stream CreateStreamFromString(string str)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream, Encoding.UTF8);
            writer.Write(str);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        public bool LockedFile(FileInfo file)
        {
            try
            {
                string filePath = file.FullName;
                FileStream fs = File.OpenWrite(filePath);
                fs.Close();
                return false;
            }
            catch (Exception) { return true; }
        }

        public static void AddRTBText(RichTextBox rtb, string value)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddRTBText(rtb, value); }));
                return;
            }

            rtb.AppendText((value) + Environment.NewLine);
            rtb.ScrollToCaret();

        }
        public static void AddRTBText(RichTextBox rtb, string value, Color color)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddRTBText(rtb, value, color); }));
                return;
            }
           // string[] str = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            
            rtb.DeselectAll();
            rtb.SelectionColor = color;
            rtb.AppendText(value + Environment.NewLine);
            rtb.SelectionFont = new Font(rtb.SelectionFont, FontStyle.Regular);
            rtb.SelectionColor = Color.Black;
            rtb.ScrollToCaret();

        }

        public static string MoveFile(string fileToMove, string folder)
        {
            int icount = 1;

            string filename = Path.GetFileNameWithoutExtension(fileToMove);
            string ext = Path.GetExtension(fileToMove);
            string newfile = Path.Combine(folder, Path.GetFileName(fileToMove));
            while (File.Exists(newfile))
            {
                newfile = Path.Combine(folder, filename + icount + ext);
                icount++;

            }

            try
            {
                File.Move(fileToMove, Path.Combine(folder, newfile));
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                newfile = "Warning: " + strEx + " exception found while moving " + filename + " to " + folder + "\r\n Error: " + ex.Message;
            }
            return newfile;
        }

        //public static string FtpSend(string file, vw_CustomerProfile cust)
        //{
        //    string Result = string.Empty;
        //    Result += "FTP Server: " + cust.P_SERVER + Environment.NewLine;
        //    Result += "Username: " + cust.P_USERNAME + Environment.NewLine;
        //    Result += "Port: " + cust.P_PORT + Environment.NewLine;
        //    try
        //    {

        //        SessionOptions sessionOptions = new SessionOptions
        //        {
        //            HostName = cust.P_SERVER.Trim(),
        //            PortNumber = int.Parse(cust.P_PORT.Trim()),
        //            UserName = cust.P_USERNAME.Trim(),
        //            Password = cust.P_PASSWORD.Trim()
        //        };
        //        if (cust.SSL)
        //        {
        //            sessionOptions.Protocol = Protocol.Sftp;
        //            sessionOptions.SshPrivateKeyPath = cust.P_SUBJECT ?? cust.P_SUBJECT.Trim();
        //            sessionOptions.SshHostKeyFingerprint = cust.P_EMAILADDRESS ?? cust.P_EMAILADDRESS.Trim();

        //        }
        //        else
        //        {
        //            sessionOptions.Protocol = Protocol.Ftp;
        //        }
        //        //try
        //        //{
        //        TransferOperationResult tr;
        //        using (Session session = new Session())
        //        {
        //            session.Open(sessionOptions);
        //            if (session.Opened)
        //            {
        //                //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Connected to " + custProfile[0].P_server + ". Sending file  :" + xmlfile + Environment.NewLine + " TO " + sessionOptions.HostName + "/" + custProfile[0].P_path + "/" + Path.GetFileName(xmlfile));
        //                TransferOptions tOptions = new TransferOptions();
        //                tOptions.OverwriteMode = OverwriteMode.Overwrite;
        //                tOptions.ResumeSupport.State = TransferResumeSupportState.Off;
        //                cust.P_PATH = string.IsNullOrEmpty(cust.P_PATH) ? "" : cust.P_PATH + "/";
        //                tr = session.PutFiles(file, "/" + cust.P_PATH + Path.GetFileName(file), true, tOptions);
        //                //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Results: ");

        //                if (tr.IsSuccess)
        //                {
        //                    Result = "File Sent Ok";
        //                    try
        //                    {
        //                        File.Delete(file);
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        Result = "File Sent Ok. Delete Error: " + ex.Message;
        //                    }
        //                }
        //                else
        //                {
        //                    Result += "Failed: Transfer Result is invalid: ";
        //                }
        //            }
        //            else
        //            {
        //                Result = "Failed to Open FTP Session" + Environment.NewLine;
        //                Result += "FTP Server: " + sessionOptions.HostName + Environment.NewLine;
        //                Result += "Username: " + sessionOptions.UserName + Environment.NewLine;
        //                Result += "Port: " + sessionOptions.PortNumber + Environment.NewLine;

        //            }

        //        }

        //    }
        //    catch (TimeoutException ex)
        //    {
        //        Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
        //    }
        //    catch (SessionRemoteException ex)
        //    {
        //        Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
        //    }
        //    catch (SessionLocalException ex)
        //    {
        //        Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
        //    }
        //    catch (Exception ex)
        //    {
        //        Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
        //    }

        //    return Result;
        //}

        //internal static string FtpSend(string file, string server, string folder, string user, string password)
        //{
        //    string Result = string.Empty;
        //    try
        //    {
        //        SessionOptions sessionOptions = new SessionOptions
        //        {
        //            HostName = server,
        //            PortNumber = 21,
        //            UserName = user,
        //            Password = password
        //        };

        //        sessionOptions.Protocol = Protocol.Ftp;

        //        //try
        //        //{
        //        TransferOperationResult tr;
        //        using (Session session = new Session())
        //        {
        //            session.Open(sessionOptions);
        //            if (session.Opened)
        //            {
        //                //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Connected to " + custProfile[0].P_server + ". Sending file  :" + xmlfile + Environment.NewLine + " TO " + sessionOptions.HostName + "/" + custProfile[0].P_path + "/" + Path.GetFileName(xmlfile));
        //                TransferOptions tOptions = new TransferOptions();
        //                tOptions.OverwriteMode = OverwriteMode.Overwrite;
        //                tOptions.ResumeSupport.State = TransferResumeSupportState.Off;

        //                if (string.IsNullOrEmpty(folder))
        //                {
        //                    folder = "";
        //                }
        //                else
        //                {
        //                    folder = folder + "/";
        //                }
        //                tr = session.PutFiles(file, "/" + folder + Path.GetFileName(file), true, tOptions);
        //                //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Results: ");                        
        //                Result += "FTP Server: " + sessionOptions.HostName + Environment.NewLine;
        //                Result += "Username: " + sessionOptions.UserName + Environment.NewLine;
        //                Result += "Port: " + sessionOptions.PortNumber + Environment.NewLine;
        //                if (tr.IsSuccess)
        //                {
        //                    Result = "File Sent Ok";
        //                    try
        //                    {
        //                        File.Delete(file);
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        Result = "File Sent Ok. Delete Error: " + ex.Message;
        //                    }

        //                }
        //                else
        //                {
        //                    Result = "Failed: Transfer Result is invalid: ";
        //                }

        //            }
        //            else
        //            {
        //                Result = "Failed to Open FTP Session" + Environment.NewLine;
        //                Result += "FTP Server: " + sessionOptions.HostName + Environment.NewLine;
        //                Result += "Username: " + sessionOptions.UserName + Environment.NewLine;
        //                Result += "Port: " + sessionOptions.PortNumber + Environment.NewLine;
        //            }
        //        }

        //    }
        //    catch (TimeoutException ex)
        //    {
        //        Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
        //    }
        //    catch (SessionRemoteException ex)
        //    {
        //        Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
        //    }
        //    catch (SessionLocalException ex)
        //    {
        //        Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
        //    }
        //    catch (Exception ex)
        //    {
        //        Result += "Failed: " + ex.GetType().Name + " " + ex.Message;
        //    }

        //    return Result;
        //}

        //internal static TransferOperationResult FtpRx(vw_CustomerProfile prof)
        //{
        //    SessionOptions sessionOptions = new SessionOptions
        //    {
        //        HostName = prof.P_SERVER ?? prof.P_SERVER.Trim(),
        //        UserName = prof.P_USERNAME ?? prof.P_USERNAME.Trim(),
        //        Password = prof.P_PASSWORD ?? prof.P_PASSWORD.Trim()
        //    };

        //    if (prof.SSL)
        //    {
        //        sessionOptions.Protocol = Protocol.Sftp;
        //        sessionOptions.SshPrivateKeyPath = prof.P_SUBJECT ?? prof.P_SUBJECT.Trim();
        //        sessionOptions.SshHostKeyFingerprint = prof.P_EMAILADDRESS ?? prof.P_EMAILADDRESS.Trim();
        //    }
        //    else
        //    {
        //        sessionOptions.Protocol = Protocol.Ftp;
        //    }
        //    sessionOptions.PortNumber = int.Parse(prof.P_PORT);
        //    sessionOptions.Timeout = new TimeSpan(0, 0, 20);
        //    try
        //    {
        //        using (Session session = new Session())
        //        {
        //            session.Open(sessionOptions);

        //            TransferOptions transferOptions = new TransferOptions();
        //            TransferOptions trOptions = transferOptions;
        //            trOptions.TransferMode = TransferMode.Automatic;
        //            trOptions.FileMask = "|*/";
        //            trOptions.OverwriteMode = OverwriteMode.Overwrite;
        //            trOptions.ResumeSupport.State = TransferResumeSupportState.On;
        //            TransferOperationResult trResult;
        //            string remoteFolder = string.Format("/{0}/*", prof.P_PATH ?? prof.P_PATH.Trim());
        //            try
        //            {
        //                trResult = session.GetFiles(remoteFolder, Path.Combine(prof.C_PATH, "Processing") + "\\", true, trOptions);
        //                trResult.Check();
        //                if (trResult.Transfers.Count > 0)
        //                {
        //                    return trResult;
        //                }
        //            }
        //            catch (Exception)
        //            {
        //                using (AppLogger log = new AppLogger(Globals.AppLogPath))
        //                {

        //                }

        //            }
        //        }
        //    }
        //    catch (Exception)
        //    {

        //    }
        //    return null;
        //}

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
    }

    //public static string FtpSend(string file, CustProfileRecord cust)
    //    {
    //        string Result = string.Empty;
    //        try
    //        {
    //            SessionOptions sessionOptions = new SessionOptions
    //            {
    //                HostName = cust.P_server,
    //                PortNumber = int.Parse(cust.P_port),
    //                UserName = cust.P_username,
    //                Password = cust.P_password
    //            };
    //            if (cust.P_ssl)
    //            {
    //                sessionOptions.Protocol = Protocol.Sftp;
    //            }
    //            else
    //            {
    //                sessionOptions.Protocol = Protocol.Ftp;
    //            }
    //            //try
    //            //{
    //            TransferOperationResult tr;
    //            using (Session session = new Session())
    //            {
    //                session.Open(sessionOptions);
    //                if (session.Opened)
    //                {
    //                    //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Connected to " + custProfile[0].P_server + ". Sending file  :" + xmlfile + Environment.NewLine + " TO " + sessionOptions.HostName + "/" + custProfile[0].P_path + "/" + Path.GetFileName(xmlfile));
    //                    TransferOptions tOptions = new TransferOptions();
    //                    tOptions.OverwriteMode = OverwriteMode.Overwrite;
    //                    tr = session.PutFiles(file, "/" + cust.P_path + "/" + Path.GetFileName(file), true, tOptions);
    //                    //NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Results: ");
    //                    if (tr.IsSuccess)
    //                    {
    //                        Result = "File Sent Ok";
    //                        try
    //                        {
    //                            File.Delete(file);
    //                        }
    //                        catch (Exception ex)
    //                        {
    //                            Result = "File Sent Ok. Delete Error: " + ex.Message;
    //                        }

    //                    }
    //                    else
    //                    {
    //                        Result = "Failed: Transfer Result is invalid: ";
    //                    }

    //                }
    //                else
    //                {

    //                }
    //            }

    //        }
    //        catch (Exception ex)
    //        {
    //            Result = "Failed: " + ex.GetType().Name + " " + ex.Message;
    //        }

    //        return Result;
    //    }
    //}
}
