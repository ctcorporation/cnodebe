﻿using CNodeBE.Forms;
using NodeData;
using NodeData.DTO;
using NodeData.Models;
using NodeResources;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using WinSCP;

namespace CNodeBE
{

    public partial class FrmCustomerProfile : Form
    {
        public Guid r_id;
        public Guid gP_id;
        string custRoot = Globals.RootPath;
        bool isSatelliteDatabound;
        string connString = Globals.DbConn;
        public FrmCustomerProfile()
        {
            InitializeComponent();
            //TODO: Create Load XSD to Rich Text 
            //


        }

        private void FillModuleListing()
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.connString())))
            {
                var ml = uow.Modules.GetAll();
                var moduleListing = (from m in uow.Modules.GetAll()
                                     orderby m.MO_ModuleName
                                     select new DataItem { ValueItem = m.MO_ID, StringItem = m.MO_ModuleName }).ToList();
                moduleListing.Insert(0, new DataItem { StringItem = "(none selected)" });
                ddlModuleListing.ValueMember = "ValueItem";
                ddlModuleListing.DisplayMember = "StringItem";
                ddlModuleListing.Items.Clear();
                ddlModuleListing.DataSource = moduleListing;


            }
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DeliveryChecked(object sender, EventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            string rbt = rb.Tag.ToString();
            switch (rbt)
            {
                case "pickup":
                    lblReceivePassword.Visible = false;
                    edReceiveUsername.Visible = false;
                    edReceivePassword.Visible = false;
                    lblReceiveUsername.Visible = false;
                    lblServerAddress.Visible = false;
                    edServerAddress.Visible = false;
                    lblFTPReceivePort.Visible = false;
                    edFTPReceivePort.Visible = false;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;


                    break;
                case "eadapter":
                    lblReceivePassword.Visible = true;
                    edReceiveUsername.Visible = true;
                    edReceivePassword.Visible = true;
                    lblReceiveUsername.Visible = true;
                    lblServerAddress.Visible = true;
                    lblServerAddress.Text = "eAdapter URL";
                    edServerAddress.Visible = true;
                    lblFTPReceivePort.Visible = false;
                    edFTPReceivePort.Visible = false;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;
                    break;
                case "ftp":
                    lblReceivePassword.Visible = true;
                    edReceiveUsername.Visible = true;
                    edReceivePassword.Visible = true;
                    lblReceiveUsername.Visible = true;
                    lblServerAddress.Visible = true;
                    lblServerAddress.Text = "Server Address";
                    edServerAddress.Visible = true;
                    lblFTPReceivePort.Visible = true;
                    edFTPReceivePort.Visible = true;
                    edFTPReceivePath.Visible = true;
                    lblFTPReceivePath.Visible = true;

                    break;
                case "email":
                    lblReceivePassword.Visible = false;
                    edReceiveUsername.Visible = false;
                    edReceivePassword.Visible = false;
                    lblReceiveUsername.Visible = false;
                    lblServerAddress.Visible = true;
                    edServerAddress.Visible = true;
                    lblServerAddress.Text = "Email Address";
                    lblFTPReceivePort.Visible = false;
                    edFTPReceivePort.Visible = false;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;
                    break;
                default:
                    lblReceivePassword.Visible = true;
                    edReceiveUsername.Visible = true;
                    edReceivePassword.Visible = true;
                    lblReceiveUsername.Visible = true;
                    lblServerAddress.Visible = true;
                    lblServerAddress.Text = "Server Address";
                    edServerAddress.Visible = true;
                    lblFTPReceivePort.Visible = true;
                    edFTPReceivePort.Visible = true;
                    edFTPReceivePath.Visible = false;
                    lblFTPReceivePath.Visible = false;
                    break;
            }
        }

        private void ReadOnlycontrols(bool toggle)
        {
            if (!toggle)
            {
                edCustomerCode.ReadOnly = true;
                cbActive.Enabled = true;
                cbHold.Enabled = true;
                edCustomerName.ReadOnly = true;
                cbDTS.Enabled = true;

            }
            else
            {
                edCustomerCode.ReadOnly = false;
                cbActive.Enabled = false;
                cbHold.Enabled = false;
                edCustomerName.ReadOnly = false;
                cbDTS.Enabled = false;
            }
        }

        private void FrmCustomerProfile_Load(object sender, EventArgs e)

        {
            FillGrid(r_id);

            FillCustomer();
            FillModuleListing();
            ddlDTSType.DisplayMember = "Text";
            ddlDTSType.ValueMember = "Value";

            var items = new[] {
            new { Text = "Replace", Value = "R" },
            new { Text = "Update", Value = "U" },
            new { Text = "Delete", Value = "D" },
            new { Text = "Modify", Value = "M" }};
            ddlDTSType.DataSource = items;
            pnlProfile.Enabled = false;
            tcCustomer.Enabled = false;
            tbSatellite.Hide();
        }

        private void CustomerBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();

        }

        public void CreateProfPath(string path, string profile)
        {
            if (profile != "*")
            {
                try
                {
                    string profilePath = Path.Combine(path, profile);
                    Directory.CreateDirectory(profilePath);
                }
                catch (UnauthorizedAccessException)
                {
                    MessageBox.Show("You do not have access to create a folder here.Please Check", "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                catch (IOException exIO)
                {
                    MessageBox.Show("Error Creating Directory: " + exIO.Message, "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                catch (Exception ex)
                {
                    var err = ex.GetType().Name + " : " + ex.Message;
                    MessageBox.Show("Error Creating Directory: " + err, "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

        }

        private void CreateRootPath(string path)
        {
            try
            {


                if (Directory.Exists(path))
                {
                    string rootPath = Path.Combine(path);
                    Directory.CreateDirectory(rootPath);
                    Directory.CreateDirectory(Path.Combine(rootPath, "Processing"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Held"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Archive"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Lib"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Failed"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Custom"));
                }
                else
                {
                    Directory.CreateDirectory(path);
                    string rootPath = Path.Combine(path);
                    Directory.CreateDirectory(Path.Combine(rootPath, "Processing"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Held"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Archive"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Lib"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Failed"));
                    Directory.CreateDirectory(Path.Combine(rootPath, "Custom"));
                }
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("You do not have access to create a folder here.Please Check", "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (IOException exIO)
            {
                MessageBox.Show("Error Creating Directory: " + exIO.Message, "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
            {
                var cust = uow.Customers.Get((Guid)cmbCustomer.SelectedValue);
                if (cust == null)
                {
                    cust = new Customer();

                }
                cust.C_NAME = edCustomerName.Text;
                cust.C_CODE = edCustomerCode.Text;
                cust.C_SHORTNAME = edShortName.Text;
                cust.C_PATH = lblActualPath.Text;
                cust.C_IS_ACTIVE = cbActive.Checked ? "Y" : "N";
                cust.C_ON_HOLD = cbHold.Checked ? "Y" : "N";
                cust.C_INVOICED = cbInvoice.Checked ? "Y" : "N";
                cust.C_FTP_CLIENT = "N";
                cust.C_SATELLITE = cbSatellite.Checked ? "Y" : "N";
                cust.C_TRIAL = cbTrial.Checked ? "Y" : "N";
                if (cbTrial.Checked)
                {
                    cust.C_TRIALSTART = DateTime.Parse(dtFrom.Text);
                    cust.C_TRIALEND = DateTime.Parse(dtTo.Text);
                }
                if (cust.C_ID == Guid.Empty)
                {
                    cust.C_ID = Guid.NewGuid();
                    uow.Customers.Add(cust);
                    sbMain.Text = "Customer Added.";
                    MessageBox.Show("Customer Added", "Customer Added", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    sbMain.Text = "Customer Updated.";
                    MessageBox.Show("Customer already exists. Customer updated", "Customer Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                this.r_id = cust.C_ID;
                uow.Complete();
                CreateRootPath(lblActualPath.Text);
            }

            btnSave.Text = "&Save";
            pnlProfile.Enabled = true;
            tcCustomer.SelectedIndex = 0;
            cbSatelliteMonitored.Checked = false;
            edSatelliteName.Text = "";
            LBLpid.Text = "";
            tcCustomer.Enabled = false;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
            {
                Profile profile;
                if (btnAdd.Text == "&Update >>")
                {
                    profile = uow.Profiles.Get(gP_id);
                }
                else
                {
                    profile = new Profile();
                }
                profile.P_C = this.r_id;
                profile.P_REASONCODE = edReasonCode.Text.Trim();
                profile.P_EVENTCODE = edEventCode.Text.Trim();
                profile.P_CWFILENAME = edCWFileName.Text.Trim();
                profile.P_CWAPPCODE = edCWAppCode.Text.Trim();
                profile.P_CWSUBJECT = edCWSubject.Text.Trim();
                profile.P_DTS = cbDTS.Checked ? "Y" : "N";
                profile.P_RENAMEFILE = cbRenameFile.Checked;
                profile.P_DIRECTION = rbSend.Checked ? "S" : "R";
                profile.P_SERVER = rbSend.Checked ? edSendCTCUrl.Text : edServerAddress.Text;
                profile.P_USERNAME = rbSend.Checked ? edSendUsername.Text : edReceiveUsername.Text;
                profile.P_PASSWORD = rbSend.Checked ? edSendPassword.Text : edReceivePassword.Text;
                profile.P_SUBJECT = rbSend.Checked ? edSendSubject.Text : edReceiveSubject.Text;
                profile.P_EMAILADDRESS = rbSend.Checked ? edSendEmailAddress.Text : edReceiveEmailAddress.Text;
                profile.P_PATH = rbSend.Checked ? edSendFolder.Text : edFTPReceivePath.Text;
                profile.P_PORT = rbSend.Checked ? edSendFtpPort.Text : edFTPReceivePort.Text;
                profile.P_NOTIFY = cbNotify.Checked ? "Y" : "N";
                profile.P_IGNOREMESSAGE = cbIgnoreMessages.Checked ? "Y" : "N";

                string sDelivery = string.Empty;
                string fileType = cmbRxFileType.Text;
                if (rbSend.Checked)
                {
                    foreach (RadioButton rb in gbSendMethod.Controls)
                    {
                        if (rb.Checked)
                        {

                            switch (rb.Name)
                            {

                                case "rbPickupSend":
                                    {
                                        sDelivery = "R";
                                        break;
                                    }
                                case " ":
                                    {
                                        sDelivery = "C";
                                        break;
                                    }
                                case "rbFTPSend":
                                    {
                                        sDelivery = "F";
                                        break;
                                    }
                                case "rbFTPSendDirect":
                                    {
                                        sDelivery = "D";
                                        break;
                                    }
                                case "rbEmailSend":
                                    {
                                        sDelivery = "E";
                                        fileType = cmbSendFileType.Text;
                                        break;
                                    }
                                case "rbSoapSend":
                                    {
                                        sDelivery = "S";
                                        break;
                                    }
                            }
                        }
                    }
                    profile.P_SSL = cbSendSSL.Checked ? "Y" : "N";
                }
                else
                {
                    foreach (RadioButton rb in gbReceiveMethod.Controls)
                    {
                        if (rb.Checked)
                        {
                            switch (rb.Name)
                            {

                                case "rbPickup":
                                    {
                                        sDelivery = "R";

                                        break;
                                    }
                                case "rbReceiveCTC":
                                    {
                                        sDelivery = "C";
                                        break;
                                    }
                                case "rbReceiveFTP":
                                    {
                                        sDelivery = "F";
                                        break;
                                    }
                                case "rbReceiveEmail":
                                    {
                                        sDelivery = "E";
                                        break;
                                    }
                                case "rbReceivePickup":
                                    {
                                        sDelivery = "P";
                                        break;
                                    }
                                case "rbReceiveSOAP":
                                    {
                                        sDelivery = "S";
                                        break;
                                    }
                            }
                        }
                    }
                    profile.P_SSL = cbReceiveSSL.Checked ? "Y" : "N";
                }
                profile.P_DELIVERY = sDelivery;
                profile.P_DESCRIPTION = edDescription.Text;
                profile.P_RECIPIENTID = edRecipientID.Text;
                profile.P_SENDERID = edSenderID.Text;
                Guid outProf;
                if (Guid.TryParse(cmbOutboundProf.SelectedValue.ToString(), out outProf))
                {
                    profile.P_OUTBOUND = outProf;
                }

                profile.P_OUTBOUNDTYPE = cmbOutboundType.Text;
                profile.P_CHARGEABLE = cbChargeable.Checked ? "Y" : "N";
                profile.P_BILLTO = rbCustomer.Checked ? edCustomerCode.Text : edRecipientID.Text;
                profile.P_MSGTYPE = cmbMsgType.Text;
                profile.P_FILETYPE = fileType;
                //profile.P_METHOD = edCustomMethod.Text;
                profile.P_METHOD = cmbCustomMethod.Text;
                profile.P_GROUPCHARGES = cbGroupCharges.Checked ? "Y" : "N";
                profile.P_SENDEREMAIL = edReceiveSender.Text;
                profile.P_XSD = GetXsdPath();
                profile.P_LIBNAME = edLibraryName.Text;

                //adding from to Method conversions fields
                //Change selected name from Conversion to CON and Mapping to MAP - manual list
                switch (cmbFromMethod.Text)
                {
                    case "Conversion":
                        {
                            profile.P_CONVFROMMETHOD = "CON";
                            break;
                        }
                    case "Mapping":
                        {
                            profile.P_CONVFROMMETHOD = "MAP";
                            break;
                        }
                    default:
                        {
                            profile.P_CONVFROMMETHOD = "";
                            break;
                        }
                }
                switch (cmbToMethod.Text)
                {
                    case "Conversion":
                        {
                            profile.P_CONVTOMETHOD = "CON";
                            break;
                        }
                    case "Mapping":
                        {
                            profile.P_CONVTOMETHOD = "MAP";
                            break;
                        }
                    default:
                        {
                            profile.P_CONVTOMETHOD = "";
                            break;
                        }
                }
                Guid g;
                if (cmbFromMethod.Text == "")
                {
                    profile.P_CONVFROMMETHODNAME = Guid.Empty;
                }
                else
                {
                    profile.P_CONVFROMMETHODNAME = Guid.TryParse(cmbFromMethodName.SelectedValue.ToString(), out g) ? g : Guid.Empty;
                }
                if (cmbToMethod.Text == "")
                {
                    profile.P_CONVTOMETHODNAME = Guid.Empty;
                }
                else
                {
                    profile.P_CONVTOMETHODNAME = Guid.TryParse(cmbToMethodName.SelectedValue.ToString(), out g) ? g : Guid.Empty;
                }
                //profile.P_CONVTOMETHODNAME = Guid.TryParse(cmbToMethodName.SelectedValue.ToString(), out g)?g: Guid.Empty;

                string listParams = string.Empty;
                foreach (var lbItem in lbParameters.Items)
                {
                    if (!string.IsNullOrEmpty(lbItem.ToString()))
                    {
                        listParams += lbItem.ToString() + '|';
                    }

                }
                profile.P_PARAMLIST = listParams;
                profile.P_ACTIVE = cbProfileActive.Checked ? "Y" : "N";
                if (profile.P_ID == Guid.Empty)
                {
                    MessageBox.Show("Customer Profile Updated. Rule Created", "Customer Profiles", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    sbMain.Text = "Customer profile rule created.";
                    uow.Profiles.Add(profile);
                }
                else
                {
                    MessageBox.Show("Customer Profile Updated", "Customer Profiles", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    sbMain.Text = "Customer profile updated.";
                }
                uow.Complete();
            }
            FillGrid(this.r_id);
            if (rbPickup.Checked)
            {
                CreateProfPath(Path.Combine(lblActualPath.Text + "\\Processing"), edRecipientID.Text);
            }
            //clear after add or update
            edDescription.Text = "";
            edCWSubject.Text = "";
            edReasonCode.Text = "";
            edRecipientID.Text = "";
            rbPickup.Checked = true;
            edFTPReceivePort.Text = "";
            edServerAddress.Text = "";
            edReceivePassword.Text = "";
            edReceivePassword.Text = "";
            btnAdd.Text = "&Add  >>";
            cmbFromMethod.Text = "";
            cmbToMethod.Text = "";
            cmbToMethodName.Text = "";
            cmbToMethodName.Text = "";
            cmbCustomMethod.Text = "";
            FillCustProfile(this.r_id);
            cmbOutboundType.Text = "";

        }

        private void FillGrid(Guid c_id)
        {
            if (c_id != Guid.Empty)
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
                {
                    var activeFilter = cbHideInactive.Checked ? "Y" : "N";
                    var profList = uow.CustProfiles.Find(x => x.C_ID == c_id && x.P_ACTIVE == activeFilter).OrderBy(y => y.P_RECIPIENTID).ThenBy(y => y.P_SENDERID).ToList();
                    BindingSource bsProfileList = new BindingSource();

                    if (profList.Count > 0)
                    {
                        dgProfile.AutoGenerateColumns = false;
                        bsProfileList.DataSource = new SortableBindingList<vw_CustomerProfile>(profList);
                        dgProfile.DataSource = bsProfileList.DataSource;
                    }
                }

            }
        }

        private void FillCustomer()
        {
            try
            {
                var custList = new[] { new { Name = "(none selected}", ID = Guid.Empty } }.ToList();
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
                {
                    var qCustList = uow.Customers.GetAll().OrderBy(x => x.C_NAME).AsQueryable();
                    custList = (from x in qCustList
                                select new { Name = x.C_NAME, ID = x.C_ID }).ToList();
                    if (custList.Count > 0)
                    {
                        custList.Insert(0, new { Name = "(none selected}", ID = Guid.Empty });
                        cmbCustomer.DataSource = custList;
                        cmbCustomer.DisplayMember = "Name";
                        cmbCustomer.ValueMember = "ID";


                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Found Accessing DB. Error was: " + ex.Message);

            }
        }

        private void FillDTSGrid(Guid pid)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
                {
                    var dtsList = uow.DTServices.Find(x => x.D_P == pid).ToList();
                    dgDts.AutoGenerateColumns = false;
                    BindingSource bsDTS = new BindingSource
                    {
                        DataSource = new SortableBindingList<DTS>(dtsList)
                    };
                    dgDts.DataSource = bsDTS;

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Found Accessing DB. Error was: " + ex.Message);
            }
        }

        private void ClearControls(bool allControls)
        {
            if (allControls)
            {
                edCustomerCode.Text = "";
                edCustomerName.Text = "";
                edShortName.Text = "";
                edRootPath.Text = "";
                cbActive.Checked = false;
                cbHold.Checked = false;
                cbTrial.Checked = false;
            }
            tvwXSD.ResetText();
            edReceiveCustomerName.Text = "";
            edDescription.Text = "";
            edFTPReceivePort.Text = "";
            edReasonCode.Text = "";
            edRecipientID.Text = "";
            edReceivePassword.Text = "";
            edReceiveUsername.Text = "";
            edServerAddress.Text = "";
            btnAdd.Text = "&Add >>";
            edDTSCurrentValue.Text = "";
            rbReceive.Checked = false;
            rbSend.Checked = false;
            edReasonCode.Text = "";
            edEventCode.Text = "";
            cmbMsgType.SelectedIndex = -1;
            cbChargeable.Checked = false;
            cbGroupCharges.Checked = false;
            edServerAddress.Text = "";
            edFTPReceivePort.Text = "";
            edFTPReceivePath.Text = "";
            btnXsdPath.Text = "";
            edReceiveSubject.Text = "";
            edReceiveEmailAddress.Text = "";
            edReceiveSender.Text = "";
            edReceiveSubject.Text = "";
            edXSDPath.Text = "";
            cmbCustomMethod.Text = "";
            lbParameters.Items.Clear();
            edLibraryName.Text = "";
            cmbFromMethod.Text = "";
            cmbToMethod.Text = "";
            cmbFromMethodName.Text = "";
            cmbToMethodName.Text = "";

        }
        private void GetCustomer(Guid customerId)
        {
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
                {
                    var heartbeats = uow.HeartBeats.GetAll();
                    var customers = uow.Customers.GetAll();

                    var cust = (from c in customers
                                where c.C_ID == customerId
                                join h in heartbeats on c.C_ID equals h.HB_C into custHb
                                from custHbLookup in custHb.DefaultIfEmpty()

                                select new
                                {
                                    ID = c.C_ID,
                                    Code = c.C_CODE,
                                    Name = c.C_NAME,
                                    ShortName = c.C_SHORTNAME,
                                    Path = c.C_PATH,
                                    Active = c.C_IS_ACTIVE == "Y",
                                    Invoiced = c.C_INVOICED == "Y",
                                    OnHold = c.C_ON_HOLD == "Y",
                                    Satellite = c.C_SATELLITE == "Y",
                                    Monitored = custHbLookup.HB_ISMONITORED == "Y",
                                    PID = custHbLookup.HB_PID,
                                    SatelliteName = custHbLookup.HB_NAME,
                                    SatellitePAth = custHbLookup.HB_PATH
                                }).FirstOrDefault();

                    if (cust != null)
                    {
                        edCustomerCode.Text = cust.Code.Trim();
                        edCustomerName.Text = cust.Name.Trim();
                        edSenderID.Text = cust.Code.Trim();
                        try
                        {
                            edRootPath.Text = cust.Path.Trim().Substring(0, cust.Path.Trim().IndexOf(cust.Code.Trim()));
                            lblWarningPath.Visible = false;
                        }
                        catch (Exception ex)
                        {
                            edRootPath.Text = cust.Path.Trim();
                            lblWarningPath.Visible = true;
                        }
                        lblActualPath.Text = cust.Path.Trim();
                        cbActive.Checked = cust.Active;
                        cbInvoice.Checked = cust.Invoiced;
                        cbHold.Checked = cust.OnHold;
                        if (cust.Satellite)
                        {
                            tbSatellite.Show();
                            cbSatellite.Checked = cust.Satellite;
                            edSatelliteName.Text = cust.SatelliteName;
                            LBLpid.Text = cust.PID.ToString();
                        }
                        else
                        {
                            tbSatellite.Hide();
                            cbSatellite.Checked = false;
                            tcCustomer.SelectedIndex = 0;
                            cbSatelliteMonitored.Checked = false;
                            edSatelliteName.Text = "";
                            LBLpid.Text = "";
                        }
                        r_id = cust.ID;
                        edShortName.Text = cust.ShortName.Trim();
                        FillGrid(r_id);
                        btnNewCustomer.Text = "&Edit";
                    }
                }
                GetCustomerModules(customerId);

            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error :" + ex.Message, "Error Locating Records", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            ClearControls(true);
            if (cmbCustomer.SelectedIndex > 0)
            {
                GetCustomer((Guid)cmbCustomer.SelectedValue);
                FillCustProfile((Guid)cmbCustomer.SelectedValue);
            }
            else
            {
                btnNewCustomer.Text = "&New";
            }
            pnlProfile.Enabled = true;



        }

        public void FillCustProfile(Guid c_id)
        {
            try
            {
                var profList = new[] { new { Name = "(none selected}", ID = Guid.Empty } }.ToList();
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
                {
                    var qProfList = uow.Profiles.Find(x => x.P_C == c_id).OrderBy(x => x.P_DESCRIPTION).AsQueryable();
                    profList = (from x in qProfList
                                select new { Name = x.P_DESCRIPTION, ID = x.P_ID }).ToList();
                    if (profList.Count > 0)
                    {

                        profList.Insert(0, new { Name = "(none selected}", ID = Guid.Empty });
                        cmbOutboundProf.DataSource = profList;
                        cmbOutboundProf.DisplayMember = "Name";
                        cmbOutboundProf.ValueMember = "ID";


                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Found Accessing DB. Error was: " + ex.Message);

            }
        }

        public void FillCustProfile(Guid c_id, vw_CustomerProfile outProf)
        {
            try
            {
                var profList = new[] { new { Name = "(none selected}", ID = Guid.Empty } }.ToList();
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
                {
                    var qProfList = uow.Profiles.Find(x => x.P_C == c_id).OrderBy(x => x.P_DESCRIPTION).AsQueryable();
                    profList = (from x in qProfList
                                select new { Name = x.P_DESCRIPTION, ID = x.P_ID }).ToList();
                    if (profList.Count > 0)
                    {
                        profList.Insert(0, new { Name = outProf.P_DESCRIPTION, ID = outProf.P_ID });
                        profList.Insert(1, new { Name = "(none selected}", ID = Guid.Empty });
                        cmbOutboundProf.DataSource = profList;
                        cmbOutboundProf.DisplayMember = "Name";
                        cmbOutboundProf.ValueMember = "ID";


                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Found Accessing DB. Error was: " + ex.Message);

            }
        }

        public void GetCustomerProfile(int dgRow)
        {
            lbParameters.Items.Clear();
            Guid gid;
            if (Guid.TryParse(dgProfile[0, dgRow].Value.ToString(), out gid))
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
                {
                    var profile = uow.CustProfiles.Find(x => x.P_ID == gid).FirstOrDefault();
                    if (profile != null)
                    {
                        gP_id = gid;

                        edDescription.Text = profile.P_DESCRIPTION.Trim();
                        edReasonCode.Text = profile.P_REASONCODE.Trim();
                        edEventCode.Text = !string.IsNullOrEmpty(profile.P_EVENTCODE) ? profile.P_EVENTCODE.Trim() : string.Empty;
                        edRecipientID.Text = profile.P_RECIPIENTID.Trim();
                        edServerAddress.Text = profile.P_SERVER.Trim();
                        edCWAppCode.Text = string.IsNullOrEmpty(profile.P_CWAPPCODE) ? "UDM" : profile.P_CWAPPCODE.Trim();
                        edCWSubject.Text = string.IsNullOrEmpty(profile.P_CWSUBJECT) ? "* " : profile.P_CWSUBJECT.Trim();
                        edCWFileName.Text = string.IsNullOrEmpty(profile.P_CWFILENAME) ? "* " : profile.P_CWFILENAME.Trim();
                        cbNotify.Checked = profile.P_NOTIFY == "Y";
                        edSenderID.Text = profile.P_SENDERID.Trim();
                        cbChargeable.Checked = profile.P_CHARGEABLE == "Y";
                        cbRenameFile.Checked = profile.P_RENAMEFILE;
                        //TODO Repair XSD Load. 
                        //rtbXSD.Text = dr["P_XSD"].ToString();
                        edXSDPath.Text = !string.IsNullOrEmpty(profile.P_XSD) ? profile.P_XSD.Trim() : string.Empty;


                        edLibraryName.Text = !string.IsNullOrEmpty(profile.P_LIBNAME) ? profile.P_LIBNAME.Trim() : string.Empty;
                        //edCustomMethod.Text = !string.IsNullOrEmpty(profile.P_METHOD) ? profile.P_METHOD.Trim() : string.Empty;
                        cmbCustomMethod.Text = !string.IsNullOrEmpty(profile.P_METHOD) ? profile.P_METHOD.Trim() : string.Empty;
                        edRecipientID.Text = profile.P_RECIPIENTID.Trim();
                        cmbMsgType.Text = profile.P_MSGTYPE.Trim();
                        cmbOutboundType.Text = profile.P_OUTBOUNDTYPE;
                        var outProf = uow.CustProfiles.Find(x => x.P_ID == profile.P_OUTBOUND).FirstOrDefault();

                        if (outProf != null)
                        {
                            FillCustProfile(profile.C_ID, outProf);
                        }
                        else
                        {
                            FillCustProfile(profile.C_ID);
                        }


                        cbProfileActive.Checked = profile.P_ACTIVE == "Y";
                        cbIgnoreMessages.Checked = profile.P_IGNOREMESSAGE == "Y";
                        cbGroupCharges.Checked = profile.P_GROUPCHARGES == "Y";
                        cbDTS.Checked = profile.P_DTS == "Y";
                        //adding from to Method conversions fields
                        //Change selected name from Conversion to CON and Mapping to MAP - manual list
                        switch (profile.P_CONVFROMMETHOD)
                        {
                            case "CON":
                                {
                                    cmbFromMethod.Text = "Conversion";
                                    break;
                                }
                            case "MAP":
                                {
                                    cmbFromMethod.Text = "Mapping";
                                    break;
                                }
                            default:
                                {
                                    cmbFromMethod.Text = "";
                                    break;
                                }
                        }
                        switch (profile.P_CONVTOMETHOD)
                        {
                            case "CON":
                                {
                                    cmbToMethod.Text = "Conversion";
                                    break;
                                }
                            case "MAP":
                                {
                                    cmbToMethod.Text = "Mapping";
                                    break;
                                }
                            default:
                                {
                                    cmbToMethod.Text = "";
                                    break;
                                }
                        }
                        cmbFromMethodName.SelectedValue = profile.P_CONVFROMMETHODNAME;
                        cmbToMethodName.SelectedValue = profile.P_CONVTOMETHODNAME;

                        string[] paramlist = !string.IsNullOrEmpty(profile.P_PARAMLIST) ? profile.P_PARAMLIST.Trim().Split('|') : null;
                        if (paramlist != null)
                        {
                            for (int i = 0; i < paramlist.Length; i++)
                            {
                                lbParameters.Items.Add(paramlist[i]);
                            }
                        }
                        else
                        {
                            lbParameters.Items.Clear();
                        }

                        //Receive Section
                        if (profile.P_DIRECTION == "R")
                        {
                            rbReceive.Checked = true;
                            rbSend.Checked = false;
                            edServerAddress.Text = !string.IsNullOrEmpty(profile.P_SERVER) ? profile.P_SERVER.Trim() : string.Empty;
                            edFTPReceivePath.Text = !string.IsNullOrEmpty(profile.P_PATH) ? profile.P_PATH.Trim() : string.Empty;
                            edReceiveUsername.Text = !string.IsNullOrEmpty(profile.P_USERNAME) ? profile.P_USERNAME.Trim() : string.Empty;
                            edReceivePassword.Text = !string.IsNullOrEmpty(profile.P_PASSWORD) ? profile.P_PASSWORD.Trim() : string.Empty;
                            edReceiveCustomerName.Text = !string.IsNullOrEmpty(profile.P_CUSTOMERCOMPANYNAME) ? profile.P_CUSTOMERCOMPANYNAME.Trim() : string.Empty;
                            edReceiveEmailAddress.Text = !string.IsNullOrEmpty(profile.P_EMAILADDRESS) ? profile.P_EMAILADDRESS.Trim() : string.Empty;
                            edFTPReceivePort.Text = !string.IsNullOrEmpty(profile.P_PORT) ? profile.P_PORT.Trim() : string.Empty;
                            cbReceiveSSL.Checked = profile.P_SSL == "Y";
                            edReceiveSubject.Text = !string.IsNullOrEmpty(profile.P_SUBJECT) ? profile.P_SUBJECT.Trim() : string.Empty;
                            edReceiveSender.Text = !string.IsNullOrEmpty(profile.P_SENDEREMAIL) ? profile.P_SENDEREMAIL.Trim() : string.Empty;
                            cmbRxFileType.Text = !string.IsNullOrEmpty(profile.P_FILETYPE) ? profile.P_FILETYPE.Trim() : string.Empty;
                            cbForwardWithFlags.Checked = profile.P_FORWARDWITHFLAGS == "Y";
                            switch (profile.P_DELIVERY.Trim())
                            {
                                case "R":
                                    {
                                        rbPickup.Checked = true;
                                        break;
                                    }
                                case "F":
                                    {
                                        rbReceiveFTP.Checked = true;
                                        cbReceiveSSL.Visible = true;
                                        break;
                                    }
                                case "E":
                                    {
                                        rbReceiveEmail.Checked = true;
                                        break;
                                    }
                                case "P":
                                    {
                                        rbReceivePickup.Checked = true;
                                        break;
                                    }
                                case "S":
                                    {
                                        rbReceiveSOAP.Checked = true;
                                        break;
                                    }
                                default:
                                    {
                                        rbPickup.Checked = true;
                                        break;
                                    }
                            }
                            //Setup Receive section
                            SetupReceive();
                        }
                        else
                        {
                            //Setup Send Section
                            rbReceive.Checked = false;
                            rbSend.Checked = true;
                            cbSendSSL.Checked = profile.P_SSL == "Y";

                            edSendCTCUrl.Text = !string.IsNullOrEmpty(profile.P_SERVER) ? profile.P_SERVER.Trim() : string.Empty;
                            edSendFolder.Text = !string.IsNullOrEmpty(profile.P_PATH) ? profile.P_PATH.Trim() : string.Empty;
                            edSendUsername.Text = !string.IsNullOrEmpty(profile.P_USERNAME) ? profile.P_USERNAME.Trim() : string.Empty;
                            edSendPassword.Text = !string.IsNullOrEmpty(profile.P_PASSWORD) ? profile.P_PASSWORD.Trim() : string.Empty;
                            edSendSubject.Text = !string.IsNullOrEmpty(profile.P_SUBJECT) ? profile.P_SUBJECT.Trim() : string.Empty;
                            edSendEmailAddress.Text = profile.P_EMAILADDRESS.Trim();
                            edSendFtpPort.Text = !string.IsNullOrEmpty(profile.P_PORT) ? profile.P_PORT.Trim() : string.Empty;
                            switch (profile.P_DELIVERY.Trim())
                            {
                                case "R":
                                    {
                                        rbPickupSend.Checked = true;
                                        break;
                                    }
                                case "F":
                                    {
                                        rbFTPSend.Checked = true;
                                        cbSendSSL.Visible = true;
                                        break;
                                    }
                                case "D":
                                    {
                                        rbFTPSendDirect.Checked = true;
                                        break;
                                    }
                                case "E":
                                    {
                                        rbEmailSend.Checked = true;
                                        break;
                                    }
                                case "C":
                                    {
                                        rbCTCSend.Checked = true;
                                        break;
                                    }
                                case "S":
                                    {
                                        rbSoapSend.Checked = true;
                                        break;
                                    }
                                default:
                                    {
                                        rbPickupSend.Checked = true;
                                        break;
                                    }
                            }
                            SetupSend();
                            //Removed as screen set in SetupSend - to be deleted
                            //if (!cbSendSSL.Checked)
                            //{
                            //    edSendSubject.Visible = false;
                            //    lblSendSubject.Visible = false;
                            //    lblSendSubject.Text = "Subject";
                            //    lblSendEmailAddress.Visible = false;
                            //    lblSendEmailAddress.Text = "Email Address(es)";
                            //    edSendEmailAddress.Visible = false;
                            //    btnSendKeyPath.Visible = false;
                            //}
                            //else
                            //{
                            //    edSendSubject.Visible = true;
                            //    lblSendSubject.Visible = true;
                            //    lblSendSubject.Text = "SFTP Key File";
                            //    lblSendEmailAddress.Visible = true;
                            //    lblSendEmailAddress.Text = "Server SHA Fingerprint";
                            //    edSendEmailAddress.Visible = true;
                            //    btnSendKeyPath.Visible = true;

                            //}
                        }
                        btnAdd.Text = "&Update >>";
                    }
                    sbMain.Text = "Update Profile settings.";
                }
                FillDTSGrid(gP_id);
            }
        }

        private void btnNewCustomer_Click(object sender, EventArgs e)
        {
            if (btnNewCustomer.Text == "&New")
            {
                ClearControls(true);
                pnlProfile.Enabled = false;
                edCustomerName.Text = "";
                edCustomerName.Focus();
                btnSave.Text = "&Save";
                edRootPath.Text = Globals.RootPath;
            }
            else
            {
                btnSave.Text = "&Update";
                pnlProfile.Enabled = true;

            }
            tcCustomer.Enabled = true;
        }
        private void dgProfile_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            GetCustomerProfile(e.RowIndex);
        }

        private void dgProfile_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {

            DialogResult drDelete = MessageBox.Show("Are you sure you wish to delete this rule?", "Delete from Customer Profile", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (drDelete == DialogResult.Yes)
            {

                Guid p_id = new Guid(dgProfile[0, e.Row.Index].Value.ToString());
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
                {

                    uow.Profiles.Remove(uow.Profiles.Get(p_id));
                    uow.Complete();
                    FillGrid(this.r_id);
                    MessageBox.Show("Client rule deleted", "Customer profile updated", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

            }
        }

        private void edCustomerCode_TextChanged(object sender, EventArgs e)
        {
            edRootPath.Text = Path.Combine(Globals.RootPath, edCustomerCode.Text);

        }

        private void rbSend_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSend.Checked)
            {
                gbReceiveOptions.Enabled = false;
                gbSendOptions.Enabled = true;
            }
            else
            {
                gbReceiveOptions.Enabled = true;
                gbSendOptions.Enabled = false;
            }
        }

        private void dgProfile_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {

            if (dgProfile.Rows[e.RowIndex].Cells["Direction"].Value.ToString() == "R")
            {
                dgProfile.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Green;
            }
            if (dgProfile.Rows[e.RowIndex].Cells["Direction"].Value.ToString() == "S")
            {
                dgProfile.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.SkyBlue;
            }
            if (dgProfile.Rows[e.RowIndex].Cells["Active"].Value.ToString() == "N")
            {
                dgProfile.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.DarkRed;
            }
        }

        private void rbReceive_CheckedChanged(object sender, EventArgs e)
        {
            if (rbSend.Checked)
            {
                gbReceiveOptions.Enabled = false;
                gbSendOptions.Enabled = true;
            }
            else
            {
                gbReceiveOptions.Enabled = true;
                gbSendOptions.Enabled = false;
            }
        }

        private void cbDTS_CheckedChanged(object sender, EventArgs e)
        {
            if (cbDTS.Checked)
            {
                gbDTS.Enabled = true;
            }
            else
            {
                gbDTS.Enabled = false;
            }
        }

        private void dgDts_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            edDTSIndex.Text = dgDts[0, e.RowIndex].Value.ToString();
            ddlDTSType.SelectedValue = dgDts[3, e.RowIndex].Value.ToString();
            edDTSSearch.Text = dgDts[2, e.RowIndex].Value.ToString();
            if (dgDts[3, e.RowIndex].Value.ToString() == "R")
            {
                rbDTSRecord.Checked = true;
            }
            else
            {
                rbDTSStructure.Checked = true;
            }
            ddlDTSFileType.Text = dgDts[4, e.RowIndex].Value.ToString();
            edDTSQual.Text = dgDts[5, e.RowIndex].Value.ToString();
            edDTSTarget.Text = dgDts[6, e.RowIndex].Value.ToString();
            edDTSValue.Text = dgDts[7, e.RowIndex].Value.ToString();
            edDTSCurrentValue.Text = dgDts[8, e.RowIndex].Value.ToString();

        }

        private void bbSave_Click(object sender, EventArgs e)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
            {
                var dts = new DTS
                {
                    D_C = r_id,
                    D_P = gP_id,
                    D_INDEX = int.Parse(edDTSIndex.Text),
                    D_FILETYPE = ddlDTSFileType.Text,
                    D_DTSTYPE = ddlDTSType.SelectedValue.ToString(),
                    D_SEARCHPATTERN = edDTSSearch.Text,
                    D_NEWVALUE = edDTSValue.Text,
                    D_QUALIFIER = edDTSQual.Text,
                    D_TARGET = edDTSTarget.Text,
                    D_CURRENTVALUE = edDTSCurrentValue.Text
                };
                if (rbDTSRecord.Checked)
                {
                    dts.D_DTS = "R";

                }
                else
                {
                    if (rbDTSStructure.Checked)
                    {
                        dts.D_DTS = "S";
                    }
                }
                uow.DTServices.Add(dts);
                uow.Complete();
                FillDTSGrid(gP_id);

            }

        }

        private void btnTestSendeAdapter_Click(object sender, EventArgs e)
        {
            edSendeAdapterResults.Clear();
            if (!ValidateSender())
            {
                return;
            }

            if (!ValidatePassword())
            {
                return;
            }

            if (!ValidateServer())
            {
                return;
            }

            sbMain.Text = "Cargowise Service Details Verified ok.\r\n";
        }

        private bool ValidateSender()
        {
            var code = edSendUsername.Text.Trim();
            if (string.IsNullOrEmpty(code))
            {
                edSendeAdapterResults.Text += ("Sender ID cannot be Blank\r\n");
                return false;
            }

            edSendeAdapterResults.Text += "Sender ID is Ok\r\n";
            return true;
        }

        private bool ValidatePassword()
        {
            var code = edSendPassword.Text.Trim();
            if (string.IsNullOrEmpty(code))
            {
                edSendeAdapterResults.Text += "Password should not be Blank\r\n";
                return false;
            }
            edSendeAdapterResults.Text += "Password is ok\r\n";
            return true;

        }
        private bool ValidateServer()
        {
            var code = edSendCTCUrl.Text.Trim();
            if (string.IsNullOrEmpty(code))
            {
                edSendeAdapterResults.Text += "Service URL cannot be Blank\r\n";
                return false;
            }
            edSendeAdapterResults.Text += "Service URL is OK.\r\n";
            try
            {
                if (CTCAdapter.Ping(edSendCTCUrl.Text, edSendUsername.Text, edSendPassword.Text))
                {
                    edSendeAdapterResults.Text += "Ping Service URL is Successful\r\n";
                    sbMain.Text = "eAdapter Correctly configured.";
                    return true;
                }
                else
                {
                    edSendeAdapterResults.Text += "Ping Service URL is Un-Successful. Please check username and password or URL\r\n";
                    return false;
                }

            }
            catch (Exception ex)
            {
                edSendeAdapterResults.Text += "Error Message: " + ex.Message + "\r\n";
                sbMain.Text = "Test eAdapater failed. Please send Results to CTC Support";

                return false;
            }
        }

        private void rbEmail_CheckedChanged(object sender, EventArgs e)
        {
            SetupReceive();
        }


        private void rbCTCSend_CheckedChanged(object sender, EventArgs e)
        {
            SetupSend();
        }


        void SetupReceive()
        {
            string strReceive = string.Empty;
            foreach (RadioButton rb in gbReceiveMethod.Controls)
            {
                if (rb.Checked)
                {
                    switch (rb.Name)
                    {


                        case "rbPickup":
                            lblServerAddress.Visible = false;
                            edServerAddress.Visible = false;
                            cbReceiveSSL.Visible = false;
                            lblFTPReceivePath.Visible = true;
                            lblFTPReceivePath.Text = "Path";
                            edFTPReceivePath.Visible = true;
                            btnReceiveFolder.Visible = true;
                            lblFTPReceivePort.Visible = false;
                            edFTPReceivePort.Visible = false;
                            lblReceiveUsername.Visible = false;
                            edReceiveUsername.Visible = false;
                            lblReceivePassword.Visible = false;
                            edReceivePassword.Visible = false;
                            lblReceiveEmailAddress.Visible = false;
                            edReceiveEmailAddress.Visible = false;
                            lblReceiveCustomerName.Visible = true;
                            edReceiveCustomerName.Visible = true;
                            lblRxFileType.Visible = false;
                            cmbRxFileType.Visible = false;
                            lblSenderEmail.Visible = false;
                            edReceiveSender.Visible = false;
                            lblReceiveSubject.Visible = false;
                            edReceiveSubject.Visible = false;
                            btnReceiveKeyPath.Visible = false;
                            bbTestRx.Visible = false;
                            lblRxResults.Visible = false;
                            txtTestResults.Visible = false;
                            break;
                        case "rbReceiveFTP":
                            lblServerAddress.Visible = true;
                            edServerAddress.Visible = true;
                            cbReceiveSSL.Visible = true;
                            cbReceiveSSL.Text = "Secure FTP";
                            lblFTPReceivePort.Visible = true;
                            edFTPReceivePort.Visible = true;
                            lblFTPReceivePath.Visible = true;
                            lblFTPReceivePath.Text = "Path";
                            edFTPReceivePath.Visible = true;
                            lblReceiveUsername.Visible = true;
                            edReceiveUsername.Visible = true;
                            lblReceivePassword.Visible = true;
                            edReceivePassword.Visible = true;
                            lblReceiveCustomerName.Visible = false;
                            edReceiveCustomerName.Visible = false;
                            lblReceiveEmailAddress.Visible = false;
                            edReceiveEmailAddress.Visible = false;
                            btnReceiveFolder.Visible = false;
                            lblSenderEmail.Visible = false;
                            edReceiveSender.Visible = false;
                            lblReceiveSubject.Visible = false;
                            lblReceiveSubject.Text = "Subject";
                            edReceiveSubject.Visible = false;
                            btnReceiveKeyPath.Visible = false;
                            edReceiveCustomerName.Visible = false;
                            lblReceiveCustomerName.Visible = false;
                            bbTestRx.Visible = true;
                            lblRxResults.Visible = true;
                            txtTestResults.Visible = true;
                            break;

                        case "rbReceiveEmail":
                            lblServerAddress.Visible = true;
                            edServerAddress.Visible = true;
                            cbReceiveSSL.Visible = true;
                            cbReceiveSSL.Text = "SSL";
                            lblFTPReceivePort.Visible = true;
                            edFTPReceivePort.Visible = true;
                            lblFTPReceivePath.Visible = false;
                            edFTPReceivePath.Visible = false;
                            lblReceiveUsername.Visible = true;
                            edReceiveUsername.Visible = true;
                            lblReceivePassword.Visible = true;
                            edReceivePassword.Visible = true;
                            btnReceiveFolder.Visible = false;
                            lblReceiveEmailAddress.Visible = true;
                            lblReceiveEmailAddress.Text = "Recipient Email";
                            edReceiveEmailAddress.Visible = true;
                            lblReceiveCustomerName.Visible = false;
                            edReceiveCustomerName.Visible = false;
                            lblSenderEmail.Visible = true;
                            edReceiveSender.Visible = true;
                            lblReceiveSubject.Visible = true;
                            lblReceiveSubject.Text = "Subject";
                            edReceiveSubject.Visible = true;
                            btnReceiveKeyPath.Visible = false;
                            bbTestRx.Visible = true;
                            lblRxResults.Visible = true;
                            txtTestResults.Visible = true;
                            break;
                        case "rbReceivePickup":
                            lblServerAddress.Visible = false;
                            edServerAddress.Visible = false;
                            lblFTPReceivePort.Visible = false;
                            edFTPReceivePort.Visible = false;
                            lblFTPReceivePath.Visible = true;
                            lblFTPReceivePath.Text = "Path";
                            edFTPReceivePath.Visible = true;
                            btnReceiveFolder.Visible = true;
                            lblReceiveUsername.Visible = false;
                            edReceiveUsername.Visible = false;
                            lblReceivePassword.Visible = false;
                            edReceivePassword.Visible = false;
                            lblReceiveEmailAddress.Visible = false;
                            edReceiveEmailAddress.Visible = false;
                            edReceiveCustomerName.Visible = false;
                            lblReceiveCustomerName.Visible = false;
                            lblReceiveEmailAddress.Visible = false;
                            lblSenderEmail.Visible = false;
                            edReceiveSender.Visible = false;
                            lblReceiveSubject.Visible = false;
                            edReceiveSubject.Visible = false;
                            btnReceiveKeyPath.Visible = false;
                            cbReceiveSSL.Visible = false;
                            break;
                        case "rbReceiveSOAP":
                            lblServerAddress.Visible = true;
                            edServerAddress.Visible = true;
                            cbReceiveSSL.Visible = false;
                            lblFTPReceivePath.Visible = true;
                            lblFTPReceivePath.Text = "API endpoint";
                            edFTPReceivePath.Visible = true;
                            btnReceiveFolder.Visible = false;
                            lblFTPReceivePort.Visible = false;
                            edFTPReceivePort.Visible = false;
                            lblReceiveUsername.Visible = true;
                            edReceiveUsername.Visible = true;
                            lblReceivePassword.Visible = true;
                            edReceivePassword.Visible = true;
                            edReceiveCustomerName.Visible = false;
                            lblReceiveCustomerName.Visible = false;
                            lblRxFileType.Visible = false;
                            cmbRxFileType.Visible = false;
                            lblReceiveEmailAddress.Visible = false;
                            edReceiveEmailAddress.Visible = false;
                            lblSendEmailAddress.Visible = false;
                            edReceiveSender.Visible = false;
                            lblReceiveSubject.Visible = true;
                            lblReceiveSubject.Text = "API Key";
                            edReceiveSubject.Visible = true;
                            btnReceiveKeyPath.Visible = false;
                            bbTestRx.Visible = true;
                            lblRxResults.Visible = true;
                            txtTestResults.Visible = true;
                            break;

                    }
                }
            }
        }

        void SetupSend()
        {
            string strSend = string.Empty;

            foreach (RadioButton rb in gbSendMethod.Controls)
            {

                if (rb.Checked == true)
                {

                    switch (rb.Name)
                    {
                        case "rbPickupSend":
                            lblSendServer.Visible = true;
                            lblSendServer.Text = "Pickup Folder";
                            edSendCTCUrl.Visible = true;
                            lblSendUsername.Visible = false;
                            edSendUsername.Visible = false;
                            lblSendPassword.Visible = false;
                            edSendPassword.Visible = false;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            btnPickupFolder.Visible = true;
                            lblTestResults.Visible = false;
                            edSendeAdapterResults.Visible = false;
                            btnTestSendeAdapter.Visible = false;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            btnSendKeyPath.Visible = false;
                            btnFTPTest.Visible = false;
                            cbSendSSL.Visible = false;
                            cbForwardWithFlags.Visible = false;
                            cmbSendFileType.Visible = false;
                            lblSendFileType.Visible = false;
                            lblSendFileNameExpected.Visible = false;
                            edSendFileName.Visible = false;

                            break;
                        case "rbCTCSend":
                            lblSendServer.Visible = true;
                            lblSendServer.Text = "Server Address";
                            edSendCTCUrl.Visible = true;
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            lblTestResults.Visible = true;
                            btnPickupFolder.Visible = false;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = true;
                            edSendEmailAddress.Visible = true;
                            edSendSubject.Visible = true;
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            btnFTPTest.Visible = false;
                            cbSendSSL.Visible = false;
                            cbForwardWithFlags.Visible = true;
                            lblSendSubject.Visible = true;
                            lblSendSubject.Text = "Subject";
                            edSendSubject.Visible = true;
                            btnSendKeyPath.Visible = false;
                            lblSendEmailAddress.Visible = true;
                            cmbSendFileType.Visible = false;
                            lblSendFileType.Visible = false;
                            lblSendFileNameExpected.Visible = false;
                            edSendFileName.Visible = false;
                            lblSendEmailAddress.Text = "Cargowise File Name";

                            break;
                        case "rbSoapSend":
                            lblSendServer.Visible = true;
                            lblSendServer.Text = "API Url";
                            edSendCTCUrl.Visible = true;
                            btnPickupFolder.Visible = false;
                            cbForwardWithFlags.Visible = false;
                            cbSendSSL.Visible = false;
                            lblSendFTPPort.Visible = false;
                            edSendFtpPort.Visible = false;
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = true;
                            lblSendPath.Text = "API endpoint";
                            edSendFolder.Visible = true;
                            lblSendSubject.Visible = true;
                            lblSendSubject.Text = "API key";
                            edSendSubject.Visible = true;
                            btnSendKeyPath.Visible = false;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            lblSendFileNameExpected.Visible = false;
                            edSendFileName.Visible = false;
                            cmbSendFileType.Visible = false;
                            lblSendFileType.Visible = false;
                            // results box
                            lblTestResults.Visible = true;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            btnFTPTest.Visible = false;

                            break;
                        case "rbFTPSend":
                            lblSendServer.Visible = true;
                            edSendCTCUrl.Visible = true;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            cbSendSSL.Visible = true;
                            lblSendPath.Visible = true;
                            edSendFolder.Visible = true;
                            btnPickupFolder.Visible = false;
                            lblTestResults.Visible = true;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = false;
                            lblSendEmailAddress.Text = "Email Address(es)";
                            edSendEmailAddress.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            lblSendFTPPort.Visible = true;
                            btnFTPTest.Visible = false;
                            cbForwardWithFlags.Visible = false;
                            cmbSendFileType.Visible = false;
                            lblSendFileType.Visible = false;
                            lblSendFileNameExpected.Visible = false;
                            edSendFileName.Visible = false;
                            break;
                        case "rbEmailSend":
                            lblSendServer.Visible = false;
                            edSendCTCUrl.Visible = false;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = false;
                            edSendUsername.Visible = false;
                            lblSendPassword.Visible = false;
                            edSendPassword.Visible = false;
                            lblSendPath.Visible = false;
                            edSendFolder.Visible = false;
                            btnPickupFolder.Visible = false;
                            lblTestResults.Visible = false;
                            edSendeAdapterResults.Visible = false;
                            btnTestSendeAdapter.Visible = false;
                            lblSendEmailAddress.Visible = true;
                            lblSendEmailAddress.Text = "Email Address(es)";
                            edSendEmailAddress.Visible = true;
                            lblSendSubject.Visible = true;
                            edSendSubject.Visible = true;
                            lblSendSubject.Text = "Subject";
                            btnSendKeyPath.Visible = false;
                            edSendFtpPort.Visible = false;
                            lblSendFTPPort.Visible = false;
                            btnFTPTest.Visible = false;
                            cbSendSSL.Visible = false;
                            cbForwardWithFlags.Visible = false;
                            cmbSendFileType.Visible = true;
                            lblSendFileType.Visible = true;
                            lblSendFileNameExpected.Visible = true;
                            edSendFileName.Visible = true;
                            break;
                        case "rbFTPSendDirect":
                            lblSendServer.Visible = true;
                            edSendCTCUrl.Visible = true;
                            lblSendServer.Text = "Server Address";
                            lblSendUsername.Visible = true;
                            edSendUsername.Visible = true;
                            lblSendPassword.Visible = true;
                            edSendPassword.Visible = true;
                            lblSendPath.Visible = true;
                            cbSendSSL.Visible = true;
                            edSendFolder.Visible = true;
                            btnPickupFolder.Visible = false;
                            lblTestResults.Visible = true;
                            edSendeAdapterResults.Visible = true;
                            btnTestSendeAdapter.Visible = true;
                            lblSendEmailAddress.Visible = false;
                            edSendEmailAddress.Visible = false;
                            lblSendSubject.Visible = false;
                            edSendSubject.Visible = false;
                            btnSendKeyPath.Visible = true;
                            edSendFtpPort.Visible = true;
                            lblSendFTPPort.Visible = true;
                            btnFTPTest.Visible = true;
                            cbForwardWithFlags.Visible = false;
                            if (!cbSendSSL.Checked)
                            {
                                edSendSubject.Visible = false;
                                lblSendSubject.Visible = false;
                                lblSendSubject.Text = "Subject";
                            }
                            else
                            {
                                edSendSubject.Visible = true;
                                lblSendSubject.Visible = true;
                                lblSendSubject.Text = "SFTP Key File";
                            }
                            break;
                    }
                }

            }

        }

        private void rbFTPSend_CheckedChanged(object sender, EventArgs e)
        {
            SetupSend();
        }

        private void rbEmailSend_CheckedChanged(object sender, EventArgs e)
        {
            SetupSend();
        }

        private void dgProfile_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.ColumnIndex != -1 && e.RowIndex != -1 && e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                DataGridViewCell c = (sender as DataGridView)[e.ColumnIndex, e.RowIndex];
                if (!c.Selected)
                {
                    c.DataGridView.ClearSelection();
                    c.DataGridView.CurrentCell = c;
                    c.Selected = true;
                }
            }
        }

        private void dgProfile_CellContextMenuStripNeeded(object sender, DataGridViewCellContextMenuStripNeededEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;

            if (e.RowIndex == -1 || e.ColumnIndex == -1)
            {
                return;
            }

            bool isActive = true;

            foreach (DataGridViewRow row in dgv.SelectedRows)
            {
                if ((string)row.Cells["Active"].Value == "Y")
                {
                    isActive = true;
                }

                if ((string)row.Cells["Active"].Value == "N")
                {
                    isActive = false;
                }
            }
            if (isActive)
            {
                markInactiveToolStripMenuItem.Text = "Mark InActive";
            }
            else
            {
                markInactiveToolStripMenuItem.Text = "Mark Active";
            }
        }

        private void markInactiveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int rowIndex = dgProfile.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataGridViewRow row = dgProfile.Rows[rowIndex];
            string strActiveToggle = "";
            if (markInactiveToolStripMenuItem.Text == "Mark InActive")
            {
                strActiveToggle = "N";
            }

            if (markInactiveToolStripMenuItem.Text == "Mark Active")
            {
                strActiveToggle = "Y";
            }

            {
                Guid p_id = new Guid(dgProfile[0, rowIndex].Value.ToString());
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
                {
                    var profile = uow.Profiles.Get(p_id);
                    if (profile != null)
                    {
                        profile.P_ACTIVE = strActiveToggle;
                    }
                    uow.Complete();
                    FillGrid(this.r_id);
                }

            }
        }

        private void duplicateProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {

            int rowIndex = dgProfile.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            DataGridViewRow row = dgProfile.Rows[rowIndex];
            Guid p_id = new Guid(dgProfile[0, rowIndex].Value.ToString());
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
            {
                var originalProfile = uow.Profiles.Get(p_id);
                if (originalProfile != null)
                {
                    var dupProfile = originalProfile.DeepClone();
                    dupProfile.P_ID = Guid.NewGuid();
                    dupProfile.P_DESCRIPTION = dupProfile.P_DESCRIPTION + " - Duplicate";
                    uow.Profiles.Add(dupProfile);
                    uow.Complete();
                    FillGrid(this.r_id);


                }

            }


        }

        private void btnNewProfile_Click(object sender, EventArgs e)
        {
            ClearProfile();
            tcMessageProfile.SelectedIndex = 0;
            btnAdd.Text = "Add  >>";

        }

        private void ClearProfile()
        {
            ClearControls(false);

        }


        private void cbTrial_CheckedChanged(object sender, EventArgs e)
        {
            if (cbTrial.Checked)
            {
                gbTrialPeriod.Enabled = true;
                dtFrom.Text = DateTime.Today.ToString("d");
                dtTo.Text = DateTime.Today.AddDays(30).ToString("d");

            }
            else
            { gbTrialPeriod.Enabled = false; }
        }

        private void btnXSDPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog fs = new OpenFileDialog();
            try
            {
                fs.DefaultExt = "*.XSD";
                fs.FileName = edXSDPath.Text;
            }
            catch (Exception)
            { }
            fs.ShowDialog();
            edXSDPath.Text = fs.FileName;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            bool ssl;
            if (cbReceiveSSL.Checked)
            {
                ssl = true;
            }
            else
            {
                ssl = false;
            }

            txtTestResults.Text = "";
            IMailServerSettings settings = new MailServerSettings
            {
                Server = edServerAddress.Text,
                Port = short.Parse(edFTPReceivePort.Text),
                UserName = edReceiveUsername.Text,
                Password = edReceivePassword.Text,
                IsSecure = ssl
            };
            using (MailModule mail = new MailModule(settings, "", Globals.TestingLoc))
            {
                txtTestResults.Text = mail.CheckMail();
            }

        }

        private void btLoadXSD_Click(object sender, EventArgs e)
        {
            loadXSDTree();
        }
        public void loadXSDTree()
        {
            tvwXSD.Nodes.Clear();
            XmlDocument doc = new XmlDocument();
            string xsdPath = Path.Combine(edRootPath.Text, "lib", edXSDPath.Text);
            try
            {
                doc.Load(xsdPath);

            }
            catch (Exception)
            {

            }
            ConvertXMLNodeToTreeNode(doc, tvwXSD.Nodes);
            tvwXSD.Nodes[0].ExpandAll();
        }

        public void ConvertXMLNodeToTreeNode(XmlNode xmlNode, TreeNodeCollection treeNodes)
        {
            TreeNode newTreeNode = treeNodes.Add(xmlNode.Name);
            switch (xmlNode.NodeType)
            {
                case XmlNodeType.ProcessingInstruction:
                case XmlNodeType.XmlDeclaration:
                    newTreeNode.Text = "<?" + xmlNode.Name + " " +
                      xmlNode.Value + "?>";
                    break;
                case XmlNodeType.Element:
                    newTreeNode.Text = "<" + xmlNode.Name + ">";
                    break;
                case XmlNodeType.Attribute:
                    newTreeNode.Text = "atrribute: " + xmlNode.Name;
                    break;
                case XmlNodeType.Text:
                case XmlNodeType.CDATA:
                    newTreeNode.Text = xmlNode.Value;
                    break;
                case XmlNodeType.Comment:
                    newTreeNode.Text = "<!--" + xmlNode.Value + "-->";
                    break;

            }
            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attrib in xmlNode.Attributes)
                {
                    ConvertXMLNodeToTreeNode(attrib, newTreeNode.Nodes);
                }
            }
            if (xmlNode.HasChildNodes)
            {
                foreach (XmlNode childNode in xmlNode.ChildNodes)
                {
                    ConvertXMLNodeToTreeNode(childNode, newTreeNode.Nodes);
                }
            }


        }

        private void btnAddParam_Click(object sender, EventArgs e)
        {

            var profParam = new frmProfileParam(string.Empty);
            profParam.ShowDialog();
            var edited = profParam.Param;
            if (edited.EndsWith(","))
            {
                edited = edited.Remove(edited.Length - 1);
            }

            lbParameters.Items.Add(edited);

        }

        private void btnReceiveFolder_Click(object sender, EventArgs e)
        {

            FolderBrowserDialog fd = new FolderBrowserDialog
            {
                SelectedPath = edFTPReceivePath.Text
            };
            fd.ShowDialog();
            edFTPReceivePath.Text = fd.SelectedPath;

        }

        private void btnPickupFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdPickup = new FolderBrowserDialog
            {
                SelectedPath = edSendCTCUrl.Text
            };
            fdPickup.ShowDialog();
            edSendCTCUrl.Text = fdPickup.SelectedPath;
        }

        private void btnFTPTest_Click(object sender, EventArgs e)
        {
            SessionOptions sessionOptions = new SessionOptions
            {
                HostName = edSendCTCUrl.Text,
                PortNumber = int.Parse(edSendFtpPort.Text),
                UserName = edSendUsername.Text,
                Password = edSendPassword.Text
            };
            if (cbSendSSL.Checked)
            {
                sessionOptions.Protocol = Protocol.Sftp;
                sessionOptions.SshHostKeyFingerprint = edSendEmailAddress.Text;
                sessionOptions.SshPrivateKeyPath = Path.Combine(edRootPath.Text, "Lib", edSendSubject.Text);
            }
            else
            {
                sessionOptions.Protocol = Protocol.Ftp;
            }
            //try
            //{

            using (Session session = new Session())
            {
                session.Open(sessionOptions);
                if (session.Opened)
                {

                    edSendeAdapterResults.Text += "FTP Server Connected and Logged in. ";

                }
                else
                {
                    edSendeAdapterResults.Text += "FTP Server Error. ";
                }
            }

        }

        private void CbHideInactive_CheckedChanged(object sender, EventArgs e)
        {
            FillGrid(r_id);
        }

        private void RbCustomer_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void lbParameters_DoubleClick(object sender, EventArgs e)
        {
            var param = ((ListBox)sender).SelectedItem.ToString();
            var selected = ((ListBox)sender).SelectedIndex;
            var profParam = new frmProfileParam(param);
            profParam.ShowDialog();
            var edited = profParam.Param;
            if (edited.EndsWith(","))
            {
                edited = edited.Remove(edited.Length - 1);
            }
            lbParameters.Items.RemoveAt(selected);
            lbParameters.Items.Add(edited);

        }

        private void cmParameterList_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (lbParameters.SelectedIndex == -1)
            {
                cmParameterList.Enabled = false;
            }
            else
            {
                cmParameterList.Enabled = true;
            }
        }

        private void removeParameterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lbParameters.Items.RemoveAt(lbParameters.SelectedIndex);
        }

        private void cbSecureFtp_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbSendSSL.Checked)
            {
                edSendSubject.Visible = false;
                lblSendSubject.Visible = false;
                lblSendSubject.Text = "Subject";
                lblSendEmailAddress.Visible = false;
                lblSendEmailAddress.Text = "Email Address(es)";
                edSendEmailAddress.Visible = false;
                btnSendKeyPath.Visible = false;
            }
            else
            {
                edSendSubject.Visible = true;
                lblSendSubject.Visible = true;
                lblSendSubject.Text = "SFTP Key File";
                lblSendEmailAddress.Visible = true;
                lblSendEmailAddress.Text = "Server Fingerprint";
                edSendEmailAddress.Visible = true;
                btnSendKeyPath.Visible = true;

            }
        }

        private void cbReceiveSSL_CheckedChanged(object sender, EventArgs e)
        {
            if (!cbReceiveSSL.Checked)
            {
                if (cbReceiveSSL.Text == "Secure FTP")
                {
                    edReceiveSubject.Visible = false;
                    lblReceiveSubject.Visible = false;
                    lblReceiveSubject.Text = "Subject";
                    lblReceiveEmailAddress.Visible = false;
                    lblReceiveEmailAddress.Text = "Email Address(es)";
                    edReceiveEmailAddress.Visible = false;
                    btnReceiveKeyPath.Visible = false;
                }

            }
            else
            {
                if (cbReceiveSSL.Text == "Secure FTP")
                {
                    edReceiveSubject.Visible = true;
                    lblReceiveSubject.Visible = true;
                    lblReceiveSubject.Text = "SFTP Key File";
                    lblReceiveEmailAddress.Visible = true;
                    lblReceiveEmailAddress.Text = "Server Fingerprint";
                    edReceiveEmailAddress.Visible = true;
                    btnReceiveKeyPath.Visible = true;
                }

            }
        }

        private void btnSendKeyPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog
            {
                FileName = Path.Combine(edRootPath.Text, "Lib", edSendSubject.Text)
            };
            fd.ShowDialog();
            edSendSubject.Text = Path.GetFileName(fd.FileName);
        }

        private void btnReceiveKeyPath_Click(object sender, EventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog
            {
                FileName = Path.Combine(edRootPath.Text, "Lib", edReceiveSubject.Text)
            };
            fd.ShowDialog();
            edReceiveSubject.Text = Path.GetFileName(fd.FileName);
        }

        private void CbSatellite_CheckedChanged(object sender, EventArgs e)
        {
            if (cbSatellite.Checked)
            {
                tbSatellite.Show();
            }
            else
            {
                tbSatellite.Hide();
            }
        }

        private void ddlModuleListing_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlModuleListing.Text == "(none selected)")
            {
                return;
            }
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.connString())))
            {
                var desc = (uow.Modules.Find(x => x.MO_ID == (Guid)ddlModuleListing.SelectedValue)).FirstOrDefault();
                edModuleDescription.Text = desc.MO_ModuleDescription;

            }
        }

        private void btnAddModule_Click(object sender, EventArgs e)
        {
            if (ddlModuleListing.Text == "(none selected)")
            {
                return;
            }
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.connString())))
            {
                var custID = (Guid)cmbCustomer.SelectedValue;
                var custModule = uow.CustomerModules.Find(x => x.MC_C == custID && x.MC_ID == (Guid)ddlModuleListing.SelectedValue).FirstOrDefault();
                if (custModule == null)
                {
                    CustomerModuleLookup cm = new CustomerModuleLookup
                    {
                        MC_C = (Guid)cmbCustomer.SelectedValue,
                        MC_MO = (Guid)ddlModuleListing.SelectedValue

                    };
                    uow.CustomerModules.Add(cm);
                    uow.Complete();
                    GetCustomerModules(custID);
                }
            }
        }

        private void GetCustomerModules(Guid customerID)
        {
            grdCustomerModules.AutoGenerateColumns = false;
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(Globals.connString())))
            {
                var modules = uow.Modules.GetAll();
                BindingSource bsCustomerModuleList = new BindingSource();
                var moduleList = (from m in modules
                                  join cm in uow.CustomerModules.GetAll() on m.MO_ID equals cm.MC_MO
                                  where cm.MC_C == customerID
                                  select new DataItem
                                  {
                                      ValueItem = m.MO_ID,
                                      StringItem = m.MO_ModuleName
                                  }
                                    ).ToList();
                bsCustomerModuleList.DataSource = new SortableBindingList<DataItem>(moduleList);
                grdCustomerModules.DataSource = bsCustomerModuleList;

            }
        }
        private string GetXsdPath()
        {
            if (!string.IsNullOrEmpty(edXSDPath.Text.Trim()))
            {
                bool filecopied = false;
                string fileName = Path.GetFileNameWithoutExtension(edXSDPath.Text);
                int i = 1;
                while (!filecopied)
                {
                    try
                    {

                        {
                            if (!File.Exists(Path.Combine(lblActualPath.Text, "Lib", fileName + Path.GetExtension(edXSDPath.Text))))
                            {
                                //if (!NodeResources.FileEquals(Path.Combine(edRootPath.Text, "Lib", fileName + Path.GetExtension(edXSDPath.Text)), edXSDPath.Text))
                                //{
                                File.Copy(edXSDPath.Text, Path.Combine(lblActualPath.Text, "Lib", fileName + Path.GetExtension(edXSDPath.Text)));
                                filecopied = true;
                                return fileName + Path.GetExtension(edXSDPath.Text);
                                //}

                            }
                            else
                            {
                                filecopied = true;
                                return edXSDPath.Text;
                            }
                        }

                    }
                    catch (IOException)
                    {
                        i++;
                        fileName = fileName + i;

                    }
                }
            }
            return string.Empty;
        }

        private void edRootPath_TextChanged(object sender, EventArgs e)
        {
            lblActualPath.Text = Path.Combine(edRootPath.Text, edCustomerCode.Text);
        }

        private void btnRootPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fdLocation = new FolderBrowserDialog();
            try
            {
                fdLocation.SelectedPath = edRootPath.Text;
            }
            catch (Exception)
            { }

            fdLocation.ShowDialog();
            edRootPath.Text = fdLocation.SelectedPath;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmbFromMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillConversionProcessList(cmbFromMethod.Text, "F", cmbFromMethodName);
        }

        private void FillConversionProcessList(string methodtype, string direction, ComboBox combo)
        {
            try
            {
                var processList = new[] { new { Name = "(none selected}", ID = Guid.Empty } }.ToList();
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
                {
                    if (methodtype == "Conversion")
                    {
                        var qList = uow.ConversionProcesses.Find(x => x.CP_Direction == direction).OrderBy(x => x.CP_MethodName).AsQueryable();
                        processList = (from x in qList
                                       select new { Name = x.CP_MethodName, ID = x.CP_ID }).ToList();

                    }
                    else
                    {
                        var qList = uow.MapHeaders.GetAll().OrderBy(x => x.MH_Name).AsQueryable();
                        processList = (from x in qList
                                       select new { Name = x.MH_Name, ID = x.MH_ID }).ToList();
                    }

                    if (processList.Count > 0)
                    {
                        processList.Insert(0, new { Name = "(none selected)", ID = Guid.Empty });
                        combo.DataSource = processList;
                        combo.DisplayMember = "Name";
                        combo.ValueMember = "ID";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Found Accessing DB. Error was: " + ex.Message);

            }
        }

        private void cmbToMethod_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillConversionProcessList(cmbToMethod.Text, "T", cmbToMethodName);
        }

        private void label33_Click(object sender, EventArgs e)
        {

        }

        private void label35_Click(object sender, EventArgs e)
        {

        }
    }


}

