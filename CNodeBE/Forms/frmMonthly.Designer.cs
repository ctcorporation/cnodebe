﻿namespace CNodeBE
{
    partial class frmMonthly
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bbClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.cbAll = new System.Windows.Forms.CheckBox();
            this.grpInvoiceDetail = new System.Windows.Forms.GroupBox();
            this.rbInvoiced = new System.Windows.Forms.RadioButton();
            this.rbNot = new System.Windows.Forms.RadioButton();
            this.rbAll = new System.Windows.Forms.RadioButton();
            this.bbGenerate = new System.Windows.Forms.Button();
            this.cmbCustomer = new System.Windows.Forms.ComboBox();
            this.bbFind = new System.Windows.Forms.Button();
            this.tcMonthly = new System.Windows.Forms.TabControl();
            this.tbMonthlyDetailed = new System.Windows.Forms.TabPage();
            this.dgMonthlyReport = new System.Windows.Forms.DataGridView();
            this.tbSummary = new System.Windows.Forms.TabPage();
            this.dgMonthlySummary = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.Customer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.C_CODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.P_DESCRIPTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T_DATETIME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.P_Recipientid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T_FILENAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Charged = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.T_MSGTYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.T_BILLTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpInvoiceDetail.SuspendLayout();
            this.tcMonthly.SuspendLayout();
            this.tbMonthlyDetailed.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMonthlyReport)).BeginInit();
            this.tbSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgMonthlySummary)).BeginInit();
            this.SuspendLayout();
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Location = new System.Drawing.Point(970, 551);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(75, 23);
            this.bbClose.TabIndex = 0;
            this.bbClose.Text = "&Close";
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Select Customer";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Select Date from ";
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFrom.Location = new System.Drawing.Point(104, 55);
            this.dtFrom.MinDate = new System.DateTime(2017, 1, 1, 0, 0, 0, 0);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(95, 20);
            this.dtFrom.TabIndex = 4;
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTo.Location = new System.Drawing.Point(266, 55);
            this.dtTo.MinDate = new System.DateTime(2017, 1, 1, 0, 0, 0, 0);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(95, 20);
            this.dtTo.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(214, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Date To";
            // 
            // cbAll
            // 
            this.cbAll.AutoSize = true;
            this.cbAll.Location = new System.Drawing.Point(405, 21);
            this.cbAll.Name = "cbAll";
            this.cbAll.Size = new System.Drawing.Size(122, 17);
            this.cbAll.TabIndex = 7;
            this.cbAll.Text = "All Active Customers";
            this.cbAll.UseVisualStyleBackColor = true;
            // 
            // grpInvoiceDetail
            // 
            this.grpInvoiceDetail.Controls.Add(this.rbInvoiced);
            this.grpInvoiceDetail.Controls.Add(this.rbNot);
            this.grpInvoiceDetail.Controls.Add(this.rbAll);
            this.grpInvoiceDetail.Location = new System.Drawing.Point(373, 45);
            this.grpInvoiceDetail.Name = "grpInvoiceDetail";
            this.grpInvoiceDetail.Size = new System.Drawing.Size(280, 44);
            this.grpInvoiceDetail.TabIndex = 8;
            this.grpInvoiceDetail.TabStop = false;
            this.grpInvoiceDetail.Text = "Report Selection Option";
            // 
            // rbInvoiced
            // 
            this.rbInvoiced.AutoSize = true;
            this.rbInvoiced.Location = new System.Drawing.Point(172, 19);
            this.rbInvoiced.Name = "rbInvoiced";
            this.rbInvoiced.Size = new System.Drawing.Size(90, 17);
            this.rbInvoiced.TabIndex = 2;
            this.rbInvoiced.Text = "Invoiced Only";
            this.rbInvoiced.UseVisualStyleBackColor = true;
            // 
            // rbNot
            // 
            this.rbNot.AutoSize = true;
            this.rbNot.Checked = true;
            this.rbNot.Location = new System.Drawing.Point(65, 19);
            this.rbNot.Name = "rbNot";
            this.rbNot.Size = new System.Drawing.Size(86, 17);
            this.rbNot.TabIndex = 1;
            this.rbNot.TabStop = true;
            this.rbNot.Text = "Not Invoiced";
            this.rbNot.UseVisualStyleBackColor = true;
            // 
            // rbAll
            // 
            this.rbAll.AutoSize = true;
            this.rbAll.Location = new System.Drawing.Point(6, 19);
            this.rbAll.Name = "rbAll";
            this.rbAll.Size = new System.Drawing.Size(36, 17);
            this.rbAll.TabIndex = 0;
            this.rbAll.Text = "All";
            this.rbAll.UseVisualStyleBackColor = true;
            // 
            // bbGenerate
            // 
            this.bbGenerate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbGenerate.Location = new System.Drawing.Point(824, 551);
            this.bbGenerate.Name = "bbGenerate";
            this.bbGenerate.Size = new System.Drawing.Size(140, 23);
            this.bbGenerate.TabIndex = 11;
            this.bbGenerate.Text = "Master Checklist Report";
            this.bbGenerate.UseVisualStyleBackColor = true;
            this.bbGenerate.Click += new System.EventHandler(this.bbGenerate_Click);
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.DisplayMember = "C_NAME";
            this.cmbCustomer.FormattingEnabled = true;
            this.cmbCustomer.Location = new System.Drawing.Point(104, 18);
            this.cmbCustomer.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Size = new System.Drawing.Size(296, 21);
            this.cmbCustomer.TabIndex = 34;
            this.cmbCustomer.ValueMember = "C_CODE";
            // 
            // bbFind
            // 
            this.bbFind.Location = new System.Drawing.Point(778, 58);
            this.bbFind.Name = "bbFind";
            this.bbFind.Size = new System.Drawing.Size(75, 23);
            this.bbFind.TabIndex = 35;
            this.bbFind.Text = "&Find";
            this.bbFind.UseVisualStyleBackColor = true;
            this.bbFind.Click += new System.EventHandler(this.bbFind_Click);
            // 
            // tcMonthly
            // 
            this.tcMonthly.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcMonthly.Controls.Add(this.tbMonthlyDetailed);
            this.tcMonthly.Controls.Add(this.tbSummary);
            this.tcMonthly.Location = new System.Drawing.Point(23, 87);
            this.tcMonthly.Name = "tcMonthly";
            this.tcMonthly.SelectedIndex = 0;
            this.tcMonthly.Size = new System.Drawing.Size(1022, 457);
            this.tcMonthly.TabIndex = 36;
            // 
            // tbMonthlyDetailed
            // 
            this.tbMonthlyDetailed.Controls.Add(this.dgMonthlyReport);
            this.tbMonthlyDetailed.Location = new System.Drawing.Point(4, 22);
            this.tbMonthlyDetailed.Name = "tbMonthlyDetailed";
            this.tbMonthlyDetailed.Padding = new System.Windows.Forms.Padding(3);
            this.tbMonthlyDetailed.Size = new System.Drawing.Size(1014, 431);
            this.tbMonthlyDetailed.TabIndex = 1;
            this.tbMonthlyDetailed.Text = "Detailed Information";
            this.tbMonthlyDetailed.UseVisualStyleBackColor = true;
            // 
            // dgMonthlyReport
            // 
            this.dgMonthlyReport.AllowUserToAddRows = false;
            this.dgMonthlyReport.AllowUserToDeleteRows = false;
            this.dgMonthlyReport.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgMonthlyReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMonthlyReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMonthlyReport.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Customer,
            this.C_CODE,
            this.P_DESCRIPTION,
            this.T_DATETIME,
            this.P_Recipientid,
            this.T_FILENAME,
            this.Charged,
            this.T_MSGTYPE,
            this.T_BILLTO});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.Format = "d";
            dataGridViewCellStyle3.NullValue = null;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgMonthlyReport.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgMonthlyReport.Location = new System.Drawing.Point(6, 6);
            this.dgMonthlyReport.MultiSelect = false;
            this.dgMonthlyReport.Name = "dgMonthlyReport";
            this.dgMonthlyReport.ReadOnly = true;
            this.dgMonthlyReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgMonthlyReport.Size = new System.Drawing.Size(1002, 419);
            this.dgMonthlyReport.TabIndex = 13;
            // 
            // tbSummary
            // 
            this.tbSummary.Controls.Add(this.dgMonthlySummary);
            this.tbSummary.Location = new System.Drawing.Point(4, 22);
            this.tbSummary.Name = "tbSummary";
            this.tbSummary.Padding = new System.Windows.Forms.Padding(3);
            this.tbSummary.Size = new System.Drawing.Size(1014, 431);
            this.tbSummary.TabIndex = 0;
            this.tbSummary.Text = "Summary Information";
            this.tbSummary.UseVisualStyleBackColor = true;
            // 
            // dgMonthlySummary
            // 
            this.dgMonthlySummary.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgMonthlySummary.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgMonthlySummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgMonthlySummary.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5});
            this.dgMonthlySummary.Location = new System.Drawing.Point(16, 15);
            this.dgMonthlySummary.Name = "dgMonthlySummary";
            this.dgMonthlySummary.Size = new System.Drawing.Size(975, 410);
            this.dgMonthlySummary.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(643, 551);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(175, 23);
            this.button1.TabIndex = 37;
            this.button1.Text = "Customer Invoice Report";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(533, 20);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(162, 17);
            this.checkBox1.TabIndex = 38;
            this.checkBox1.Text = "Include Test/Trial Customers";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // Customer
            // 
            this.Customer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Customer.DataPropertyName = "C_NAME";
            this.Customer.FillWeight = 73.67657F;
            this.Customer.HeaderText = "Customer";
            this.Customer.Name = "Customer";
            this.Customer.ReadOnly = true;
            // 
            // C_CODE
            // 
            this.C_CODE.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.C_CODE.DataPropertyName = "C_CODE";
            this.C_CODE.FillWeight = 73.67657F;
            this.C_CODE.HeaderText = "Code";
            this.C_CODE.Name = "C_CODE";
            this.C_CODE.ReadOnly = true;
            this.C_CODE.Width = 89;
            // 
            // P_DESCRIPTION
            // 
            this.P_DESCRIPTION.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.P_DESCRIPTION.DataPropertyName = "P_DESCRIPTION";
            this.P_DESCRIPTION.FillWeight = 73.67657F;
            this.P_DESCRIPTION.HeaderText = "Profile Used";
            this.P_DESCRIPTION.Name = "P_DESCRIPTION";
            this.P_DESCRIPTION.ReadOnly = true;
            // 
            // T_DATETIME
            // 
            this.T_DATETIME.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.T_DATETIME.DataPropertyName = "T_DATETIME";
            this.T_DATETIME.FillWeight = 80F;
            this.T_DATETIME.HeaderText = "Date";
            this.T_DATETIME.Name = "T_DATETIME";
            this.T_DATETIME.ReadOnly = true;
            this.T_DATETIME.Width = 80;
            // 
            // P_Recipientid
            // 
            this.P_Recipientid.DataPropertyName = "P_Recipientid";
            this.P_Recipientid.FillWeight = 73.67657F;
            this.P_Recipientid.HeaderText = "Recipient";
            this.P_Recipientid.Name = "P_Recipientid";
            this.P_Recipientid.ReadOnly = true;
            // 
            // T_FILENAME
            // 
            this.T_FILENAME.DataPropertyName = "T_FILENAME";
            this.T_FILENAME.FillWeight = 73.67657F;
            this.T_FILENAME.HeaderText = "File Name";
            this.T_FILENAME.Name = "T_FILENAME";
            this.T_FILENAME.ReadOnly = true;
            // 
            // Charged
            // 
            this.Charged.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Charged.DataPropertyName = "CHARGEABLE";
            this.Charged.FalseValue = "0";
            this.Charged.HeaderText = "Chargeable";
            this.Charged.Name = "Charged";
            this.Charged.ReadOnly = true;
            this.Charged.TrueValue = "1";
            this.Charged.Width = 70;
            // 
            // T_MSGTYPE
            // 
            this.T_MSGTYPE.DataPropertyName = "T_MSGTYPE";
            this.T_MSGTYPE.FillWeight = 73.67657F;
            this.T_MSGTYPE.HeaderText = "Message Type";
            this.T_MSGTYPE.Name = "T_MSGTYPE";
            this.T_MSGTYPE.ReadOnly = true;
            // 
            // T_BILLTO
            // 
            this.T_BILLTO.DataPropertyName = "T_BILLTO";
            this.T_BILLTO.HeaderText = "Bill To";
            this.T_BILLTO.Name = "T_BILLTO";
            this.T_BILLTO.ReadOnly = true;
            this.T_BILLTO.Visible = false;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "C_NAME";
            this.Column1.HeaderText = "Customer";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "P_DESCRIPTION";
            this.Column2.HeaderText = "Receive/ Send Profile Description";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "T_MSGTYPE";
            this.Column3.HeaderText = "Message Type";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column4.DataPropertyName = "T_DIRECTION";
            this.Column4.HeaderText = "Direction";
            this.Column4.Name = "Column4";
            this.Column4.Width = 80;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.Column5.DataPropertyName = "CNT";
            this.Column5.HeaderText = "Totals";
            this.Column5.Name = "Column5";
            this.Column5.Width = 60;
            // 
            // frmMonthly
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1057, 586);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tcMonthly);
            this.Controls.Add(this.bbFind);
            this.Controls.Add(this.cmbCustomer);
            this.Controls.Add(this.bbGenerate);
            this.Controls.Add(this.grpInvoiceDetail);
            this.Controls.Add(this.cbAll);
            this.Controls.Add(this.dtTo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dtFrom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bbClose);
            this.Name = "frmMonthly";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Monthly Reports";
            this.Load += new System.EventHandler(this.frmMonthly_Load);
            this.grpInvoiceDetail.ResumeLayout(false);
            this.grpInvoiceDetail.PerformLayout();
            this.tcMonthly.ResumeLayout(false);
            this.tbMonthlyDetailed.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgMonthlyReport)).EndInit();
            this.tbSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgMonthlySummary)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox cbAll;
        private System.Windows.Forms.GroupBox grpInvoiceDetail;
        private System.Windows.Forms.RadioButton rbInvoiced;
        private System.Windows.Forms.RadioButton rbNot;
        private System.Windows.Forms.RadioButton rbAll;
        private System.Windows.Forms.Button bbGenerate;
        private System.Windows.Forms.ComboBox cmbCustomer;
        private System.Windows.Forms.Button bbFind;
        private System.Windows.Forms.TabControl tcMonthly;
        private System.Windows.Forms.TabPage tbSummary;
        private System.Windows.Forms.TabPage tbMonthlyDetailed;
        private System.Windows.Forms.DataGridView dgMonthlyReport;
        private System.Windows.Forms.DataGridView dgMonthlySummary;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Customer;
        private System.Windows.Forms.DataGridViewTextBoxColumn C_CODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_DESCRIPTION;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_DATETIME;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_Recipientid;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_FILENAME;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Charged;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_MSGTYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn T_BILLTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
    }
}