﻿namespace CNodeBE
{
    partial class CWTestModule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bbClose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.edSendCTCUrl = new System.Windows.Forms.TextBox();
            this.btnTest = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.edSendFile = new System.Windows.Forms.TextBox();
            this.bbDir = new System.Windows.Forms.Button();
            this.fdSendFile = new System.Windows.Forms.OpenFileDialog();
            this.bbSend = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.edSendPassword = new System.Windows.Forms.TextBox();
            this.lblSendPassword = new System.Windows.Forms.Label();
            this.edSendUsername = new System.Windows.Forms.TextBox();
            this.lblSendUsername = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.edRecipientID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbCustomer = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbProfieList = new System.Windows.Forms.ComboBox();
            this.edSendeAdapterResults = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // bbClose
            // 
            this.bbClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.bbClose.Location = new System.Drawing.Point(590, 354);
            this.bbClose.Name = "bbClose";
            this.bbClose.Size = new System.Drawing.Size(100, 23);
            this.bbClose.TabIndex = 0;
            this.bbClose.Text = "C&lose";
            this.bbClose.UseVisualStyleBackColor = true;
            this.bbClose.Click += new System.EventHandler(this.bbClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cargowise eAdapter URL";
            // 
            // edSendCTCUrl
            // 
            this.edSendCTCUrl.Location = new System.Drawing.Point(147, 68);
            this.edSendCTCUrl.Name = "edSendCTCUrl";
            this.edSendCTCUrl.Size = new System.Drawing.Size(403, 20);
            this.edSendCTCUrl.TabIndex = 2;
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(590, 68);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(100, 23);
            this.btnTest.TabIndex = 3;
            this.btnTest.Text = "Test Connection";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Send File";
            // 
            // edSendFile
            // 
            this.edSendFile.Location = new System.Drawing.Point(147, 140);
            this.edSendFile.Name = "edSendFile";
            this.edSendFile.Size = new System.Drawing.Size(378, 20);
            this.edSendFile.TabIndex = 7;
            // 
            // bbDir
            // 
            this.bbDir.Location = new System.Drawing.Point(523, 138);
            this.bbDir.Name = "bbDir";
            this.bbDir.Size = new System.Drawing.Size(28, 23);
            this.bbDir.TabIndex = 8;
            this.bbDir.Text = "...";
            this.bbDir.UseVisualStyleBackColor = true;
            this.bbDir.Click += new System.EventHandler(this.bbDir_Click);
            // 
            // fdSendFile
            // 
            this.fdSendFile.DefaultExt = "xml";
            this.fdSendFile.FileName = "openFileDialog1";
            this.fdSendFile.Filter = "XML Files|*.xml|All Files| *.*";
            // 
            // bbSend
            // 
            this.bbSend.Location = new System.Drawing.Point(590, 138);
            this.bbSend.Name = "bbSend";
            this.bbSend.Size = new System.Drawing.Size(100, 23);
            this.bbSend.TabIndex = 9;
            this.bbSend.Text = "Send File";
            this.bbSend.UseVisualStyleBackColor = true;
            this.bbSend.Click += new System.EventHandler(this.bbSend_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 15);
            this.label2.TabIndex = 10;
            this.label2.Text = "Results";
            // 
            // edSendPassword
            // 
            this.edSendPassword.Location = new System.Drawing.Point(147, 116);
            this.edSendPassword.Margin = new System.Windows.Forms.Padding(2);
            this.edSendPassword.Name = "edSendPassword";
            this.edSendPassword.Size = new System.Drawing.Size(179, 20);
            this.edSendPassword.TabIndex = 26;
            // 
            // lblSendPassword
            // 
            this.lblSendPassword.AutoSize = true;
            this.lblSendPassword.Location = new System.Drawing.Point(13, 118);
            this.lblSendPassword.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendPassword.Name = "lblSendPassword";
            this.lblSendPassword.Size = new System.Drawing.Size(61, 15);
            this.lblSendPassword.TabIndex = 27;
            this.lblSendPassword.Text = "Password";
            // 
            // edSendUsername
            // 
            this.edSendUsername.Location = new System.Drawing.Point(147, 92);
            this.edSendUsername.Margin = new System.Windows.Forms.Padding(2);
            this.edSendUsername.Name = "edSendUsername";
            this.edSendUsername.Size = new System.Drawing.Size(179, 20);
            this.edSendUsername.TabIndex = 24;
            // 
            // lblSendUsername
            // 
            this.lblSendUsername.AutoSize = true;
            this.lblSendUsername.Location = new System.Drawing.Point(13, 93);
            this.lblSendUsername.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendUsername.Name = "lblSendUsername";
            this.lblSendUsername.Size = new System.Drawing.Size(65, 15);
            this.lblSendUsername.TabIndex = 25;
            this.lblSendUsername.Text = "Username";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(343, 97);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 15);
            this.label5.TabIndex = 30;
            this.label5.Text = "Recipient ID";
            // 
            // edRecipientID
            // 
            this.edRecipientID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edRecipientID.Location = new System.Drawing.Point(410, 94);
            this.edRecipientID.Name = "edRecipientID";
            this.edRecipientID.Size = new System.Drawing.Size(100, 20);
            this.edRecipientID.TabIndex = 31;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 18);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 15);
            this.label4.TabIndex = 33;
            this.label4.Text = "Select Customer";
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.FormattingEnabled = true;
            this.cmbCustomer.Location = new System.Drawing.Point(147, 18);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Size = new System.Drawing.Size(404, 21);
            this.cmbCustomer.TabIndex = 34;
            this.cmbCustomer.SelectedIndexChanged += new System.EventHandler(this.cmbCustomer_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(150, 15);
            this.label6.TabIndex = 35;
            this.label6.Text = "Select Send Profile to Test";
            // 
            // cmbProfieList
            // 
            this.cmbProfieList.FormattingEnabled = true;
            this.cmbProfieList.Location = new System.Drawing.Point(147, 43);
            this.cmbProfieList.Name = "cmbProfieList";
            this.cmbProfieList.Size = new System.Drawing.Size(403, 21);
            this.cmbProfieList.TabIndex = 36;
            this.cmbProfieList.SelectedIndexChanged += new System.EventHandler(this.cmbProfieList_SelectedIndexChanged);
            // 
            // edSendeAdapterResults
            // 
            this.edSendeAdapterResults.Location = new System.Drawing.Point(16, 184);
            this.edSendeAdapterResults.Name = "edSendeAdapterResults";
            this.edSendeAdapterResults.Size = new System.Drawing.Size(673, 164);
            this.edSendeAdapterResults.TabIndex = 37;
            this.edSendeAdapterResults.Text = "";
            // 
            // CWTestModule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 389);
            this.Controls.Add(this.edSendeAdapterResults);
            this.Controls.Add(this.cmbProfieList);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbCustomer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.edRecipientID);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.edSendPassword);
            this.Controls.Add(this.lblSendPassword);
            this.Controls.Add(this.edSendUsername);
            this.Controls.Add(this.lblSendUsername);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.bbSend);
            this.Controls.Add(this.bbDir);
            this.Controls.Add(this.edSendFile);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.edSendCTCUrl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bbClose);
            this.Name = "CWTestModule";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cargowise eAdaptor Test Module";
            this.Load += new System.EventHandler(this.CWTestModule_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bbClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox edSendCTCUrl;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edSendFile;
        private System.Windows.Forms.Button bbDir;
        private System.Windows.Forms.OpenFileDialog fdSendFile;
        private System.Windows.Forms.Button bbSend;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edSendPassword;
        private System.Windows.Forms.Label lblSendPassword;
        private System.Windows.Forms.TextBox edSendUsername;
        private System.Windows.Forms.Label lblSendUsername;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox edRecipientID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbCustomer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbProfieList;
        private System.Windows.Forms.RichTextBox edSendeAdapterResults;
    }
}