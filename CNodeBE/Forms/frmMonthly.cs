﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;


namespace CNodeBE
{
    public partial class frmMonthly : Form
    {
        public Guid r_id;
        public Guid gP_id;
        public SqlConnection sqlConn;
        public SqlDataAdapter daMonthly;


        public frmMonthly()
        {
            InitializeComponent();
        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void frmMonthly_Load(object sender, EventArgs e)
        {
            sqlConn = new SqlConnection(Globals.DbConn);
            try
            {
                sqlConn.Open();
                sqlConn.Close();
            }
            catch (SqlException ex)
            {
                sqlConn.ConnectionString = Globals.connString();

            }
            DateTime date = DateTime.Now;
            var firstDayofMonth = new DateTime(date.Year, date.Month, 1);
            var lastDayofMonth = firstDayofMonth.AddMonths(1).AddDays(-1);
            dtFrom.Text = firstDayofMonth.ToString("dd/MM/yy");
            dtTo.Text = lastDayofMonth.ToString("dd/MM/yy");

            FillCustomer();

        }

        private void FillDetailGrid()
        {
            foreach (RadioButton rbg in grpInvoiceDetail.Controls)
            {
                switch (rbg.Name)
                {
                    case "rbAll":
                        {
                            if (rbg.Checked)
                            {

                            }
                            break;
                        }
                    case "rbInvoiced":
                        {

                            if (rbg.Checked)
                            {

                            }
                            break;
                        }
                    case "rbNot":
                        {
                            if (rbg.Checked)
                            {

                            }
                            break;
                        }
                }
            }


            SqlDataAdapter daMonthly = new SqlDataAdapter("SELECT C_CODE, C_NAME, CAST(CASE C_ON_HOLD WHEN 'Y' THEN 1 ELSE 0 END AS BIT) as ON_HOLD, CAST(CASE C_IS_ACTIVE when 'Y' then 1 else 0 end as BIT) as IS_ACTIVE," +
                " T_DATETIME, T_FILENAME, Cast(CASE T_TRIAL WHEN 'Y' then 1 else 0 end as BIT) as TRIAL, T_INVOICED, T_INVOICEDATE, P_DESCRIPTION, Cast(CASE T_CHARGEABLE WHEN 'Y' then 1 else 0 END as BIT) as CHARGEABLE, T_MSGTYPE, T_BILLTO," +
                " P_Recipientid FROM vw_Transaction WHERE (@C_CODE IS NULL or C_CODE = @C_CODE) AND (Convert(Date,T_DATETIME) >= Convert(Date, @DateFrom)) AND (Convert(Date, T_DATETIME) <=Convert(Date, @DateTo)) and (C_IS_ACTIVE = 'Y') Order by T_DATETIME", sqlConn);
            daMonthly.SelectCommand.Parameters.Add("@C_CODE", SqlDbType.Char, 15);
            daMonthly.SelectCommand.Parameters.AddWithValue("@DATEFROM", DateTime.Parse(dtFrom.Text));
            daMonthly.SelectCommand.Parameters.AddWithValue("@DATETO", DateTime.Parse(dtTo.Text));


            if (cmbCustomer.Text != "(none selected)")
            {
                daMonthly.SelectCommand.Parameters[0].Value = cmbCustomer.SelectedValue;
            }
            if (cbAll.Checked)
            {
                daMonthly.SelectCommand.Parameters[0].Value = DBNull.Value;

            }
            DataSet dsInvoices = new DataSet();
            daMonthly.Fill(dsInvoices, "INVOICELIST");
            dgMonthlyReport.AutoGenerateColumns = false;
            dgMonthlyReport.DataSource = dsInvoices;
            dgMonthlyReport.DataMember = "INVOICELIST";


        }

        private void FillCustomer()
        {
            SqlDataAdapter daCustomer = new SqlDataAdapter("Select C_ID, C_CODE, C_NAME from Customer Order by C_NAME", sqlConn);
            DataSet dsCustomer = new DataSet();
            daCustomer.Fill(dsCustomer, "CUSTOMER");
            DataRow dr = dsCustomer.Tables["CUSTOMER"].NewRow();
            dr["C_NAME"] = "(none selected)";
            dsCustomer.Tables["CUSTOMER"].Rows.InsertAt(dr, 0);
            cmbCustomer.DataSource = dsCustomer.Tables["CUSTOMER"];
            cmbCustomer.DisplayMember = "C_CUSTOMER";
            cmbCustomer.ValueMember = "C_CODE";
        }

        private void FillSummaryGrid()
        {
            foreach (RadioButton rbg in grpInvoiceDetail.Controls)
            {
                switch (rbg.Name)
                {
                    case "rbAll":
                        {
                            if (rbg.Checked)
                            {

                            }
                            break;
                        }
                    case "rbInvoiced":
                        {

                            if (rbg.Checked)
                            {

                            }
                            break;
                        }
                    case "rbNot":
                        {
                            if (rbg.Checked)
                            {

                            }
                            break;
                        }
                }
            }


            SqlDataAdapter daMonthly = new SqlDataAdapter("SELECT COUNT(*) AS CNT, C_NAME, P_DESCRIPTION, T_BILLTO, CAST(CASE T_CHARGEABLE WHEN 'Y' then 1 else 0 END as BIT) as CHARGEABLE, T_MSGTYPE, P_DIRECTION FROM vw_Transaction" +
                " WHERE (@C_CODE is NULL or C_CODE=@C_CODE) AND (CONVERT(DATE,T_DATETIME) >= Convert(Date, @DATEFROM)) AND (Convert(Date, T_DATETIME) <= Convert(Date, @DATETO)) and (C_IS_ACTIVE='Y')" +
                " GROUP BY C_NAME, P_DESCRIPTION, T_BILLTO, T_CHARGEABLE, T_MSGTYPE, P_DIRECTION ORDER BY C_NAME, CNT", sqlConn);
            daMonthly.SelectCommand.Parameters.Add("@C_CODE", SqlDbType.Char, 15);
            daMonthly.SelectCommand.Parameters.AddWithValue("@DATEFROM", DateTime.Parse(dtFrom.Text));
            daMonthly.SelectCommand.Parameters.AddWithValue("@DATETO", DateTime.Parse(dtTo.Text));

            if (cmbCustomer.Text != "(none selected)")
            {
                daMonthly.SelectCommand.Parameters[0].Value = cmbCustomer.SelectedValue;
            }
            if (cbAll.Checked)
            {
                daMonthly.SelectCommand.Parameters[0].Value = DBNull.Value;

            }
            DataSet dsInvoiceSummary = new DataSet();
            daMonthly.Fill(dsInvoiceSummary, "INVOICESUMMARY");
            dgMonthlySummary.AutoGenerateColumns = false;
            dgMonthlySummary.DataSource = dsInvoiceSummary;
            dgMonthlySummary.DataMember = "INVOICESUMMARY";

        }

        private void bbFind_Click(object sender, EventArgs e)
        {
            FillDetailGrid();
            FillSummaryGrid();
        }

        private void bbGenerate_Click(object sender, EventArgs e)
        {
            InvoiceReport();
        }

        private void CustSummReport(string CustCode)
        {
            try
            {
                string customerName = "";
                SqlDataAdapter daCustSummary = new SqlDataAdapter("SELECT P_DESCRIPTION, P_MSGTYPE, CAST(T_CHARGEABLE WHEN 'Y'then 1 else 0 END as BIT) As CHARGEABLE, P_SENDERID, P_Recipientid, COUNT(*) AS CNT, C_NAME, C_SHORTNAME, P_GROUPCHARGES " +
                    "FROM vw_Transaction WHERE (@C_CODE is NULL or C_CODE=@C_CODE) AND (CONVERT)DATE,T_DATETIME) >= CONVERT(DATE,@DATEFROM)) AND (Convert(Date, T_DATETIME) <= Convert(Date, (@DATETO)) AND " +
                    "(T_CHARGEABLE =@T_CHARGEABLE) and (C_IS_ACTIVE='Y') GROUP BY P_DESCRIPTION, P_MSGTYPE, T_CHARGEABLE, P_SENDERID, P_Recipientid, C_NAME, C_SHORTNAME, P_GROUPCHARGES" +
                    " ORDER BY P_MSGTYPE,CNT desc", sqlConn);
                daCustSummary.SelectCommand.Parameters.Add("@C_CODE", SqlDbType.Char, 15);
                daCustSummary.SelectCommand.Parameters.AddWithValue("@DATEFROM", DateTime.Parse(dtFrom.Text));
                daCustSummary.SelectCommand.Parameters.AddWithValue("@DATETO", DateTime.Parse(dtTo.Text));
                // Fill with Chargeable records.

                SqlParameter CHARGEABLE = daCustSummary.SelectCommand.Parameters.Add("@T_CHARGEABLE", SqlDbType.Char, 1);
                CHARGEABLE.Value = "Y";
                daCustSummary.SelectCommand.Parameters[0].Value = CustCode;
                DataSet dsCustSummary = new DataSet();
                daCustSummary.Fill(dsCustSummary, "INSUMM-CHARGE");
                // Fill with non-chargeable records (ie those that are eAdapter or on Trial. 
                CHARGEABLE.Value = "N";
                daCustSummary.SelectCommand.Parameters[0].Value = CustCode;
                daCustSummary.Fill(dsCustSummary, "INSUMM-NONCHARGE");

                double breakRate = 0;
                int msgCountTotal = 0;
                bool groupCharges = false;
                sqlConn.Open();
                sqlConn.Close();
                if (dsCustSummary.Tables["INSUMM-CHARGE"].Rows.Count == 0)
                {
                    return;
                }
                else
                {
                    daCustSummary.SelectCommand.CommandText = "SELECT COUNT(*) AS TOTALCNT, B_NAME, C_CODE,  T_BILLTO, CAST(T_CHARGEABLE WHEN 'Y' then 1 else 0 END as BIT) as CHARGEABLE, P_MSGTYPE, P_GROUPCHARGES FROM vw_Transaction" +
                        " WHERE (@C_CODE is NULL or C_CODE=@C_CODE) AND (CONVERT(DATE,T_DATETIME) >= CONVERT(DATE,@DATEFROM)) AND (CONVERT(DATE,T_DATETIME) <= CONVERT(DATE,@DATETO)) AND " +
                        "(T_CHARGEABLE =@T_CHARGEABLE) AND (C_IS_ACTIVE='Y') GROUP BY B_NAME, C_CODE, T_BILLTO, T_CHARGEABLE, P_MSGTYPE, P_GROUPCHARGES" +
                        " ORDER BY P_MSGTYPE";
                    CHARGEABLE.Value = "Y";
                    // First fill it with Chargeable records to match the first Table. 
                    daCustSummary.Fill(dsCustSummary, "Break-CHARGE");
                    string breakType = string.Empty;
                    // Then fill it with Non-Chargeable records to match the Second Table.                
                    dsCustSummary.Tables[0].Columns.Add("Charge Break", typeof(string));
                    dsCustSummary.Tables[0].Columns.Add("Amount", typeof(string));
                    dsCustSummary.Tables[0].Columns.Add("Total", typeof(double));
                    bool capped = false;
                    bool newRate = true;
                    string oldRate = "";
                    foreach (DataRow dr in dsCustSummary.Tables[0].Rows)
                    {
                        CustCode = dr["C_SHORTNAME"].ToString().Trim();
                        customerName = dr["C_NAME"].ToString();
                        breakType = dr["P_MSGTYPE"].ToString().Trim();
                        if (breakType != oldRate)
                            newRate = true;
                        else
                            newRate = false;
                        DataRow[] dbreak = dsCustSummary.Tables["Break-CHARGE"].Select("P_MSGTYPE = '" + breakType + "'");
                        //dr["GroupCharges"] = dbreak[0]["P_GROUPCHARGES"].ToString();
                        for (int iRow = 0; iRow < dbreak.Length; iRow++)
                        {
                            oldRate = breakType;
                            if (dbreak.Count() > 0)
                            {
                                if (int.Parse(dbreak[iRow]["TOTALCNT"].ToString()) < 50)
                                {
                                    dr["Charge Break"] = "0 - 50";
                                    breakRate = 0.66;
                                }
                                else
                                    if (int.Parse(dbreak[iRow]["TOTALCNT"].ToString()) < 100)
                                {
                                    dr["Charge Break"] = "50 - 100";
                                    breakRate = 0.6;
                                }
                                else
                                        if (int.Parse(dbreak[iRow]["TOTALCNT"].ToString()) < 500)
                                {
                                    dr["Charge Break"] = "100 - 500";
                                    breakRate = 0.5;
                                }
                                else
                                            if (int.Parse(dbreak[iRow]["TOTALCNT"].ToString()) < 1000)
                                {
                                    dr["Charge Break"] = "500 - 1000";
                                    breakRate = 0.3;
                                }
                                else
                                                if (int.Parse(dbreak[iRow]["TOTALCNT"].ToString()) < 2000)
                                {
                                    dr["Charge Break"] = "1000 - 2000";
                                    breakRate = 0.15;
                                }
                                else
                                                    if (int.Parse(dbreak[iRow]["TOTALCNT"].ToString()) < 10000)
                                {
                                    dr["Charge Break"] = "2000 - 10000";
                                    breakRate = 0.15;
                                    capped = true;
                                }
                                else
                                                        if (int.Parse(dbreak[iRow]["TOTALCNT"].ToString()) < 200000)
                                {
                                    dr["Charge Break"] = "10000 - 20000";
                                    breakRate = 0.15;
                                    capped = true;
                                }

                                double dBrate = 0;
                                switch (dr["P_MSGTYPE"].ToString().Trim())
                                {

                                    //case "Forward":
                                    //    dr["Amount"] = "$" + (breakRate / 2).ToString();
                                    //    dBrate = breakRate / 2;
                                    //    break;
                                    default:
                                        dr["Amount"] = "$" + breakRate.ToString();
                                        dBrate = breakRate;

                                        break;
                                }
                                if (capped)
                                {
                                    dr["Total"] = 0.00;
                                    dr["Amount"] = "Capped";
                                }
                                else
                                {
                                    dr["Total"] = int.Parse(dr["CNT"].ToString()) * dBrate;
                                    if (dr["P_GROUPCHARGES"].ToString() == "Y")
                                    {
                                        dr["Total"] = int.Parse(dbreak[iRow]["TOTALCNT"].ToString()) * dBrate;
                                        groupCharges = true;
                                        msgCountTotal += (int)dr["CNT"];
                                        if (!newRate)
                                            dr["Total"] = 0;
                                    }
                                }
                            }
                        }

                    }

                    var repFilename = Path.Combine(Globals.ReportLoc, CustCode + "-Customer-" + DateTime.Parse(dtFrom.Text).ToString("ddMMMyy") + "-" + DateTime.Parse(dtTo.Text).ToString("ddMMMyy") + ".xlsx");
                    int i = 1;
                    if (File.Exists(repFilename))
                    {
                        try
                        {
                            File.Delete(repFilename);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Unable to Delete previous report (" + repFilename + ") ensure it is not open", "Monthly Customer Report", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        // repFilename = Path.Combine(Globals.ReportLoc, CustCode + "-Customer Report-" + DateTime.Parse(dtFrom.Text).ToString("ddMMMyy") + "-" + DateTime.Parse(dtTo.Text).ToString("ddMMMyy") + "-" + i + ".xlsx");
                        // i++;
                    }

                    FileInfo fiReport = new FileInfo(repFilename);

                    using (var pkg = new ExcelPackage(fiReport))
                    {
                        ExcelWorksheet ws = pkg.Workbook.Worksheets.Add("CTC Node Report.");
                        ws.TabColor = Color.Red;
                        ws.DefaultRowHeight = 12;
                        ws.HeaderFooter.FirstFooter.LeftAlignedText = string.Format("Generated: {0}", DateTime.Now.ToShortDateString());
                        ws.Row(1).Height = 20;
                        ws.Row(2).Height = 18;
                        // Start adding the header
                        using (ExcelRange objRange = ws.Cells["A1:L1"])
                        {
                            objRange.Merge = true;
                            objRange.Style.Font.Bold = true;
                            objRange.Style.Font.Color.SetColor(Color.White);
                            objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        }
                        ws.Cells[1, 1].Value = "CTC Node Customer Message Summary report.";
                        ws.Cells["A2:L2"].Merge = true;
                        ws.Cells["A2"].Value = "Chargeable Transactions Date Range from " + dtFrom.Text + " to " + dtTo.Text + "- " + customerName;
                        // First of all the first row
                        // 4 is the 4th Line down on the Excel Spreadsheet
                        ws.Cells[4, 1].Value = "Message Profile Description";
                        ws.Cells[4, 2].Value = "Message Type";
                        ws.Cells[4, 3].Value = "Chargeable";
                        ws.Cells[4, 4].Value = "Sender";
                        ws.Cells[4, 5].Value = "Recipient";
                        ws.Cells[4, 6].Value = "Qty";
                        ws.Cells[4, 7].Value = "Customer";
                        ws.Cells[4, 8].Value = "Short_Name";
                        ws.Cells[4, 9].Value = "Group Charges";
                        ws.Cells[4, 10].Value = "Break";
                        ws.Cells[4, 11].Value = "Rate";
                        ws.Cells[4, 12].Value = "Total";



                        //   ws.Column(12).Hidden = true;


                        //ws.Cells[1, 1].Value = "";
                        using (var range = ws.Cells[4, 1, 4, 12])
                        {
                            range.Style.Font.Bold = true;
                            range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            range.Style.Fill.BackgroundColor.SetColor(Color.Black);
                            range.Style.Font.Color.SetColor(Color.WhiteSmoke);
                            range.Style.ShrinkToFit = false;
                        }
                        int rowcount = dsCustSummary.Tables[0].Rows.Count + 4;
                        ws.Cells["A5"].LoadFromDataTable(dsCustSummary.Tables[0], false);
                        ws.Cells.AutoFitColumns();
                        ws.Column(3).Hidden = true;
                        ws.Column(4).Hidden = true;
                        ws.Column(5).Hidden = true;
                        ws.Column(7).Hidden = true;
                        ws.Column(8).Hidden = true;
                        //ws.Column(9).Hidden = true;
                        ws.Cells["A" + (rowcount + 1).ToString()].Value = "TOTAL";
                        ws.Cells["F" + (rowcount + 1).ToString()].Formula = "SUM(F4:F" + rowcount.ToString() + ")";
                        ws.Cells["L4:L" + (rowcount + 1).ToString()].Style.Numberformat.Format = "$0.00";
                        //Group Charges totals.
                        if (groupCharges)
                        {
                            ws.Cells["L" + (rowcount + 1).ToString()].Value = msgCountTotal * breakRate;
                        }
                        else
                        {
                            ws.Cells["L" + (rowcount + 1).ToString()].Formula = "SUM(L4:L" + rowcount.ToString() + ")";
                        }
                        if (capped)
                        {
                            ws.Cells["L" + (rowcount + 1).ToString()].Value = "$500.00 *";
                        }
                        //Set the Totals ROW formatting
                        ExcelRange totalsCharge = ws.Cells["A" + (rowcount + 1).ToString() + ":L" + (rowcount + 1).ToString()];

                        totalsCharge.Style.Font.Bold = true;
                        totalsCharge.Style.Border.Top.Style = ExcelBorderStyle.Double;
                        totalsCharge.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        int nonStart = dsCustSummary.Tables[0].Rows.Count + 8;
                        ws.Cells[dsCustSummary.Tables[0].Rows.Count + 6, 1].Value = "* Denotes Capped amount of $500";
                        string s = "A" + (dsCustSummary.Tables[0].Rows.Count + 6).ToString() + ":B" + (dsCustSummary.Tables[0].Rows.Count + 6).ToString();
                        using (ExcelRange objRange = ws.Cells[s])
                        {
                            objRange.Merge = true;
                            objRange.Style.Font.Bold = true;
                            objRange.Style.Font.Color.SetColor(Color.Red);
                            objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            //objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        }

                        s = "A" + nonStart.ToString() + ":L" + nonStart.ToString();
                        ws.Cells[s].Merge = true;
                        ws.Cells["A" + nonStart.ToString()].Value = "Non-Chargeable Transactions Date Range from " + dtFrom.Text + " to " + dtTo.Text;
                        // First of all the first row

                        ws.Cells[nonStart + 1, 1].Value = "Message Profile Description";
                        ws.Cells[nonStart + 1, 2].Value = "Message Type";
                        ws.Cells[nonStart + 1, 3].Value = "Chargeable";
                        ws.Cells[nonStart + 1, 4].Value = "Sender";
                        ws.Cells[nonStart + 1, 5].Value = "Recipient";
                        ws.Cells[nonStart + 1, 6].Value = "Qty";
                        ws.Cells[nonStart + 1, 7].Value = "Customer";


                        // ws.Cells.AutoFitColumns();
                        //ws.Column(7).Hidden = true;

                        //ws.Cells[1, 1].Value = "";
                        using (var range = ws.Cells[nonStart + 1, 1, nonStart + 1, 12])
                        {
                            range.Style.Font.Bold = true;
                            range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            range.Style.Fill.BackgroundColor.SetColor(Color.Black);
                            range.Style.Font.Color.SetColor(Color.WhiteSmoke);
                            range.Style.ShrinkToFit = false;
                        }
                        ws.Cells["A" + (nonStart + 2).ToString()].LoadFromDataTable(dsCustSummary.Tables[1], false);
                        rowcount = nonStart + dsCustSummary.Tables[1].Rows.Count + 2;
                        ws.Cells["A5"].LoadFromDataTable(dsCustSummary.Tables[0], false);
                        ws.Cells["A" + (rowcount + 1).ToString()].Value = "TOTAL";
                        ws.Cells["F" + (rowcount + 1).ToString()].Formula = "SUM(F" + (nonStart + 2) + ":F" + rowcount.ToString() + ")";
                        totalsCharge = ws.Cells["A" + (rowcount + 1).ToString() + ":L" + (rowcount + 1).ToString()];
                        totalsCharge.Style.Font.Bold = true;
                        totalsCharge.Style.Border.Top.Style = ExcelBorderStyle.Double;
                        totalsCharge.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        pkg.Save();
                    }
                }
            }
            // This table is for the Break Rate Calculations.


            catch (Exception ex)
            {
                MessageBox.Show("There was an error generating the report. Error was: " + ex.Message);
            }
        }

        private void CustomerSummary()
        {
            if (Directory.Exists(Globals.ReportLoc))
            {
                if (cmbCustomer.Text == "(none selected)" && !cbAll.Checked)
                {
                    MessageBox.Show("Please either select a customer or Click 'Select All' to report for all customers", "Generate Reports");
                }
                else
                {


                    if (cmbCustomer.Text != "(none selected)")
                    {

                        CustSummReport(cmbCustomer.SelectedValue.ToString());

                    }
                    if (cbAll.Checked)
                    {
                        foreach (object item in cmbCustomer.Items)
                        {
                            DataRowView row = item as DataRowView;
                            if (row != null)
                            {
                                string displayValue = row[1].ToString();
                                if (row[1].ToString() != "")
                                {
                                    CustSummReport(displayValue);
                                }
                                // do something
                            }
                        }
                    }
                    MessageBox.Show("All Reports saved to  " + Globals.ReportLoc);
                }
            }
            else
            {
                MessageBox.Show("Unable to access the Reports Location", "Unable to run Reports", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        private void InvoiceReport()
        {
            try
            {
                string customerName = "";
                string sqlReport = "SELECT B_NAME,  P_MSGTYPE, COUNT(*) AS CNT, P_DIRECTION, P_GROUPCHARGES FROM vw_Transaction WHERE (@C_CODE is NULL or C_CODE=@C_CODE) AND (T_DATETIME >= @DATEFROM AND T_DATETIME <= @DATETO) AND (T_CHARGEABLE='Y') and (C_IS_ACTIVE='Y') GROUP BY B_NAME, T_BILLTO, P_MSGTYPE, P_DIRECTION, P_GROUPCHARGES ORDER BY B_NAME, CNT";
                SqlDataAdapter daReport = new SqlDataAdapter(sqlReport, sqlConn);
                daReport.SelectCommand.Parameters.Add("@C_CODE", SqlDbType.Char, 15);

                daReport.SelectCommand.Parameters.AddWithValue("@DATEFROM", DateTime.Parse(dtFrom.Text));
                daReport.SelectCommand.Parameters.AddWithValue("@DATETO", DateTime.Parse(dtTo.Text));
                if (cmbCustomer.Text != "(none selected)")
                {
                    daReport.SelectCommand.Parameters[0].Value = cmbCustomer.SelectedValue;
                }
                if (cbAll.Checked)
                {
                    daReport.SelectCommand.Parameters[0].Value = DBNull.Value;

                }
                DataSet dsReport = new DataSet();
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();

                }
                sqlConn.Open();
                daReport.Fill(dsReport, "Invoice");
                sqlConn.Close();
                daReport.SelectCommand.CommandText = "SELECT COUNT(*) AS CNT, B_NAME, C_CODE, T_BILLTO, P_MSGTYPE, P_GROUPCHARGES FROM vw_Transaction WHERE (@C_CODE is NULL or C_CODE=@C_CODE) AND (T_DATETIME >= @DATEFROM AND T_DATETIME <= @DATETO) AND (T_CHARGEABLE = 'Y') AND (C_IS_ACTIVE='Y') GROUP BY B_NAME, C_CODE, T_BILLTO, P_MSGTYPE, P_GROUPCHARGES ORDER BY B_NAME, CNT";

                daReport.Fill(dsReport, "Break");
                dsReport.Tables[0].Columns.Add("Charge Break", typeof(string));
                dsReport.Tables[0].Columns.Add("Amount", typeof(string));
                dsReport.Tables[0].Columns.Add("Total", typeof(double));
                double breakRate = 0;
                foreach (DataRow dr in dsReport.Tables[0].Rows)
                {
                    customerName = dr["B_NAME"].ToString();
                    DataRow[] dbreak = dsReport.Tables[1].Select("B_NAME = '" + dr["B_NAME"] + "'");
                    if (dbreak.Count() > 0)
                    {
                        if (int.Parse(dbreak[0]["CNT"].ToString()) < 50)
                        {
                            dr["Charge Break"] = "0 - 50";
                            breakRate = 0.66;
                        }
                        else
                            if (int.Parse(dbreak[0]["CNT"].ToString()) < 100)
                        {
                            dr["Charge Break"] = "50 - 100";
                            breakRate = 0.6;
                        }
                        else
                                if (int.Parse(dbreak[0]["CNT"].ToString()) < 500)
                        {
                            dr["Charge Break"] = "100 - 500";
                            breakRate = 0.5;
                        }
                        else
                                    if (int.Parse(dbreak[0]["CNT"].ToString()) < 1000)
                        {
                            dr["Charge Break"] = "500 - 1000";
                            breakRate = 0.3;
                        }
                        else
                                        if (int.Parse(dbreak[0]["CNT"].ToString()) < 2000)
                        {
                            dr["Charge Break"] = "1000 - 2000";
                            breakRate = 0.15;
                        }
                        else
                                            if (int.Parse(dbreak[0]["CNT"].ToString()) < 10000)
                        {
                            dr["Charge Break"] = "2000 - 10000";
                            breakRate = 500;
                        }
                        double dBrate = 0;
                        switch (dr["P_MSGTYPE"].ToString().Trim())
                        {
                            case "Original":
                                dr["Amount"] = "$" + breakRate;
                                dBrate = breakRate;
                                break;
                            case "Forward":
                                dr["Amount"] = "$" + breakRate / 2;
                                dBrate = breakRate / 2;
                                break;
                            case "eAdapter":
                                dr["Amount"] = "$0.00";
                                break;
                        }
                        if (int.Parse(dr["CNT"].ToString()) * dBrate > 500)
                        {
                            dr["Total"] = 500;
                        }
                        else
                            dr["Total"] = int.Parse(dr["CNT"].ToString()) * dBrate;

                    }

                }
                string sRep = "";
                if (cmbCustomer.Text != "(none selected)")
                {
                    sRep = cmbCustomer.Text;
                }
                if (cbAll.Checked)
                {
                    sRep = "All Customers";

                }
                var repFilename = Path.Combine(Globals.ReportLoc, "InvoiceReport for " + sRep + "- " + DateTime.Parse(dtFrom.Text).ToString("ddMMMyy") + "-" + DateTime.Parse(dtTo.Text).ToString("ddMMMyy") + ".xlsx");
                int i = 1;
                while (File.Exists(repFilename))
                {
                    repFilename = Path.Combine(Globals.ReportLoc, "InvoiceReport for " + sRep + "- " + DateTime.Parse(dtFrom.Text).ToString("ddMMMyy") + "-" + DateTime.Parse(dtTo.Text).ToString("ddMMMyy") + "-" + i + ".xlsx");
                    i++;
                }
                FileInfo fiReport = new FileInfo(repFilename);

                using (var pkg = new ExcelPackage(fiReport))
                {
                    ExcelWorksheet ws = pkg.Workbook.Worksheets.Add("Invoice Master Report for " + DateTime.Parse(dtFrom.Text).ToString("ddMMMyy") + "-" + DateTime.Parse(dtTo.Text).ToString("ddMMMyy"));
                    ws.TabColor = Color.Blue;
                    ws.DefaultRowHeight = 12;
                    ws.HeaderFooter.FirstFooter.LeftAlignedText = string.Format("Generated: {0}", DateTime.Now.ToShortDateString());
                    ws.Row(1).Height = 20;
                    ws.Row(2).Height = 18;
                    // Start adding the header
                    using (ExcelRange objRange = ws.Cells["A1:H1"])
                    {
                        objRange.Merge = true;
                        objRange.Style.Font.Bold = true;
                        objRange.Style.Font.Color.SetColor(Color.White);
                        objRange.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        objRange.Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        objRange.Style.Fill.PatternType = ExcelFillStyle.Solid;
                    }
                    ws.Cells[1, 1].Value = "CTC Node Invoice Master report.";
                    ws.Cells["A2:I2"].Merge = true;
                    ws.Cells["A2"].Value = "Date Range from " + dtFrom.Text + " to " + dtTo.Text;
                    // First of all the first row
                    ws.Cells[4, 1].Value = "Company name";
                    ws.Cells[4, 2].Value = "Message Type";
                    ws.Cells[4, 3].Value = "Qty";
                    ws.Cells[4, 4].Value = "Direction";
                    ws.Cells[4, 5].Value = "Charge break";
                    ws.Cells[4, 6].Value = "Amount";
                    ws.Cells[4, 7].Value = "Total";

                    //ws.Cells[1, 1].Value = "";
                    using (var range = ws.Cells[4, 1, 4, 7])
                    {
                        range.Style.Font.Bold = true;
                        range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        range.Style.Fill.BackgroundColor.SetColor(Color.Black);
                        range.Style.Font.Color.SetColor(Color.WhiteSmoke);
                        range.Style.ShrinkToFit = false;
                    }
                    ws.Cells["A5"].LoadFromDataTable(dsReport.Tables[0], false);
                    int rolCount = dsReport.Tables[0].Rows.Count + 4;

                    ws.Cells[rolCount + 1, 1].Value = "TOTALS";
                    ws.Cells[rolCount + 1, 3].Formula = "SUM(C4:C" + rolCount.ToString() + ")";
                    ws.Cells[rolCount + 1, 7].Formula = "SUM(G4:G" + rolCount.ToString() + ")";
                    ExcelRange totalRange = ws.Cells[rolCount + 1, 1, rolCount + 1, 7];
                    totalRange.Style.Numberformat.Format = "$0.00";
                    totalRange.Style.Font.Bold = true;
                    totalRange.Style.Border.Top.Style = ExcelBorderStyle.Double;
                    totalRange.Style.Border.Bottom.Style = ExcelBorderStyle.Thin;


                    pkg.Save();
                }
                MessageBox.Show("Invoice Report saved. " + repFilename);
            }

            catch (Exception ex)
            {
                MessageBox.Show("There was an error generating the report. Error was: " + ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.Text == "(none selected)" && !cbAll.Checked)
            {
                MessageBox.Show("Please either select a customer or Click 'Select All' to report for all customers", "Generate Reports");
            }
            else
            {
                CustomerSummary();

            }
        }
    }

}
