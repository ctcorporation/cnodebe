﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using NodeResources;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace CNodeBE
{

    public partial class CWTestModule : Form
    {
        public NodeData.IConnectionManager connMgr;
        public List<vw_CustomerProfile> customers;

        public CWTestModule()
        {
            InitializeComponent();
        }



        private bool validateSender()
        {
            var code = edSendUsername.Text.Trim();
            if (string.IsNullOrEmpty(code))
            {
                edSendeAdapterResults.Text += ("Sender ID cannot be Blank\r\n");
                return false;
            }

            edSendeAdapterResults.Text += "Sender ID is Ok\r\n";
            return true;
        }

        private bool validatePassword()
        {
            var code = edSendPassword.Text.Trim();
            if (string.IsNullOrEmpty(code))
            {
                edSendeAdapterResults.Text += "Password should not be Blank\r\n";
                return false;
            }
            edSendeAdapterResults.Text += "Password is ok\r\n";
            return true;

        }
        private bool validateServer()
        {
            var code = edSendCTCUrl.Text.Trim();
            if (string.IsNullOrEmpty(code))
            {
                edSendeAdapterResults.Text += "Service URL cannot be Blank\r\n";
                return false;
            }
            edSendeAdapterResults.Text += "Service URL is OK.\r\n";
            try
            {
                if (CTCAdapter.Ping(edSendCTCUrl.Text, edSendUsername.Text, edSendPassword.Text))
                {
                    edSendeAdapterResults.Text += "Ping Service URL is Successful\r\n";
                    return true;
                }
                else
                {
                    edSendeAdapterResults.Text += "Ping Service URL is Un-Successful. Please check username and password or URL\r\n";
                    return false;
                }

            }
            catch (Exception ex)
            {
                edSendeAdapterResults.Text += "Error Message: " + ex.Message + "\r\n";
                return false;
            }
        }



        private void bbDir_Click(object sender, EventArgs e)
        {
            fdSendFile.ShowDialog();
            edSendFile.Text = fdSendFile.FileName;
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            edSendeAdapterResults.Clear();
            if (!validateSender())
            {
                return;
            }
            //if (!validateRecipient()) return;
            if (!validatePassword())
            {
                return;
            }

            if (!validateServer())
            {
                return;
            }
        }

        private void bbSend_Click(object sender, EventArgs e)
        {
            if (File.Exists(edSendFile.Text))
            {
                string ctcMessage = CTCAdapter.SendMessage(edSendCTCUrl.Text, edSendFile.Text, edRecipientID.Text, edSendUsername.Text, edSendPassword.Text, null);
                NodeResources.AddText(edSendeAdapterResults, ctcMessage);
            }
            else
            {
                NodeResources.AddText(edSendeAdapterResults, "File not found");
            }

        }

        private void bbClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void CWTestModule_Load(object sender, EventArgs e)
        {
            connMgr = new ConnectionManager(Globals.ServerName, Globals.DataBase, Globals.UserName, Globals.Password);

            FillCustomers();
        }

        private void FillCustomers()
        {
            try
            {
                var custList = new[] { new { Name = "(none selected}", ID = Guid.Empty } }.ToList();
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
                {
                    var qCustList = uow.Customers.GetAll().OrderBy(x => x.C_NAME).AsQueryable();
                    custList = (from x in qCustList
                                select new { Name = x.C_NAME, ID = x.C_ID }).ToList();
                    if (custList.Count > 0)
                    {
                        custList.Insert(0, new { Name = "(none selected}", ID = Guid.Empty });
                        cmbCustomer.DataSource = custList;
                        cmbCustomer.DisplayMember = "Name";
                        cmbCustomer.ValueMember = "ID";


                    }
                }
                //SqlDataAdapter daCustomer = new SqlDataAdapter("Select C_ID, C_NAME from Customer Order by C_NAME", sqlConn);
                //DataSet dsCustomer = new DataSet();
                //daCustomer.Fill(dsCustomer, "CUSTOMER");
                //DataRow dr = dsCustomer.Tables["CUSTOMER"].NewRow();
                //dr["C_NAME"] = "(none selected)";
                //dsCustomer.Tables["CUSTOMER"].Rows.InsertAt(dr, 0);
                //cmbCustomer.DataSource = dsCustomer.Tables["CUSTOMER"];
                //cmbCustomer.DisplayMember = "C_CUSTOMER";
                //cmbCustomer.ValueMember = "C_ID";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Found Accessing DB. Error was: " + ex.Message);

            }


        }

        private void cmbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbCustomer.SelectedIndex > 0)
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
                {
                    var profList = (from p in uow.CustProfiles.Find(x => x.C_ID == (Guid)cmbCustomer.SelectedValue
                                                           && x.P_ACTIVE == "Y"
                                                           && x.P_DELIVERY == "C").AsQueryable()
                                    select new Item { Id = p.P_ID, Display = p.P_DESCRIPTION }).Distinct().ToList();
                    if (profList.Count > 0)
                    {
                        List<Item> list = new List<Item>
                        {
                         new Item{ Id=Guid.Empty, Display="(none selected)"}
                        };
                        foreach (var p in profList)
                        {
                            list.Add(new Item { Id = p.Id, Display = p.Display });
                        }
                        cmbProfieList.DataSource = list;
                        cmbProfieList.DisplayMember = "Display";
                        cmbProfieList.ValueMember = "Id";

                    }
                }
            }
        }

        private void cmbProfieList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbProfieList.SelectedIndex > 0)
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
                {
                    var profile = uow.CustProfiles.Find(x => x.P_ID == (Guid)cmbProfieList.SelectedValue).FirstOrDefault();
                    if (profile != null)
                    {
                        edSendCTCUrl.Text = profile.P_SERVER;
                        edSendUsername.Text = profile.P_USERNAME;
                        edSendPassword.Text = profile.P_PASSWORD;

                    }
                }


            }
        }
    }
    public class Item
    {
        public Guid Id { get; set; }
        public string Display { get; set; }
    }
}
