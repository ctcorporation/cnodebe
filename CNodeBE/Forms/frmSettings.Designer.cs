﻿namespace CNodeBE
{
    partial class FrmSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmSettings));
            this.btnClose = new System.Windows.Forms.Button();
            this.tcSettings = new System.Windows.Forms.TabControl();
            this.tbSettings = new System.Windows.Forms.TabPage();
            this.btnWinscpLocaiton = new System.Windows.Forms.Button();
            this.edWinscpLocation = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.edMainTimer = new System.Windows.Forms.TextBox();
            this.edCyclecount = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.tbData = new System.Windows.Forms.TabPage();
            this.edCTCResults = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnCTCTestConnection = new System.Windows.Forms.Button();
            this.edCTCPassword = new System.Windows.Forms.TextBox();
            this.edCTCUserName = new System.Windows.Forms.TextBox();
            this.edCTCDatabase = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.edCTCServer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbTransport = new System.Windows.Forms.TabPage();
            this.rtbTransResults = new System.Windows.Forms.RichTextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.btnTransTest = new System.Windows.Forms.Button();
            this.edTransPassword = new System.Windows.Forms.TextBox();
            this.edTransUserName = new System.Windows.Forms.TextBox();
            this.edTransDatabase = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.edTransServer = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.tbSecurity = new System.Windows.Forms.TabPage();
            this.numGuardian = new System.Windows.Forms.NumericUpDown();
            this.label26 = new System.Windows.Forms.Label();
            this.numPollingTime = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.edTestingLocation = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.btnFailedLoc = new System.Windows.Forms.Button();
            this.edFailedLoc = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.bbCustomReceive = new System.Windows.Forms.Button();
            this.edCustomReceive = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.bbCustRoot = new System.Windows.Forms.Button();
            this.edCustRoot = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnDirXml = new System.Windows.Forms.Button();
            this.edXMLLocation = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbEmail = new System.Windows.Forms.TabPage();
            this.edAlerts = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.edSMTPPassword = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.edSMTPUsername = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.edEmail = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.edSMTPPort = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.edSMTPServer = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.bbPDF2Xml = new System.Windows.Forms.Button();
            this.edPdf2XMLLoc = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.fdInLocation = new System.Windows.Forms.FolderBrowserDialog();
            this.btnArchiveLoc = new System.Windows.Forms.Button();
            this.txtArchiveLoc = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tcSettings.SuspendLayout();
            this.tbSettings.SuspendLayout();
            this.tbData.SuspendLayout();
            this.tbTransport.SuspendLayout();
            this.tbSecurity.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numGuardian)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPollingTime)).BeginInit();
            this.tbEmail.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnClose.Location = new System.Drawing.Point(541, 399);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(61, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tcSettings
            // 
            this.tcSettings.Controls.Add(this.tbSettings);
            this.tcSettings.Controls.Add(this.tbData);
            this.tcSettings.Controls.Add(this.tbTransport);
            this.tcSettings.Controls.Add(this.tbSecurity);
            this.tcSettings.Controls.Add(this.tbEmail);
            this.tcSettings.Controls.Add(this.tabPage1);
            this.tcSettings.Location = new System.Drawing.Point(9, 18);
            this.tcSettings.Margin = new System.Windows.Forms.Padding(2);
            this.tcSettings.Name = "tcSettings";
            this.tcSettings.SelectedIndex = 0;
            this.tcSettings.Size = new System.Drawing.Size(595, 366);
            this.tcSettings.TabIndex = 1;
            // 
            // tbSettings
            // 
            this.tbSettings.Controls.Add(this.btnWinscpLocaiton);
            this.tbSettings.Controls.Add(this.edWinscpLocation);
            this.tbSettings.Controls.Add(this.label7);
            this.tbSettings.Controls.Add(this.edMainTimer);
            this.tbSettings.Controls.Add(this.edCyclecount);
            this.tbSettings.Controls.Add(this.label28);
            this.tbSettings.Controls.Add(this.label27);
            this.tbSettings.Location = new System.Drawing.Point(4, 22);
            this.tbSettings.Name = "tbSettings";
            this.tbSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tbSettings.Size = new System.Drawing.Size(587, 340);
            this.tbSettings.TabIndex = 4;
            this.tbSettings.Text = "App Settings";
            this.tbSettings.UseVisualStyleBackColor = true;
            // 
            // btnWinscpLocaiton
            // 
            this.btnWinscpLocaiton.Location = new System.Drawing.Point(471, 40);
            this.btnWinscpLocaiton.Margin = new System.Windows.Forms.Padding(2);
            this.btnWinscpLocaiton.Name = "btnWinscpLocaiton";
            this.btnWinscpLocaiton.Size = new System.Drawing.Size(25, 20);
            this.btnWinscpLocaiton.TabIndex = 32;
            this.btnWinscpLocaiton.Text = "...";
            this.btnWinscpLocaiton.UseVisualStyleBackColor = true;
            // 
            // edWinscpLocation
            // 
            this.edWinscpLocation.Location = new System.Drawing.Point(146, 40);
            this.edWinscpLocation.Margin = new System.Windows.Forms.Padding(2);
            this.edWinscpLocation.Name = "edWinscpLocation";
            this.edWinscpLocation.Size = new System.Drawing.Size(329, 20);
            this.edWinscpLocation.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 44);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(127, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Location of WinSCP.EXE";
            // 
            // edMainTimer
            // 
            this.edMainTimer.Location = new System.Drawing.Point(280, 17);
            this.edMainTimer.Name = "edMainTimer";
            this.edMainTimer.Size = new System.Drawing.Size(63, 20);
            this.edMainTimer.TabIndex = 3;
            // 
            // edCyclecount
            // 
            this.edCyclecount.Location = new System.Drawing.Point(75, 17);
            this.edCyclecount.Name = "edCyclecount";
            this.edCyclecount.Size = new System.Drawing.Size(63, 20);
            this.edCyclecount.TabIndex = 2;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(154, 20);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(120, 13);
            this.label28.TabIndex = 1;
            this.label28.Text = "Main Timer (In Minutes) ";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(5, 20);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(64, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Cycle Count";
            // 
            // tbData
            // 
            this.tbData.Controls.Add(this.edCTCResults);
            this.tbData.Controls.Add(this.label5);
            this.tbData.Controls.Add(this.btnCTCTestConnection);
            this.tbData.Controls.Add(this.edCTCPassword);
            this.tbData.Controls.Add(this.edCTCUserName);
            this.tbData.Controls.Add(this.edCTCDatabase);
            this.tbData.Controls.Add(this.label4);
            this.tbData.Controls.Add(this.label3);
            this.tbData.Controls.Add(this.label2);
            this.tbData.Controls.Add(this.edCTCServer);
            this.tbData.Controls.Add(this.label1);
            this.tbData.Location = new System.Drawing.Point(4, 22);
            this.tbData.Margin = new System.Windows.Forms.Padding(2);
            this.tbData.Name = "tbData";
            this.tbData.Padding = new System.Windows.Forms.Padding(2);
            this.tbData.Size = new System.Drawing.Size(587, 340);
            this.tbData.TabIndex = 0;
            this.tbData.Text = "Database (CTCNode)";
            this.tbData.UseVisualStyleBackColor = true;
            // 
            // edCTCResults
            // 
            this.edCTCResults.Location = new System.Drawing.Point(27, 166);
            this.edCTCResults.Margin = new System.Windows.Forms.Padding(2);
            this.edCTCResults.Multiline = true;
            this.edCTCResults.Name = "edCTCResults";
            this.edCTCResults.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.edCTCResults.Size = new System.Drawing.Size(511, 61);
            this.edCTCResults.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(27, 151);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(99, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Connection Results";
            // 
            // btnCTCTestConnection
            // 
            this.btnCTCTestConnection.Location = new System.Drawing.Point(361, 85);
            this.btnCTCTestConnection.Margin = new System.Windows.Forms.Padding(2);
            this.btnCTCTestConnection.Name = "btnCTCTestConnection";
            this.btnCTCTestConnection.Size = new System.Drawing.Size(99, 19);
            this.btnCTCTestConnection.TabIndex = 8;
            this.btnCTCTestConnection.Text = "Test Connection";
            this.btnCTCTestConnection.UseVisualStyleBackColor = true;
            this.btnCTCTestConnection.Click += new System.EventHandler(this.btnTestConnection_Click);
            // 
            // edCTCPassword
            // 
            this.edCTCPassword.Location = new System.Drawing.Point(156, 116);
            this.edCTCPassword.Margin = new System.Windows.Forms.Padding(2);
            this.edCTCPassword.Name = "edCTCPassword";
            this.edCTCPassword.Size = new System.Drawing.Size(173, 20);
            this.edCTCPassword.TabIndex = 7;
            // 
            // edCTCUserName
            // 
            this.edCTCUserName.Location = new System.Drawing.Point(156, 88);
            this.edCTCUserName.Margin = new System.Windows.Forms.Padding(2);
            this.edCTCUserName.Name = "edCTCUserName";
            this.edCTCUserName.Size = new System.Drawing.Size(173, 20);
            this.edCTCUserName.TabIndex = 6;
            // 
            // edCTCDatabase
            // 
            this.edCTCDatabase.Location = new System.Drawing.Point(156, 58);
            this.edCTCDatabase.Margin = new System.Windows.Forms.Padding(2);
            this.edCTCDatabase.Name = "edCTCDatabase";
            this.edCTCDatabase.Size = new System.Drawing.Size(173, 20);
            this.edCTCDatabase.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 116);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Password";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 88);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "User Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(27, 58);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Database Name";
            // 
            // edCTCServer
            // 
            this.edCTCServer.Location = new System.Drawing.Point(156, 29);
            this.edCTCServer.Margin = new System.Windows.Forms.Padding(2);
            this.edCTCServer.Name = "edCTCServer";
            this.edCTCServer.Size = new System.Drawing.Size(355, 20);
            this.edCTCServer.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 31);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Server - Server/Intance";
            // 
            // tbTransport
            // 
            this.tbTransport.Controls.Add(this.rtbTransResults);
            this.tbTransport.Controls.Add(this.label19);
            this.tbTransport.Controls.Add(this.btnTransTest);
            this.tbTransport.Controls.Add(this.edTransPassword);
            this.tbTransport.Controls.Add(this.edTransUserName);
            this.tbTransport.Controls.Add(this.edTransDatabase);
            this.tbTransport.Controls.Add(this.label20);
            this.tbTransport.Controls.Add(this.label21);
            this.tbTransport.Controls.Add(this.label22);
            this.tbTransport.Controls.Add(this.edTransServer);
            this.tbTransport.Controls.Add(this.label23);
            this.tbTransport.Location = new System.Drawing.Point(4, 22);
            this.tbTransport.Name = "tbTransport";
            this.tbTransport.Padding = new System.Windows.Forms.Padding(3);
            this.tbTransport.Size = new System.Drawing.Size(587, 340);
            this.tbTransport.TabIndex = 3;
            this.tbTransport.Text = "Database (TransMod)";
            this.tbTransport.UseVisualStyleBackColor = true;
            // 
            // rtbTransResults
            // 
            this.rtbTransResults.Location = new System.Drawing.Point(25, 173);
            this.rtbTransResults.Name = "rtbTransResults";
            this.rtbTransResults.Size = new System.Drawing.Size(535, 87);
            this.rtbTransResults.TabIndex = 22;
            this.rtbTransResults.Text = "";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(27, 152);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(99, 13);
            this.label19.TabIndex = 21;
            this.label19.Text = "Connection Results";
            // 
            // btnTransTest
            // 
            this.btnTransTest.Location = new System.Drawing.Point(361, 86);
            this.btnTransTest.Margin = new System.Windows.Forms.Padding(2);
            this.btnTransTest.Name = "btnTransTest";
            this.btnTransTest.Size = new System.Drawing.Size(99, 19);
            this.btnTransTest.TabIndex = 20;
            this.btnTransTest.Text = "Test Connection";
            this.btnTransTest.UseVisualStyleBackColor = true;
            this.btnTransTest.Click += new System.EventHandler(this.btnTransTest_Click);
            // 
            // edTransPassword
            // 
            this.edTransPassword.Location = new System.Drawing.Point(156, 117);
            this.edTransPassword.Margin = new System.Windows.Forms.Padding(2);
            this.edTransPassword.Name = "edTransPassword";
            this.edTransPassword.Size = new System.Drawing.Size(173, 20);
            this.edTransPassword.TabIndex = 19;
            // 
            // edTransUserName
            // 
            this.edTransUserName.Location = new System.Drawing.Point(156, 89);
            this.edTransUserName.Margin = new System.Windows.Forms.Padding(2);
            this.edTransUserName.Name = "edTransUserName";
            this.edTransUserName.Size = new System.Drawing.Size(173, 20);
            this.edTransUserName.TabIndex = 18;
            // 
            // edTransDatabase
            // 
            this.edTransDatabase.Location = new System.Drawing.Point(156, 59);
            this.edTransDatabase.Margin = new System.Windows.Forms.Padding(2);
            this.edTransDatabase.Name = "edTransDatabase";
            this.edTransDatabase.Size = new System.Drawing.Size(173, 20);
            this.edTransDatabase.TabIndex = 17;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(27, 117);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 13);
            this.label20.TabIndex = 16;
            this.label20.Text = "Password";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(27, 89);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 13);
            this.label21.TabIndex = 15;
            this.label21.Text = "User Name";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(27, 59);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(84, 13);
            this.label22.TabIndex = 14;
            this.label22.Text = "Database Name";
            // 
            // edTransServer
            // 
            this.edTransServer.Location = new System.Drawing.Point(156, 30);
            this.edTransServer.Margin = new System.Windows.Forms.Padding(2);
            this.edTransServer.Name = "edTransServer";
            this.edTransServer.Size = new System.Drawing.Size(355, 20);
            this.edTransServer.TabIndex = 13;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(24, 32);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(119, 13);
            this.label23.TabIndex = 12;
            this.label23.Text = "Server - Server/Intance";
            // 
            // tbSecurity
            // 
            this.tbSecurity.Controls.Add(this.btnArchiveLoc);
            this.tbSecurity.Controls.Add(this.txtArchiveLoc);
            this.tbSecurity.Controls.Add(this.label10);
            this.tbSecurity.Controls.Add(this.numGuardian);
            this.tbSecurity.Controls.Add(this.label26);
            this.tbSecurity.Controls.Add(this.numPollingTime);
            this.tbSecurity.Controls.Add(this.label25);
            this.tbSecurity.Controls.Add(this.button1);
            this.tbSecurity.Controls.Add(this.edTestingLocation);
            this.tbSecurity.Controls.Add(this.label24);
            this.tbSecurity.Controls.Add(this.btnFailedLoc);
            this.tbSecurity.Controls.Add(this.edFailedLoc);
            this.tbSecurity.Controls.Add(this.label18);
            this.tbSecurity.Controls.Add(this.bbCustomReceive);
            this.tbSecurity.Controls.Add(this.edCustomReceive);
            this.tbSecurity.Controls.Add(this.label17);
            this.tbSecurity.Controls.Add(this.bbCustRoot);
            this.tbSecurity.Controls.Add(this.edCustRoot);
            this.tbSecurity.Controls.Add(this.label9);
            this.tbSecurity.Controls.Add(this.btnDirXml);
            this.tbSecurity.Controls.Add(this.edXMLLocation);
            this.tbSecurity.Controls.Add(this.label6);
            this.tbSecurity.Location = new System.Drawing.Point(4, 22);
            this.tbSecurity.Margin = new System.Windows.Forms.Padding(2);
            this.tbSecurity.Name = "tbSecurity";
            this.tbSecurity.Padding = new System.Windows.Forms.Padding(2);
            this.tbSecurity.Size = new System.Drawing.Size(587, 340);
            this.tbSecurity.TabIndex = 1;
            this.tbSecurity.Text = "File Locations";
            this.tbSecurity.UseVisualStyleBackColor = true;
            this.tbSecurity.Click += new System.EventHandler(this.tbSecurity_Click);
            // 
            // numGuardian
            // 
            this.numGuardian.Location = new System.Drawing.Point(493, 54);
            this.numGuardian.Name = "numGuardian";
            this.numGuardian.Size = new System.Drawing.Size(48, 20);
            this.numGuardian.TabIndex = 26;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(287, 56);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(200, 13);
            this.label26.TabIndex = 25;
            this.label26.Text = "Guardian Service Refresh time (Minutes) ";
            // 
            // numPollingTime
            // 
            this.numPollingTime.Location = new System.Drawing.Point(211, 53);
            this.numPollingTime.Name = "numPollingTime";
            this.numPollingTime.Size = new System.Drawing.Size(48, 20);
            this.numPollingTime.TabIndex = 24;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(21, 56);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(163, 13);
            this.label25.TabIndex = 23;
            this.label25.Text = "CTC Node Polling Time (Minutes)";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(536, 81);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 20);
            this.button1.TabIndex = 22;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // edTestingLocation
            // 
            this.edTestingLocation.Location = new System.Drawing.Point(211, 81);
            this.edTestingLocation.Margin = new System.Windows.Forms.Padding(2);
            this.edTestingLocation.Name = "edTestingLocation";
            this.edTestingLocation.Size = new System.Drawing.Size(329, 20);
            this.edTestingLocation.TabIndex = 21;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(18, 81);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(108, 13);
            this.label24.TabIndex = 20;
            this.label24.Text = "Test Pickup Location";
            // 
            // btnFailedLoc
            // 
            this.btnFailedLoc.Location = new System.Drawing.Point(537, 153);
            this.btnFailedLoc.Margin = new System.Windows.Forms.Padding(2);
            this.btnFailedLoc.Name = "btnFailedLoc";
            this.btnFailedLoc.Size = new System.Drawing.Size(25, 20);
            this.btnFailedLoc.TabIndex = 19;
            this.btnFailedLoc.Text = "...";
            this.btnFailedLoc.UseVisualStyleBackColor = true;
            this.btnFailedLoc.Click += new System.EventHandler(this.button1_Click);
            // 
            // edFailedLoc
            // 
            this.edFailedLoc.Location = new System.Drawing.Point(212, 153);
            this.edFailedLoc.Margin = new System.Windows.Forms.Padding(2);
            this.edFailedLoc.Name = "edFailedLoc";
            this.edFailedLoc.Size = new System.Drawing.Size(329, 20);
            this.edFailedLoc.TabIndex = 18;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(20, 157);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(96, 13);
            this.label18.TabIndex = 17;
            this.label18.Text = "Failed files location";
            // 
            // bbCustomReceive
            // 
            this.bbCustomReceive.Location = new System.Drawing.Point(536, 129);
            this.bbCustomReceive.Margin = new System.Windows.Forms.Padding(2);
            this.bbCustomReceive.Name = "bbCustomReceive";
            this.bbCustomReceive.Size = new System.Drawing.Size(25, 20);
            this.bbCustomReceive.TabIndex = 16;
            this.bbCustomReceive.Text = "...";
            this.bbCustomReceive.UseVisualStyleBackColor = true;
            this.bbCustomReceive.Click += new System.EventHandler(this.bbCustomReceive_Click);
            // 
            // edCustomReceive
            // 
            this.edCustomReceive.Location = new System.Drawing.Point(211, 129);
            this.edCustomReceive.Margin = new System.Windows.Forms.Padding(2);
            this.edCustomReceive.Name = "edCustomReceive";
            this.edCustomReceive.Size = new System.Drawing.Size(329, 20);
            this.edCustomReceive.TabIndex = 15;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(18, 131);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(110, 13);
            this.label17.TabIndex = 14;
            this.label17.Text = "Custom Receive Path";
            // 
            // bbCustRoot
            // 
            this.bbCustRoot.Location = new System.Drawing.Point(536, 105);
            this.bbCustRoot.Margin = new System.Windows.Forms.Padding(2);
            this.bbCustRoot.Name = "bbCustRoot";
            this.bbCustRoot.Size = new System.Drawing.Size(25, 20);
            this.bbCustRoot.TabIndex = 13;
            this.bbCustRoot.Text = "...";
            this.bbCustRoot.UseVisualStyleBackColor = true;
            this.bbCustRoot.Click += new System.EventHandler(this.bbCustRoot_Click);
            // 
            // edCustRoot
            // 
            this.edCustRoot.Location = new System.Drawing.Point(211, 105);
            this.edCustRoot.Margin = new System.Windows.Forms.Padding(2);
            this.edCustRoot.Name = "edCustRoot";
            this.edCustRoot.Size = new System.Drawing.Size(329, 20);
            this.edCustRoot.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 106);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(126, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Customer Files Root Path";
            // 
            // btnDirXml
            // 
            this.btnDirXml.Location = new System.Drawing.Point(539, 27);
            this.btnDirXml.Margin = new System.Windows.Forms.Padding(2);
            this.btnDirXml.Name = "btnDirXml";
            this.btnDirXml.Size = new System.Drawing.Size(25, 20);
            this.btnDirXml.TabIndex = 2;
            this.btnDirXml.Text = "...";
            this.btnDirXml.UseVisualStyleBackColor = true;
            this.btnDirXml.Click += new System.EventHandler(this.btnDirXml_Click);
            // 
            // edXMLLocation
            // 
            this.edXMLLocation.Location = new System.Drawing.Point(211, 27);
            this.edXMLLocation.Margin = new System.Windows.Forms.Padding(2);
            this.edXMLLocation.Name = "edXMLLocation";
            this.edXMLLocation.Size = new System.Drawing.Size(329, 20);
            this.edXMLLocation.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 31);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "CTC Adapter Output files";
            // 
            // tbEmail
            // 
            this.tbEmail.Controls.Add(this.edAlerts);
            this.tbEmail.Controls.Add(this.label16);
            this.tbEmail.Controls.Add(this.edSMTPPassword);
            this.tbEmail.Controls.Add(this.label15);
            this.tbEmail.Controls.Add(this.edSMTPUsername);
            this.tbEmail.Controls.Add(this.label14);
            this.tbEmail.Controls.Add(this.edEmail);
            this.tbEmail.Controls.Add(this.label13);
            this.tbEmail.Controls.Add(this.edSMTPPort);
            this.tbEmail.Controls.Add(this.label12);
            this.tbEmail.Controls.Add(this.edSMTPServer);
            this.tbEmail.Controls.Add(this.label11);
            this.tbEmail.Location = new System.Drawing.Point(4, 22);
            this.tbEmail.Margin = new System.Windows.Forms.Padding(2);
            this.tbEmail.Name = "tbEmail";
            this.tbEmail.Padding = new System.Windows.Forms.Padding(2);
            this.tbEmail.Size = new System.Drawing.Size(587, 340);
            this.tbEmail.TabIndex = 2;
            this.tbEmail.Text = "Email Settings";
            this.tbEmail.UseVisualStyleBackColor = true;
            // 
            // edAlerts
            // 
            this.edAlerts.Location = new System.Drawing.Point(103, 192);
            this.edAlerts.Margin = new System.Windows.Forms.Padding(2);
            this.edAlerts.Name = "edAlerts";
            this.edAlerts.Size = new System.Drawing.Size(250, 20);
            this.edAlerts.TabIndex = 11;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(27, 193);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(77, 13);
            this.label16.TabIndex = 10;
            this.label16.Text = "Send Errors to:";
            // 
            // edSMTPPassword
            // 
            this.edSMTPPassword.Location = new System.Drawing.Point(103, 156);
            this.edSMTPPassword.Margin = new System.Windows.Forms.Padding(2);
            this.edSMTPPassword.Name = "edSMTPPassword";
            this.edSMTPPassword.Size = new System.Drawing.Size(250, 20);
            this.edSMTPPassword.TabIndex = 9;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(27, 159);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 13);
            this.label15.TabIndex = 8;
            this.label15.Text = "Password";
            // 
            // edSMTPUsername
            // 
            this.edSMTPUsername.Location = new System.Drawing.Point(103, 121);
            this.edSMTPUsername.Margin = new System.Windows.Forms.Padding(2);
            this.edSMTPUsername.Name = "edSMTPUsername";
            this.edSMTPUsername.Size = new System.Drawing.Size(250, 20);
            this.edSMTPUsername.TabIndex = 7;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(27, 123);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Username";
            // 
            // edEmail
            // 
            this.edEmail.Location = new System.Drawing.Point(103, 86);
            this.edEmail.Margin = new System.Windows.Forms.Padding(2);
            this.edEmail.Name = "edEmail";
            this.edEmail.Size = new System.Drawing.Size(250, 20);
            this.edEmail.TabIndex = 5;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(27, 87);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Email Address";
            // 
            // edSMTPPort
            // 
            this.edSMTPPort.Location = new System.Drawing.Point(103, 51);
            this.edSMTPPort.Margin = new System.Windows.Forms.Padding(2);
            this.edSMTPPort.Name = "edSMTPPort";
            this.edSMTPPort.Size = new System.Drawing.Size(63, 20);
            this.edSMTPPort.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(27, 51);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "SMTP Port";
            // 
            // edSMTPServer
            // 
            this.edSMTPServer.Location = new System.Drawing.Point(103, 16);
            this.edSMTPServer.Margin = new System.Windows.Forms.Padding(2);
            this.edSMTPServer.Name = "edSMTPServer";
            this.edSMTPServer.Size = new System.Drawing.Size(414, 20);
            this.edSMTPServer.TabIndex = 1;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 16);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "SMTP Server";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.bbPDF2Xml);
            this.tabPage1.Controls.Add(this.edPdf2XMLLoc);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(587, 340);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "PDF 2 Xml Satellite";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // bbPDF2Xml
            // 
            this.bbPDF2Xml.Location = new System.Drawing.Point(534, 19);
            this.bbPDF2Xml.Margin = new System.Windows.Forms.Padding(2);
            this.bbPDF2Xml.Name = "bbPDF2Xml";
            this.bbPDF2Xml.Size = new System.Drawing.Size(25, 20);
            this.bbPDF2Xml.TabIndex = 32;
            this.bbPDF2Xml.Text = "...";
            this.bbPDF2Xml.UseVisualStyleBackColor = true;
            this.bbPDF2Xml.Click += new System.EventHandler(this.bbPDF2Xml_Click);
            // 
            // edPdf2XMLLoc
            // 
            this.edPdf2XMLLoc.Location = new System.Drawing.Point(209, 19);
            this.edPdf2XMLLoc.Margin = new System.Windows.Forms.Padding(2);
            this.edPdf2XMLLoc.Name = "edPdf2XMLLoc";
            this.edPdf2XMLLoc.Size = new System.Drawing.Size(329, 20);
            this.edPdf2XMLLoc.TabIndex = 31;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 23);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(156, 13);
            this.label8.TabIndex = 30;
            this.label8.Text = "PDF 2 Xml Processing Location";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(478, 399);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(50, 23);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnArchiveLoc
            // 
            this.btnArchiveLoc.Location = new System.Drawing.Point(537, 178);
            this.btnArchiveLoc.Margin = new System.Windows.Forms.Padding(2);
            this.btnArchiveLoc.Name = "btnArchiveLoc";
            this.btnArchiveLoc.Size = new System.Drawing.Size(25, 20);
            this.btnArchiveLoc.TabIndex = 29;
            this.btnArchiveLoc.Text = "...";
            this.btnArchiveLoc.UseVisualStyleBackColor = true;
            this.btnArchiveLoc.Click += new System.EventHandler(this.btnArchiveLoc_Click);
            // 
            // txtArchiveLoc
            // 
            this.txtArchiveLoc.Location = new System.Drawing.Point(212, 178);
            this.txtArchiveLoc.Margin = new System.Windows.Forms.Padding(2);
            this.txtArchiveLoc.Name = "txtArchiveLoc";
            this.txtArchiveLoc.Size = new System.Drawing.Size(329, 20);
            this.txtArchiveLoc.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 182);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 27;
            this.label10.Text = "Archive Location";
            // 
            // FrmSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(610, 430);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tcSettings);
            this.Controls.Add(this.btnClose);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmSettings";
            this.Text = "System Settings";
            this.Load += new System.EventHandler(this.FrmSettings_Load);
            this.tcSettings.ResumeLayout(false);
            this.tbSettings.ResumeLayout(false);
            this.tbSettings.PerformLayout();
            this.tbData.ResumeLayout(false);
            this.tbData.PerformLayout();
            this.tbTransport.ResumeLayout(false);
            this.tbTransport.PerformLayout();
            this.tbSecurity.ResumeLayout(false);
            this.tbSecurity.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numGuardian)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numPollingTime)).EndInit();
            this.tbEmail.ResumeLayout(false);
            this.tbEmail.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TabControl tcSettings;
        private System.Windows.Forms.TabPage tbData;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCTCTestConnection;
        private System.Windows.Forms.TextBox edCTCPassword;
        private System.Windows.Forms.TextBox edCTCUserName;
        private System.Windows.Forms.TextBox edCTCDatabase;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox edCTCServer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tbSecurity;
        private System.Windows.Forms.TextBox edCTCResults;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnDirXml;
        private System.Windows.Forms.TextBox edXMLLocation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FolderBrowserDialog fdInLocation;
        private System.Windows.Forms.Button bbCustRoot;
        private System.Windows.Forms.TextBox edCustRoot;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tbEmail;
        private System.Windows.Forms.TextBox edSMTPPassword;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox edSMTPUsername;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox edEmail;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox edSMTPPort;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox edSMTPServer;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox edAlerts;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button bbCustomReceive;
        private System.Windows.Forms.TextBox edCustomReceive;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnFailedLoc;
        private System.Windows.Forms.TextBox edFailedLoc;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabPage tbTransport;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnTransTest;
        private System.Windows.Forms.TextBox edTransPassword;
        private System.Windows.Forms.TextBox edTransUserName;
        private System.Windows.Forms.TextBox edTransDatabase;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox edTransServer;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.RichTextBox rtbTransResults;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox edTestingLocation;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.NumericUpDown numPollingTime;
        private System.Windows.Forms.NumericUpDown numGuardian;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TabPage tbSettings;
        private System.Windows.Forms.TextBox edMainTimer;
        private System.Windows.Forms.TextBox edCyclecount;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button btnWinscpLocaiton;
        private System.Windows.Forms.TextBox edWinscpLocation;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button bbPDF2Xml;
        private System.Windows.Forms.TextBox edPdf2XMLLoc;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnArchiveLoc;
        private System.Windows.Forms.TextBox txtArchiveLoc;
        private System.Windows.Forms.Label label10;
    }
}