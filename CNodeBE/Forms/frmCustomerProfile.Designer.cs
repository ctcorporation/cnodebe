﻿namespace CNodeBE
{
    partial class FrmCustomerProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCustomerProfile));
            this.btnClose = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pnlProfile = new System.Windows.Forms.Panel();
            this.cbHideInactive = new System.Windows.Forms.CheckBox();
            this.btnNewProfile = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.tcMessageProfile = new System.Windows.Forms.TabControl();
            this.tbMessageHeader = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cmbOutboundType = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.cmbMsgType = new System.Windows.Forms.ComboBox();
            this.cmbOutboundProf = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.cbIgnoreMessages = new System.Windows.Forms.CheckBox();
            this.cbRenameFile = new System.Windows.Forms.CheckBox();
            this.cbNotify = new System.Windows.Forms.CheckBox();
            this.cbGroupCharges = new System.Windows.Forms.CheckBox();
            this.cbProfileActive = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.edCWAppCode = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.edCWFileName = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.edCWSubject = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.edEventCode = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.cbDTS = new System.Windows.Forms.CheckBox();
            this.edReasonCode = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.edSenderID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.edRecipientID = new System.Windows.Forms.TextBox();
            this.gbDirection = new System.Windows.Forms.GroupBox();
            this.rbReceive = new System.Windows.Forms.RadioButton();
            this.rbSend = new System.Windows.Forms.RadioButton();
            this.edDescription = new System.Windows.Forms.TextBox();
            this.cbChargeable = new System.Windows.Forms.CheckBox();
            this.grpBillto = new System.Windows.Forms.GroupBox();
            this.rbVendor = new System.Windows.Forms.RadioButton();
            this.rbCustomer = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.tbRecieveProperties = new System.Windows.Forms.TabPage();
            this.gbReceiveOptions = new System.Windows.Forms.GroupBox();
            this.btnReceiveKeyPath = new System.Windows.Forms.Button();
            this.edReceiveCustomerName = new System.Windows.Forms.TextBox();
            this.lblReceiveCustomerName = new System.Windows.Forms.Label();
            this.btnReceiveFolder = new System.Windows.Forms.Button();
            this.txtTestResults = new System.Windows.Forms.TextBox();
            this.lblRxResults = new System.Windows.Forms.Label();
            this.bbTestRx = new System.Windows.Forms.Button();
            this.edReceiveSubject = new System.Windows.Forms.TextBox();
            this.lblReceiveSubject = new System.Windows.Forms.Label();
            this.edReceiveSender = new System.Windows.Forms.TextBox();
            this.lblSenderEmail = new System.Windows.Forms.Label();
            this.cbReceiveSSL = new System.Windows.Forms.CheckBox();
            this.cmbRxFileType = new System.Windows.Forms.ComboBox();
            this.lblRxFileType = new System.Windows.Forms.Label();
            this.edReceiveEmailAddress = new System.Windows.Forms.TextBox();
            this.lblReceiveEmailAddress = new System.Windows.Forms.Label();
            this.gbReceiveMethod = new System.Windows.Forms.GroupBox();
            this.rbReceiveSOAP = new System.Windows.Forms.RadioButton();
            this.rbReceivePickup = new System.Windows.Forms.RadioButton();
            this.rbReceiveEmail = new System.Windows.Forms.RadioButton();
            this.rbReceiveFTP = new System.Windows.Forms.RadioButton();
            this.rbPickup = new System.Windows.Forms.RadioButton();
            this.edFTPReceivePath = new System.Windows.Forms.TextBox();
            this.lblFTPReceivePath = new System.Windows.Forms.Label();
            this.edFTPReceivePort = new System.Windows.Forms.TextBox();
            this.lblFTPReceivePort = new System.Windows.Forms.Label();
            this.edReceivePassword = new System.Windows.Forms.TextBox();
            this.lblReceivePassword = new System.Windows.Forms.Label();
            this.edReceiveUsername = new System.Windows.Forms.TextBox();
            this.lblReceiveUsername = new System.Windows.Forms.Label();
            this.edServerAddress = new System.Windows.Forms.TextBox();
            this.lblServerAddress = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbSendProperties = new System.Windows.Forms.TabPage();
            this.gbSendOptions = new System.Windows.Forms.GroupBox();
            this.cbForwardWithFlags = new System.Windows.Forms.CheckBox();
            this.edSendFileName = new System.Windows.Forms.TextBox();
            this.lblSendFileNameExpected = new System.Windows.Forms.Label();
            this.cmbSendFileType = new System.Windows.Forms.ComboBox();
            this.lblSendFileType = new System.Windows.Forms.Label();
            this.btnSendKeyPath = new System.Windows.Forms.Button();
            this.cbSendSSL = new System.Windows.Forms.CheckBox();
            this.btnFTPTest = new System.Windows.Forms.Button();
            this.btnPickupFolder = new System.Windows.Forms.Button();
            this.edSendSubject = new System.Windows.Forms.TextBox();
            this.lblSendSubject = new System.Windows.Forms.Label();
            this.edSendEmailAddress = new System.Windows.Forms.TextBox();
            this.lblSendEmailAddress = new System.Windows.Forms.Label();
            this.edSendFtpPort = new System.Windows.Forms.TextBox();
            this.lblSendFTPPort = new System.Windows.Forms.Label();
            this.btnTestSendeAdapter = new System.Windows.Forms.Button();
            this.edSendeAdapterResults = new System.Windows.Forms.TextBox();
            this.lblTestResults = new System.Windows.Forms.Label();
            this.edSendFolder = new System.Windows.Forms.TextBox();
            this.gbSendMethod = new System.Windows.Forms.GroupBox();
            this.rbSoapSend = new System.Windows.Forms.RadioButton();
            this.rbFTPSendDirect = new System.Windows.Forms.RadioButton();
            this.rbEmailSend = new System.Windows.Forms.RadioButton();
            this.rbFTPSend = new System.Windows.Forms.RadioButton();
            this.rbCTCSend = new System.Windows.Forms.RadioButton();
            this.rbPickupSend = new System.Windows.Forms.RadioButton();
            this.lblSendPath = new System.Windows.Forms.Label();
            this.edSendPassword = new System.Windows.Forms.TextBox();
            this.lblSendPassword = new System.Windows.Forms.Label();
            this.edSendUsername = new System.Windows.Forms.TextBox();
            this.lblSendUsername = new System.Windows.Forms.Label();
            this.edSendCTCUrl = new System.Windows.Forms.TextBox();
            this.lblSendServer = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tbConversion = new System.Windows.Forms.TabPage();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbCustomMethod = new System.Windows.Forms.ComboBox();
            this.lblLegacyConversion = new System.Windows.Forms.Label();
            this.lblConCommon = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmbToMethodName = new System.Windows.Forms.ComboBox();
            this.cmbToMethod = new System.Windows.Forms.ComboBox();
            this.lblToMethodName = new System.Windows.Forms.Label();
            this.lblConToMethod = new System.Windows.Forms.Label();
            this.panelConFrom = new System.Windows.Forms.Panel();
            this.cmbFromMethodName = new System.Windows.Forms.ComboBox();
            this.cmbFromMethod = new System.Windows.Forms.ComboBox();
            this.lblConFromMethod = new System.Windows.Forms.Label();
            this.lblFromMethodName = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.btnAddParam = new System.Windows.Forms.Button();
            this.lbParameters = new System.Windows.Forms.ListBox();
            this.cmParameterList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.removeParameterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label34 = new System.Windows.Forms.Label();
            this.btnLoadXSD = new System.Windows.Forms.Button();
            this.btnXsdPath = new System.Windows.Forms.Button();
            this.edLibraryName = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.edXSDPath = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tdXDS = new System.Windows.Forms.TabPage();
            this.label11 = new System.Windows.Forms.Label();
            this.ddlCWContext = new System.Windows.Forms.ComboBox();
            this.lblXSDDataResult = new System.Windows.Forms.Label();
            this.tvwXSD = new System.Windows.Forms.TreeView();
            this.tbDTS = new System.Windows.Forms.TabPage();
            this.gbDTS = new System.Windows.Forms.GroupBox();
            this.edDTSCurrentValue = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.edDTSValue = new System.Windows.Forms.TextBox();
            this.edDTSTarget = new System.Windows.Forms.TextBox();
            this.edDTSSearch = new System.Windows.Forms.TextBox();
            this.edDTSQual = new System.Windows.Forms.TextBox();
            this.bbSave = new System.Windows.Forms.Button();
            this.dgDts = new System.Windows.Forms.DataGridView();
            this.DTSIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTSType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTSSearch = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTSModifies = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTSFileType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTSQual = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTSTarget = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTSNewValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbModifies = new System.Windows.Forms.GroupBox();
            this.rbDTSStructure = new System.Windows.Forms.RadioButton();
            this.rbDTSRecord = new System.Windows.Forms.RadioButton();
            this.ddlDTSFileType = new System.Windows.Forms.ComboBox();
            this.ddlDTSType = new System.Windows.Forms.ComboBox();
            this.edDTSIndex = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.dgProfile = new System.Windows.Forms.DataGridView();
            this.P_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Active = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.P_DESCRIPTION = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.P_SENDERID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.P_Recipientid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.P_REASONCODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Direction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmProfiles = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.duplicateProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.markInactiveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAdd = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.ssMain = new System.Windows.Forms.StatusStrip();
            this.sbMain = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnNewCustomer = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbCustomer = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cbActive = new System.Windows.Forms.CheckBox();
            this.edCustomerName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.fdProfile = new System.Windows.Forms.FolderBrowserDialog();
            this.tcCustomer = new System.Windows.Forms.TabControl();
            this.tbCustomer = new System.Windows.Forms.TabPage();
            this.lblWarningPath = new System.Windows.Forms.Label();
            this.btnRootPath = new System.Windows.Forms.Button();
            this.lblActualPath = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.cbSatellite = new System.Windows.Forms.CheckBox();
            this.edRootPath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.edCustomerCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbAccounting = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.grdCustomerModules = new System.Windows.Forms.DataGridView();
            this.ModuleID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ModuleName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.edModuleDescription = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.btnAddModule = new System.Windows.Forms.Button();
            this.ddlModuleListing = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.cbInvoice = new System.Windows.Forms.CheckBox();
            this.cbTrial = new System.Windows.Forms.CheckBox();
            this.cbHold = new System.Windows.Forms.CheckBox();
            this.gbTrialPeriod = new System.Windows.Forms.GroupBox();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.label31 = new System.Windows.Forms.Label();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.edShortName = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tbSatellite = new System.Windows.Forms.TabPage();
            this.cbSatelliteMonitored = new System.Windows.Forms.CheckBox();
            this.btnRestartSatellite = new System.Windows.Forms.Button();
            this.LBLpid = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.edSatelliteName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.pnlProfile.SuspendLayout();
            this.tcMessageProfile.SuspendLayout();
            this.tbMessageHeader.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbDirection.SuspendLayout();
            this.grpBillto.SuspendLayout();
            this.tbRecieveProperties.SuspendLayout();
            this.gbReceiveOptions.SuspendLayout();
            this.gbReceiveMethod.SuspendLayout();
            this.tbSendProperties.SuspendLayout();
            this.gbSendOptions.SuspendLayout();
            this.gbSendMethod.SuspendLayout();
            this.tbConversion.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelConFrom.SuspendLayout();
            this.cmParameterList.SuspendLayout();
            this.tdXDS.SuspendLayout();
            this.tbDTS.SuspendLayout();
            this.gbDTS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDts)).BeginInit();
            this.gbModifies.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProfile)).BeginInit();
            this.cmProfiles.SuspendLayout();
            this.ssMain.SuspendLayout();
            this.tcCustomer.SuspendLayout();
            this.tbCustomer.SuspendLayout();
            this.tbAccounting.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCustomerModules)).BeginInit();
            this.gbTrialPeriod.SuspendLayout();
            this.tbSatellite.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Location = new System.Drawing.Point(1235, 903);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(62, 22);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(104, 328);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(5, 5);
            this.button1.TabIndex = 11;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // pnlProfile
            // 
            this.pnlProfile.Controls.Add(this.cbHideInactive);
            this.pnlProfile.Controls.Add(this.btnNewProfile);
            this.pnlProfile.Controls.Add(this.label29);
            this.pnlProfile.Controls.Add(this.label28);
            this.pnlProfile.Controls.Add(this.tcMessageProfile);
            this.pnlProfile.Controls.Add(this.dgProfile);
            this.pnlProfile.Controls.Add(this.btnAdd);
            this.pnlProfile.Location = new System.Drawing.Point(14, 269);
            this.pnlProfile.Margin = new System.Windows.Forms.Padding(2);
            this.pnlProfile.Name = "pnlProfile";
            this.pnlProfile.Size = new System.Drawing.Size(1118, 435);
            this.pnlProfile.TabIndex = 12;
            // 
            // cbHideInactive
            // 
            this.cbHideInactive.AutoSize = true;
            this.cbHideInactive.Checked = true;
            this.cbHideInactive.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbHideInactive.Location = new System.Drawing.Point(669, 6);
            this.cbHideInactive.Name = "cbHideInactive";
            this.cbHideInactive.Size = new System.Drawing.Size(92, 17);
            this.cbHideInactive.TabIndex = 31;
            this.cbHideInactive.Text = "Hide Disabled";
            this.cbHideInactive.UseVisualStyleBackColor = true;
            this.cbHideInactive.CheckedChanged += new System.EventHandler(this.CbHideInactive_CheckedChanged);
            // 
            // btnNewProfile
            // 
            this.btnNewProfile.Location = new System.Drawing.Point(483, 400);
            this.btnNewProfile.Margin = new System.Windows.Forms.Padding(2);
            this.btnNewProfile.Name = "btnNewProfile";
            this.btnNewProfile.Size = new System.Drawing.Size(77, 24);
            this.btnNewProfile.TabIndex = 30;
            this.btnNewProfile.Text = "&New";
            this.btnNewProfile.UseVisualStyleBackColor = true;
            this.btnNewProfile.Click += new System.EventHandler(this.btnNewProfile_Click);
            // 
            // label29
            // 
            this.label29.BackColor = System.Drawing.Color.SkyBlue;
            this.label29.Location = new System.Drawing.Point(779, 404);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(96, 20);
            this.label29.TabIndex = 29;
            this.label29.Text = "Send Profile";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.BackColor = System.Drawing.Color.Green;
            this.label28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label28.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label28.Location = new System.Drawing.Point(669, 404);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(96, 20);
            this.label28.TabIndex = 28;
            this.label28.Text = "Receive Profile";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tcMessageProfile
            // 
            this.tcMessageProfile.Controls.Add(this.tbMessageHeader);
            this.tcMessageProfile.Controls.Add(this.tbRecieveProperties);
            this.tcMessageProfile.Controls.Add(this.tbSendProperties);
            this.tcMessageProfile.Controls.Add(this.tbConversion);
            this.tcMessageProfile.Controls.Add(this.tdXDS);
            this.tcMessageProfile.Controls.Add(this.tbDTS);
            this.tcMessageProfile.Location = new System.Drawing.Point(13, 6);
            this.tcMessageProfile.Name = "tcMessageProfile";
            this.tcMessageProfile.SelectedIndex = 0;
            this.tcMessageProfile.Size = new System.Drawing.Size(642, 386);
            this.tcMessageProfile.TabIndex = 27;
            // 
            // tbMessageHeader
            // 
            this.tbMessageHeader.Controls.Add(this.groupBox4);
            this.tbMessageHeader.Controls.Add(this.cbIgnoreMessages);
            this.tbMessageHeader.Controls.Add(this.cbRenameFile);
            this.tbMessageHeader.Controls.Add(this.cbNotify);
            this.tbMessageHeader.Controls.Add(this.cbGroupCharges);
            this.tbMessageHeader.Controls.Add(this.cbProfileActive);
            this.tbMessageHeader.Controls.Add(this.groupBox1);
            this.tbMessageHeader.Controls.Add(this.gbDirection);
            this.tbMessageHeader.Controls.Add(this.edDescription);
            this.tbMessageHeader.Controls.Add(this.cbChargeable);
            this.tbMessageHeader.Controls.Add(this.grpBillto);
            this.tbMessageHeader.Controls.Add(this.label6);
            this.tbMessageHeader.Location = new System.Drawing.Point(4, 22);
            this.tbMessageHeader.Name = "tbMessageHeader";
            this.tbMessageHeader.Padding = new System.Windows.Forms.Padding(3);
            this.tbMessageHeader.Size = new System.Drawing.Size(634, 360);
            this.tbMessageHeader.TabIndex = 0;
            this.tbMessageHeader.Text = "Profile Properties";
            this.tbMessageHeader.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cmbOutboundType);
            this.groupBox4.Controls.Add(this.label35);
            this.groupBox4.Controls.Add(this.cmbMsgType);
            this.groupBox4.Controls.Add(this.cmbOutboundProf);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Location = new System.Drawing.Point(14, 231);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox4.Size = new System.Drawing.Size(394, 108);
            this.groupBox4.TabIndex = 37;
            this.groupBox4.TabStop = false;
            // 
            // cmbOutboundType
            // 
            this.cmbOutboundType.FormattingEnabled = true;
            this.cmbOutboundType.Items.AddRange(new object[] {
            "Individual",
            "Bulk"});
            this.cmbOutboundType.Location = new System.Drawing.Point(93, 80);
            this.cmbOutboundType.Margin = new System.Windows.Forms.Padding(2);
            this.cmbOutboundType.Name = "cmbOutboundType";
            this.cmbOutboundType.Size = new System.Drawing.Size(205, 21);
            this.cmbOutboundType.TabIndex = 38;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(-2, 82);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(81, 13);
            this.label35.TabIndex = 37;
            this.label35.Text = "Outbound Type";
            this.label35.Click += new System.EventHandler(this.label35_Click);
            // 
            // cmbMsgType
            // 
            this.cmbMsgType.FormattingEnabled = true;
            this.cmbMsgType.Items.AddRange(new object[] {
            "(none selected)",
            "Original",
            "Response",
            "Forward",
            "eAdapter",
            "Conversion",
            "PDF2XML"});
            this.cmbMsgType.Location = new System.Drawing.Point(93, 9);
            this.cmbMsgType.Name = "cmbMsgType";
            this.cmbMsgType.Size = new System.Drawing.Size(205, 21);
            this.cmbMsgType.TabIndex = 6;
            // 
            // cmbOutboundProf
            // 
            this.cmbOutboundProf.FormattingEnabled = true;
            this.cmbOutboundProf.Location = new System.Drawing.Point(93, 43);
            this.cmbOutboundProf.Margin = new System.Windows.Forms.Padding(2);
            this.cmbOutboundProf.Name = "cmbOutboundProf";
            this.cmbOutboundProf.Size = new System.Drawing.Size(205, 21);
            this.cmbOutboundProf.TabIndex = 36;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(-2, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Message Type";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(-2, 46);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(86, 13);
            this.label33.TabIndex = 35;
            this.label33.Text = "Outbound Profile";
            this.label33.Click += new System.EventHandler(this.label33_Click);
            // 
            // cbIgnoreMessages
            // 
            this.cbIgnoreMessages.AutoSize = true;
            this.cbIgnoreMessages.Location = new System.Drawing.Point(418, 164);
            this.cbIgnoreMessages.Name = "cbIgnoreMessages";
            this.cbIgnoreMessages.Size = new System.Drawing.Size(118, 17);
            this.cbIgnoreMessages.TabIndex = 34;
            this.cbIgnoreMessages.Text = "Ignore Profile Errors";
            this.cbIgnoreMessages.UseVisualStyleBackColor = true;
            // 
            // cbRenameFile
            // 
            this.cbRenameFile.AutoSize = true;
            this.cbRenameFile.Location = new System.Drawing.Point(418, 225);
            this.cbRenameFile.Name = "cbRenameFile";
            this.cbRenameFile.Size = new System.Drawing.Size(116, 17);
            this.cbRenameFile.TabIndex = 33;
            this.cbRenameFile.Text = "Rename to Original";
            this.cbRenameFile.UseVisualStyleBackColor = true;
            // 
            // cbNotify
            // 
            this.cbNotify.AutoSize = true;
            this.cbNotify.Location = new System.Drawing.Point(418, 204);
            this.cbNotify.Name = "cbNotify";
            this.cbNotify.Size = new System.Drawing.Size(102, 17);
            this.cbNotify.TabIndex = 32;
            this.cbNotify.Text = "Notify on Failure";
            this.cbNotify.UseVisualStyleBackColor = true;
            // 
            // cbGroupCharges
            // 
            this.cbGroupCharges.AutoSize = true;
            this.cbGroupCharges.Location = new System.Drawing.Point(418, 183);
            this.cbGroupCharges.Name = "cbGroupCharges";
            this.cbGroupCharges.Size = new System.Drawing.Size(97, 17);
            this.cbGroupCharges.TabIndex = 31;
            this.cbGroupCharges.Text = "Group Charges";
            this.cbGroupCharges.UseVisualStyleBackColor = true;
            // 
            // cbProfileActive
            // 
            this.cbProfileActive.AutoSize = true;
            this.cbProfileActive.Location = new System.Drawing.Point(418, 145);
            this.cbProfileActive.Name = "cbProfileActive";
            this.cbProfileActive.Size = new System.Drawing.Size(67, 17);
            this.cbProfileActive.TabIndex = 30;
            this.cbProfileActive.Text = "Is Active";
            this.cbProfileActive.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.edEventCode);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.cbDTS);
            this.groupBox1.Controls.Add(this.edReasonCode);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.edSenderID);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.edRecipientID);
            this.groupBox1.Location = new System.Drawing.Point(14, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(394, 195);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cargowise XML Details";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.PowderBlue;
            this.groupBox2.Controls.Add(this.edCWAppCode);
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this.edCWFileName);
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Controls.Add(this.edCWSubject);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Location = new System.Drawing.Point(2, 78);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(392, 114);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cargowise Flags";
            // 
            // edCWAppCode
            // 
            this.edCWAppCode.Location = new System.Drawing.Point(124, 81);
            this.edCWAppCode.Name = "edCWAppCode";
            this.edCWAppCode.Size = new System.Drawing.Size(56, 20);
            this.edCWAppCode.TabIndex = 41;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(12, 84);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(106, 13);
            this.label40.TabIndex = 40;
            this.label40.Text = "Cargowise App Code";
            // 
            // edCWFileName
            // 
            this.edCWFileName.Location = new System.Drawing.Point(93, 51);
            this.edCWFileName.Name = "edCWFileName";
            this.edCWFileName.Size = new System.Drawing.Size(293, 20);
            this.edCWFileName.TabIndex = 39;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(9, 23);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(43, 13);
            this.label38.TabIndex = 36;
            this.label38.Text = "Subject";
            // 
            // edCWSubject
            // 
            this.edCWSubject.Location = new System.Drawing.Point(56, 20);
            this.edCWSubject.Name = "edCWSubject";
            this.edCWSubject.Size = new System.Drawing.Size(330, 20);
            this.edCWSubject.TabIndex = 37;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(12, 54);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(75, 13);
            this.label39.TabIndex = 38;
            this.label39.Text = "CW File Name";
            // 
            // edEventCode
            // 
            this.edEventCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edEventCode.Location = new System.Drawing.Point(247, 52);
            this.edEventCode.Margin = new System.Windows.Forms.Padding(2);
            this.edEventCode.Name = "edEventCode";
            this.edEventCode.Size = new System.Drawing.Size(40, 20);
            this.edEventCode.TabIndex = 34;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(180, 55);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(63, 13);
            this.label32.TabIndex = 35;
            this.label32.Text = "Event Code";
            // 
            // cbDTS
            // 
            this.cbDTS.AutoSize = true;
            this.cbDTS.Location = new System.Drawing.Point(300, 54);
            this.cbDTS.Name = "cbDTS";
            this.cbDTS.Size = new System.Drawing.Size(94, 17);
            this.cbDTS.TabIndex = 33;
            this.cbDTS.Text = "DTS Required";
            this.cbDTS.UseVisualStyleBackColor = true;
            // 
            // edReasonCode
            // 
            this.edReasonCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edReasonCode.Location = new System.Drawing.Point(132, 51);
            this.edReasonCode.Margin = new System.Windows.Forms.Padding(2);
            this.edReasonCode.Name = "edReasonCode";
            this.edReasonCode.Size = new System.Drawing.Size(40, 20);
            this.edReasonCode.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 55);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "Message Purpose Code";
            // 
            // edSenderID
            // 
            this.edSenderID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edSenderID.Location = new System.Drawing.Point(73, 23);
            this.edSenderID.Margin = new System.Windows.Forms.Padding(2);
            this.edSenderID.Name = "edSenderID";
            this.edSenderID.Size = new System.Drawing.Size(103, 20);
            this.edSenderID.TabIndex = 27;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(180, 25);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 28;
            this.label5.Text = "Recipient ID ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(8, 26);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 30;
            this.label10.Text = "Sender ID";
            // 
            // edRecipientID
            // 
            this.edRecipientID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edRecipientID.Location = new System.Drawing.Point(252, 23);
            this.edRecipientID.Margin = new System.Windows.Forms.Padding(2);
            this.edRecipientID.Name = "edRecipientID";
            this.edRecipientID.Size = new System.Drawing.Size(124, 20);
            this.edRecipientID.TabIndex = 29;
            // 
            // gbDirection
            // 
            this.gbDirection.Controls.Add(this.rbReceive);
            this.gbDirection.Controls.Add(this.rbSend);
            this.gbDirection.Location = new System.Drawing.Point(408, 33);
            this.gbDirection.Name = "gbDirection";
            this.gbDirection.Size = new System.Drawing.Size(80, 85);
            this.gbDirection.TabIndex = 27;
            this.gbDirection.TabStop = false;
            this.gbDirection.Text = "Direction";
            // 
            // rbReceive
            // 
            this.rbReceive.AutoSize = true;
            this.rbReceive.Location = new System.Drawing.Point(6, 19);
            this.rbReceive.Name = "rbReceive";
            this.rbReceive.Size = new System.Drawing.Size(65, 17);
            this.rbReceive.TabIndex = 4;
            this.rbReceive.TabStop = true;
            this.rbReceive.Text = "Receive";
            this.rbReceive.UseVisualStyleBackColor = true;
            this.rbReceive.CheckedChanged += new System.EventHandler(this.rbReceive_CheckedChanged);
            // 
            // rbSend
            // 
            this.rbSend.AutoSize = true;
            this.rbSend.Location = new System.Drawing.Point(6, 42);
            this.rbSend.Name = "rbSend";
            this.rbSend.Size = new System.Drawing.Size(50, 17);
            this.rbSend.TabIndex = 5;
            this.rbSend.TabStop = true;
            this.rbSend.Text = "Send";
            this.rbSend.UseVisualStyleBackColor = true;
            this.rbSend.CheckedChanged += new System.EventHandler(this.rbSend_CheckedChanged);
            // 
            // edDescription
            // 
            this.edDescription.Location = new System.Drawing.Point(87, 8);
            this.edDescription.Margin = new System.Windows.Forms.Padding(2);
            this.edDescription.Name = "edDescription";
            this.edDescription.Size = new System.Drawing.Size(437, 20);
            this.edDescription.TabIndex = 0;
            // 
            // cbChargeable
            // 
            this.cbChargeable.AutoSize = true;
            this.cbChargeable.Location = new System.Drawing.Point(418, 124);
            this.cbChargeable.Name = "cbChargeable";
            this.cbChargeable.Size = new System.Drawing.Size(126, 17);
            this.cbChargeable.TabIndex = 7;
            this.cbChargeable.Text = "Chargeable Message";
            this.cbChargeable.UseVisualStyleBackColor = true;
            // 
            // grpBillto
            // 
            this.grpBillto.Controls.Add(this.rbVendor);
            this.grpBillto.Controls.Add(this.rbCustomer);
            this.grpBillto.Location = new System.Drawing.Point(487, 33);
            this.grpBillto.Name = "grpBillto";
            this.grpBillto.Size = new System.Drawing.Size(96, 85);
            this.grpBillto.TabIndex = 8;
            this.grpBillto.TabStop = false;
            this.grpBillto.Text = "Bill To:";
            // 
            // rbVendor
            // 
            this.rbVendor.AutoSize = true;
            this.rbVendor.Location = new System.Drawing.Point(16, 41);
            this.rbVendor.Name = "rbVendor";
            this.rbVendor.Size = new System.Drawing.Size(59, 17);
            this.rbVendor.TabIndex = 1;
            this.rbVendor.Text = "Vendor";
            this.rbVendor.UseVisualStyleBackColor = true;
            // 
            // rbCustomer
            // 
            this.rbCustomer.AutoSize = true;
            this.rbCustomer.Checked = true;
            this.rbCustomer.Location = new System.Drawing.Point(16, 19);
            this.rbCustomer.Name = "rbCustomer";
            this.rbCustomer.Size = new System.Drawing.Size(69, 17);
            this.rbCustomer.TabIndex = 0;
            this.rbCustomer.TabStop = true;
            this.rbCustomer.Text = "Customer";
            this.rbCustomer.UseVisualStyleBackColor = true;
            this.rbCustomer.CheckedChanged += new System.EventHandler(this.RbCustomer_CheckedChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(15, 11);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Description";
            // 
            // tbRecieveProperties
            // 
            this.tbRecieveProperties.Controls.Add(this.gbReceiveOptions);
            this.tbRecieveProperties.Location = new System.Drawing.Point(4, 22);
            this.tbRecieveProperties.Name = "tbRecieveProperties";
            this.tbRecieveProperties.Padding = new System.Windows.Forms.Padding(3);
            this.tbRecieveProperties.Size = new System.Drawing.Size(634, 360);
            this.tbRecieveProperties.TabIndex = 1;
            this.tbRecieveProperties.Text = "Receive Properties";
            this.tbRecieveProperties.UseVisualStyleBackColor = true;
            // 
            // gbReceiveOptions
            // 
            this.gbReceiveOptions.Controls.Add(this.btnReceiveKeyPath);
            this.gbReceiveOptions.Controls.Add(this.edReceiveCustomerName);
            this.gbReceiveOptions.Controls.Add(this.lblReceiveCustomerName);
            this.gbReceiveOptions.Controls.Add(this.btnReceiveFolder);
            this.gbReceiveOptions.Controls.Add(this.txtTestResults);
            this.gbReceiveOptions.Controls.Add(this.lblRxResults);
            this.gbReceiveOptions.Controls.Add(this.bbTestRx);
            this.gbReceiveOptions.Controls.Add(this.edReceiveSubject);
            this.gbReceiveOptions.Controls.Add(this.lblReceiveSubject);
            this.gbReceiveOptions.Controls.Add(this.edReceiveSender);
            this.gbReceiveOptions.Controls.Add(this.lblSenderEmail);
            this.gbReceiveOptions.Controls.Add(this.cbReceiveSSL);
            this.gbReceiveOptions.Controls.Add(this.cmbRxFileType);
            this.gbReceiveOptions.Controls.Add(this.lblRxFileType);
            this.gbReceiveOptions.Controls.Add(this.edReceiveEmailAddress);
            this.gbReceiveOptions.Controls.Add(this.lblReceiveEmailAddress);
            this.gbReceiveOptions.Controls.Add(this.gbReceiveMethod);
            this.gbReceiveOptions.Controls.Add(this.edFTPReceivePath);
            this.gbReceiveOptions.Controls.Add(this.lblFTPReceivePath);
            this.gbReceiveOptions.Controls.Add(this.edFTPReceivePort);
            this.gbReceiveOptions.Controls.Add(this.lblFTPReceivePort);
            this.gbReceiveOptions.Controls.Add(this.edReceivePassword);
            this.gbReceiveOptions.Controls.Add(this.lblReceivePassword);
            this.gbReceiveOptions.Controls.Add(this.edReceiveUsername);
            this.gbReceiveOptions.Controls.Add(this.lblReceiveUsername);
            this.gbReceiveOptions.Controls.Add(this.edServerAddress);
            this.gbReceiveOptions.Controls.Add(this.lblServerAddress);
            this.gbReceiveOptions.Controls.Add(this.label8);
            this.gbReceiveOptions.Location = new System.Drawing.Point(9, 5);
            this.gbReceiveOptions.Margin = new System.Windows.Forms.Padding(2);
            this.gbReceiveOptions.Name = "gbReceiveOptions";
            this.gbReceiveOptions.Padding = new System.Windows.Forms.Padding(2);
            this.gbReceiveOptions.Size = new System.Drawing.Size(610, 350);
            this.gbReceiveOptions.TabIndex = 19;
            this.gbReceiveOptions.TabStop = false;
            this.gbReceiveOptions.Text = "Receive Options";
            // 
            // btnReceiveKeyPath
            // 
            this.btnReceiveKeyPath.Location = new System.Drawing.Point(517, 216);
            this.btnReceiveKeyPath.Name = "btnReceiveKeyPath";
            this.btnReceiveKeyPath.Size = new System.Drawing.Size(32, 23);
            this.btnReceiveKeyPath.TabIndex = 42;
            this.btnReceiveKeyPath.Text = "...";
            this.btnReceiveKeyPath.UseVisualStyleBackColor = true;
            this.btnReceiveKeyPath.Click += new System.EventHandler(this.btnReceiveKeyPath_Click);
            // 
            // edReceiveCustomerName
            // 
            this.edReceiveCustomerName.Location = new System.Drawing.Point(108, 143);
            this.edReceiveCustomerName.Margin = new System.Windows.Forms.Padding(2);
            this.edReceiveCustomerName.Name = "edReceiveCustomerName";
            this.edReceiveCustomerName.Size = new System.Drawing.Size(161, 20);
            this.edReceiveCustomerName.TabIndex = 10;
            // 
            // lblReceiveCustomerName
            // 
            this.lblReceiveCustomerName.AutoSize = true;
            this.lblReceiveCustomerName.Location = new System.Drawing.Point(14, 146);
            this.lblReceiveCustomerName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReceiveCustomerName.Name = "lblReceiveCustomerName";
            this.lblReceiveCustomerName.Size = new System.Drawing.Size(82, 13);
            this.lblReceiveCustomerName.TabIndex = 41;
            this.lblReceiveCustomerName.Text = "Customer Name";
            // 
            // btnReceiveFolder
            // 
            this.btnReceiveFolder.Location = new System.Drawing.Point(368, 89);
            this.btnReceiveFolder.Name = "btnReceiveFolder";
            this.btnReceiveFolder.Size = new System.Drawing.Size(25, 20);
            this.btnReceiveFolder.TabIndex = 39;
            this.btnReceiveFolder.Text = "...";
            this.btnReceiveFolder.UseVisualStyleBackColor = true;
            this.btnReceiveFolder.Click += new System.EventHandler(this.btnReceiveFolder_Click);
            // 
            // txtTestResults
            // 
            this.txtTestResults.Location = new System.Drawing.Point(109, 272);
            this.txtTestResults.Multiline = true;
            this.txtTestResults.Name = "txtTestResults";
            this.txtTestResults.Size = new System.Drawing.Size(408, 52);
            this.txtTestResults.TabIndex = 14;
            // 
            // lblRxResults
            // 
            this.lblRxResults.AutoSize = true;
            this.lblRxResults.Location = new System.Drawing.Point(17, 272);
            this.lblRxResults.Name = "lblRxResults";
            this.lblRxResults.Size = new System.Drawing.Size(80, 13);
            this.lblRxResults.TabIndex = 37;
            this.lblRxResults.Text = "Receive results";
            // 
            // bbTestRx
            // 
            this.bbTestRx.Location = new System.Drawing.Point(108, 243);
            this.bbTestRx.Name = "bbTestRx";
            this.bbTestRx.Size = new System.Drawing.Size(97, 23);
            this.bbTestRx.TabIndex = 9;
            this.bbTestRx.Text = "&Test Receive";
            this.bbTestRx.UseVisualStyleBackColor = true;
            this.bbTestRx.Click += new System.EventHandler(this.button3_Click);
            // 
            // edReceiveSubject
            // 
            this.edReceiveSubject.Location = new System.Drawing.Point(108, 217);
            this.edReceiveSubject.Margin = new System.Windows.Forms.Padding(2);
            this.edReceiveSubject.Name = "edReceiveSubject";
            this.edReceiveSubject.Size = new System.Drawing.Size(410, 20);
            this.edReceiveSubject.TabIndex = 13;
            // 
            // lblReceiveSubject
            // 
            this.lblReceiveSubject.AutoSize = true;
            this.lblReceiveSubject.Location = new System.Drawing.Point(14, 220);
            this.lblReceiveSubject.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReceiveSubject.Name = "lblReceiveSubject";
            this.lblReceiveSubject.Size = new System.Drawing.Size(91, 13);
            this.lblReceiveSubject.TabIndex = 35;
            this.lblReceiveSubject.Text = "Expected Subject";
            // 
            // edReceiveSender
            // 
            this.edReceiveSender.Location = new System.Drawing.Point(108, 193);
            this.edReceiveSender.Margin = new System.Windows.Forms.Padding(2);
            this.edReceiveSender.Name = "edReceiveSender";
            this.edReceiveSender.Size = new System.Drawing.Size(410, 20);
            this.edReceiveSender.TabIndex = 12;
            // 
            // lblSenderEmail
            // 
            this.lblSenderEmail.AutoSize = true;
            this.lblSenderEmail.Location = new System.Drawing.Point(14, 196);
            this.lblSenderEmail.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSenderEmail.Name = "lblSenderEmail";
            this.lblSenderEmail.Size = new System.Drawing.Size(69, 13);
            this.lblSenderEmail.TabIndex = 33;
            this.lblSenderEmail.Text = "Sender Email";
            // 
            // cbReceiveSSL
            // 
            this.cbReceiveSSL.AutoSize = true;
            this.cbReceiveSSL.Location = new System.Drawing.Point(525, 68);
            this.cbReceiveSSL.Name = "cbReceiveSSL";
            this.cbReceiveSSL.Size = new System.Drawing.Size(83, 17);
            this.cbReceiveSSL.TabIndex = 31;
            this.cbReceiveSSL.Text = "Secure FTP";
            this.cbReceiveSSL.UseVisualStyleBackColor = true;
            this.cbReceiveSSL.CheckedChanged += new System.EventHandler(this.cbReceiveSSL_CheckedChanged);
            // 
            // cmbRxFileType
            // 
            this.cmbRxFileType.FormattingEnabled = true;
            this.cmbRxFileType.Items.AddRange(new object[] {
            "*.*",
            ".TXT",
            ".XML",
            ".XLS",
            ".CSV",
            ".DAT",
            ".PDF",
            ".XLSX"});
            this.cmbRxFileType.Location = new System.Drawing.Point(448, 142);
            this.cmbRxFileType.Name = "cmbRxFileType";
            this.cmbRxFileType.Size = new System.Drawing.Size(64, 21);
            this.cmbRxFileType.TabIndex = 30;
            // 
            // lblRxFileType
            // 
            this.lblRxFileType.AutoSize = true;
            this.lblRxFileType.Location = new System.Drawing.Point(337, 146);
            this.lblRxFileType.Name = "lblRxFileType";
            this.lblRxFileType.Size = new System.Drawing.Size(98, 13);
            this.lblRxFileType.TabIndex = 29;
            this.lblRxFileType.Text = "File Type Expected";
            // 
            // edReceiveEmailAddress
            // 
            this.edReceiveEmailAddress.Location = new System.Drawing.Point(108, 169);
            this.edReceiveEmailAddress.Margin = new System.Windows.Forms.Padding(2);
            this.edReceiveEmailAddress.Name = "edReceiveEmailAddress";
            this.edReceiveEmailAddress.Size = new System.Drawing.Size(410, 20);
            this.edReceiveEmailAddress.TabIndex = 11;
            // 
            // lblReceiveEmailAddress
            // 
            this.lblReceiveEmailAddress.AutoSize = true;
            this.lblReceiveEmailAddress.Location = new System.Drawing.Point(14, 172);
            this.lblReceiveEmailAddress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReceiveEmailAddress.Name = "lblReceiveEmailAddress";
            this.lblReceiveEmailAddress.Size = new System.Drawing.Size(80, 13);
            this.lblReceiveEmailAddress.TabIndex = 28;
            this.lblReceiveEmailAddress.Text = "Recipient Email";
            // 
            // gbReceiveMethod
            // 
            this.gbReceiveMethod.Controls.Add(this.rbReceiveSOAP);
            this.gbReceiveMethod.Controls.Add(this.rbReceivePickup);
            this.gbReceiveMethod.Controls.Add(this.rbReceiveEmail);
            this.gbReceiveMethod.Controls.Add(this.rbReceiveFTP);
            this.gbReceiveMethod.Controls.Add(this.rbPickup);
            this.gbReceiveMethod.Location = new System.Drawing.Point(15, 18);
            this.gbReceiveMethod.Margin = new System.Windows.Forms.Padding(2);
            this.gbReceiveMethod.Name = "gbReceiveMethod";
            this.gbReceiveMethod.Padding = new System.Windows.Forms.Padding(2);
            this.gbReceiveMethod.Size = new System.Drawing.Size(502, 38);
            this.gbReceiveMethod.TabIndex = 3;
            this.gbReceiveMethod.TabStop = false;
            this.gbReceiveMethod.Text = "Delivery Method (Receive)";
            // 
            // rbReceiveSOAP
            // 
            this.rbReceiveSOAP.AutoSize = true;
            this.rbReceiveSOAP.Location = new System.Drawing.Point(379, 16);
            this.rbReceiveSOAP.Name = "rbReceiveSOAP";
            this.rbReceiveSOAP.Size = new System.Drawing.Size(68, 17);
            this.rbReceiveSOAP.TabIndex = 5;
            this.rbReceiveSOAP.TabStop = true;
            this.rbReceiveSOAP.Text = "&Web API";
            this.rbReceiveSOAP.UseVisualStyleBackColor = true;
            // 
            // rbReceivePickup
            // 
            this.rbReceivePickup.AutoSize = true;
            this.rbReceivePickup.Location = new System.Drawing.Point(259, 16);
            this.rbReceivePickup.Name = "rbReceivePickup";
            this.rbReceivePickup.Size = new System.Drawing.Size(113, 17);
            this.rbReceivePickup.TabIndex = 4;
            this.rbReceivePickup.TabStop = true;
            this.rbReceivePickup.Text = "Pickup from Folder";
            this.rbReceivePickup.UseVisualStyleBackColor = true;
            this.rbReceivePickup.CheckedChanged += new System.EventHandler(this.rbEmail_CheckedChanged);
            // 
            // rbReceiveEmail
            // 
            this.rbReceiveEmail.AutoSize = true;
            this.rbReceiveEmail.Location = new System.Drawing.Point(203, 17);
            this.rbReceiveEmail.Margin = new System.Windows.Forms.Padding(2);
            this.rbReceiveEmail.Name = "rbReceiveEmail";
            this.rbReceiveEmail.Size = new System.Drawing.Size(50, 17);
            this.rbReceiveEmail.TabIndex = 3;
            this.rbReceiveEmail.TabStop = true;
            this.rbReceiveEmail.Tag = "email";
            this.rbReceiveEmail.Text = "E&mail";
            this.rbReceiveEmail.UseVisualStyleBackColor = true;
            this.rbReceiveEmail.CheckedChanged += new System.EventHandler(this.rbEmail_CheckedChanged);
            // 
            // rbReceiveFTP
            // 
            this.rbReceiveFTP.AutoSize = true;
            this.rbReceiveFTP.Location = new System.Drawing.Point(137, 17);
            this.rbReceiveFTP.Margin = new System.Windows.Forms.Padding(2);
            this.rbReceiveFTP.Name = "rbReceiveFTP";
            this.rbReceiveFTP.Size = new System.Drawing.Size(62, 17);
            this.rbReceiveFTP.TabIndex = 2;
            this.rbReceiveFTP.TabStop = true;
            this.rbReceiveFTP.Tag = "ftp";
            this.rbReceiveFTP.Text = "via FTP";
            this.rbReceiveFTP.UseVisualStyleBackColor = true;
            this.rbReceiveFTP.CheckedChanged += new System.EventHandler(this.rbEmail_CheckedChanged);
            // 
            // rbPickup
            // 
            this.rbPickup.AutoSize = true;
            this.rbPickup.Location = new System.Drawing.Point(5, 17);
            this.rbPickup.Margin = new System.Windows.Forms.Padding(2);
            this.rbPickup.Name = "rbPickup";
            this.rbPickup.Size = new System.Drawing.Size(134, 17);
            this.rbPickup.TabIndex = 0;
            this.rbPickup.TabStop = true;
            this.rbPickup.Tag = "pickup";
            this.rbPickup.Text = "Pickup (via CTC Node)";
            this.rbPickup.UseVisualStyleBackColor = true;
            this.rbPickup.CheckedChanged += new System.EventHandler(this.rbEmail_CheckedChanged);
            // 
            // edFTPReceivePath
            // 
            this.edFTPReceivePath.Location = new System.Drawing.Point(108, 89);
            this.edFTPReceivePath.Name = "edFTPReceivePath";
            this.edFTPReceivePath.Size = new System.Drawing.Size(263, 20);
            this.edFTPReceivePath.TabIndex = 6;
            // 
            // lblFTPReceivePath
            // 
            this.lblFTPReceivePath.AutoSize = true;
            this.lblFTPReceivePath.Location = new System.Drawing.Point(14, 92);
            this.lblFTPReceivePath.Name = "lblFTPReceivePath";
            this.lblFTPReceivePath.Size = new System.Drawing.Size(29, 13);
            this.lblFTPReceivePath.TabIndex = 26;
            this.lblFTPReceivePath.Text = "Path";
            // 
            // edFTPReceivePort
            // 
            this.edFTPReceivePort.Location = new System.Drawing.Point(499, 89);
            this.edFTPReceivePort.Margin = new System.Windows.Forms.Padding(2);
            this.edFTPReceivePort.Name = "edFTPReceivePort";
            this.edFTPReceivePort.Size = new System.Drawing.Size(46, 20);
            this.edFTPReceivePort.TabIndex = 5;
            // 
            // lblFTPReceivePort
            // 
            this.lblFTPReceivePort.AutoSize = true;
            this.lblFTPReceivePort.Location = new System.Drawing.Point(405, 92);
            this.lblFTPReceivePort.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblFTPReceivePort.Name = "lblFTPReceivePort";
            this.lblFTPReceivePort.Size = new System.Drawing.Size(63, 13);
            this.lblFTPReceivePort.TabIndex = 25;
            this.lblFTPReceivePort.Text = "Default Port";
            // 
            // edReceivePassword
            // 
            this.edReceivePassword.Location = new System.Drawing.Point(340, 117);
            this.edReceivePassword.Margin = new System.Windows.Forms.Padding(2);
            this.edReceivePassword.Name = "edReceivePassword";
            this.edReceivePassword.Size = new System.Drawing.Size(103, 20);
            this.edReceivePassword.TabIndex = 8;
            // 
            // lblReceivePassword
            // 
            this.lblReceivePassword.AutoSize = true;
            this.lblReceivePassword.Location = new System.Drawing.Point(273, 120);
            this.lblReceivePassword.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReceivePassword.Name = "lblReceivePassword";
            this.lblReceivePassword.Size = new System.Drawing.Size(53, 13);
            this.lblReceivePassword.TabIndex = 23;
            this.lblReceivePassword.Text = "Password";
            // 
            // edReceiveUsername
            // 
            this.edReceiveUsername.Location = new System.Drawing.Point(108, 117);
            this.edReceiveUsername.Margin = new System.Windows.Forms.Padding(2);
            this.edReceiveUsername.Name = "edReceiveUsername";
            this.edReceiveUsername.Size = new System.Drawing.Size(161, 20);
            this.edReceiveUsername.TabIndex = 7;
            // 
            // lblReceiveUsername
            // 
            this.lblReceiveUsername.AutoSize = true;
            this.lblReceiveUsername.Location = new System.Drawing.Point(14, 120);
            this.lblReceiveUsername.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblReceiveUsername.Name = "lblReceiveUsername";
            this.lblReceiveUsername.Size = new System.Drawing.Size(58, 13);
            this.lblReceiveUsername.TabIndex = 21;
            this.lblReceiveUsername.Text = "User name";
            // 
            // edServerAddress
            // 
            this.edServerAddress.Location = new System.Drawing.Point(108, 65);
            this.edServerAddress.Margin = new System.Windows.Forms.Padding(2);
            this.edServerAddress.Name = "edServerAddress";
            this.edServerAddress.Size = new System.Drawing.Size(410, 20);
            this.edServerAddress.TabIndex = 4;
            // 
            // lblServerAddress
            // 
            this.lblServerAddress.AutoSize = true;
            this.lblServerAddress.Location = new System.Drawing.Point(14, 68);
            this.lblServerAddress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblServerAddress.Name = "lblServerAddress";
            this.lblServerAddress.Size = new System.Drawing.Size(79, 13);
            this.lblServerAddress.TabIndex = 19;
            this.lblServerAddress.Text = "Server Address";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1, -11);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Send (on Forward) Details";
            // 
            // tbSendProperties
            // 
            this.tbSendProperties.Controls.Add(this.gbSendOptions);
            this.tbSendProperties.Location = new System.Drawing.Point(4, 22);
            this.tbSendProperties.Name = "tbSendProperties";
            this.tbSendProperties.Size = new System.Drawing.Size(634, 360);
            this.tbSendProperties.TabIndex = 2;
            this.tbSendProperties.Text = "Send Properties";
            this.tbSendProperties.UseVisualStyleBackColor = true;
            // 
            // gbSendOptions
            // 
            this.gbSendOptions.Controls.Add(this.cbForwardWithFlags);
            this.gbSendOptions.Controls.Add(this.edSendFileName);
            this.gbSendOptions.Controls.Add(this.lblSendFileNameExpected);
            this.gbSendOptions.Controls.Add(this.cmbSendFileType);
            this.gbSendOptions.Controls.Add(this.lblSendFileType);
            this.gbSendOptions.Controls.Add(this.btnSendKeyPath);
            this.gbSendOptions.Controls.Add(this.cbSendSSL);
            this.gbSendOptions.Controls.Add(this.btnFTPTest);
            this.gbSendOptions.Controls.Add(this.btnPickupFolder);
            this.gbSendOptions.Controls.Add(this.edSendSubject);
            this.gbSendOptions.Controls.Add(this.lblSendSubject);
            this.gbSendOptions.Controls.Add(this.edSendEmailAddress);
            this.gbSendOptions.Controls.Add(this.lblSendEmailAddress);
            this.gbSendOptions.Controls.Add(this.edSendFtpPort);
            this.gbSendOptions.Controls.Add(this.lblSendFTPPort);
            this.gbSendOptions.Controls.Add(this.btnTestSendeAdapter);
            this.gbSendOptions.Controls.Add(this.edSendeAdapterResults);
            this.gbSendOptions.Controls.Add(this.lblTestResults);
            this.gbSendOptions.Controls.Add(this.edSendFolder);
            this.gbSendOptions.Controls.Add(this.gbSendMethod);
            this.gbSendOptions.Controls.Add(this.lblSendPath);
            this.gbSendOptions.Controls.Add(this.edSendPassword);
            this.gbSendOptions.Controls.Add(this.lblSendPassword);
            this.gbSendOptions.Controls.Add(this.edSendUsername);
            this.gbSendOptions.Controls.Add(this.lblSendUsername);
            this.gbSendOptions.Controls.Add(this.edSendCTCUrl);
            this.gbSendOptions.Controls.Add(this.lblSendServer);
            this.gbSendOptions.Controls.Add(this.label16);
            this.gbSendOptions.Location = new System.Drawing.Point(9, 4);
            this.gbSendOptions.Margin = new System.Windows.Forms.Padding(2);
            this.gbSendOptions.Name = "gbSendOptions";
            this.gbSendOptions.Padding = new System.Windows.Forms.Padding(2);
            this.gbSendOptions.Size = new System.Drawing.Size(623, 341);
            this.gbSendOptions.TabIndex = 21;
            this.gbSendOptions.TabStop = false;
            this.gbSendOptions.Text = "Send Options";
            // 
            // cbForwardWithFlags
            // 
            this.cbForwardWithFlags.AutoSize = true;
            this.cbForwardWithFlags.Location = new System.Drawing.Point(511, 66);
            this.cbForwardWithFlags.Name = "cbForwardWithFlags";
            this.cbForwardWithFlags.Size = new System.Drawing.Size(113, 17);
            this.cbForwardWithFlags.TabIndex = 43;
            this.cbForwardWithFlags.Text = "Forward CW Flags";
            this.cbForwardWithFlags.UseVisualStyleBackColor = true;
            // 
            // edSendFileName
            // 
            this.edSendFileName.Location = new System.Drawing.Point(128, 181);
            this.edSendFileName.Name = "edSendFileName";
            this.edSendFileName.Size = new System.Drawing.Size(202, 20);
            this.edSendFileName.TabIndex = 42;
            // 
            // lblSendFileNameExpected
            // 
            this.lblSendFileNameExpected.AutoSize = true;
            this.lblSendFileNameExpected.Location = new System.Drawing.Point(19, 184);
            this.lblSendFileNameExpected.Name = "lblSendFileNameExpected";
            this.lblSendFileNameExpected.Size = new System.Drawing.Size(102, 13);
            this.lblSendFileNameExpected.TabIndex = 41;
            this.lblSendFileNameExpected.Text = "File Name Expected";
            // 
            // cmbSendFileType
            // 
            this.cmbSendFileType.FormattingEnabled = true;
            this.cmbSendFileType.Items.AddRange(new object[] {
            "*.*",
            ".TXT",
            ".XML",
            ".XLS",
            ".CSV",
            ".DAT",
            ".PDF",
            ".XLSX"});
            this.cmbSendFileType.Location = new System.Drawing.Point(440, 181);
            this.cmbSendFileType.Name = "cmbSendFileType";
            this.cmbSendFileType.Size = new System.Drawing.Size(64, 21);
            this.cmbSendFileType.TabIndex = 40;
            // 
            // lblSendFileType
            // 
            this.lblSendFileType.AutoSize = true;
            this.lblSendFileType.Location = new System.Drawing.Point(336, 184);
            this.lblSendFileType.Name = "lblSendFileType";
            this.lblSendFileType.Size = new System.Drawing.Size(98, 13);
            this.lblSendFileType.TabIndex = 39;
            this.lblSendFileType.Text = "File Type Expected";
            // 
            // btnSendKeyPath
            // 
            this.btnSendKeyPath.Location = new System.Drawing.Point(502, 131);
            this.btnSendKeyPath.Name = "btnSendKeyPath";
            this.btnSendKeyPath.Size = new System.Drawing.Size(32, 23);
            this.btnSendKeyPath.TabIndex = 38;
            this.btnSendKeyPath.Text = "...";
            this.btnSendKeyPath.UseVisualStyleBackColor = true;
            this.btnSendKeyPath.Click += new System.EventHandler(this.btnSendKeyPath_Click);
            // 
            // cbSendSSL
            // 
            this.cbSendSSL.AutoSize = true;
            this.cbSendSSL.Location = new System.Drawing.Point(511, 66);
            this.cbSendSSL.Name = "cbSendSSL";
            this.cbSendSSL.Size = new System.Drawing.Size(83, 17);
            this.cbSendSSL.TabIndex = 37;
            this.cbSendSSL.Text = "Secure FTP";
            this.cbSendSSL.UseVisualStyleBackColor = true;
            this.cbSendSSL.CheckedChanged += new System.EventHandler(this.cbSecureFtp_CheckedChanged);
            // 
            // btnFTPTest
            // 
            this.btnFTPTest.Location = new System.Drawing.Point(306, 202);
            this.btnFTPTest.Margin = new System.Windows.Forms.Padding(2);
            this.btnFTPTest.Name = "btnFTPTest";
            this.btnFTPTest.Size = new System.Drawing.Size(79, 23);
            this.btnFTPTest.TabIndex = 36;
            this.btnFTPTest.Text = "Test FTP ";
            this.btnFTPTest.UseVisualStyleBackColor = true;
            this.btnFTPTest.Click += new System.EventHandler(this.btnFTPTest_Click);
            // 
            // btnPickupFolder
            // 
            this.btnPickupFolder.Location = new System.Drawing.Point(476, 64);
            this.btnPickupFolder.Name = "btnPickupFolder";
            this.btnPickupFolder.Size = new System.Drawing.Size(28, 20);
            this.btnPickupFolder.TabIndex = 35;
            this.btnPickupFolder.Text = "...";
            this.btnPickupFolder.UseVisualStyleBackColor = true;
            this.btnPickupFolder.Click += new System.EventHandler(this.btnPickupFolder_Click);
            // 
            // edSendSubject
            // 
            this.edSendSubject.Location = new System.Drawing.Point(94, 133);
            this.edSendSubject.Margin = new System.Windows.Forms.Padding(2);
            this.edSendSubject.Name = "edSendSubject";
            this.edSendSubject.Size = new System.Drawing.Size(410, 20);
            this.edSendSubject.TabIndex = 33;
            // 
            // lblSendSubject
            // 
            this.lblSendSubject.AutoSize = true;
            this.lblSendSubject.Location = new System.Drawing.Point(19, 139);
            this.lblSendSubject.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendSubject.Name = "lblSendSubject";
            this.lblSendSubject.Size = new System.Drawing.Size(43, 13);
            this.lblSendSubject.TabIndex = 34;
            this.lblSendSubject.Text = "Subject";
            // 
            // edSendEmailAddress
            // 
            this.edSendEmailAddress.Location = new System.Drawing.Point(128, 156);
            this.edSendEmailAddress.Margin = new System.Windows.Forms.Padding(2);
            this.edSendEmailAddress.Name = "edSendEmailAddress";
            this.edSendEmailAddress.Size = new System.Drawing.Size(376, 20);
            this.edSendEmailAddress.TabIndex = 31;
            // 
            // lblSendEmailAddress
            // 
            this.lblSendEmailAddress.AutoSize = true;
            this.lblSendEmailAddress.Location = new System.Drawing.Point(19, 158);
            this.lblSendEmailAddress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendEmailAddress.Name = "lblSendEmailAddress";
            this.lblSendEmailAddress.Size = new System.Drawing.Size(90, 13);
            this.lblSendEmailAddress.TabIndex = 32;
            this.lblSendEmailAddress.Text = "Email Address(es)";
            // 
            // edSendFtpPort
            // 
            this.edSendFtpPort.Location = new System.Drawing.Point(94, 87);
            this.edSendFtpPort.Margin = new System.Windows.Forms.Padding(2);
            this.edSendFtpPort.Name = "edSendFtpPort";
            this.edSendFtpPort.Size = new System.Drawing.Size(46, 20);
            this.edSendFtpPort.TabIndex = 29;
            // 
            // lblSendFTPPort
            // 
            this.lblSendFTPPort.AutoSize = true;
            this.lblSendFTPPort.Location = new System.Drawing.Point(19, 90);
            this.lblSendFTPPort.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendFTPPort.Name = "lblSendFTPPort";
            this.lblSendFTPPort.Size = new System.Drawing.Size(63, 13);
            this.lblSendFTPPort.TabIndex = 30;
            this.lblSendFTPPort.Text = "Default Port";
            // 
            // btnTestSendeAdapter
            // 
            this.btnTestSendeAdapter.Location = new System.Drawing.Point(140, 202);
            this.btnTestSendeAdapter.Name = "btnTestSendeAdapter";
            this.btnTestSendeAdapter.Size = new System.Drawing.Size(125, 23);
            this.btnTestSendeAdapter.TabIndex = 28;
            this.btnTestSendeAdapter.Text = "Test eAdapter Service";
            this.btnTestSendeAdapter.UseVisualStyleBackColor = true;
            this.btnTestSendeAdapter.Click += new System.EventHandler(this.btnTestSendeAdapter_Click);
            // 
            // edSendeAdapterResults
            // 
            this.edSendeAdapterResults.Location = new System.Drawing.Point(19, 230);
            this.edSendeAdapterResults.Multiline = true;
            this.edSendeAdapterResults.Name = "edSendeAdapterResults";
            this.edSendeAdapterResults.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.edSendeAdapterResults.Size = new System.Drawing.Size(575, 106);
            this.edSendeAdapterResults.TabIndex = 27;
            // 
            // lblTestResults
            // 
            this.lblTestResults.AutoSize = true;
            this.lblTestResults.Location = new System.Drawing.Point(19, 207);
            this.lblTestResults.Name = "lblTestResults";
            this.lblTestResults.Size = new System.Drawing.Size(112, 13);
            this.lblTestResults.TabIndex = 26;
            this.lblTestResults.Text = "eAdapter Test Results";
            // 
            // edSendFolder
            // 
            this.edSendFolder.Location = new System.Drawing.Point(228, 87);
            this.edSendFolder.Margin = new System.Windows.Forms.Padding(2);
            this.edSendFolder.Name = "edSendFolder";
            this.edSendFolder.Size = new System.Drawing.Size(276, 20);
            this.edSendFolder.TabIndex = 24;
            // 
            // gbSendMethod
            // 
            this.gbSendMethod.Controls.Add(this.rbSoapSend);
            this.gbSendMethod.Controls.Add(this.rbFTPSendDirect);
            this.gbSendMethod.Controls.Add(this.rbEmailSend);
            this.gbSendMethod.Controls.Add(this.rbFTPSend);
            this.gbSendMethod.Controls.Add(this.rbCTCSend);
            this.gbSendMethod.Controls.Add(this.rbPickupSend);
            this.gbSendMethod.Location = new System.Drawing.Point(12, 17);
            this.gbSendMethod.Margin = new System.Windows.Forms.Padding(2);
            this.gbSendMethod.Name = "gbSendMethod";
            this.gbSendMethod.Padding = new System.Windows.Forms.Padding(2);
            this.gbSendMethod.Size = new System.Drawing.Size(595, 41);
            this.gbSendMethod.TabIndex = 20;
            this.gbSendMethod.TabStop = false;
            this.gbSendMethod.Text = "Delivery Method (Send)";
            // 
            // rbSoapSend
            // 
            this.rbSoapSend.AutoSize = true;
            this.rbSoapSend.Location = new System.Drawing.Point(470, 18);
            this.rbSoapSend.Name = "rbSoapSend";
            this.rbSoapSend.Size = new System.Drawing.Size(68, 17);
            this.rbSoapSend.TabIndex = 5;
            this.rbSoapSend.TabStop = true;
            this.rbSoapSend.Text = "&Web API";
            this.rbSoapSend.UseVisualStyleBackColor = true;
            this.rbSoapSend.CheckedChanged += new System.EventHandler(this.rbCTCSend_CheckedChanged);
            // 
            // rbFTPSendDirect
            // 
            this.rbFTPSendDirect.AutoSize = true;
            this.rbFTPSendDirect.Location = new System.Drawing.Point(222, 17);
            this.rbFTPSendDirect.Margin = new System.Windows.Forms.Padding(2);
            this.rbFTPSendDirect.Name = "rbFTPSendDirect";
            this.rbFTPSendDirect.Size = new System.Drawing.Size(82, 17);
            this.rbFTPSendDirect.TabIndex = 4;
            this.rbFTPSendDirect.TabStop = true;
            this.rbFTPSendDirect.Tag = "ftp";
            this.rbFTPSendDirect.Text = "FTP (&Direct)";
            this.rbFTPSendDirect.UseVisualStyleBackColor = true;
            this.rbFTPSendDirect.CheckedChanged += new System.EventHandler(this.rbCTCSend_CheckedChanged);
            // 
            // rbEmailSend
            // 
            this.rbEmailSend.AutoSize = true;
            this.rbEmailSend.Location = new System.Drawing.Point(408, 17);
            this.rbEmailSend.Margin = new System.Windows.Forms.Padding(2);
            this.rbEmailSend.Name = "rbEmailSend";
            this.rbEmailSend.Size = new System.Drawing.Size(50, 17);
            this.rbEmailSend.TabIndex = 3;
            this.rbEmailSend.TabStop = true;
            this.rbEmailSend.Tag = "email";
            this.rbEmailSend.Text = "E&mail";
            this.rbEmailSend.UseVisualStyleBackColor = true;
            this.rbEmailSend.CheckedChanged += new System.EventHandler(this.rbCTCSend_CheckedChanged);
            // 
            // rbFTPSend
            // 
            this.rbFTPSend.AutoSize = true;
            this.rbFTPSend.Location = new System.Drawing.Point(133, 17);
            this.rbFTPSend.Margin = new System.Windows.Forms.Padding(2);
            this.rbFTPSend.Name = "rbFTPSend";
            this.rbFTPSend.Size = new System.Drawing.Size(85, 17);
            this.rbFTPSend.TabIndex = 2;
            this.rbFTPSend.TabStop = true;
            this.rbFTPSend.Tag = "ftp";
            this.rbFTPSend.Text = "FTP (&Polling)";
            this.rbFTPSend.UseVisualStyleBackColor = true;
            this.rbFTPSend.CheckedChanged += new System.EventHandler(this.rbCTCSend_CheckedChanged);
            // 
            // rbCTCSend
            // 
            this.rbCTCSend.AutoSize = true;
            this.rbCTCSend.Location = new System.Drawing.Point(310, 17);
            this.rbCTCSend.Margin = new System.Windows.Forms.Padding(2);
            this.rbCTCSend.Name = "rbCTCSend";
            this.rbCTCSend.Size = new System.Drawing.Size(86, 17);
            this.rbCTCSend.TabIndex = 1;
            this.rbCTCSend.TabStop = true;
            this.rbCTCSend.Tag = "eadapter";
            this.rbCTCSend.Text = "&CTC Adapter";
            this.rbCTCSend.UseVisualStyleBackColor = true;
            this.rbCTCSend.CheckedChanged += new System.EventHandler(this.rbCTCSend_CheckedChanged);
            // 
            // rbPickupSend
            // 
            this.rbPickupSend.AutoSize = true;
            this.rbPickupSend.Location = new System.Drawing.Point(5, 17);
            this.rbPickupSend.Margin = new System.Windows.Forms.Padding(2);
            this.rbPickupSend.Name = "rbPickupSend";
            this.rbPickupSend.Size = new System.Drawing.Size(122, 17);
            this.rbPickupSend.TabIndex = 0;
            this.rbPickupSend.TabStop = true;
            this.rbPickupSend.Tag = "pickup";
            this.rbPickupSend.Text = "Pick&up (FTP - Node)";
            this.rbPickupSend.UseVisualStyleBackColor = true;
            this.rbPickupSend.CheckedChanged += new System.EventHandler(this.rbCTCSend_CheckedChanged);
            // 
            // lblSendPath
            // 
            this.lblSendPath.AutoSize = true;
            this.lblSendPath.Location = new System.Drawing.Point(158, 90);
            this.lblSendPath.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendPath.Name = "lblSendPath";
            this.lblSendPath.Size = new System.Drawing.Size(63, 13);
            this.lblSendPath.TabIndex = 25;
            this.lblSendPath.Text = "Folder/Path";
            // 
            // edSendPassword
            // 
            this.edSendPassword.Location = new System.Drawing.Point(334, 110);
            this.edSendPassword.Margin = new System.Windows.Forms.Padding(2);
            this.edSendPassword.Name = "edSendPassword";
            this.edSendPassword.Size = new System.Drawing.Size(170, 20);
            this.edSendPassword.TabIndex = 22;
            // 
            // lblSendPassword
            // 
            this.lblSendPassword.AutoSize = true;
            this.lblSendPassword.Location = new System.Drawing.Point(277, 113);
            this.lblSendPassword.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendPassword.Name = "lblSendPassword";
            this.lblSendPassword.Size = new System.Drawing.Size(53, 13);
            this.lblSendPassword.TabIndex = 23;
            this.lblSendPassword.Text = "Password";
            // 
            // edSendUsername
            // 
            this.edSendUsername.Location = new System.Drawing.Point(94, 110);
            this.edSendUsername.Margin = new System.Windows.Forms.Padding(2);
            this.edSendUsername.Name = "edSendUsername";
            this.edSendUsername.Size = new System.Drawing.Size(179, 20);
            this.edSendUsername.TabIndex = 20;
            // 
            // lblSendUsername
            // 
            this.lblSendUsername.AutoSize = true;
            this.lblSendUsername.Location = new System.Drawing.Point(19, 114);
            this.lblSendUsername.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendUsername.Name = "lblSendUsername";
            this.lblSendUsername.Size = new System.Drawing.Size(55, 13);
            this.lblSendUsername.TabIndex = 21;
            this.lblSendUsername.Text = "Username";
            // 
            // edSendCTCUrl
            // 
            this.edSendCTCUrl.Location = new System.Drawing.Point(95, 64);
            this.edSendCTCUrl.Margin = new System.Windows.Forms.Padding(2);
            this.edSendCTCUrl.Name = "edSendCTCUrl";
            this.edSendCTCUrl.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.edSendCTCUrl.Size = new System.Drawing.Size(383, 20);
            this.edSendCTCUrl.TabIndex = 4;
            // 
            // lblSendServer
            // 
            this.lblSendServer.AutoSize = true;
            this.lblSendServer.Location = new System.Drawing.Point(19, 66);
            this.lblSendServer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSendServer.Name = "lblSendServer";
            this.lblSendServer.Size = new System.Drawing.Size(79, 13);
            this.lblSendServer.TabIndex = 19;
            this.lblSendServer.Text = "Server Address";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(1, -11);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(154, 13);
            this.label16.TabIndex = 18;
            this.label16.Text = "Send (on Forward) Details";
            // 
            // tbConversion
            // 
            this.tbConversion.Controls.Add(this.label12);
            this.tbConversion.Controls.Add(this.cmbCustomMethod);
            this.tbConversion.Controls.Add(this.lblLegacyConversion);
            this.tbConversion.Controls.Add(this.lblConCommon);
            this.tbConversion.Controls.Add(this.panel1);
            this.tbConversion.Controls.Add(this.panelConFrom);
            this.tbConversion.Controls.Add(this.label36);
            this.tbConversion.Controls.Add(this.btnAddParam);
            this.tbConversion.Controls.Add(this.lbParameters);
            this.tbConversion.Controls.Add(this.label34);
            this.tbConversion.Controls.Add(this.btnLoadXSD);
            this.tbConversion.Controls.Add(this.btnXsdPath);
            this.tbConversion.Controls.Add(this.edLibraryName);
            this.tbConversion.Controls.Add(this.label17);
            this.tbConversion.Controls.Add(this.edXSDPath);
            this.tbConversion.Controls.Add(this.label18);
            this.tbConversion.Controls.Add(this.label19);
            this.tbConversion.Location = new System.Drawing.Point(4, 22);
            this.tbConversion.Name = "tbConversion";
            this.tbConversion.Size = new System.Drawing.Size(634, 360);
            this.tbConversion.TabIndex = 3;
            this.tbConversion.Text = "Conversion Library";
            this.tbConversion.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(20, 189);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 13);
            this.label12.TabIndex = 59;
            this.label12.Text = "common or main";
            // 
            // cmbCustomMethod
            // 
            this.cmbCustomMethod.FormattingEnabled = true;
            this.cmbCustomMethod.Items.AddRange(new object[] {
            "ArcToCW",
            "CWTOARC",
            "CCTOCW",
            "CWTOCC",
            "ContainerChainToCW",
            "CargowiseToFreightTracker",
            "CargowiseToMoveit",
            "COMMONTOSAP",
            "CARGOOFFICETOCW",
            "CWTOCARGOOFFICE",
            "DoConvert",
            "MoveToProcessing",
            "ProcessInbound",
            "GSI_Orders"});
            this.cmbCustomMethod.Location = new System.Drawing.Point(181, 220);
            this.cmbCustomMethod.Name = "cmbCustomMethod";
            this.cmbCustomMethod.Size = new System.Drawing.Size(254, 21);
            this.cmbCustomMethod.TabIndex = 58;
            // 
            // lblLegacyConversion
            // 
            this.lblLegacyConversion.AutoSize = true;
            this.lblLegacyConversion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLegacyConversion.Location = new System.Drawing.Point(178, 189);
            this.lblLegacyConversion.Name = "lblLegacyConversion";
            this.lblLegacyConversion.Size = new System.Drawing.Size(93, 13);
            this.lblLegacyConversion.TabIndex = 56;
            this.lblLegacyConversion.Text = "legacy conversion";
            // 
            // lblConCommon
            // 
            this.lblConCommon.AutoSize = true;
            this.lblConCommon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConCommon.Location = new System.Drawing.Point(172, 123);
            this.lblConCommon.Name = "lblConCommon";
            this.lblConCommon.Size = new System.Drawing.Size(92, 13);
            this.lblConCommon.TabIndex = 55;
            this.lblConCommon.Text = "-> COMMON ->";
            // 
            // panel1
            // 
            this.panel1.AccessibleName = "";
            this.panel1.BackColor = System.Drawing.Color.Gainsboro;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.cmbToMethodName);
            this.panel1.Controls.Add(this.cmbToMethod);
            this.panel1.Controls.Add(this.lblToMethodName);
            this.panel1.Controls.Add(this.lblConToMethod);
            this.panel1.Location = new System.Drawing.Point(270, 52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(165, 100);
            this.panel1.TabIndex = 54;
            // 
            // cmbToMethodName
            // 
            this.cmbToMethodName.FormattingEnabled = true;
            this.cmbToMethodName.Location = new System.Drawing.Point(9, 67);
            this.cmbToMethodName.Name = "cmbToMethodName";
            this.cmbToMethodName.Size = new System.Drawing.Size(121, 21);
            this.cmbToMethodName.TabIndex = 54;
            // 
            // cmbToMethod
            // 
            this.cmbToMethod.FormattingEnabled = true;
            this.cmbToMethod.Items.AddRange(new object[] {
            "Conversion",
            "Mapping"});
            this.cmbToMethod.Location = new System.Drawing.Point(9, 25);
            this.cmbToMethod.Name = "cmbToMethod";
            this.cmbToMethod.Size = new System.Drawing.Size(121, 21);
            this.cmbToMethod.TabIndex = 53;
            this.cmbToMethod.SelectedIndexChanged += new System.EventHandler(this.cmbToMethod_SelectedIndexChanged);
            // 
            // lblToMethodName
            // 
            this.lblToMethodName.AutoSize = true;
            this.lblToMethodName.Location = new System.Drawing.Point(6, 50);
            this.lblToMethodName.Name = "lblToMethodName";
            this.lblToMethodName.Size = new System.Drawing.Size(90, 13);
            this.lblToMethodName.TabIndex = 52;
            this.lblToMethodName.Text = "To Method Name";
            // 
            // lblConToMethod
            // 
            this.lblConToMethod.AutoSize = true;
            this.lblConToMethod.Location = new System.Drawing.Point(4, 9);
            this.lblConToMethod.Name = "lblConToMethod";
            this.lblConToMethod.Size = new System.Drawing.Size(59, 13);
            this.lblConToMethod.TabIndex = 51;
            this.lblConToMethod.Text = "To Method";
            // 
            // panelConFrom
            // 
            this.panelConFrom.AccessibleName = "";
            this.panelConFrom.BackColor = System.Drawing.Color.LightGray;
            this.panelConFrom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelConFrom.Controls.Add(this.cmbFromMethodName);
            this.panelConFrom.Controls.Add(this.cmbFromMethod);
            this.panelConFrom.Controls.Add(this.lblConFromMethod);
            this.panelConFrom.Controls.Add(this.lblFromMethodName);
            this.panelConFrom.Location = new System.Drawing.Point(16, 52);
            this.panelConFrom.Name = "panelConFrom";
            this.panelConFrom.Size = new System.Drawing.Size(152, 100);
            this.panelConFrom.TabIndex = 53;
            this.panelConFrom.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // cmbFromMethodName
            // 
            this.cmbFromMethodName.FormattingEnabled = true;
            this.cmbFromMethodName.Location = new System.Drawing.Point(6, 65);
            this.cmbFromMethodName.Name = "cmbFromMethodName";
            this.cmbFromMethodName.Size = new System.Drawing.Size(121, 21);
            this.cmbFromMethodName.TabIndex = 54;
            // 
            // cmbFromMethod
            // 
            this.cmbFromMethod.FormattingEnabled = true;
            this.cmbFromMethod.Items.AddRange(new object[] {
            "Conversion",
            "Mapping"});
            this.cmbFromMethod.Location = new System.Drawing.Point(6, 23);
            this.cmbFromMethod.Name = "cmbFromMethod";
            this.cmbFromMethod.Size = new System.Drawing.Size(121, 21);
            this.cmbFromMethod.TabIndex = 53;
            this.cmbFromMethod.SelectedIndexChanged += new System.EventHandler(this.cmbFromMethod_SelectedIndexChanged);
            // 
            // lblConFromMethod
            // 
            this.lblConFromMethod.AutoSize = true;
            this.lblConFromMethod.Location = new System.Drawing.Point(3, 6);
            this.lblConFromMethod.Name = "lblConFromMethod";
            this.lblConFromMethod.Size = new System.Drawing.Size(69, 13);
            this.lblConFromMethod.TabIndex = 50;
            this.lblConFromMethod.Text = "From Method";
            // 
            // lblFromMethodName
            // 
            this.lblFromMethodName.AutoSize = true;
            this.lblFromMethodName.Location = new System.Drawing.Point(2, 48);
            this.lblFromMethodName.Name = "lblFromMethodName";
            this.lblFromMethodName.Size = new System.Drawing.Size(100, 13);
            this.lblFromMethodName.TabIndex = 52;
            this.lblFromMethodName.Text = "From Method Name";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(178, 205);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(112, 13);
            this.label36.TabIndex = 48;
            this.label36.Text = "Custom Method Name";
            // 
            // btnAddParam
            // 
            this.btnAddParam.Location = new System.Drawing.Point(549, 330);
            this.btnAddParam.Name = "btnAddParam";
            this.btnAddParam.Size = new System.Drawing.Size(75, 23);
            this.btnAddParam.TabIndex = 47;
            this.btnAddParam.Text = "Add Param";
            this.btnAddParam.UseVisualStyleBackColor = true;
            this.btnAddParam.Click += new System.EventHandler(this.btnAddParam_Click);
            // 
            // lbParameters
            // 
            this.lbParameters.ContextMenuStrip = this.cmParameterList;
            this.lbParameters.FormattingEnabled = true;
            this.lbParameters.Location = new System.Drawing.Point(21, 258);
            this.lbParameters.Name = "lbParameters";
            this.lbParameters.Size = new System.Drawing.Size(522, 95);
            this.lbParameters.TabIndex = 31;
            this.lbParameters.DoubleClick += new System.EventHandler(this.lbParameters_DoubleClick);
            // 
            // cmParameterList
            // 
            this.cmParameterList.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.cmParameterList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.removeParameterToolStripMenuItem});
            this.cmParameterList.Name = "cmParameterList";
            this.cmParameterList.Size = new System.Drawing.Size(175, 26);
            this.cmParameterList.Opening += new System.ComponentModel.CancelEventHandler(this.cmParameterList_Opening);
            // 
            // removeParameterToolStripMenuItem
            // 
            this.removeParameterToolStripMenuItem.Name = "removeParameterToolStripMenuItem";
            this.removeParameterToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.removeParameterToolStripMenuItem.Text = "Remove Parameter";
            this.removeParameterToolStripMenuItem.Click += new System.EventHandler(this.removeParameterToolStripMenuItem_Click);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(24, 242);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(98, 13);
            this.label34.TabIndex = 44;
            this.label34.Text = "Special Parameters";
            // 
            // btnLoadXSD
            // 
            this.btnLoadXSD.Location = new System.Drawing.Point(401, 21);
            this.btnLoadXSD.Name = "btnLoadXSD";
            this.btnLoadXSD.Size = new System.Drawing.Size(75, 23);
            this.btnLoadXSD.TabIndex = 42;
            this.btnLoadXSD.Text = "&Load XSD";
            this.btnLoadXSD.UseVisualStyleBackColor = true;
            this.btnLoadXSD.Click += new System.EventHandler(this.btLoadXSD_Click);
            // 
            // btnXsdPath
            // 
            this.btnXsdPath.Location = new System.Drawing.Point(365, 24);
            this.btnXsdPath.Margin = new System.Windows.Forms.Padding(2);
            this.btnXsdPath.Name = "btnXsdPath";
            this.btnXsdPath.Size = new System.Drawing.Size(25, 20);
            this.btnXsdPath.TabIndex = 39;
            this.btnXsdPath.Text = "...";
            this.btnXsdPath.UseVisualStyleBackColor = true;
            this.btnXsdPath.Click += new System.EventHandler(this.btnXSDPath_Click);
            // 
            // edLibraryName
            // 
            this.edLibraryName.Location = new System.Drawing.Point(22, 222);
            this.edLibraryName.Name = "edLibraryName";
            this.edLibraryName.Size = new System.Drawing.Size(122, 20);
            this.edLibraryName.TabIndex = 38;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(24, 206);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(125, 13);
            this.label17.TabIndex = 37;
            this.label17.Text = "Conversion Library Name";
            // 
            // edXSDPath
            // 
            this.edXSDPath.Location = new System.Drawing.Point(69, 24);
            this.edXSDPath.Name = "edXSDPath";
            this.edXSDPath.Size = new System.Drawing.Size(297, 20);
            this.edXSDPath.TabIndex = 35;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(11, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(54, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "XSD Path";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(15, 8);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(211, 13);
            this.label19.TabIndex = 34;
            this.label19.Text = "Custom XSD and Conversion Library";
            // 
            // tdXDS
            // 
            this.tdXDS.Controls.Add(this.label11);
            this.tdXDS.Controls.Add(this.ddlCWContext);
            this.tdXDS.Controls.Add(this.lblXSDDataResult);
            this.tdXDS.Controls.Add(this.tvwXSD);
            this.tdXDS.Location = new System.Drawing.Point(4, 22);
            this.tdXDS.Name = "tdXDS";
            this.tdXDS.Size = new System.Drawing.Size(634, 360);
            this.tdXDS.TabIndex = 5;
            this.tdXDS.Text = "XSD";
            this.tdXDS.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(233, 111);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 13);
            this.label11.TabIndex = 61;
            this.label11.Text = "Cargowise Data Context";
            // 
            // ddlCWContext
            // 
            this.ddlCWContext.FormattingEnabled = true;
            this.ddlCWContext.Location = new System.Drawing.Point(231, 128);
            this.ddlCWContext.Name = "ddlCWContext";
            this.ddlCWContext.Size = new System.Drawing.Size(162, 21);
            this.ddlCWContext.TabIndex = 60;
            // 
            // lblXSDDataResult
            // 
            this.lblXSDDataResult.AutoSize = true;
            this.lblXSDDataResult.Location = new System.Drawing.Point(25, 100);
            this.lblXSDDataResult.Name = "lblXSDDataResult";
            this.lblXSDDataResult.Size = new System.Drawing.Size(55, 13);
            this.lblXSDDataResult.TabIndex = 59;
            this.lblXSDDataResult.Text = "XSD Data";
            // 
            // tvwXSD
            // 
            this.tvwXSD.Location = new System.Drawing.Point(25, 111);
            this.tvwXSD.Name = "tvwXSD";
            this.tvwXSD.Size = new System.Drawing.Size(171, 89);
            this.tvwXSD.TabIndex = 58;
            // 
            // tbDTS
            // 
            this.tbDTS.Controls.Add(this.gbDTS);
            this.tbDTS.Location = new System.Drawing.Point(4, 22);
            this.tbDTS.Name = "tbDTS";
            this.tbDTS.Size = new System.Drawing.Size(634, 360);
            this.tbDTS.TabIndex = 4;
            this.tbDTS.Text = "DTS";
            this.tbDTS.UseVisualStyleBackColor = true;
            // 
            // gbDTS
            // 
            this.gbDTS.Controls.Add(this.edDTSCurrentValue);
            this.gbDTS.Controls.Add(this.label23);
            this.gbDTS.Controls.Add(this.edDTSValue);
            this.gbDTS.Controls.Add(this.edDTSTarget);
            this.gbDTS.Controls.Add(this.edDTSSearch);
            this.gbDTS.Controls.Add(this.edDTSQual);
            this.gbDTS.Controls.Add(this.bbSave);
            this.gbDTS.Controls.Add(this.dgDts);
            this.gbDTS.Controls.Add(this.gbModifies);
            this.gbDTS.Controls.Add(this.ddlDTSFileType);
            this.gbDTS.Controls.Add(this.ddlDTSType);
            this.gbDTS.Controls.Add(this.edDTSIndex);
            this.gbDTS.Controls.Add(this.label27);
            this.gbDTS.Controls.Add(this.label26);
            this.gbDTS.Controls.Add(this.label25);
            this.gbDTS.Controls.Add(this.label24);
            this.gbDTS.Controls.Add(this.label22);
            this.gbDTS.Controls.Add(this.label21);
            this.gbDTS.Controls.Add(this.label20);
            this.gbDTS.Location = new System.Drawing.Point(9, 14);
            this.gbDTS.Name = "gbDTS";
            this.gbDTS.Size = new System.Drawing.Size(622, 307);
            this.gbDTS.TabIndex = 0;
            this.gbDTS.TabStop = false;
            this.gbDTS.Text = "Data Transformation Profiles";
            // 
            // edDTSCurrentValue
            // 
            this.edDTSCurrentValue.Location = new System.Drawing.Point(14, 237);
            this.edDTSCurrentValue.Name = "edDTSCurrentValue";
            this.edDTSCurrentValue.Size = new System.Drawing.Size(258, 20);
            this.edDTSCurrentValue.TabIndex = 19;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(13, 262);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(59, 13);
            this.label23.TabIndex = 18;
            this.label23.Text = "New Value";
            // 
            // edDTSValue
            // 
            this.edDTSValue.Location = new System.Drawing.Point(13, 278);
            this.edDTSValue.Name = "edDTSValue";
            this.edDTSValue.Size = new System.Drawing.Size(259, 20);
            this.edDTSValue.TabIndex = 17;
            // 
            // edDTSTarget
            // 
            this.edDTSTarget.Location = new System.Drawing.Point(13, 196);
            this.edDTSTarget.Name = "edDTSTarget";
            this.edDTSTarget.Size = new System.Drawing.Size(260, 20);
            this.edDTSTarget.TabIndex = 16;
            // 
            // edDTSSearch
            // 
            this.edDTSSearch.Location = new System.Drawing.Point(13, 157);
            this.edDTSSearch.Name = "edDTSSearch";
            this.edDTSSearch.Size = new System.Drawing.Size(260, 20);
            this.edDTSSearch.TabIndex = 15;
            // 
            // edDTSQual
            // 
            this.edDTSQual.Location = new System.Drawing.Point(13, 101);
            this.edDTSQual.Multiline = true;
            this.edDTSQual.Name = "edDTSQual";
            this.edDTSQual.Size = new System.Drawing.Size(327, 36);
            this.edDTSQual.TabIndex = 14;
            // 
            // bbSave
            // 
            this.bbSave.Location = new System.Drawing.Point(376, 262);
            this.bbSave.Name = "bbSave";
            this.bbSave.Size = new System.Drawing.Size(75, 23);
            this.bbSave.TabIndex = 13;
            this.bbSave.Text = "Add DTS";
            this.bbSave.UseVisualStyleBackColor = true;
            this.bbSave.Click += new System.EventHandler(this.bbSave_Click);
            // 
            // dgDts
            // 
            this.dgDts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.DTSIndex,
            this.DTSType,
            this.DTSSearch,
            this.DTSModifies,
            this.DTSFileType,
            this.DTSQual,
            this.DTSTarget,
            this.DTSNewValue,
            this.CurrentValue});
            this.dgDts.Location = new System.Drawing.Point(376, 19);
            this.dgDts.Name = "dgDts";
            this.dgDts.ReadOnly = true;
            this.dgDts.RowHeadersVisible = false;
            this.dgDts.RowHeadersWidth = 51;
            this.dgDts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDts.Size = new System.Drawing.Size(240, 232);
            this.dgDts.TabIndex = 12;
            this.dgDts.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDts_CellDoubleClick);
            // 
            // DTSIndex
            // 
            this.DTSIndex.DataPropertyName = "D_INDEX";
            this.DTSIndex.HeaderText = "Index";
            this.DTSIndex.MinimumWidth = 6;
            this.DTSIndex.Name = "DTSIndex";
            this.DTSIndex.ReadOnly = true;
            this.DTSIndex.Width = 40;
            // 
            // DTSType
            // 
            this.DTSType.DataPropertyName = "D_DTS";
            this.DTSType.HeaderText = "Type";
            this.DTSType.MinimumWidth = 6;
            this.DTSType.Name = "DTSType";
            this.DTSType.ReadOnly = true;
            this.DTSType.Width = 40;
            // 
            // DTSSearch
            // 
            this.DTSSearch.DataPropertyName = "D_SEARCHPATTERN";
            this.DTSSearch.HeaderText = "Search";
            this.DTSSearch.MinimumWidth = 6;
            this.DTSSearch.Name = "DTSSearch";
            this.DTSSearch.ReadOnly = true;
            this.DTSSearch.Width = 125;
            // 
            // DTSModifies
            // 
            this.DTSModifies.DataPropertyName = "D_DTSTYPE";
            this.DTSModifies.HeaderText = "Modifies";
            this.DTSModifies.MinimumWidth = 6;
            this.DTSModifies.Name = "DTSModifies";
            this.DTSModifies.ReadOnly = true;
            this.DTSModifies.Width = 50;
            // 
            // DTSFileType
            // 
            this.DTSFileType.DataPropertyName = "D_FILETYPE";
            this.DTSFileType.HeaderText = "File Type";
            this.DTSFileType.MinimumWidth = 6;
            this.DTSFileType.Name = "DTSFileType";
            this.DTSFileType.ReadOnly = true;
            this.DTSFileType.Visible = false;
            this.DTSFileType.Width = 125;
            // 
            // DTSQual
            // 
            this.DTSQual.DataPropertyName = "D_QUALIFIER";
            this.DTSQual.HeaderText = "Qualifier";
            this.DTSQual.MinimumWidth = 6;
            this.DTSQual.Name = "DTSQual";
            this.DTSQual.ReadOnly = true;
            this.DTSQual.Visible = false;
            this.DTSQual.Width = 125;
            // 
            // DTSTarget
            // 
            this.DTSTarget.DataPropertyName = "D_TARGET";
            this.DTSTarget.HeaderText = "Target";
            this.DTSTarget.MinimumWidth = 6;
            this.DTSTarget.Name = "DTSTarget";
            this.DTSTarget.ReadOnly = true;
            this.DTSTarget.Visible = false;
            this.DTSTarget.Width = 125;
            // 
            // DTSNewValue
            // 
            this.DTSNewValue.DataPropertyName = "D_NEWVALUE";
            this.DTSNewValue.HeaderText = "New Value";
            this.DTSNewValue.MinimumWidth = 6;
            this.DTSNewValue.Name = "DTSNewValue";
            this.DTSNewValue.ReadOnly = true;
            this.DTSNewValue.Visible = false;
            this.DTSNewValue.Width = 125;
            // 
            // CurrentValue
            // 
            this.CurrentValue.DataPropertyName = "D_CURRENTVALUE";
            this.CurrentValue.HeaderText = "Current Value";
            this.CurrentValue.MinimumWidth = 6;
            this.CurrentValue.Name = "CurrentValue";
            this.CurrentValue.ReadOnly = true;
            this.CurrentValue.Visible = false;
            this.CurrentValue.Width = 125;
            // 
            // gbModifies
            // 
            this.gbModifies.Controls.Add(this.rbDTSStructure);
            this.gbModifies.Controls.Add(this.rbDTSRecord);
            this.gbModifies.Location = new System.Drawing.Point(163, 46);
            this.gbModifies.Name = "gbModifies";
            this.gbModifies.Size = new System.Drawing.Size(177, 34);
            this.gbModifies.TabIndex = 11;
            this.gbModifies.TabStop = false;
            this.gbModifies.Text = "Modifies:";
            // 
            // rbDTSStructure
            // 
            this.rbDTSStructure.AutoSize = true;
            this.rbDTSStructure.Location = new System.Drawing.Point(103, 14);
            this.rbDTSStructure.Name = "rbDTSStructure";
            this.rbDTSStructure.Size = new System.Drawing.Size(68, 17);
            this.rbDTSStructure.TabIndex = 1;
            this.rbDTSStructure.TabStop = true;
            this.rbDTSStructure.Text = "Structure";
            this.rbDTSStructure.UseVisualStyleBackColor = true;
            // 
            // rbDTSRecord
            // 
            this.rbDTSRecord.AutoSize = true;
            this.rbDTSRecord.Location = new System.Drawing.Point(8, 14);
            this.rbDTSRecord.Name = "rbDTSRecord";
            this.rbDTSRecord.Size = new System.Drawing.Size(60, 17);
            this.rbDTSRecord.TabIndex = 0;
            this.rbDTSRecord.TabStop = true;
            this.rbDTSRecord.Text = "Record";
            this.rbDTSRecord.UseVisualStyleBackColor = true;
            // 
            // ddlDTSFileType
            // 
            this.ddlDTSFileType.FormattingEnabled = true;
            this.ddlDTSFileType.Items.AddRange(new object[] {
            "XML",
            "TXT",
            "CSV",
            "XLS"});
            this.ddlDTSFileType.Location = new System.Drawing.Point(71, 52);
            this.ddlDTSFileType.Name = "ddlDTSFileType";
            this.ddlDTSFileType.Size = new System.Drawing.Size(59, 21);
            this.ddlDTSFileType.TabIndex = 10;
            // 
            // ddlDTSType
            // 
            this.ddlDTSType.FormattingEnabled = true;
            this.ddlDTSType.Location = new System.Drawing.Point(163, 19);
            this.ddlDTSType.Name = "ddlDTSType";
            this.ddlDTSType.Size = new System.Drawing.Size(75, 21);
            this.ddlDTSType.TabIndex = 9;
            // 
            // edDTSIndex
            // 
            this.edDTSIndex.Location = new System.Drawing.Point(54, 19);
            this.edDTSIndex.Name = "edDTSIndex";
            this.edDTSIndex.Size = new System.Drawing.Size(31, 20);
            this.edDTSIndex.TabIndex = 8;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(12, 219);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(166, 13);
            this.label27.TabIndex = 7;
            this.label27.Text = "Current Value to Replace/Update";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(13, 180);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(38, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "Target";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(12, 142);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(118, 13);
            this.label25.TabIndex = 5;
            this.label25.Text = "Seach for Parent (XML)";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(13, 82);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(70, 13);
            this.label24.TabIndex = 4;
            this.label24.Text = "DTS Qualifier";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(101, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(56, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "DTS Type";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(15, 22);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(33, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Index";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(15, 55);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(50, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "File Type";
            // 
            // dgProfile
            // 
            this.dgProfile.AllowUserToAddRows = false;
            this.dgProfile.AllowUserToResizeColumns = false;
            this.dgProfile.AllowUserToResizeRows = false;
            this.dgProfile.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProfile.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.P_ID,
            this.Active,
            this.P_DESCRIPTION,
            this.P_SENDERID,
            this.P_Recipientid,
            this.P_REASONCODE,
            this.Direction});
            this.dgProfile.ContextMenuStrip = this.cmProfiles;
            this.dgProfile.DataMember = "CUSTOMERPROFILE";
            this.dgProfile.Location = new System.Drawing.Point(660, 28);
            this.dgProfile.Margin = new System.Windows.Forms.Padding(2);
            this.dgProfile.MultiSelect = false;
            this.dgProfile.Name = "dgProfile";
            this.dgProfile.ReadOnly = true;
            this.dgProfile.RowHeadersVisible = false;
            this.dgProfile.RowHeadersWidth = 51;
            this.dgProfile.RowTemplate.Height = 28;
            this.dgProfile.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProfile.Size = new System.Drawing.Size(447, 364);
            this.dgProfile.TabIndex = 20;
            this.dgProfile.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.dgProfile_CellContextMenuStripNeeded);
            this.dgProfile.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProfile_CellDoubleClick);
            this.dgProfile.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgProfile_CellFormatting);
            this.dgProfile.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgProfile_CellMouseDown);
            this.dgProfile.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dgProfile_UserDeletingRow);
            // 
            // P_ID
            // 
            this.P_ID.DataPropertyName = "P_ID";
            this.P_ID.HeaderText = "P_ID";
            this.P_ID.MinimumWidth = 6;
            this.P_ID.Name = "P_ID";
            this.P_ID.ReadOnly = true;
            this.P_ID.Visible = false;
            this.P_ID.Width = 125;
            // 
            // Active
            // 
            this.Active.DataPropertyName = "P_ACTIVE";
            this.Active.FalseValue = "N";
            this.Active.HeaderText = "Active";
            this.Active.MinimumWidth = 6;
            this.Active.Name = "Active";
            this.Active.ReadOnly = true;
            this.Active.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Active.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Active.TrueValue = "Y";
            this.Active.Width = 40;
            // 
            // P_DESCRIPTION
            // 
            this.P_DESCRIPTION.DataPropertyName = "P_DESCRIPTION";
            this.P_DESCRIPTION.HeaderText = "Description";
            this.P_DESCRIPTION.MinimumWidth = 6;
            this.P_DESCRIPTION.Name = "P_DESCRIPTION";
            this.P_DESCRIPTION.ReadOnly = true;
            this.P_DESCRIPTION.Width = 250;
            // 
            // P_SENDERID
            // 
            this.P_SENDERID.DataPropertyName = "P_SENDERID";
            this.P_SENDERID.HeaderText = "Sender ID";
            this.P_SENDERID.MinimumWidth = 6;
            this.P_SENDERID.Name = "P_SENDERID";
            this.P_SENDERID.ReadOnly = true;
            this.P_SENDERID.Width = 125;
            // 
            // P_Recipientid
            // 
            this.P_Recipientid.DataPropertyName = "P_Recipientid";
            this.P_Recipientid.HeaderText = "Recipient ID";
            this.P_Recipientid.MinimumWidth = 6;
            this.P_Recipientid.Name = "P_Recipientid";
            this.P_Recipientid.ReadOnly = true;
            this.P_Recipientid.Width = 200;
            // 
            // P_REASONCODE
            // 
            this.P_REASONCODE.DataPropertyName = "P_REASONCODE";
            this.P_REASONCODE.HeaderText = "Code";
            this.P_REASONCODE.MinimumWidth = 6;
            this.P_REASONCODE.Name = "P_REASONCODE";
            this.P_REASONCODE.ReadOnly = true;
            this.P_REASONCODE.Width = 125;
            // 
            // Direction
            // 
            this.Direction.DataPropertyName = "P_DIRECTION";
            this.Direction.HeaderText = "Direction";
            this.Direction.MinimumWidth = 6;
            this.Direction.Name = "Direction";
            this.Direction.ReadOnly = true;
            this.Direction.Visible = false;
            this.Direction.Width = 125;
            // 
            // cmProfiles
            // 
            this.cmProfiles.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.cmProfiles.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.duplicateProfileToolStripMenuItem,
            this.toolStripMenuItem1,
            this.markInactiveToolStripMenuItem});
            this.cmProfiles.Name = "cmProfiles";
            this.cmProfiles.Size = new System.Drawing.Size(162, 54);
            // 
            // duplicateProfileToolStripMenuItem
            // 
            this.duplicateProfileToolStripMenuItem.Name = "duplicateProfileToolStripMenuItem";
            this.duplicateProfileToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.duplicateProfileToolStripMenuItem.Text = "Duplicate Profile";
            this.duplicateProfileToolStripMenuItem.Click += new System.EventHandler(this.duplicateProfileToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(158, 6);
            // 
            // markInactiveToolStripMenuItem
            // 
            this.markInactiveToolStripMenuItem.Name = "markInactiveToolStripMenuItem";
            this.markInactiveToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.markInactiveToolStripMenuItem.Text = "Mark Inactive";
            this.markInactiveToolStripMenuItem.Click += new System.EventHandler(this.markInactiveToolStripMenuItem_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(574, 400);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(77, 24);
            this.btnAdd.TabIndex = 9;
            this.btnAdd.Text = "Add  >>";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 254);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(214, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Customer Send and Receive Profiles";
            // 
            // ssMain
            // 
            this.ssMain.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.ssMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sbMain});
            this.ssMain.Location = new System.Drawing.Point(0, 707);
            this.ssMain.Name = "ssMain";
            this.ssMain.Padding = new System.Windows.Forms.Padding(1, 0, 9, 0);
            this.ssMain.Size = new System.Drawing.Size(1180, 22);
            this.ssMain.TabIndex = 18;
            this.ssMain.Text = "statusStrip1";
            // 
            // sbMain
            // 
            this.sbMain.Name = "sbMain";
            this.sbMain.Size = new System.Drawing.Size(42, 17);
            this.sbMain.Text = "slMain";
            // 
            // btnNewCustomer
            // 
            this.btnNewCustomer.Location = new System.Drawing.Point(418, 8);
            this.btnNewCustomer.Margin = new System.Windows.Forms.Padding(2);
            this.btnNewCustomer.Name = "btnNewCustomer";
            this.btnNewCustomer.Size = new System.Drawing.Size(54, 19);
            this.btnNewCustomer.TabIndex = 21;
            this.btnNewCustomer.Text = "&New";
            this.btnNewCustomer.UseVisualStyleBackColor = true;
            this.btnNewCustomer.Click += new System.EventHandler(this.btnNewCustomer_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(1231, 305);
            this.btnSave.Margin = new System.Windows.Forms.Padding(2);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(54, 19);
            this.btnSave.TabIndex = 20;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // cmbCustomer
            // 
            this.cmbCustomer.DisplayMember = "C_NAME";
            this.cmbCustomer.FormattingEnabled = true;
            this.cmbCustomer.Location = new System.Drawing.Point(108, 8);
            this.cmbCustomer.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCustomer.Name = "cmbCustomer";
            this.cmbCustomer.Size = new System.Drawing.Size(296, 21);
            this.cmbCustomer.TabIndex = 33;
            this.cmbCustomer.ValueMember = "C_ID";
            this.cmbCustomer.SelectedIndexChanged += new System.EventHandler(this.cmbCustomer_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 14);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 13);
            this.label13.TabIndex = 34;
            this.label13.Text = "Select Customer";
            // 
            // cbActive
            // 
            this.cbActive.AutoSize = true;
            this.cbActive.Location = new System.Drawing.Point(503, 38);
            this.cbActive.Margin = new System.Windows.Forms.Padding(2);
            this.cbActive.Name = "cbActive";
            this.cbActive.Size = new System.Drawing.Size(67, 17);
            this.cbActive.TabIndex = 33;
            this.cbActive.Text = "Is Active";
            this.cbActive.UseVisualStyleBackColor = true;
            // 
            // edCustomerName
            // 
            this.edCustomerName.Location = new System.Drawing.Point(107, 5);
            this.edCustomerName.Margin = new System.Windows.Forms.Padding(2);
            this.edCustomerName.Name = "edCustomerName";
            this.edCustomerName.Size = new System.Drawing.Size(296, 20);
            this.edCustomerName.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Customer Name";
            // 
            // tcCustomer
            // 
            this.tcCustomer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcCustomer.Controls.Add(this.tbCustomer);
            this.tcCustomer.Controls.Add(this.tbAccounting);
            this.tcCustomer.Controls.Add(this.tbSatellite);
            this.tcCustomer.Location = new System.Drawing.Point(12, 30);
            this.tcCustomer.Name = "tcCustomer";
            this.tcCustomer.SelectedIndex = 0;
            this.tcCustomer.Size = new System.Drawing.Size(1168, 208);
            this.tcCustomer.TabIndex = 36;
            // 
            // tbCustomer
            // 
            this.tbCustomer.Controls.Add(this.lblWarningPath);
            this.tbCustomer.Controls.Add(this.btnRootPath);
            this.tbCustomer.Controls.Add(this.lblActualPath);
            this.tbCustomer.Controls.Add(this.label44);
            this.tbCustomer.Controls.Add(this.cbSatellite);
            this.tbCustomer.Controls.Add(this.edRootPath);
            this.tbCustomer.Controls.Add(this.label3);
            this.tbCustomer.Controls.Add(this.edCustomerCode);
            this.tbCustomer.Controls.Add(this.label2);
            this.tbCustomer.Controls.Add(this.cbActive);
            this.tbCustomer.Controls.Add(this.label1);
            this.tbCustomer.Controls.Add(this.edCustomerName);
            this.tbCustomer.Location = new System.Drawing.Point(4, 22);
            this.tbCustomer.Name = "tbCustomer";
            this.tbCustomer.Padding = new System.Windows.Forms.Padding(3);
            this.tbCustomer.Size = new System.Drawing.Size(1160, 182);
            this.tbCustomer.TabIndex = 0;
            this.tbCustomer.Text = "Customer";
            this.tbCustomer.UseVisualStyleBackColor = true;
            // 
            // lblWarningPath
            // 
            this.lblWarningPath.AutoSize = true;
            this.lblWarningPath.Image = global::CNodeBE.Properties.Resources._16;
            this.lblWarningPath.Location = new System.Drawing.Point(76, 75);
            this.lblWarningPath.Name = "lblWarningPath";
            this.lblWarningPath.Size = new System.Drawing.Size(16, 13);
            this.lblWarningPath.TabIndex = 45;
            this.lblWarningPath.Text = "   ";
            this.lblWarningPath.Visible = false;
            // 
            // btnRootPath
            // 
            this.btnRootPath.Location = new System.Drawing.Point(456, 35);
            this.btnRootPath.Name = "btnRootPath";
            this.btnRootPath.Size = new System.Drawing.Size(32, 23);
            this.btnRootPath.TabIndex = 44;
            this.btnRootPath.Text = "...";
            this.btnRootPath.UseVisualStyleBackColor = true;
            this.btnRootPath.Click += new System.EventHandler(this.btnRootPath_Click);
            // 
            // lblActualPath
            // 
            this.lblActualPath.AutoSize = true;
            this.lblActualPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblActualPath.Location = new System.Drawing.Point(103, 75);
            this.lblActualPath.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblActualPath.Name = "lblActualPath";
            this.lblActualPath.Size = new System.Drawing.Size(19, 13);
            this.lblActualPath.TabIndex = 43;
            this.lblActualPath.Text = "...";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label44.Location = new System.Drawing.Point(9, 75);
            this.label44.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(62, 13);
            this.label44.TabIndex = 42;
            this.label44.Text = "Actual Path";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbSatellite
            // 
            this.cbSatellite.AutoSize = true;
            this.cbSatellite.Location = new System.Drawing.Point(576, 38);
            this.cbSatellite.Name = "cbSatellite";
            this.cbSatellite.Size = new System.Drawing.Size(85, 17);
            this.cbSatellite.TabIndex = 41;
            this.cbSatellite.Text = "Has Satellite";
            this.cbSatellite.UseVisualStyleBackColor = true;
            this.cbSatellite.CheckedChanged += new System.EventHandler(this.CbSatellite_CheckedChanged);
            // 
            // edRootPath
            // 
            this.edRootPath.Location = new System.Drawing.Point(107, 36);
            this.edRootPath.Margin = new System.Windows.Forms.Padding(2);
            this.edRootPath.Name = "edRootPath";
            this.edRootPath.Size = new System.Drawing.Size(344, 20);
            this.edRootPath.TabIndex = 39;
            this.edRootPath.TextChanged += new System.EventHandler(this.edRootPath_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 40);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 40;
            this.label3.Text = "Root Path";
            // 
            // edCustomerCode
            // 
            this.edCustomerCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edCustomerCode.Location = new System.Drawing.Point(582, 6);
            this.edCustomerCode.Margin = new System.Windows.Forms.Padding(2);
            this.edCustomerCode.Name = "edCustomerCode";
            this.edCustomerCode.Size = new System.Drawing.Size(79, 20);
            this.edCustomerCode.TabIndex = 37;
            this.edCustomerCode.TextChanged += new System.EventHandler(this.edRootPath_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(424, 8);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 13);
            this.label2.TabIndex = 38;
            this.label2.Text = "Customer Code (Sender ID)";
            // 
            // tbAccounting
            // 
            this.tbAccounting.Controls.Add(this.groupBox3);
            this.tbAccounting.Controls.Add(this.cbInvoice);
            this.tbAccounting.Controls.Add(this.cbTrial);
            this.tbAccounting.Controls.Add(this.cbHold);
            this.tbAccounting.Controls.Add(this.gbTrialPeriod);
            this.tbAccounting.Controls.Add(this.edShortName);
            this.tbAccounting.Controls.Add(this.label37);
            this.tbAccounting.Location = new System.Drawing.Point(4, 22);
            this.tbAccounting.Name = "tbAccounting";
            this.tbAccounting.Size = new System.Drawing.Size(1160, 182);
            this.tbAccounting.TabIndex = 2;
            this.tbAccounting.Text = "Accounting";
            this.tbAccounting.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.grdCustomerModules);
            this.groupBox3.Controls.Add(this.edModuleDescription);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Controls.Add(this.btnAddModule);
            this.groupBox3.Controls.Add(this.ddlModuleListing);
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.label41);
            this.groupBox3.Location = new System.Drawing.Point(9, 35);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(570, 175);
            this.groupBox3.TabIndex = 47;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Module Access";
            // 
            // grdCustomerModules
            // 
            this.grdCustomerModules.AllowUserToAddRows = false;
            this.grdCustomerModules.AllowUserToDeleteRows = false;
            this.grdCustomerModules.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdCustomerModules.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ModuleID,
            this.ModuleName});
            this.grdCustomerModules.Location = new System.Drawing.Point(271, 36);
            this.grdCustomerModules.Name = "grdCustomerModules";
            this.grdCustomerModules.ReadOnly = true;
            this.grdCustomerModules.RowHeadersWidth = 51;
            this.grdCustomerModules.Size = new System.Drawing.Size(293, 133);
            this.grdCustomerModules.TabIndex = 1;
            // 
            // ModuleID
            // 
            this.ModuleID.DataPropertyName = "ValueItem";
            this.ModuleID.HeaderText = "Module ID";
            this.ModuleID.MinimumWidth = 6;
            this.ModuleID.Name = "ModuleID";
            this.ModuleID.ReadOnly = true;
            this.ModuleID.Visible = false;
            this.ModuleID.Width = 125;
            // 
            // ModuleName
            // 
            this.ModuleName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ModuleName.DataPropertyName = "StringItem";
            this.ModuleName.HeaderText = "Module";
            this.ModuleName.MinimumWidth = 6;
            this.ModuleName.Name = "ModuleName";
            this.ModuleName.ReadOnly = true;
            // 
            // edModuleDescription
            // 
            this.edModuleDescription.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.edModuleDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edModuleDescription.Location = new System.Drawing.Point(13, 107);
            this.edModuleDescription.Multiline = true;
            this.edModuleDescription.Name = "edModuleDescription";
            this.edModuleDescription.ReadOnly = true;
            this.edModuleDescription.Size = new System.Drawing.Size(243, 62);
            this.edModuleDescription.TabIndex = 5;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(18, 90);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(98, 13);
            this.label43.TabIndex = 4;
            this.label43.Text = "Module Description";
            // 
            // btnAddModule
            // 
            this.btnAddModule.Image = global::CNodeBE.Properties.Resources.add_white_page;
            this.btnAddModule.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddModule.Location = new System.Drawing.Point(204, 53);
            this.btnAddModule.Name = "btnAddModule";
            this.btnAddModule.Size = new System.Drawing.Size(52, 23);
            this.btnAddModule.TabIndex = 3;
            this.btnAddModule.Text = "Add";
            this.btnAddModule.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAddModule.UseVisualStyleBackColor = true;
            this.btnAddModule.Click += new System.EventHandler(this.btnAddModule_Click);
            // 
            // ddlModuleListing
            // 
            this.ddlModuleListing.FormattingEnabled = true;
            this.ddlModuleListing.Location = new System.Drawing.Point(13, 53);
            this.ddlModuleListing.Name = "ddlModuleListing";
            this.ddlModuleListing.Size = new System.Drawing.Size(176, 21);
            this.ddlModuleListing.TabIndex = 2;
            this.ddlModuleListing.SelectedIndexChanged += new System.EventHandler(this.ddlModuleListing_SelectedIndexChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(13, 36);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(64, 13);
            this.label42.TabIndex = 1;
            this.label42.Text = "Add Module";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(265, 20);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(80, 13);
            this.label41.TabIndex = 0;
            this.label41.Text = "Modules in Use";
            // 
            // cbInvoice
            // 
            this.cbInvoice.AutoSize = true;
            this.cbInvoice.Location = new System.Drawing.Point(450, 12);
            this.cbInvoice.Name = "cbInvoice";
            this.cbInvoice.Size = new System.Drawing.Size(101, 17);
            this.cbInvoice.TabIndex = 46;
            this.cbInvoice.Text = "Invoice Monthly";
            this.cbInvoice.UseVisualStyleBackColor = true;
            // 
            // cbTrial
            // 
            this.cbTrial.AutoSize = true;
            this.cbTrial.Location = new System.Drawing.Point(368, 12);
            this.cbTrial.Name = "cbTrial";
            this.cbTrial.Size = new System.Drawing.Size(76, 17);
            this.cbTrial.TabIndex = 45;
            this.cbTrial.Text = "Trial Mode";
            this.cbTrial.UseVisualStyleBackColor = true;
            // 
            // cbHold
            // 
            this.cbHold.AutoSize = true;
            this.cbHold.Location = new System.Drawing.Point(259, 12);
            this.cbHold.Margin = new System.Windows.Forms.Padding(2);
            this.cbHold.Name = "cbHold";
            this.cbHold.Size = new System.Drawing.Size(104, 17);
            this.cbHold.TabIndex = 44;
            this.cbHold.Text = "Account on hold";
            this.cbHold.UseVisualStyleBackColor = true;
            // 
            // gbTrialPeriod
            // 
            this.gbTrialPeriod.Controls.Add(this.dtTo);
            this.gbTrialPeriod.Controls.Add(this.label31);
            this.gbTrialPeriod.Controls.Add(this.dtFrom);
            this.gbTrialPeriod.Controls.Add(this.label30);
            this.gbTrialPeriod.Location = new System.Drawing.Point(613, 9);
            this.gbTrialPeriod.Name = "gbTrialPeriod";
            this.gbTrialPeriod.Size = new System.Drawing.Size(284, 53);
            this.gbTrialPeriod.TabIndex = 43;
            this.gbTrialPeriod.TabStop = false;
            this.gbTrialPeriod.Text = "Trial Period";
            // 
            // dtTo
            // 
            this.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtTo.Location = new System.Drawing.Point(177, 17);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(101, 20);
            this.dtTo.TabIndex = 3;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(145, 21);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(20, 13);
            this.label31.TabIndex = 2;
            this.label31.Text = "To";
            // 
            // dtFrom
            // 
            this.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFrom.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.dtFrom.Location = new System.Drawing.Point(44, 17);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(98, 20);
            this.dtFrom.TabIndex = 1;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(7, 21);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(30, 13);
            this.label30.TabIndex = 0;
            this.label30.Text = "From";
            // 
            // edShortName
            // 
            this.edShortName.CausesValidation = false;
            this.edShortName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.edShortName.Location = new System.Drawing.Point(120, 9);
            this.edShortName.Margin = new System.Windows.Forms.Padding(2);
            this.edShortName.Name = "edShortName";
            this.edShortName.Size = new System.Drawing.Size(112, 20);
            this.edShortName.TabIndex = 42;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(6, 11);
            this.label37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(110, 13);
            this.label37.TabIndex = 41;
            this.label37.Text = "Customer Short Name";
            // 
            // tbSatellite
            // 
            this.tbSatellite.Controls.Add(this.cbSatelliteMonitored);
            this.tbSatellite.Controls.Add(this.btnRestartSatellite);
            this.tbSatellite.Controls.Add(this.LBLpid);
            this.tbSatellite.Controls.Add(this.label15);
            this.tbSatellite.Controls.Add(this.edSatelliteName);
            this.tbSatellite.Controls.Add(this.label14);
            this.tbSatellite.Location = new System.Drawing.Point(4, 22);
            this.tbSatellite.Name = "tbSatellite";
            this.tbSatellite.Padding = new System.Windows.Forms.Padding(3);
            this.tbSatellite.Size = new System.Drawing.Size(1160, 182);
            this.tbSatellite.TabIndex = 1;
            this.tbSatellite.Text = "Satellite";
            this.tbSatellite.UseVisualStyleBackColor = true;
            // 
            // cbSatelliteMonitored
            // 
            this.cbSatelliteMonitored.AutoSize = true;
            this.cbSatelliteMonitored.Location = new System.Drawing.Point(17, 38);
            this.cbSatelliteMonitored.Name = "cbSatelliteMonitored";
            this.cbSatelliteMonitored.Size = new System.Drawing.Size(115, 17);
            this.cbSatelliteMonitored.TabIndex = 5;
            this.cbSatelliteMonitored.Text = "Satellite Monitoring";
            this.cbSatelliteMonitored.UseVisualStyleBackColor = true;
            // 
            // btnRestartSatellite
            // 
            this.btnRestartSatellite.Location = new System.Drawing.Point(648, 7);
            this.btnRestartSatellite.Name = "btnRestartSatellite";
            this.btnRestartSatellite.Size = new System.Drawing.Size(75, 23);
            this.btnRestartSatellite.TabIndex = 4;
            this.btnRestartSatellite.Text = "&Restart";
            this.btnRestartSatellite.UseVisualStyleBackColor = true;
            // 
            // LBLpid
            // 
            this.LBLpid.AutoSize = true;
            this.LBLpid.Location = new System.Drawing.Point(494, 10);
            this.LBLpid.Name = "LBLpid";
            this.LBLpid.Size = new System.Drawing.Size(25, 13);
            this.LBLpid.TabIndex = 3;
            this.LBLpid.Text = "PID";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(414, 10);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Current PID";
            // 
            // edSatelliteName
            // 
            this.edSatelliteName.Location = new System.Drawing.Point(152, 7);
            this.edSatelliteName.Name = "edSatelliteName";
            this.edSatelliteName.Size = new System.Drawing.Size(236, 20);
            this.edSatelliteName.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(13, 10);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(130, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Satellite Application Name";
            // 
            // FrmCustomerProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1180, 729);
            this.Controls.Add(this.tcCustomer);
            this.Controls.Add(this.cmbCustomer);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btnNewCustomer);
            this.Controls.Add(this.ssMain);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.pnlProfile);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnClose);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimizeBox = false;
            this.Name = "FrmCustomerProfile";
            this.Text = "Current Modules in use";
            this.Load += new System.EventHandler(this.FrmCustomerProfile_Load);
            this.pnlProfile.ResumeLayout(false);
            this.pnlProfile.PerformLayout();
            this.tcMessageProfile.ResumeLayout(false);
            this.tbMessageHeader.ResumeLayout(false);
            this.tbMessageHeader.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbDirection.ResumeLayout(false);
            this.gbDirection.PerformLayout();
            this.grpBillto.ResumeLayout(false);
            this.grpBillto.PerformLayout();
            this.tbRecieveProperties.ResumeLayout(false);
            this.gbReceiveOptions.ResumeLayout(false);
            this.gbReceiveOptions.PerformLayout();
            this.gbReceiveMethod.ResumeLayout(false);
            this.gbReceiveMethod.PerformLayout();
            this.tbSendProperties.ResumeLayout(false);
            this.gbSendOptions.ResumeLayout(false);
            this.gbSendOptions.PerformLayout();
            this.gbSendMethod.ResumeLayout(false);
            this.gbSendMethod.PerformLayout();
            this.tbConversion.ResumeLayout(false);
            this.tbConversion.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelConFrom.ResumeLayout(false);
            this.panelConFrom.PerformLayout();
            this.cmParameterList.ResumeLayout(false);
            this.tdXDS.ResumeLayout(false);
            this.tdXDS.PerformLayout();
            this.tbDTS.ResumeLayout(false);
            this.gbDTS.ResumeLayout(false);
            this.gbDTS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDts)).EndInit();
            this.gbModifies.ResumeLayout(false);
            this.gbModifies.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProfile)).EndInit();
            this.cmProfiles.ResumeLayout(false);
            this.ssMain.ResumeLayout(false);
            this.ssMain.PerformLayout();
            this.tcCustomer.ResumeLayout(false);
            this.tbCustomer.ResumeLayout(false);
            this.tbCustomer.PerformLayout();
            this.tbAccounting.ResumeLayout(false);
            this.tbAccounting.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCustomerModules)).EndInit();
            this.gbTrialPeriod.ResumeLayout(false);
            this.gbTrialPeriod.PerformLayout();
            this.tbSatellite.ResumeLayout(false);
            this.tbSatellite.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pnlProfile;
        private System.Windows.Forms.GroupBox gbReceiveOptions;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.TextBox edFTPReceivePort;
        private System.Windows.Forms.Label lblFTPReceivePort;
        private System.Windows.Forms.TextBox edReceivePassword;
        private System.Windows.Forms.Label lblReceivePassword;
        private System.Windows.Forms.TextBox edReceiveUsername;
        private System.Windows.Forms.Label lblReceiveUsername;
        private System.Windows.Forms.TextBox edServerAddress;
        private System.Windows.Forms.Label lblServerAddress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox gbReceiveMethod;
        private System.Windows.Forms.RadioButton rbPickup;
        private System.Windows.Forms.TextBox edDescription;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridView dgProfile;
        private System.Windows.Forms.StatusStrip ssMain;
        private System.Windows.Forms.ToolStripStatusLabel sbMain;
        private System.Windows.Forms.RadioButton rbReceiveFTP;
        private System.Windows.Forms.Button btnNewCustomer;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cmbCustomer;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox cbActive;
        private System.Windows.Forms.TextBox edCustomerName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rbReceiveEmail;
        private System.Windows.Forms.TextBox edFTPReceivePath;
        private System.Windows.Forms.Label lblFTPReceivePath;
        private System.Windows.Forms.CheckBox cbChargeable;
        private System.Windows.Forms.GroupBox grpBillto;
        private System.Windows.Forms.RadioButton rbVendor;
        private System.Windows.Forms.RadioButton rbCustomer;
        private System.Windows.Forms.ComboBox cmbMsgType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabControl tcMessageProfile;
        private System.Windows.Forms.TabPage tbMessageHeader;
        private System.Windows.Forms.TabPage tbRecieveProperties;
        private System.Windows.Forms.TabPage tbSendProperties;
        private System.Windows.Forms.TabPage tbConversion;
        private System.Windows.Forms.TabPage tbDTS;
        private System.Windows.Forms.GroupBox gbSendOptions;
        private System.Windows.Forms.TextBox edSendFolder;
        private System.Windows.Forms.Label lblSendPath;
        private System.Windows.Forms.TextBox edSendPassword;
        private System.Windows.Forms.Label lblSendPassword;
        private System.Windows.Forms.TextBox edSendUsername;
        private System.Windows.Forms.Label lblSendUsername;
        private System.Windows.Forms.TextBox edSendCTCUrl;
        private System.Windows.Forms.Label lblSendServer;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox gbSendMethod;
        private System.Windows.Forms.RadioButton rbEmailSend;
        private System.Windows.Forms.RadioButton rbFTPSend;
        private System.Windows.Forms.RadioButton rbCTCSend;
        private System.Windows.Forms.RadioButton rbPickupSend;
        private System.Windows.Forms.TextBox edLibraryName;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox edXSDPath;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.RadioButton rbSend;
        private System.Windows.Forms.RadioButton rbReceive;
        private System.Windows.Forms.Button btnXsdPath;
        private System.Windows.Forms.GroupBox gbDirection;
        private System.Windows.Forms.GroupBox gbDTS;
        private System.Windows.Forms.Button bbSave;
        private System.Windows.Forms.DataGridView dgDts;
        private System.Windows.Forms.GroupBox gbModifies;
        private System.Windows.Forms.RadioButton rbDTSStructure;
        private System.Windows.Forms.RadioButton rbDTSRecord;
        private System.Windows.Forms.ComboBox ddlDTSFileType;
        private System.Windows.Forms.ComboBox ddlDTSType;
        private System.Windows.Forms.TextBox edDTSIndex;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox edDTSValue;
        private System.Windows.Forms.TextBox edDTSTarget;
        private System.Windows.Forms.TextBox edDTSSearch;
        private System.Windows.Forms.TextBox edDTSQual;
        private System.Windows.Forms.TextBox edDTSCurrentValue;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSType;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSSearch;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSModifies;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSFileType;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSQual;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSTarget;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTSNewValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentValue;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Button btnTestSendeAdapter;
        private System.Windows.Forms.TextBox edSendeAdapterResults;
        private System.Windows.Forms.Label lblTestResults;
        private System.Windows.Forms.ContextMenuStrip cmProfiles;
        private System.Windows.Forms.ToolStripMenuItem duplicateProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem markInactiveToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_ID;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Active;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_DESCRIPTION;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_SENDERID;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_Recipientid;
        private System.Windows.Forms.DataGridViewTextBoxColumn P_REASONCODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn Direction;
        private System.Windows.Forms.CheckBox cbProfileActive;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox cbDTS;
        private System.Windows.Forms.TextBox edReasonCode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox edSenderID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox edRecipientID;
        private System.Windows.Forms.TextBox edReceiveEmailAddress;
        private System.Windows.Forms.Label lblReceiveEmailAddress;
        private System.Windows.Forms.ComboBox cmbRxFileType;
        private System.Windows.Forms.Label lblRxFileType;
        private System.Windows.Forms.TextBox edSendEmailAddress;
        private System.Windows.Forms.Label lblSendEmailAddress;
        private System.Windows.Forms.TextBox edSendFtpPort;
        private System.Windows.Forms.Label lblSendFTPPort;
        private System.Windows.Forms.CheckBox cbReceiveSSL;
        private System.Windows.Forms.TextBox edReceiveSubject;
        private System.Windows.Forms.Label lblReceiveSubject;
        private System.Windows.Forms.TextBox edReceiveSender;
        private System.Windows.Forms.Label lblSenderEmail;
        private System.Windows.Forms.Button btnNewProfile;
        private System.Windows.Forms.FolderBrowserDialog fdProfile;
        private System.Windows.Forms.RadioButton rbFTPSendDirect;
        private System.Windows.Forms.TextBox edEventCode;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtTestResults;
        private System.Windows.Forms.Label lblRxResults;
        private System.Windows.Forms.Button bbTestRx;
        private System.Windows.Forms.CheckBox cbGroupCharges;
        private System.Windows.Forms.TextBox edSendSubject;
        private System.Windows.Forms.Label lblSendSubject;
        private System.Windows.Forms.Button btnLoadXSD;
        private System.Windows.Forms.Button btnAddParam;
        private System.Windows.Forms.ListBox lbParameters;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.RadioButton rbSoapSend;
        private System.Windows.Forms.RadioButton rbReceivePickup;
        private System.Windows.Forms.Button btnReceiveFolder;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button btnPickupFolder;
        private System.Windows.Forms.TextBox edReceiveCustomerName;
        private System.Windows.Forms.Label lblReceiveCustomerName;
        private System.Windows.Forms.Button btnFTPTest;
        private System.Windows.Forms.CheckBox cbNotify;
        private System.Windows.Forms.CheckBox cbHideInactive;
        private System.Windows.Forms.ContextMenuStrip cmParameterList;
        private System.Windows.Forms.ToolStripMenuItem removeParameterToolStripMenuItem;
        private System.Windows.Forms.CheckBox cbSendSSL;
        private System.Windows.Forms.Button btnReceiveKeyPath;
        private System.Windows.Forms.Button btnSendKeyPath;
        private System.Windows.Forms.TabControl tcCustomer;
        private System.Windows.Forms.TabPage tbCustomer;
        private System.Windows.Forms.CheckBox cbSatellite;
        private System.Windows.Forms.TextBox edRootPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox edCustomerCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tbSatellite;
        private System.Windows.Forms.CheckBox cbSatelliteMonitored;
        private System.Windows.Forms.Button btnRestartSatellite;
        private System.Windows.Forms.Label LBLpid;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox edSatelliteName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage tbAccounting;
        private System.Windows.Forms.CheckBox cbTrial;
        private System.Windows.Forms.CheckBox cbHold;
        private System.Windows.Forms.GroupBox gbTrialPeriod;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox edShortName;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label lblSendFileType;
        private System.Windows.Forms.CheckBox cbInvoice;
        private System.Windows.Forms.TextBox edSendFileName;
        private System.Windows.Forms.Label lblSendFileNameExpected;
        private System.Windows.Forms.ComboBox cmbSendFileType;
        private System.Windows.Forms.CheckBox cbForwardWithFlags;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox edCWAppCode;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox edCWFileName;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox edCWSubject;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.DataGridView grdCustomerModules;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnAddModule;
        private System.Windows.Forms.ComboBox ddlModuleListing;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox edModuleDescription;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModuleID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ModuleName;
        private System.Windows.Forms.CheckBox cbRenameFile;
        private System.Windows.Forms.Label lblActualPath;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button btnRootPath;
        private System.Windows.Forms.Label lblWarningPath;
        private System.Windows.Forms.CheckBox cbIgnoreMessages;
        private System.Windows.Forms.RadioButton rbReceiveSOAP;
        private System.Windows.Forms.Label lblFromMethodName;
        private System.Windows.Forms.Label lblConToMethod;
        private System.Windows.Forms.Label lblConFromMethod;
        private System.Windows.Forms.Panel panelConFrom;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblToMethodName;
        private System.Windows.Forms.Label lblConCommon;
        private System.Windows.Forms.ComboBox cmbToMethodName;
        private System.Windows.Forms.ComboBox cmbToMethod;
        private System.Windows.Forms.ComboBox cmbFromMethodName;
        private System.Windows.Forms.ComboBox cmbFromMethod;
        private System.Windows.Forms.Label lblLegacyConversion;
        private System.Windows.Forms.ComboBox cmbCustomMethod;
        private System.Windows.Forms.TabPage tdXDS;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox ddlCWContext;
        private System.Windows.Forms.Label lblXSDDataResult;
        private System.Windows.Forms.TreeView tvwXSD;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cmbOutboundType;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.ComboBox cmbOutboundProf;
    }
}