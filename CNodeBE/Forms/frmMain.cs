﻿using CNodeBE.Classes;
using NodeData;
using NodeData.DTO;
using NodeData.Models;
//using NodeLibrary.XmlLib;
//using NodeLibrary.XmlLib.XUE;
using NodeResources;
using SendModule;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using XML_Locker.FreightTracker;
using XMLLocker.Cargowise;
using XMLocker.Cargowise;
using XUS;
using Color = System.Drawing.Color;
//using CWShipment = NodeLibrary.XmlLib.Shipment;

namespace CNodeBE
{


    public partial class FrmMain : Form
    {
        private IMailServerSettings _mailServer { get; set; }
        private System.Timers.Timer tmrMain;
        private System.Timers.Timer tmrHeartBeat;
        System.Timers.Timer _highTideTimer;
        Stopwatch eventStopWatch;
        private bool isLocked;
        private int totfilesRx;
        private int totfilesTx;
        private int cwRx;
        private int prevHour = -1;
        private int cwTx;
        private int filesRx;
        private int filesTx;
        private bool isRestart = false;
        private bool isStop = false;
        private string startTime = "";
        private NodeData.IConnectionManager connMgr;
        private string appVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        bool isDataBound;


        public FrmMain()
        {
            InitializeComponent();
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            tmrHeartBeat = new System.Timers.Timer
            {
                Interval = (int)TimeSpan.FromMinutes(5).TotalMilliseconds,
                Enabled = true
            };
            _highTideTimer = new System.Timers.Timer
            {
                Interval = (int)TimeSpan.FromMinutes(15).TotalMilliseconds
            };
            _highTideTimer.Elapsed += new ElapsedEventHandler(HighTideTimer_Elapsed);
            // set version on form

            lblVersion.Text = "version " + appVersion;
        }

        private void HighTideTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (eventStopWatch.IsRunning)
            {
                if (eventStopWatch.Elapsed.TotalMinutes >= 5)
                {
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " High Tide Timer Activated: Elapsed time" + eventStopWatch.Elapsed.TotalMinutes + "minutes. ", Color.Purple);
                    tmrMain.Stop();
                    tmrMain.Start();
                }
            }
        }

        //TODO: Update Order(Excel) to PCFS and put into Library. 
        //TODO: Create EMOTrans CW -> Global XML routine
        //TODO: Update EMOTrans CW -> Global to put into Library. 

        private Guid FindTransactionID(NodeData.Models.TransReference tr)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
            {
                var trid = uow.Transactions.Find(x => tr.Reference1 == null || x.T_REF1 == tr.Reference1
                                                 && (tr.Reference2 == null || x.T_REF2 == tr.Reference2)
                                                 && (tr.Reference3 == null || x.T_REF3 == tr.Reference3)
                                                 ).FirstOrDefault();
                return trid == null ? Guid.NewGuid() : trid.T_ID;
            }



        }

        public void ProcessTodoList()
        {
            eventStopWatch.Reset();
            eventStopWatch.Start();
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " TODO List - Sending and Cleanup Phase Started.");
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
            {
                var todos = uow.ToDos.GetAll();
                var profList = uow.Profiles.GetAll();
                var todoList = (from t in todos
                                join p in profList on t.L_P equals p.P_ID
                                select new { t.L_ID, t.L_FILENAME, t.L_DATE, p.P_RECIPIENTID, p.P_REASONCODE, p.P_SENDERID, p.P_EVENTCODE }).ToList();
                string ctcMessage = string.Empty;
                Guid L_Id;
                ProfileOperations profOps = new ProfileOperations(connMgr.ConnString);

                if (todoList != null)
                {
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing TODO List ... " + todoList.Count.ToString());
                    foreach (var t in todoList)
                    {

                        vw_CustomerProfile custProfileRecord = profOps.GetProfile(t.P_SENDERID, t.P_RECIPIENTID, t.P_REASONCODE, t.P_EVENTCODE, string.Empty, string.Empty, string.Empty);
                        if (custProfileRecord != null)
                        {
                            if (custProfileRecord.P_LIBNAME == "COMMON")
                            {
                                continue;
                            }
                            if (File.Exists(t.L_FILENAME))
                            {
                                ProcessFileDelivery(custProfileRecord, t.L_FILENAME, null);
                            }


                        }

                        else
                        {
                            //File no longer found
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Not Found: " + t.L_FILENAME + ". " + "");
                            ctcMessage = " Error File Not Found: " + t.L_FILENAME + " .";
                        }
                        L_Id = t.L_ID;
                        RemoveFromTodo(L_Id);
                    }

                }


            }
            eventStopWatch.Stop();
        }

        private void tmrMain_Elapsed(object sender, ElapsedEventArgs e)
        {
            tmrMain.Stop();
            ListSatellites();
            MonitorLogs();
            ProcessInboundQueue();
            ScanXMLFolders();
            ProcessOutboundFolder();
            tslMain.Text = "Next Process at " + DateTime.Now.AddMinutes(3);
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Timer - Next Process at " + DateTime.Now.AddMinutes(3));
            System.Windows.Forms.Application.DoEvents();
            if (btnStart.Text == "&Stop")
            {
                tmrMain.Start();

            }

        }


        private void MonitorLogs()
        {
            if (DateTime.Now.ToString("HH:mm") == "00:00" || DateTime.Now.ToString("HH:mm") == "00:01")
            {
                var systemPath = System.Environment.
                            GetFolderPath(
                                Environment.SpecialFolder.Desktop
                            );
                string rtf = edLog.Rtf;
                byte[] temp = System.Text.Encoding.UTF8.GetBytes(rtf);

                string logs = System.Text.Encoding.UTF8.GetString(temp);


                File.WriteAllText(Path.Combine(systemPath, DateTime.Now.ToString("MM_dd_yyyy") + "CTCNodeBE-Log.txt"), logs);
                edLog.Text = "";
            }
        }

        private void CheckRestart()
        {

            isStop = true;
            btnStart_Click(btnStart, null);

        }



        //This handles all of the Files in the Task List where the TL_Processed = false;

        private void ProcessInboundQueue()
        {
            string tempStart = string.Format("{0:g} ", DateTime.Now);

            if (prevHour != DateTime.Now.Hour)
            {
                CheckRestart();
            }
            else
            {
                if (!cbInbound.Checked)
                {
                    tslMain.Text = "Bypass Inbound Queue as not flagged Active...";
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Bypass Inbound Queue as not flagged Active...");
                    return;
                }
                tslMain.Text = "Processing Inbound Queue - Tasklist...";
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Inbound Queue - Tasklist...");
                eventStopWatch.Reset();
                eventStopWatch.Start();

                //List<TaskList> inboundQueue;
                using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.NodeDataContext(connMgr.ConnString)))
                {
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Inbound Queue - Finding Unprocessed...");
                    var inboundQueue = uow.TaskLists.Find(x => !x.TL_Processed).OrderBy(x => x.TL_ReceiveDate).ToList();
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Inbound Queue - Tasklist count = " + inboundQueue.Count.ToString());
                    tslMain.Text = "Processing Tasklist ... " + inboundQueue.Count.ToString();
                    if (inboundQueue.Count > 0)
                    {
                        int cyclecount = 0;
                        Guid pid = new Guid();
                        foreach (NodeData.Models.TaskList t in inboundQueue)
                        {
                            tslMain.Text = "Processing Inbound Tasks ... " + cyclecount.ToString() + " of " + inboundQueue.Count.ToString();
                            if (cyclecount < Globals.CycleCount)
                            {

                                ProfileOperations profOps = new ProfileOperations(connMgr.ConnString);
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Inbound Queue - File = " + t.TL_FileName);
                                if (File.Exists(Path.Combine(t.TL_Path, t.TL_FileName)))
                                {
                                    ProcessResult pr = new ProcessResult();
                                    string ext = Path.GetExtension(t.TL_FileName);
                                    if (ext.ToUpper().Contains("XML"))
                                    {
                                        if (CWFunctions.IsCargowiseFile(Path.Combine(t.TL_Path, t.TL_FileName)))
                                        {
                                            // This is a Cargowise file. to be Processed
                                            string eventCode = CWFunctions.GetCodefromCW(Path.Combine(t.TL_Path, t.TL_FileName), "EventType");
                                            if (eventCode == "NotFound")
                                            {
                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Event Code not Found error - File No longer Exists. Existing ", Color.Orange);
                                                continue;
                                            }
                                            string actionPurpose = CWFunctions.GetCodefromCW(Path.Combine(t.TL_Path, t.TL_FileName), "ActionPurpose");
                                            // If purpose type null or error then check legacy code type Purpose
                                            if (CWFunctions.GetXMLType(Path.Combine(t.TL_Path, t.TL_FileName)) == "XMS")
                                            {
                                                string legEventCode = CWFunctions.GetCodefromCW(Path.Combine(t.TL_Path, t.TL_FileName), "Purpose");
                                                actionPurpose = !string.IsNullOrEmpty(legEventCode) ? legEventCode : "";
                                            }

                                            var profile = profOps.GetProfile(t.TL_SenderID, t.TL_RecipientID, actionPurpose, eventCode, t.TL_Subject, string.Empty, Path.Combine(t.TL_Path, t.TL_FileName));
                                            //ProfileErrorNotification(profile, t, eventCode, actionPurpose);
                                            if (!ProfileErrorNotification(profile, t, eventCode, actionPurpose))
                                            {
                                                NodeResources.MoveFile(Path.Combine(t.TL_Path, t.TL_FileName), Globals.FailLoc);
                                                //continue - break and goes back to loop
                                                continue;
                                            }

                                            pid = profile.P_ID;
                                            //pid = ProfileOperations.CustProfileID(t.TL_SenderID, t.TL_RecipientID, eventCode, actionPurpose, t.TL_Subject, Path.Combine(t.TL_Path, t.TL_FileName));
                                            pr = ProcessCargowiseXMLFile(Path.Combine(t.TL_Path, t.TL_FileName), t);
                                        }
                                        else
                                        {
                                            if (t.TL_P != null)
                                            {
                                                if (t.TL_P != Guid.Empty)
                                                {
                                                    var profile = profOps.GetProfile(t);
                                                    if (profile == null)
                                                    {
                                                        profile = profOps.GetInactiveProfile(t);

                                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " WARNING: Profile matched but no Active Profiles found. Check Email ", Color.Red);
                                                        using (MailModule mail = new MailModule(_mailServer))
                                                        {
                                                            mail.SendMsg(Path.Combine(t.TL_Path, t.TL_FileName), Globals.AlertsTo, "Inactive File Received :" + t.TL_FileName, "WARNING: File recieved but found profile is inactive." + Environment.NewLine +
                                                                "Check the following profile: " + Environment.NewLine +
                                                                "Customer Name: " + profile.C_NAME + Environment.NewLine +
                                                                "Profile Name: " + profile.P_DESCRIPTION + Environment.NewLine +
                                                                "File will be removed from queue and is attached to this email.");
                                                        }
                                                        NodeResources.MoveFile(Path.Combine(t.TL_Path, t.TL_FileName), Globals.FailLoc);
                                                        continue;

                                                    }
                                                    pid = (Guid)t.TL_P;
                                                    pr = ProcessCustomMethod(profile, t);
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (t.TL_P != null)
                                        {
                                            if (t.TL_P != Guid.Empty)
                                            {
                                                var profile = profOps.GetProfile(t);
                                                pid = (Guid)t.TL_P;
                                                pr = ProcessCustomMethod(profile, t);
                                            }
                                        }
                                    }
                                    if (pid != null)
                                    {
                                        t.TL_P = pid;
                                        if (pr.Processed)
                                        {
                                            t.TL_Processed = true;
                                            t.TL_ProcessDate = DateTime.Now;
                                            try
                                            {
                                                File.Delete(pr.FileName);
                                            }
                                            catch (Exception)
                                            {

                                            }

                                        }
                                        uow.Complete();
                                    }

                                }
                                else
                                {
                                    string message = Path.Combine(t.TL_ReceivedFolder, t.TL_FileName);
                                    t.TL_Processed = true;
                                    t.TL_ProcessDate = DateTime.Now;
                                    if (t.TL_ReceivedFolder == @"\\ctc-nas2.ctc.local\Public\Helpdesk\xml\ICE")
                                    {

                                        try
                                        {
                                            File.Delete(t.TL_FileName);
                                        }
                                        catch (Exception)
                                        {

                                        }

                                    }
                                    try
                                    {
                                        uow.Complete();
                                    }
                                    catch (CommitFailedException ex)
                                    {
                                        var errorMessages = ex.Message;
                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Commit Failed Error on file " + t.TL_FileName + ".Sending Alert via email", Color.Red);
                                        if(t.TL_ErrorCounter < 3)
                                        {
                                            using (MailModule mail = new MailModule(_mailServer))
                                            {
                                                mail.SendMsg(message, Globals.AlertsTo, errorMessages + " on File: " + t.TL_FileName, "There was an error during the commit Process of the ProcessInboundQueue. Error message is : " + ex.Message + Environment.NewLine + ex.Source);
                                            }

                                            t.TL_ErrorCounter += 1;

                                            uow.Complete();
                                        }
                                        

                                    }
                                    catch (DbEntityValidationException ex)
                                    {
                                        var errorMessages = ex.EntityValidationErrors
                            .SelectMany(x => x.ValidationErrors)
                            .Select(x => x.ErrorMessage);

                                        // Join the list to a single string.
                                        var fullErrorMessage = string.Join("; ", errorMessages);

                                        // Combine the original exception message with the new one.
                                        var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                                        // Throw a new DbEntityValidationException with the improved exception message.
                                        throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                                    }
                                }
                            }
                            cyclecount++;
                        }
                    }
                }
                eventStopWatch.Stop();

            }
        }

        private void ProcessOutboundFolder()
        {

            if (!cbOutbound.Checked)
            {
                tslMain.Text = "Outbound Queue is not flagged Active...";
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Outbound Queue is not flagged Active...");
                return;
            }


            tslMain.Text = "Processing Outbound Queue - Tasklist...";
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Outbound Queue - Tasklist...");
            eventStopWatch.Reset();
            eventStopWatch.Start();

            //List<TaskList> outboundQueue;
            using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.NodeDataContext(connMgr.ConnString)))
            {
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Outbound Queue - Finding Unprocessed...");
                var outboundQueue = uow.OutboundTaskLists.Find(x => !x.OTL_Processed).OrderBy(x => x.OTL_ReceiveDate).ToList();
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Outbound Queue - Tasklist count = " + outboundQueue.Count.ToString());
                tslMain.Text = "Processing Tasklist ... " + outboundQueue.Count.ToString();
                if (outboundQueue.Count > 0)
                {
                    int cyclecount = 0;
                    Guid pid = new Guid();
                    foreach (NodeData.Models.OutboundTaskList t in outboundQueue)
                    {

                        var profile = uow.Profiles.Find(x => x.P_ID == t.OTL_P).FirstOrDefault();

                        if (profile != null)
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Outbound Queue - File = " + t.OTL_FileName);
                            string file = Path.Combine(t.OTL_Path, t.OTL_FileName);
                            if (File.Exists(file))
                            {
                                switch (profile.P_DELIVERY)
                                {
                                    //Pickup (FTP-Node)
                                    case "R":
                                        NodeResources.MoveFile(Path.Combine(t.OTL_Path, t.OTL_FileName), profile.P_SERVER);
                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Outbound Queue - File = " + t.OTL_FileName + "successfully sent.");
                                        break;
                                    //CTC Adapter
                                    case "C":
                                        break;
                                    //FTP - Polling
                                    case "F":
                                        break;
                                    //FTP - Direct
                                    case "D":
                                        IFtpHelper ftpHelper = new FtpHelper(Globals.WinSCPLocation);

                                        var result = ftpHelper.FtpSend(Path.Combine(t.OTL_Path, t.OTL_FileName), profile.P_SERVER, Convert.ToInt32(profile.P_PORT), profile.P_PATH, profile.P_USERNAME, profile.P_PASSWORD);

                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Outbound File = " + t.OTL_FileName + " - Result : " + result);

                                        if (result == "File Sent Ok")
                                        {
                                            t.OTL_Processed = true;
                                            t.OTL_ProcessDate = DateTime.Now;
                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Outbound Queue - File = " + t.OTL_FileName + "successfully sent.");
                                            try
                                            {
                                                File.Delete(file);
                                            }
                                            catch (Exception)
                                            {

                                            }
                                        }
                                        else
                                        {
                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Outbound Queue - File = " + t.OTL_FileName + "transfer failed.");
                                            NodeResources.MoveFile(file, Globals.FailLoc);
                                        }
                                        break;
                                    //Email
                                    case "E":
                                        using (MailModule mail = new MailModule(_mailServer))
                                        {
                                            var sent = mail.SendMsg(file, Globals.AlertsTo, profile.P_SUBJECT, profile.P_MESSAGEDESCR);

                                            if (sent == "Message Sent OK")
                                            {
                                                t.OTL_Processed = true;
                                                t.OTL_ProcessDate = DateTime.Now;
                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Outbound Queue - File = " + t.OTL_FileName + "successfully sent.");
                                                try
                                                {
                                                    File.Delete(file);
                                                }
                                                catch (Exception)
                                                {

                                                }

                                            }
                                            else
                                            {
                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Outbound Queue - File = " + t.OTL_FileName + "transfer failed.");
                                                NodeResources.MoveFile(file, Globals.FailLoc);
                                            }
                                        }
                                        break;
                                    //Web API
                                    case "S":
                                        break;
                                }
                            }

                        }
                        else
                        {
                            t.OTL_Processed = true;
                            t.OTL_ProcessDate = DateTime.Now;
                            NodeResources.MoveFile(Path.Combine(t.OTL_Path, t.OTL_FileName), Globals.FailLoc);
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Oubound Profile not found. ");

                        }

                    }
                    uow.Complete();
                }
                else
                {
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Outbound Queue - No more Outbound Tasks found.");
                }
            }

            eventStopWatch.Stop();



        }

        private bool ProfileErrorNotification(vw_CustomerProfile profile, TaskList t, string eventCode, string actionPurpose)
        {
            bool result = true;
            var profileErrorSubject = string.Empty;
            var profileErrorMsg = string.Empty;
            var profileignore = false;

            var msgEventCode = !string.IsNullOrEmpty(eventCode) ? eventCode : "No Event Code";

            var profileErrorMsgDetails = Environment.NewLine +
                "File removed from the queue and attached to this email" + Environment.NewLine + Environment.NewLine +
                "File name : " + t.TL_FileName + Environment.NewLine +
                "SENDER ID : " + t.TL_SenderID + Environment.NewLine +
                "Recipient ID : " + t.TL_RecipientID + Environment.NewLine +
                "Subject : " + t.TL_Subject + Environment.NewLine +
                "Event Code : " + msgEventCode + Environment.NewLine;

            if (profile == null)
            {
                // This is a Cargowise file. to be Processed - get purpose on null profile

                string legEventCode = CWFunctions.GetCodefromCW(Path.Combine(t.TL_Path, t.TL_FileName), "Purpose");


                profileErrorSubject = "CTCNODE - No profile found for file : " + t.TL_OrginalFileName;
                profileErrorMsg = "No profile found for the attached file." + Environment.NewLine + profileErrorMsgDetails + Environment.NewLine;

                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Profile does not exist for file : " + t.TL_OrginalFileName, Color.Red);
                result = false;
            }
            else
            {
                //check if profile ignore message checked the return false to exit
                profileignore = profile.P_IGNOREMESSAGE == "Y" ? true : false;
                // added to check cargowise profile inactive
                if (profile.P_ACTIVE == "N")
                {
                    profileErrorSubject = "CTCNODE - Profile Inactive for Profile : (" + profile.P_SENDERID + " ) " + profile.P_DESCRIPTION;
                    profileErrorMsg = "Receive profile found but inactive for the attached file." + profileErrorMsgDetails + Environment.NewLine +
                        Environment.NewLine +
                        "Customer Name : " + profile.C_NAME + Environment.NewLine +
                        "Profile Description : " + profile.P_DESCRIPTION + Environment.NewLine;

                    result = false;
                }

            }

            if (!result && !profileignore)
            {
                if(t.TL_ErrorCounter < 3)
                {
                    using (MailModule mail = new MailModule(_mailServer))
                    {
                        mail.SendMsg(Path.Combine(t.TL_Path, t.TL_FileName), Globals.AlertsTo, profileErrorSubject, profileErrorMsg);
                    }
                }
                else
                {
                    result = false;
                }
                
            }
            return result;
        }

        private void ProcessNonXML(Guid guid, object tL_P)
        {
            throw new NotImplementedException();
        }

        private void tmrHead_Elapsed(object sender, ElapsedEventArgs e)
        {
            tmrHeartBeat.Stop();
            //ListSatellites();
            tmrHeartBeat.Start();

        }


        public NodeData.Models.TransReference MoveToProcessing(string xmlfile, string folder, string Ref1, string Ref2)
        {
            var tRef = new NodeData.Models.TransReference
            {
                Ref1Type = NodeData.Models.RefType.Custom,
                Reference1 = Ref1,
                Ref2Type = NodeData.Models.RefType.Operation,
                Reference2 = Ref2,
                Ref3Type = NodeData.Models.RefType.Transaction
            };

            string f = string.Empty;
            f = NodeResources.MoveFile(xmlfile, folder);
            tRef.Reference3 = f;
            if (!f.StartsWith("Warning"))
            {

            }
            return tRef;
        }

        public NodeData.Models.TransReference ProcessInbound(string xmlfile, string folder, string Ref1, string Ref2)
        {
            var tRef = new NodeData.Models.TransReference
            {
                Ref1Type = NodeData.Models.RefType.Custom,
                Reference1 = Ref1,
                Ref2Type = NodeData.Models.RefType.Operation,
                Reference2 = Ref2,
                Ref3Type = NodeData.Models.RefType.Transaction
            };
            string f = string.Empty;
            f = NodeResources.MoveFile(xmlfile, folder);
            tRef.Reference3 = f;
            if (!f.StartsWith("Warning"))
            {

            }

            return tRef;
        }

        private PackingSet GetPackingSetFromLink(XUS.UniversalInterchange cwFile, int packingLineLink)
        {
            PackingSet result = new PackingSet();
            if (cwFile.Body.BodyField.Shipment.PackingLineCollection != null)
            {
                foreach (XUS.PackingLine pl in cwFile.Body.BodyField.Shipment.PackingLineCollection.PackingLine)
                {
                    if (pl.Link == packingLineLink)
                    {
                        result.Pcs = pl.PackQty;
                        result.Weight = pl.Weight;
                        result.Volume = pl.Volume;
                        result.PackType = pl.PackType.Code;
                    }
                }
            }

            return result;

        }

        private string GetContainerNoFromLink(XUS.UniversalInterchange cwFile, int containerLink)
        {
            string result = string.Empty;
            foreach (var cont in cwFile.Body.BodyField.Shipment.ContainerCollection.Container)
            {
                if (cont.Link == containerLink)
                {
                    result = cont.ContainerNumber;
                }
            }
            return result;
        }

        public bool MsgValid(IMsgSummary MsgtoValid, IMsgAtt Attachment, string Recipient, string SenderEmail, string Subject, string FileType)
        {
            bool bValid = false;
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
            {
                var prof = uow.CustProfiles.Find(x => SenderEmail == null || x.P_EMAILADDRESS == SenderEmail
                                                   && Subject == null || x.P_SUBJECT == Subject).FirstOrDefault();
                if (prof != null)
                {
                    int intpos = FileType.IndexOf(prof.P_FILETYPE.ToLower().Trim());
                    bValid = intpos > -1 ? true : false;

                }
                else
                {
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Unable to match Receive profile:  Sender:" + SenderEmail + " Recipient:" + Recipient + " Subject:" + Subject);
                    bValid = false;
                }

            }
            return bValid;
        }



        public void ScanXMLFolders()
        {
            eventStopWatch.Reset();
            eventStopWatch.Start();
            if (!cbOutbound.Checked)
            {
                tslMain.Text = "Processing ScanXML Queue... Not Active";
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Outbound Queue... Not Active");
                return;
            }
            tslMain.Text = "Processing Outbound Queue - Tasklist...";
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Outbound Queue - Tasklist...");

            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Outbound Queue - Scanning for All XML Files...");
            string sFilesPath = string.Empty;
            if (tslMode.Text == "Production")
            {
                sFilesPath = Globals.XMLLoc;
            }
            else
            {
                sFilesPath = Globals.TestingLoc;
            }
            DirectoryInfo cargowiseXMLFolder = new DirectoryInfo(sFilesPath);
            tmrMain.Stop();
            if (Directory.Exists(sFilesPath))
            {
                int cyclecount = 0;
                foreach (var xmlFile in cargowiseXMLFolder.GetFiles("*.XML"))
                {
                    if (cyclecount < Globals.CycleCount)
                    {
                        if (CheckIfStopped())
                        {
                            return;
                        }
                        else
                        {
                            try
                            {
                                ProcessCargowiseXMLFile(xmlFile.FullName, null);
                            }
                            catch (IOException exIO)
                            {
                                if (exIO.Message.Contains("Could not find file"))
                                {
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " WARNING. File Name no longer found. Skipping");
                                }
                            }
                            catch (XmlException Xex)
                            {
                                System.GC.Collect();
                                System.GC.WaitForPendingFinalizers();
                                FixXML(xmlFile.FullName, Xex.LineNumber, Xex.LinePosition);
                                NodeResources.MoveFile(xmlFile.FullName, Globals.FailLoc);
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Error " + Xex.GetType().Name + " found while processing " + xmlFile, Color.Red);
                            }
                            catch (Exception ex)
                            {
                                string strEx = ex.GetType().Name;
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error Found while processing " + xmlFile.FullName + " .Error was:" + ex.Message, Color.Red);


                                NodeResources.MoveFile(xmlFile.FullName, Globals.FailLoc);
                            }
                        }
                        cyclecount++;

                    }

                }
            }
            else
            {
                MessageBox.Show("Please note that the XML Folder does not exist. Please check setup", "Error locating Folder", MessageBoxButtons.OK, MessageBoxIcon.Error);
                FrmSettings FormSettings = new FrmSettings();
                FormSettings.MailServerSettings = _mailServer;
                FormSettings.ShowDialog();
                DialogResult dr = FormSettings.DialogResult;
                if (dr == DialogResult.OK)
                {
                    loadSettings();
                }
            }
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Outbound Queue - No more XML Files found.");

            ProcessTodoList();
            eventStopWatch.Stop();
            tmrMain.Start();
        }

        private void FixXML(string fullName, int lineNumber, int linePosition)
        {
            int iL = 0;
            string newXml = string.Empty;

            using (StreamReader sr = new StreamReader(fullName))
            {
                string line = sr.ReadLine();

                while (line != null)
                {
                    iL++;
                    if (iL > 20)
                    {
                        string newString = string.Empty;
                        bool xmlTag = false;
                        string xmlTagName = string.Empty;

                        for (int iP = 0; iP < line.Length; iP++)
                        {
                            string iR = string.Empty;
                            iR = line[iP].ToString();
                            //newString += line[iP];
                            switch (iR)
                            {
                                case "<":
                                    if (iP == 0)
                                    {
                                        xmlTag = true;
                                        newString += iR;
                                        continue;
                                    }
                                    else
                                    {
                                        if (iP + 1 < line.Length)
                                        {
                                            if (line[iP + 1] == '/')
                                            {
                                                xmlTag = true;
                                                newString += iR;
                                                continue;

                                            }
                                        }
                                        xmlTag = false;
                                    }
                                    break;
                                case ">":
                                    if (xmlTag)
                                    {
                                        xmlTag = false;
                                        newString += iR;
                                        continue;
                                    }
                                    break;
                                case "&":
                                    string suspectChar = iR + line[iP + 1] + line[iP + 2] + line[iP + 3] + line[iP + 4] + line[iP + 5];
                                    switch (suspectChar)
                                    {
                                        case "&quot;":
                                            newString += suspectChar;
                                            iP = iP + 5;
                                            continue;
                                            break;

                                        case "&apos;":
                                            newString += suspectChar;
                                            iP = iP + 5;
                                            continue;
                                            break;
                                        case "&amp;":
                                            newString += suspectChar;
                                            iP = iP + 4;
                                            continue;
                                            break;
                                        case "&lt;":
                                            newString += suspectChar;
                                            iP = iP + 3;
                                            continue;
                                            break;
                                        case "&gt;":
                                            newString += suspectChar;
                                            iP = iP + 3;
                                            continue;
                                            break;
                                    }
                                    break;


                            }
                            if (!xmlTag)
                            {
                                iR = System.Security.SecurityElement.Escape(line[iP].ToString());
                            }
                            newString += iR;

                        }
                        line = newString;
                    }
                    newXml += line + Environment.NewLine;
                    line = sr.ReadLine();
                }

                int icount = 0;
                string filename = Path.GetFileNameWithoutExtension(fullName);
                string ext = Path.GetExtension(fullName);
                string newfile = fullName;
                string path = Path.GetDirectoryName(fullName);
                while (File.Exists(newfile))
                {
                    newfile = Path.Combine(path, filename + icount + ext);
                    icount++;

                }
                using (var newStream = NodeResources.CreateStreamFromString(newXml))
                {
                    using (System.IO.FileStream outputFile = new FileStream(newfile, FileMode.Create))
                    {

                        newStream.CopyTo(outputFile);
                        outputFile.Close();
                    }

                }
            }
        }

        private ProcessResult ProcessCustomMethod(vw_CustomerProfile profile, TaskList task)
        {
            ProcessResult thisResult = new ProcessResult
            {
                Processed = false,
                FileName = Path.Combine(task.TL_Path, task.TL_FileName)
            };
            string file = Path.Combine(task.TL_Path, task.TL_FileName);
            string transaction_file = task.TL_OrginalFileName;
            //if (!string.IsNullOrEmpty(profile.P_METHOD))
            //{
            Type custType = this.GetType();
            MethodInfo custMethodToRun = custType.GetMethod(profile.P_METHOD);
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Sender ID: " + profile.P_SENDERID + " Recipient ID:" + profile.P_RECIPIENTID + "(" + profile.P_DESCRIPTION + ")", Color.Blue);
            switch (profile.P_METHOD)
            {
                case "MoveToProcessing":
                    string newPath = Path.Combine(profile.C_PATH, "Processing");
                    if (!string.IsNullOrEmpty(profile.P_PARAMLIST))
                    {
                        var paramlist = profile.P_PARAMLIST.Split('|').ToList();
                        foreach (var p in paramlist)
                        {
                            if (!string.IsNullOrEmpty(p.Trim()))
                            {
                                DirectoryInfo dir = new DirectoryInfo(Path.GetFullPath(p.Trim()));
                                if (dir.Exists)
                                {
                                    newPath = p.Trim();
                                    break;
                                }
                            }
                        }

                    }
                    var varResult = custMethodToRun.Invoke(this, new object[] { file, newPath, profile.P_XSD, profile.P_DESCRIPTION });
                    if (varResult != null)
                    {
                        AddTransaction(profile.C_ID, profile.P_ID, transaction_file, profile.P_DIRECTION, true, (NodeData.Models.TransReference)varResult);
                        thisResult.Processed = true;
                        thisResult.FileName = file;

                    }
                    break;
                case "ProcessInbound":
                    var varResultInbound = custMethodToRun.Invoke(this, new object[] { file, Globals.XMLLoc, profile.P_XSD, profile.P_DESCRIPTION });
                    if (varResultInbound != null)
                    {
                        AddTransaction(profile.C_ID, profile.P_ID, transaction_file, profile.P_DIRECTION, true, (NodeData.Models.TransReference)varResultInbound);
                        thisResult.Processed = true;
                        thisResult.FileName = file;

                    }
                    break;
                default:
                    thisResult = CheckMsgType(profile, task);

                    break;
            }
            //AddTransactionLog((Guid)dr["P_ID"], dr["C_CODE"].ToString(), xmlFile, "Custom Conversion to Universal XML:" + conversionResult, DateTime.Now, "Y", dr["P_BILLTO"].ToString(), dr["P_RECIPIENTID"].ToString());
            // }
            return thisResult;

        }

        private ProcessResult CheckMsgType(vw_CustomerProfile profile, TaskList task)
        {
            ProcessResult thisResult = new ProcessResult
            {
                Processed = false,
                FileName = Path.Combine(task.TL_Path, task.TL_FileName)
            };
            string file = Path.Combine(task.TL_Path, task.TL_FileName);
            string transaction_file = task.TL_OrginalFileName;
            Type custType = this.GetType();
            MethodInfo custMethodToRun = custType.GetMethod(profile.P_METHOD);
            switch (profile.P_MSGTYPE.Trim())
            {
                case "PDF2XML":
                    string newPath = Path.Combine(Globals.Pdf2Xml, "Processing");
                    if (!string.IsNullOrEmpty(profile.P_PARAMLIST))
                    {
                        var paramlist = profile.P_PARAMLIST.Split('|').ToList();
                        foreach (var p in paramlist)
                        {
                            if (!string.IsNullOrEmpty(p.Trim()))
                            {
                                DirectoryInfo dir = new DirectoryInfo(Path.GetFullPath(p.Trim()));
                                if (dir.Exists)
                                {
                                    newPath = p.Trim();
                                    break;
                                }
                            }
                        }

                    }
                    NodeResources.MoveFile(file, newPath);
                    AddTransaction(profile.C_ID, profile.P_ID, transaction_file, profile.P_DIRECTION, true, new NodeData.Models.TransReference { Ref1Type = NodeData.Models.RefType.FileName, Reference1 = file });
                    thisResult.Processed = true;
                    thisResult.FileName = file;

                    break;
                case "Conversion":
                    if (!string.IsNullOrEmpty(profile.P_LIBNAME))
                    {

                        string libtoLoad = Path.Combine(profile.C_PATH + "\\Lib", profile.P_LIBNAME);
                        if (File.Exists(libtoLoad))
                        {
                            if (File.Exists(file))
                            {
                                Assembly custLib = Assembly.LoadFile(libtoLoad);
                                Type convertMethod = custLib.GetTypes().ToList().FirstOrDefault(f => f.Name == "ConvertToCW");
                                dynamic CustomerXMLLibrary = Activator.CreateInstance(convertMethod);
                                var conversionResult = CustomerXMLLibrary.DoConvert(file, profile.P_SENDERID, profile.P_RECIPIENTID, Globals.XMLLoc);
                                string tr;
                                tr = conversionResult.ToString();
                                var ts = new NodeData.Models.TransReference();
                                ts = ArrtoRef(tr.Split('*'));

                                if (ts.Reference3 == "Conversion Complete")
                                {
                                    AddTransaction(profile.C_ID, profile.P_ID, transaction_file, profile.P_DIRECTION, true, ts);
                                    thisResult.Processed = true;

                                }

                                else
                                {
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Conversion failed :" + ts.Reference3);
                                    NodeResources.MoveFile(file, Globals.FailLoc);
                                    thisResult.Processed = true;

                                }
                            }

                        }

                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(profile.P_METHOD))
                        {
                            var varResultDll = custMethodToRun.Invoke(this, new object[] { file, profile });
                            if (varResultDll != null)
                            {
                                AddTransaction(profile.C_ID, profile.P_ID, transaction_file, "R", true, (NodeData.Models.TransReference)varResultDll);
                                thisResult.Processed = true;

                            }
                        }


                    }
                    break;

                case "Forward":
                    break;

                default:
                    break;
            }
            return thisResult;
        }

        private ProcessResult ProcessCustomFiles(string xmlFile, string custCode)
        {
            ProcessResult thisResult = new ProcessResult
            {
                Processed = true,
                FileName = xmlFile
            };
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File " + Path.GetFileName(xmlFile) + " Is not a Cargowise File. Checking for Against Customer.");
            try
            {
                FileInfo processingFile = new FileInfo(xmlFile);
                filesRx++;
                NodeResources.AddLabel(lblFilesRx, filesRx.ToString());
                totfilesRx++;
                NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
                string msgDir = "";
                var trRef = new NodeData.Models.TransReference();
                string archiveFile;
                string recipientID = "";
                string senderID = "";
                string custPath = Globals.FailLoc;
                string filesPath = string.Empty;

                if (tslMode.Text == "Production")
                {
                    filesPath = Globals.XMLLoc;
                }
                else
                {
                    filesPath = Globals.TestingLoc;
                }
                if (processingFile.Extension.ToUpper() == ".XML")
                {
                    using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
                    {
                        var profList = uow.CustProfiles.Find(x => (x.P_XSD != null && x.P_XSD.Length > 0)
                                                              && (string.IsNullOrEmpty(custCode) || x.C_CODE == custCode)).OrderBy(x => x.C_CODE).ToList();
                        if (profList.Count > 0)
                        {

                            ProfileOperations profOps = new ProfileOperations(connMgr.ConnString);
                            bool processed = false;

                            while (!processed)
                            {
                                XDocument xDoc = XDocument.Load(xmlFile);
                                var ctcXml = xDoc.Descendants();
                                var node = ctcXml.Elements().Where(e => e.Name.LocalName == "SenderID").FirstOrDefault();
                                if (node != null)
                                {
                                    if (node.Value == "COMTECSYD")
                                    {
                                        processed = true;
                                        thisResult.Processed = true;
                                        thisResult.FileName = xmlFile;
                                        return thisResult;

                                    }
                                }
                                CTCErrorCode cTCErrorCode = new CTCErrorCode();
                                foreach (var prof in profList)
                                {
                                    try
                                    {
                                        if (prof.P_ACTIVE == "N")
                                        {
                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Customer profile: (" + prof.C_CODE + ")" + prof.P_DESCRIPTION + " is Not active. Skipping.", Color.Orange);
                                            continue;
                                        }
                                        if (profOps.IdentifyXML(prof, xmlFile) == prof)
                                        {
                                            string schemaFile = Path.Combine(prof.C_PATH, "Lib", prof.P_XSD);
                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Schema File Found (" + schemaFile + "). Continue processing Profile: " + prof.P_DESCRIPTION, Color.Blue);
                                            string custMethod = prof.P_METHOD;
                                            string custParams = prof.P_PARAMLIST;
                                            if (!string.IsNullOrEmpty(custMethod))
                                            {
                                                Type custType = this.GetType();
                                                MethodInfo custMethodToRun = custType.GetMethod(custMethod);
                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " - " + custMethod + " :" + prof.C_PATH + ". " + prof.P_DESCRIPTION);

                                                switch (custMethod)
                                                {
                                                    case "MoveToProcessing":
                                                        string newPath = prof.C_PATH;
                                                        if (!string.IsNullOrEmpty(custParams))
                                                        {
                                                            string[] paramlist = custParams.Split('|');
                                                            if (!string.IsNullOrWhiteSpace(paramlist[0]))
                                                            {
                                                                DirectoryInfo dir = new DirectoryInfo(Path.GetFullPath(paramlist[0]));
                                                                if (dir.Exists)
                                                                {
                                                                    newPath = paramlist[0];
                                                                }
                                                            }

                                                        }
                                                        var varResult = custMethodToRun.Invoke(this, new object[] { xmlFile, newPath, prof.P_XSD, prof.P_DESCRIPTION });
                                                        if (varResult != null)
                                                        {
                                                            AddTransaction(prof.C_ID, prof.P_ID, xmlFile, prof.P_DIRECTION, true, (NodeData.Models.TransReference)varResult);
                                                            processed = true;
                                                            thisResult.FileName = null;
                                                            thisResult.Processed = true;


                                                        }
                                                        break;
                                                    case "ProcessInbound":
                                                        var varResultInbound = custMethodToRun.Invoke(this, new object[] { xmlFile, Globals.XMLLoc, prof.P_XSD, prof.P_DESCRIPTION });
                                                        if (varResultInbound != null)
                                                        {
                                                            AddTransaction(prof.C_ID, prof.P_ID, xmlFile, prof.P_DIRECTION, true, (NodeData.Models.TransReference)varResultInbound);
                                                            processed = true;
                                                            thisResult.FileName = null;
                                                            thisResult.Processed = true;


                                                        }
                                                        break;
                                                    default:
                                                        if (!string.IsNullOrEmpty(prof.P_LIBNAME))
                                                        {
                                                            string libtoLoad = Path.Combine(prof.C_PATH + "\\Lib", prof.P_LIBNAME);
                                                            Assembly custLib = Assembly.LoadFile(libtoLoad);
                                                            Type convertMethod = custLib.GetTypes().ToList().FirstOrDefault(f => f.Name == "ConvertToCW");
                                                            dynamic CustomerXMLLibrary = Activator.CreateInstance(convertMethod);
                                                            var conversionResult = CustomerXMLLibrary.DoConvert(xmlFile, prof.P_SENDERID, prof.P_RECIPIENTID, Globals.XMLLoc);
                                                            string tr;
                                                            tr = conversionResult.ToString();
                                                            var ts = new NodeData.Models.TransReference();
                                                            ts = ArrtoRef(tr.Split('*'));

                                                            if (ts.Reference3 == "Conversion Complete")
                                                            {
                                                                AddTransaction(prof.C_ID, prof.P_ID, xmlFile, msgDir, true, ts);
                                                                processed = true;
                                                                thisResult.Processed = true;
                                                            }

                                                            else
                                                            {
                                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Conversion failed :" + ts.Reference3);
                                                                NodeResources.MoveFile(xmlFile, Globals.FailLoc);
                                                                processed = true;
                                                                thisResult.Processed = true;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            var varResultDll = custMethodToRun.Invoke(this, new object[] { xmlFile, prof });
                                                            if (varResultDll != null)
                                                            {
                                                                AddTransaction(prof.C_ID, prof.P_ID, xmlFile, msgDir, true, (NodeData.Models.TransReference)varResultDll);
                                                                processed = true;
                                                                thisResult.Processed = true;
                                                            }
                                                        }

                                                        break;
                                                }

                                            }
                                            else
                                            {
                                                string ftpSend = string.Empty;

                                                ProcessFileDelivery(prof, xmlFile, null);

                                            }

                                            recipientID = prof.P_RECIPIENTID;
                                            senderID = prof.P_SENDERID;
                                            archiveFile = Globals.ArchiveFile(prof.C_PATH + "\\Archive", xmlFile);
                                            processed = true;
                                            thisResult.Processed = true;
                                            break;
                                        }


                                    }
                                    catch (XmlSchemaValidationException ex)
                                    {
                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Invalid Schema found. Continue Scanning for matching Customer Schema ");
                                        if (prof.P_NOTIFY == "Y")
                                        {
                                            using (MailModule mail = new MailModule(_mailServer))
                                            {
                                                mail.SendMsg("", Globals.AlertsTo, "XML Schema Validation issue", "Invalid schema definition error:" + ex.Message);
                                            }

                                        }


                                    }
                                    catch (XmlException ex)
                                    {
                                        ProcessingErrors procerror = new ProcessingErrors
                                        {
                                            ErrorCode = new CTCErrorCode
                                            {
                                                Code = NodeError.e111,
                                                Description = "Customer XML Error " + ex.Message + ". ",
                                                Severity = "Warning"

                                            },
                                            FileName = xmlFile,
                                            RecipientId = recipientID,
                                            SenderId = senderID
                                        };

                                        AddProcessingError(procerror);
                                        thisResult.FolderLoc = Globals.FailLoc;
                                        thisResult.Processed = false;
                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node (XML) Exception Found: Custom Processing:" + ex.Message + ". ", Color.Red);
                                        //   return thisResult;

                                    }
                                    catch (Exception ex)
                                    {
                                        ProcessingErrors procerror = new ProcessingErrors
                                        {
                                            ErrorCode = new CTCErrorCode
                                            {
                                                Code = NodeError.e111,
                                                Description = "Customer XML Error: " + ex.Message + ". ",
                                                Severity = "Warning"
                                            },
                                            FileName = xmlFile,
                                            RecipientId = recipientID,
                                            SenderId = senderID
                                        };

                                        AddProcessingError(procerror);
                                        thisResult.FolderLoc = Globals.FailLoc;
                                        thisResult.Processed = false;
                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node (Trap All) Exception Found: Custom Processing:" + ex.Message + ". ", Color.Red);
                                        //   return thisResult;
                                    }
                                }
                                if (!processed)
                                {
                                    ProcessingErrors procerror = new ProcessingErrors
                                    {
                                        ErrorCode = new CTCErrorCode
                                        {
                                            Code = NodeError.e104,
                                            Description = "Unable to process: " + Path.GetFileName(xmlFile) + ". No matching routines found. Moving to failed.",
                                            Severity = "Error"

                                        },
                                        FileName = xmlFile,
                                        RecipientId = recipientID,
                                        SenderId = senderID
                                    };

                                    AddProcessingError(procerror);
                                    thisResult.FolderLoc = Globals.FailLoc;
                                    thisResult.Processed = false;
                                    NodeResources.MoveFile(Path.GetFileName(xmlFile), Path.Combine(Path.Combine(filesPath, "Failed")));
                                    //File.Move(xmlFile, Path.Combine(Path.Combine(Globals.XMLLoc, "Failed"), Path.GetFileName(xmlFile)));
                                    AddProcessingError(procerror);
                                    thisResult.Processed = false;
                                    thisResult.FolderLoc = custPath;
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Unable to process " + Path.GetFileName(xmlFile) + "No matching routines found. " + "");
                                    processed = true;
                                    //  return thisResult;
                                }

                            }



                        }


                    }

                }
            }
            catch (Exception ex)
            {

                ProcessingErrors procerror = new ProcessingErrors
                {
                    ErrorCode = new CTCErrorCode
                    {
                        Code = NodeError.e104,
                        Description = "customer XML Error: " + ex.Message + ". "
                    }

                };
                thisResult.FolderLoc = Globals.FailLoc;
                thisResult.Processed = false;
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node (Trap All) Exception Found: Custom Processing:" + ex.Message + ". ", Color.Red);
                //   return thisResult;
            }

            return thisResult;
        }

        private NodeData.Models.TransReference ArrtoRef(string[] v)
        {
            string[] r3;
            var result = new NodeData.Models.TransReference();
            switch (v.Count())
            {
                case 1:
                    r3 = v[0].Split('|');
                    result.Ref3Type = NodeData.Models.RefType.Operation;
                    result.Reference3 = r3[1];
                    break;
                case 3:
                    string[] r1 = v[0].Split('|');
                    result.Ref1Type = (NodeData.Models.RefType)System.Enum.Parse(typeof(NodeData.Models.RefType), r1[0]);
                    result.Reference1 = r1[1];
                    string[] r2 = v[1].Split('|');
                    result.Ref2Type = (NodeData.Models.RefType)System.Enum.Parse(typeof(NodeData.Models.RefType), r2[0]);
                    result.Reference2 = r2[1];
                    r3 = v[2].Split('|');
                    result.Ref3Type = (NodeData.Models.RefType)System.Enum.Parse(typeof(NodeData.Models.RefType), r3[0]);
                    result.Reference3 = r3[1];

                    break;

            }

            return result;
        }

        private void FillTodoList(string pathToScan, Guid profileID)
        {
            DirectoryInfo diTodo = new DirectoryInfo(pathToScan);
            foreach (var fileTodo in diTodo.GetFiles())
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
                {
                    var todo = uow.ToDos.Find(x => x.L_P == profileID && x.L_FILENAME == fileTodo.FullName).FirstOrDefault();
                    if (todo == null)
                    {
                        uow.ToDos.Add(new ToDo
                        {
                            L_P = profileID,
                            L_FILENAME = fileTodo.FullName,
                            L_DATE = DateTime.Now,
                            L_LASTRESULT = "Added to Todo List",
                            L_ID = Guid.NewGuid()
                        });
                        uow.Complete();
                    }
                }
            }

        }

        private string GetCustPath(string senderID)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
            {
                var custpath = uow.Customers.Find(x => x.C_CODE == senderID).FirstOrDefault();
                return custpath != null ? custpath.C_PATH : string.Empty;
            }

        }
        private Guid GetCustId(string senderID)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
            {
                var custid = uow.Customers.Find(x => x.C_CODE == senderID).FirstOrDefault().C_ID;
                return custid;
            }
        }

        private Guid AddTodo(Guid P_ID, string fileName, string lastresult, DateTime lastdate)
        {
            ToDo todo;
            Guid todoid = Guid.Empty;
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
                {
                    todo = uow.ToDos.Find(x => x.L_P == P_ID && x.L_FILENAME == fileName).FirstOrDefault();
                    if (todo == null)
                    {
                        todo = new ToDo();
                    }
                    todo.L_P = P_ID;
                    todo.L_FILENAME = fileName;
                    todo.L_DATE = lastdate;
                    todo.L_LASTRESULT = lastresult;
                    if (todo.L_ID == Guid.Empty)
                    {
                        uow.ToDos.Add(todo);
                    }
                    uow.Complete();
                    todoid = todo.L_ID;
                }
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " when adding Todo Record : " + ex.Message, Color.Red);
                using (MailModule mail = new MailModule(_mailServer))
                {
                    mail.SendMsg("", Globals.AlertsTo, "Error Adding Todo Record ", "Error adding Todo Record. Error was:" + strEx + ": " + ex.Message);
                }
            }
            return todoid;
        }

        private void AddTransaction(Guid custid, Guid profile, string fileName, string direction, bool original, NodeData.Models.TransReference trRefs)
        {
            try
            {
                Transaction newTransaction = new Transaction();
                string msgType = original ? "Original" : "Duplicate";
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
                {
                    newTransaction = uow.Transactions.Find(x => x.T_FILENAME == fileName && x.T_MSGTYPE == msgType).FirstOrDefault();
                    if (newTransaction == null)
                    {
                        newTransaction = new Transaction();
                    }
                    newTransaction.T_C = custid;
                    newTransaction.T_P = profile;
                    newTransaction.T_FILENAME = Path.GetFileName(fileName);
                    newTransaction.T_DATETIME = DateTime.Now;
                    newTransaction.T_TRIAL = "N";
                    newTransaction.T_DIRECTION = direction;
                    newTransaction.T_REF1 = trRefs == null ? null : trRefs.Reference1;
                    newTransaction.T_REF2 = trRefs == null ? null : trRefs.Reference2;
                    newTransaction.T_REF3 = trRefs == null ? null : trRefs.Reference3.Truncate(200);
                    newTransaction.T_REF1TYPE = trRefs == null ? null : trRefs.Ref1Type.ToString();
                    newTransaction.T_REF2TYPE = trRefs == null ? null : trRefs.Ref2Type.ToString();
                    newTransaction.T_REF3TYPE = trRefs == null ? null : trRefs.Ref3Type.ToString();
                    newTransaction.T_MSGTYPE = msgType;
                    newTransaction.T_INVOICED = "N";
                    newTransaction.T_CHARGEABLE = "Y";
                    if (newTransaction.T_ID == Guid.Empty)
                    {
                        uow.Transactions.Add(newTransaction);
                    }
                    uow.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                string msgType = original ? "Original" : "Duplicate";
                string innerMessage = (ex.InnerException != null) ? ex.InnerException.Message : "Inner Error Empty";

                var errorMessages = ex.EntityValidationErrors
                   .SelectMany(x => x.ValidationErrors)
                   .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                string strEx = ex.GetType().Name;
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " when adding Transaction Record : " + ex.Message, Color.Red);
                using (MailModule mail = new MailModule(_mailServer))
                {
                    var profileErrorMsgDetails = Environment.NewLine +
                        "Error on NodeBE AddTransaction - catch DbEntityValidationException " + Environment.NewLine +
                        "Message Type : " + msgType + Environment.NewLine +
                        ex.GetType().Name + " : Error adding Transaction Record. " + Environment.NewLine +
                        "Error = " + strEx + " : " + exceptionMessage + Environment.NewLine +
                        "Inner Error = " + innerMessage + " : " + Environment.NewLine +
                        "File Name = " + fileName;
                    mail.SendMsg("", Globals.AlertsTo, "Error Adding Transaction Record - DbEntityValidationException", profileErrorMsgDetails);
                }
                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
            catch (Exception ex)
            {
                string msgType = original ? "Original" : "Duplicate";
                string strEx = ex.GetType().Name;
                string innerMessage = (ex.InnerException != null) ? ex.InnerException.Message : "Inner Error Empty";

                using (MailModule mail = new MailModule(_mailServer))
                {
                    var profileErrorMsgDetails = Environment.NewLine +
                        "Error on NodeBE AddTransaction - catch Exception" + Environment.NewLine +
                        "Message Type : " + msgType + Environment.NewLine +
                        ex.GetType().Name + " : Error adding Transaction Record." + Environment.NewLine +
                        "Error = " + strEx + " : " + ex.Message + Environment.NewLine +
                        "Inner Error = " + innerMessage + " : " + Environment.NewLine + Environment.NewLine +
                        "File Name = " + fileName;
                    mail.SendMsg("", Globals.AlertsTo, "Error Adding Transaction Record - Exception", profileErrorMsgDetails);
                }
            }
        }



        private void AddTransactionLog(Guid P_ID, string senderid, string filename, string lastresult, DateTime lastdate, string success, string cust, string recipientid)
        {
            Guid custid = Guid.Empty;
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
            {
                uow.TransLogs.Add(new TransactionLog
                {
                    X_FILENAME = Path.GetFileName(filename),
                    X_LASTRESULT = lastresult.Truncate(120),
                    X_DATE = lastdate,
                    X_C = GetCustId(cust),
                    X_P = P_ID,
                    X_SUCCESS = success
                });
                uow.Complete();
            }
        }

        private ProcessResult ProcessUDM(string xmlfile, string originalFile, vw_CustomerProfile profile)
        {
            FileInfo processingFile = new FileInfo(xmlfile);
            ProcessResult processResult = new ProcessResult();
            ProcessResult thisResult = processResult;
            thisResult.Processed = true;
            XUS.UniversalInterchange cwFile = new XUS.UniversalInterchange();
            using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(XUS.UniversalInterchange));
                cwFile = (XUS.UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            if (cwFile.Body.BodyField == null)
            {
                //TODO: Process as XUA
                return ProcessXUA(xmlfile, originalFile);
            }
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            cwRx++;
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());
            string senderID = string.Empty;
            string recipientID = string.Empty;
            string reasonCode = cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose != null ? cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose.Code : string.Empty;
            string eventCode = cwFile.Body.BodyField.Shipment.DataContext.EventType != null ? cwFile.Body.BodyField.Shipment.DataContext.EventType.Code : string.Empty; ;
            string msgDir = "";
            // BuildTransRef will Identify 3 References and their reference type.
            var trRef = BuildTransRef(cwFile);
            try
            {
                cwRx++;
                NodeResources.AddLabel(lblCwRx, cwRx.ToString());
                senderID = cwFile.Header.SenderID;
                recipientID = cwFile.Header.RecipientID;
                // These are Transmod files. Looking for Local Transport and transport Booking and a Purpose code of TRN
                if (trRef.Ref1Type == NodeData.Models.RefType.Transport && reasonCode == "TRN")
                {


                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Transmod Local Transport (" + senderID + ")", Color.Blue);
                    var tModString = new ConnectionManager(Globals.TransServer, Globals.TransDatabase, Globals.TransUsername, Globals.TransPassword).ConnString;
                    TransModFunctions tModFunction = new TransModFunctions(tModString);
                    // Process the Transmod file into the Database
                    var proc = tModFunction.ProcessImportLocalTransport(cwFile);
                    proc.FileName = xmlfile;
                    if (tModFunction.Skipped)
                    {
                        // Only if there was an error OR it is FCL (Transport Booking)
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + tModFunction.ErrMsg + "  (" + senderID + ":" + trRef.Reference1 + ")");
                        return proc;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tModFunction.ErrMsg))
                        {
                            //Something went pear shaped. Send the message and the error file
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Error Importing Transport Job :  (" + senderID + ":" + trRef.Reference1 + ")", Color.Red);
                            using (MailModule mail = new MailModule(_mailServer))
                            {
                                mail.SendMsg(xmlfile, "support@ctcorp.com.au", "Error Importing Transport Job: (" + senderID + ":" + trRef.Reference1 + ")",
                                    tModFunction.ErrMsg + Environment.NewLine +
                                    "SenderID:" + senderID + Environment.NewLine +
                                                "Recipient:" + recipientID + Environment.NewLine +
                                                "Purpose Code:" + reasonCode + Environment.NewLine +
                                                "Event Code: " + eventCode + Environment.NewLine
                                                );
                            }
                        }
                        else
                        {
                            //File Processed
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Transport Job :  (" + senderID + ":" + trRef.Reference1 + ")", Color.Green);
                        }
                    }
                    // Drop out of this function as nothing else is needed with this file. 
                    return proc;
                }
                //if (trRef.Ref1Type == NodeData.Models.RefType.Transport && reasonCode == "TRN")
                //{
                //    ProcessResult proc = processResult;
                //    // Globals.ArchiveFile(Globals.ArchLoc, xmlfile);
                //    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Transmod Transport Booking (" + senderID + ")", Color.Blue);
                //    proc = TransModFunctions.(xmlfile);
                //    return (proc);
                //}

                FileProcessor fp = new FileProcessor(xmlfile, connMgr.ConnString);
                ApiLog cNodeLog = fp.CheckApiLog(originalFile, xmlfile);
                if (profile == null)
                {
                    ProfileOperations profOps = new ProfileOperations(connMgr.ConnString);
                    profile = profOps.GetProfile(senderID, recipientID, reasonCode, eventCode, cNodeLog.F_SUBJECT, cNodeLog.F_APPCODE, cNodeLog.F_CWFILENAME);
                    if (profile == null)
                    {

                        // No customer Profile found so will move to Root Path for Pickup. 

                        string cwxml = string.Empty;
                        string custpath = Path.Combine(GetCustPath(senderID), "Failed");

                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + recipientID + " Does not have a Document Profile. Moving to Root Path" + "");
                        using (MailModule mail = new MailModule(_mailServer))
                        {
                            mail.SendMsg(xmlfile, "andy.duggan@ctcorp.com.au", "Missing profile for " + senderID, "SenderID:" + senderID + Environment.NewLine +
                                            "Recipient:" + recipientID + Environment.NewLine +
                                            "Purpose Code:" + reasonCode + Environment.NewLine +
                                            "Event Code: " + eventCode + Environment.NewLine +
                                            "Moving File to : " + custpath);
                        }
                        if (!Directory.Exists(custpath) || (custpath == "Failed"))
                        {
                            try
                            {
                                Directory.CreateDirectory(custpath);
                            }
                            catch (IOException)
                            {
                                using (MailModule mail = new MailModule(_mailServer))
                                {
                                    mail.SendMsg(xmlfile, "andy.duggan@ctcorp.com.au", "Unable to create Path", "Unable to Create Fail Path :" + custpath);
                                }
                                thisResult.FolderLoc = Globals.FailLoc;
                                thisResult.Processed = false;
                                thisResult.FileName = xmlfile;
                                return thisResult;
                            }
                        }
                        NodeResources.MoveFile(xmlfile, custpath);
                        ProcessingErrors procerror = new ProcessingErrors
                        {
                            ErrorCode = new CTCErrorCode
                            {
                                Code = NodeError.e100,
                                Description = "Customer Profile Not Found. Move to Retry folder"
                            },
                            FileName = xmlfile,
                            RecipientId = recipientID,
                            SenderId = senderID
                        };
                        AddProcessingError(procerror);
                        thisResult.FolderLoc = Globals.FailLoc;
                        thisResult.Processed = false;
                        thisResult.FileName = xmlfile;
                        return thisResult;
                    }

                }
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Sender ID: " + senderID + " Recipient ID:" + recipientID + "(" + eventCode + "|" + reasonCode + ")", Color.Blue);

                Guid remID = Guid.NewGuid();


                //Is Client Active and not on hold
                if (profile.C_IS_ACTIVE != "Y")
                {
                    if (string.IsNullOrEmpty(profile.C_ON_HOLD))
                    {
                        NodeResources.MoveFile(xmlfile, Globals.FailLoc);
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Invalid XUS with no profile found. Moving to Failed and skipping", Color.Red);

                    }
                    else
                    {
                        NodeResources.MoveFile(xmlfile, Path.Combine(profile.C_PATH, "Held"));
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + profile.C_NAME + " is not Active or is on Hold. Moving to Hold and skipping", Color.Orange);

                    }
                    thisResult.Processed = true;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }
                else
                {
                    if (profile.P_ACTIVE == "N")
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Customer profile: (" + profile.C_CODE + ")" + profile.P_DESCRIPTION + " is Not active. Skipping.", Color.Orange);
                        thisResult.Processed = true;
                        thisResult.FileName = xmlfile;
                        return thisResult;
                    }
                    else
                    {
                        remID = AddTodo(profile.P_ID, xmlfile, "Add_Transaction", DateTime.Now);
                        AddTransaction(profile.C_ID, profile.P_ID, string.IsNullOrEmpty(originalFile) ? Path.GetFileName(xmlfile) : originalFile, msgDir, true, trRef);
                        if (profile.P_DTS == "Y")
                        {
                            remID = AddTodo(profile.P_ID, xmlfile, "DTS_Processing", DateTime.Now);
                            DataTransformService DTS = new DataTransformService(Globals.DbConn);

                            DTS.Transform(profile.P_ID, xmlfile, _mailServer);
                        }
                        if (!string.IsNullOrEmpty(profile.P_MSGTYPE))
                        {
                            if (profile.P_MSGTYPE.Trim() == "Conversion")
                            {
                                // Rename the Cargowise XML File to the new Global XML
                                //TODO: Check the Accounting issue here. Doubling Accounting charge.
                                thisResult = ConvertCWFile(profile, xmlfile);
                                //AddTransaction(custProfileRecord.C_ID, custProfileRecord.P_ID, Path.GetFileName(xmlfile), msgDir, true, trRef, archiveFile);
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Conversion for Profile : " + profile.P_DESCRIPTION + " complete.");
                                return thisResult;
                            }
                        }
                    }


                    //Create the Delete from TODO list ready to be removed
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Profile : " + profile.P_DESCRIPTION);
                    thisResult = ProcessFileDelivery(profile, xmlfile, cNodeLog);
                    if (thisResult.Processed)
                    {
                        RemoveFromTodo(remID);
                    }
                    //addSummRecord(DateTime.Now, recipientID, senderID, custProfileRecord.P_MSGTYPE, trRef, xmlfile, result);

                }
                return thisResult;
            }
            catch (System.InvalidOperationException ex)
            {
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Cargowise File Failed: " + ex.Message);

                string err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                ProcessingErrors procerror = new ProcessingErrors
                {
                    ErrorCode = new CTCErrorCode
                    {
                        Code = NodeError.e111,
                        Description = "Processing Cargowise File Failed: " + ex.Message
                    },
                    FileName = xmlfile,
                    RecipientId = recipientID,
                    SenderId = senderID
                };
                AddProcessingError(procerror);
                thisResult.FolderLoc = Globals.FailLoc;
                thisResult.FileName = xmlfile;
                thisResult.Processed = false;
                return thisResult;
            }
            catch (IOException ex)
            {
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Process Cargowise XML Error Found: " + ex.Message + ". " + "");
                string err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                ProcessingErrors procerror = new ProcessingErrors
                {
                    ErrorCode = new CTCErrorCode
                    {
                        Code = NodeError.e109,
                        Description = "Processing Cargowise File Failed: " + ex.Message
                    },
                    FileName = xmlfile,
                    RecipientId = recipientID,
                    SenderId = senderID
                };

                AddProcessingError(procerror);
                thisResult.FolderLoc = Globals.FailLoc;
                thisResult.Processed = false;
                thisResult.FileName = xmlfile;
                return thisResult;
            }
            catch (Exception ex)
            {
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Process Cargowise XML Error Found: " + ex.Message + ". " + "");
                string err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                ProcessingErrors procerror = new ProcessingErrors
                {
                    ErrorCode = new CTCErrorCode
                    {
                        Code = NodeError.e111,
                        Description = "Processing Cargowise File Failed: " + ex.Message
                    },
                    FileName = xmlfile,
                    RecipientId = recipientID,
                    SenderId = senderID
                };

                AddProcessingError(procerror);
                thisResult.FolderLoc = Globals.FailLoc;
                thisResult.Processed = false;
                thisResult.FileName = xmlfile;
                return thisResult;
            }

        }

        private NodeData.Models.TransReference BuildTransRef(UniversalInterchange cwFile)
        {
            //Build Transaction References from DataContext
            List<XUS.DataSource> dscoll = new List<XUS.DataSource>();
            XUS.DataContext dc = new XUS.DataContext();
            dc = cwFile.Body.BodyField.Shipment.DataContext;
            int iS = 0; //shipment counter
            int iB = 0; //Brokerage Counter
            int iC = 0; // consol Counter
            int iO = 0; // Order Manager Counter
            int iT = 0; //Port and Local Transport Counter
            var result = new NodeData.Models.TransReference();
            if (dc.DataSourceCollection != null)
            {
                try
                {

                    dscoll = cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection.ToList();
                }
                catch (Exception)
                {
                    result.Ref3Type = NodeData.Models.RefType.Error;
                    result.Reference3 = "XML File Missing DataSourceCollection tag";
                    return result;
                }
                try
                {

                    var trn = (from t in dscoll
                               where t.Type == "LocalTransport" || t.Type == "TransportBooking"
                               select t).FirstOrDefault();
                    if (trn != null)
                    {
                        result.Ref1Type = NodeData.Models.RefType.Transport;
                        result.Reference1 = trn.Key;
                        return result;
                    }
                    foreach (XUS.DataSource ds in dscoll)
                    {
                        switch (ds.Type)
                        {
                            case "ForwardingShipment":
                                iS++;
                                break;
                            case "CustomsDeclaration":
                                iB++;
                                break;
                            case "ForwardingConsol":
                                iC++;
                                break;
                            case "WarehouseOrder":
                                iO++;
                                break;
                            case "OrderManagerOrder":
                                var orders = cwFile.Body.BodyField.Shipment.Order;
                                result.Ref3Type = NodeData.Models.RefType.Order;
                                result.Reference3 = orders.OrderNumber;
                                return result;

                                break;

                        }
                    }
                    if (iC > 0 | iS > 0 | iB > 0) // Must be a Mastr and/r House bill number
                    {
                        if (cwFile.Body.BodyField.Shipment.WayBillType != null)
                        {
                            switch (cwFile.Body.BodyField.Shipment.WayBillType.Code)
                            {
                                case "MWB":

                                    result.Ref1Type = NodeData.Models.RefType.Master;
                                    break;
                                case "HWB":
                                    result.Ref1Type = NodeData.Models.RefType.Housebill;
                                    break;
                            }
                            result.Reference1 = cwFile.Body.BodyField.Shipment.WayBillNumber;
                        }
                        else
                        {
                            result.Reference1 = cwFile.Body.BodyField.Shipment.AgentsReference;
                            result.Ref1Type = NodeData.Models.RefType.AgentRef;
                        }
                    }
                    if (iC == 1 && iS == 0) // Consol Only
                    {
                        var type = (from ds in dscoll
                                    where ds.Type == "ForwardingConsol"
                                    select ds.Key).FirstOrDefault();
                        if (!string.IsNullOrEmpty(type))
                        {
                            result.Ref2Type = NodeData.Models.RefType.Consol;
                            result.Reference2 = type;
                            if (cwFile.Body.BodyField.Shipment.SubShipmentCollection != null)
                            {
                                if (cwFile.Body.BodyField.Shipment.SubShipmentCollection[0].WayBillType.Code == "HWB")
                                {
                                    result.Reference3 = cwFile.Body.BodyField.Shipment.SubShipmentCollection[0].WayBillNumber;
                                    result.Ref3Type = NodeData.Models.RefType.Housebill;
                                }
                            }
                        }

                    }
                    else if (iS > 1) //Consol and multiple Shipment 
                    {
                        var type = (from ds in dscoll
                                    where ds.Type == "ForwardingConsol"
                                    select ds.Key).FirstOrDefault();
                        if (!string.IsNullOrEmpty(type))
                        {
                            result.Ref2Type = NodeData.Models.RefType.Consol;
                            result.Reference2 = type;
                        }

                    }
                    else if (iB == 1 && iS == 0) //Brokerage Only
                    {
                        result.Ref2Type = NodeData.Models.RefType.Brokerage;
                        result.Reference2 = cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection[0].Key;
                        result.Reference3 = cwFile.Body.BodyField.Shipment.OwnerRef;
                        result.Ref3Type = NodeData.Models.RefType.Order;
                    }
                    else if (iS == 1) //Shipment only
                    {
                        result.Ref2Type = NodeData.Models.RefType.Shipment;
                        if (cwFile.Body.BodyField.Shipment.SubShipmentCollection != null)
                        {
                            result.Reference2 = cwFile.Body.BodyField.Shipment.SubShipmentCollection[0].DataContext.DataSourceCollection[0].Key;
                            if (cwFile.Body.BodyField.Shipment.SubShipmentCollection[0].WayBillType.Code == "HWB")
                            {
                                if (!string.IsNullOrEmpty(cwFile.Body.BodyField.Shipment.SubShipmentCollection[0].WayBillNumber))
                                {
                                    result.Reference3 = cwFile.Body.BodyField.Shipment.SubShipmentCollection[0].WayBillNumber;
                                    result.Ref3Type = NodeData.Models.RefType.Housebill;
                                }
                            }
                        }
                        else
                        {
                            result.Reference2 = dscoll[0].Key;
                            if (!string.IsNullOrEmpty(cwFile.Body.BodyField.Shipment.WayBillNumber))
                            {
                                result.Reference3 = cwFile.Body.BodyField.Shipment.WayBillNumber;
                                result.Ref3Type = NodeData.Models.RefType.Housebill;
                            }

                        }

                    }
                    else if (iO == 1) //Order manager only
                    {
                        if (string.IsNullOrEmpty(cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection[0].Key.ToString()))
                        {
                            result.Reference1 = "NEW";
                        }
                        result.Ref3Type = NodeData.Models.RefType.WHSOrder;
                        result.Reference3 = cwFile.Body.BodyField.Shipment.Order.OrderNumber;


                    }

                }
                catch (Exception ex)
                {
                    result.Ref3Type = NodeData.Models.RefType.Error;
                    result.Reference3 = "Error in Header: " + ex.Message;
                }
            }
            else
            {
                result.Ref3Type = NodeData.Models.RefType.Error;
                result.Reference3 = "Error in Header: No Data Soruce Collection";
            }
            return result;
        }

        private ProcessResult ProcessFileDelivery(vw_CustomerProfile prof, string xmlfile, ApiLog cNodeLog)
        {
            //add so if outbound not checked to Not process
            //if (!cbOutbound.Checked)
            //{
            //    thisResult.Processed = false;
            //    thisResult.FileName = xmlfile;
            //    return thisResult;
            //}

            tslMain.Text = "Processing Delivery ....";
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Delivery " + prof.P_DELIVERY, Color.Orange);
            ProcessResult thisResult = new ProcessResult();

            string result = string.Empty;
            if (prof.P_ACTIVE == "N")
            {

                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Customer profile: (" + prof.C_CODE + ")" + prof.P_DESCRIPTION + " is Not active. Skipping.", Color.Orange);
                File.Delete(xmlfile);
                thisResult.Processed = true;
                thisResult.FileName = xmlfile;
                return thisResult;
            }
            switch (prof.P_DELIVERY)
            {
                case ("R"):
                    //case R is for FTP retrieve ie drop in folder - custome or vendor pickup
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + prof.C_NAME + " Recipient found " + prof.P_RECIPIENTID + ". Moving to Folder", Color.Green);
                        result = "File Saved";
                        if (string.IsNullOrEmpty(prof.P_SERVER))
                        {
                            NodeResources.MoveFile(xmlfile, Path.Combine(prof.C_PATH, "Processing"));
                        }
                        else
                        {
                            if (!Directory.Exists(prof.P_SERVER))
                            {
                                try
                                {
                                    Directory.CreateDirectory(prof.P_SERVER);
                                }
                                catch (Exception ex)
                                {
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Moving file failed Error was : " + ex.Message + ". ", Color.Red);
                                    string err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;

                                    using (MailModule mail = new MailModule(_mailServer))
                                    {
                                        mail.SendMsg(xmlfile, Globals.AlertsTo, "Unable to move File to Folder " + prof.P_SERVER, "Error Message: " + err);
                                    }

                                    thisResult.Processed = true;
                                    thisResult.FileName = xmlfile;
                                    break;
                                }


                            }
                            NodeResources.MoveFile(xmlfile, prof.P_SERVER);
                            thisResult.Processed = true;
                            thisResult.FileName = xmlfile;

                        }
                    }
                    break;
                case ("D"):
                    //To send via FTP - direct send
                    {
                        string ftpSend = string.Empty;
                        if (File.Exists(xmlfile))
                        {
                            try
                            {

                                NodeData.Models.Ivw_CustomerProfile v = new NodeData.Models.vw_CustomerProfile();
                                IFtpHelper ftp = new FtpHelper(xmlfile, Globals.WinSCPLocation);


                                string server = prof.P_SERVER;
                                string username = prof.P_USERNAME;
                                string password = prof.P_PASSWORD;
                                string folder = prof.P_PATH;
                                bool ssl = prof.P_SSL == "Y" ? true : false;
                                int port = int.Parse(prof.P_PORT);
                                ftp.FtpPort = port;
                                string ftpResult = string.Empty;
                                if (prof.P_SSL == "Y" ? true : false)
                                {
                                    ftp.sshfingerPrint = prof.P_EMAILADDRESS;
                                    ftp.PrivateKeyPath = prof.P_SUBJECT;
                                    ftp.FileName = xmlfile;
                                    var o = ftp.BuildSessionOptions(server, port, username, password, ssl);
                                    ftp.FTPsessionOptions = o;
                                    ftpResult = ftp.FtpSend(prof.P_PATH);
                                }
                                else
                                {

                                    ftpResult = ftp.FtpSend(xmlfile, server, port, folder, username, password);

                                }

                                // string ftpResult = NodeResources.FtpSend(xmlfile, prof);
                                if (ftpResult.Contains("File Sent Ok"))
                                {

                                    filesTx++;
                                    NodeResources.AddLabel(lblFilesTx, filesTx.ToString());
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "File " + Path.GetFileName(xmlfile) + " Sent. Response: " + ftpResult, Color.Green);
                                    result = ftpResult;
                                    try
                                    {
                                        System.GC.Collect();
                                        System.GC.WaitForPendingFinalizers();
                                        File.Delete(xmlfile);
                                        thisResult.Processed = true;
                                        thisResult.FileName = xmlfile;
                                        thisResult.FolderLoc = Path.GetDirectoryName(xmlfile);
                                    }
                                    catch (Exception ex)
                                    {
                                        ProcessingErrors procerror = new ProcessingErrors
                                        {
                                            ErrorCode = new CTCErrorCode
                                            {
                                                Code = NodeError.e104,
                                                Description = "Failed to Delete: " + ex.Message + ". "
                                            },
                                            FileName = xmlfile,
                                            ProcId = prof.P_ID,
                                            RecipientId = prof.P_RECIPIENTID,
                                            SenderId = prof.P_SENDERID
                                        };
                                        result = "Failed: " + ex.Message;
                                    }

                                }
                                else
                                {
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " FTP Transfer Result is invalid: Profile " + prof.P_DESCRIPTION, Color.Red);
                                    if (prof.P_IGNOREMESSAGE != "Y")
                                    {
                                        using (MailModule mail = new MailModule(_mailServer))
                                        {
                                            mail.SendMsg(xmlfile, Globals.AlertsTo, "FTP Failed to send (" + prof.P_DESCRIPTION + ")", "The FTP Proces failed to send the file." + Environment.NewLine +
                                                "Error Message was :" + result);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                AddTodo(prof.P_ID, xmlfile, ftpSend, DateTime.Now);
                                ProcessingErrors procerror = new ProcessingErrors
                                {
                                    ErrorCode = new CTCErrorCode
                                    {
                                        Code = NodeError.e104,
                                        Description = "Cargowise FTP Send Error Found: " + ex.Message + ". "
                                    },
                                    FileName = xmlfile,
                                    ProcId = prof.P_ID,
                                    RecipientId = prof.P_RECIPIENTID,
                                    SenderId = prof.P_SENDERID
                                };

                                AddProcessingError(procerror);
                                thisResult.FolderLoc = prof.C_PATH + "\\Processing\\";
                                thisResult.Processed = false;
                                thisResult.FileName = xmlfile;
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Cargowise FTP Send Error Found: " + ex.Message + ". " + "", Color.Red);
                                string err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                                using (MailModule mail = new MailModule(_mailServer))
                                {
                                    mail.SendMsg(xmlfile, Globals.AlertsTo, "FTP Failed to send (" + prof.P_DESCRIPTION + ")", "The FTP Proces failed to send the file." + Environment.NewLine +
                                        "Error Message was :" + result);
                                }

                            }

                        }
                        break;
                    }
                case ("C"):
                    //sent via CTC eAdaptor. CW to CW. Pretend to be cargowise.
                    {
                        try
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + prof.C_NAME + " Recipient found " + prof.P_RECIPIENTID + ".Sending file via CTC Adapter" + "", Color.Green);
                            string sendlog = string.Empty;
                            if (prof.P_MSGTYPE != null)
                            {
                                if (prof.P_MSGTYPE.Trim() == "Forward")
                                {
                                    if (cNodeLog != null)
                                    {
                                        sendlog = CTCAdapter.SendMessage(prof.P_SERVER, xmlfile, prof.P_RECIPIENTID, prof.P_USERNAME, prof.P_PASSWORD, cNodeLog.F_TRACKINGID, cNodeLog.F_SUBJECT, cNodeLog.F_CWFILENAME);
                                    }
                                }
                                else
                                {
                                    sendlog = CTCAdapter.SendMessage(prof.P_SERVER, xmlfile, prof.P_RECIPIENTID, prof.P_USERNAME, prof.P_PASSWORD, null);
                                }
                            }


                            // Test without sending. UnComment the following
                            //String sendlog = "Send OK";

                            if (sendlog.Contains("SEND OK"))
                            {
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Transmitted Ok. ");
                                cwTx++;
                                NodeResources.AddLabel(lblCWTx, cwTx.ToString());
                                System.GC.Collect();
                                System.GC.WaitForPendingFinalizers();
                                File.Delete(xmlfile);
                                result = "CTC Adapter send Ok";
                                thisResult.Processed = true;
                                thisResult.FileName = xmlfile;

                            }
                            else
                            {
                                result = "CTC Adapter send Failed: " + sendlog;
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " CTC Adapter send Failed: " + sendlog, Color.Red);
                                ProcessingErrors procerror = new ProcessingErrors
                                {
                                    ErrorCode = new CTCErrorCode
                                    {
                                        Code = NodeError.e104,
                                        Description = "CTC Adapter send Failed: " + sendlog
                                    },
                                    FileName = xmlfile,
                                    ProcId = prof.P_ID,
                                    RecipientId = prof.P_RECIPIENTID,
                                    SenderId = prof.P_SENDERID
                                };
                                AddProcessingError(procerror);
                                thisResult.FolderLoc = Globals.FailLoc;
                                thisResult.Processed = false;
                                thisResult.FileName = xmlfile;
                            }
                        }
                        catch (Exception ex)
                        {
                            ProcessingErrors procerror = new ProcessingErrors
                            {
                                ErrorCode = new CTCErrorCode
                                {
                                    Code = NodeError.e104,
                                    Description = "CTC Adapter send Failed: " + ex.Message
                                },
                                FileName = xmlfile,
                                ProcId = prof.P_ID,
                                RecipientId = prof.P_RECIPIENTID,
                                SenderId = prof.P_SENDERID
                            };

                            AddProcessingError(procerror);
                            thisResult.FolderLoc = Globals.FailLoc;
                            thisResult.Processed = false;
                            thisResult.FileName = xmlfile;
                            AddTransactionLog(prof.P_ID, prof.P_SENDERID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via CTC Adapter Failed with the Result: " + ex.Message, DateTime.Now, thisResult.Processed ? "Y" : "N", prof.P_BILLTO, prof.P_RECIPIENTID);
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " CTC Adapter send Failed: " + ex.Message, Color.Red);
                            result = "CTC Adapter send failed: " + ex.Message;
                        }
                    }
                    break;
                case "E":
                    //Send via email
                    {
                        Guid remID = AddTodo(prof.P_ID, xmlfile, "Added to todo List", DateTime.Now);
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + prof.C_NAME + " Recipient found " + prof.P_RECIPIENTID + ".Sending file via Email" + "");
                        string mailResult = string.Empty;
                        using (MailModule mail = new MailModule(_mailServer))
                        {
                            mailResult = mail.SendMsg(xmlfile, prof.P_EMAILADDRESS, prof.P_SUBJECT, "Cargowise File Attached");
                        }
                        if (mailResult == "Message Sent OK")
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Transmitted Ok. ", Color.Green);
                            RemoveFromTodo(remID);
                            cwTx++;
                            NodeResources.AddLabel(lblCWTx, cwTx.ToString());
                            result = mailResult;
                            thisResult.Processed = true;
                            thisResult.FileName = xmlfile;
                        }
                        else
                        {
                            remID = AddTodo(prof.P_ID, xmlfile, "Message send failed. Error message was: " + mailResult, DateTime.Now);
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Message send failed. Error message was: " + mailResult, Color.Red);
                            result = "Email send failed: " + mailResult;
                            ProcessingErrors procerror = new ProcessingErrors
                            {
                                ErrorCode = new CTCErrorCode
                                {
                                    Code = NodeError.e105,
                                    Description = "Email message failed to send " + mailResult
                                },
                                FileName = xmlfile,
                                ProcId = prof.P_ID,
                                RecipientId = prof.P_RECIPIENTID,
                                SenderId = prof.P_SENDERID
                            };
                            AddProcessingError(procerror);
                            thisResult.FolderLoc = prof.C_PATH + "\\Processing\\";
                            thisResult.Processed = false;
                            thisResult.FileName = xmlfile;

                        }
                        AddTransactionLog(prof.P_ID, prof.P_SENDERID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via Email with the Result: " + mailResult, DateTime.Now, thisResult.Processed ? "Y" : "N", prof.P_BILLTO, prof.P_RECIPIENTID);
                        break;
                    }
                case "S":
                    {
                        try
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + prof.C_NAME + " Recipient found " + prof.P_RECIPIENTID + ".Sending file via Web API" + "", Color.Green);
                            string sendlog = string.Empty;

                            var sendAPI = new RestSendAPI(prof.P_SERVER, prof.P_PATH);
                            sendAPI.SendFile(xmlfile);
                            if (!sendAPI.APISendResult)
                            {
                                //api failed
                                result = sendAPI.ErrString;
                                thisResult.Processed = true;
                                thisResult.FileName = xmlfile;
                                using (MailModule mail = new MailModule(_mailServer))
                                {
                                    mail.SendMsg(xmlfile, Globals.AlertsTo, "Web API Failed to send (" + prof.P_DESCRIPTION + ")", "The Web API Process failed to send the file." + Environment.NewLine +
                                        "Error Message was :" + sendAPI.ErrString);
                                }

                            }
                            else
                            {
                                //api send ok
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " API File Transmitted Ok. ");
                                cwTx++;
                                NodeResources.AddLabel(lblCWTx, cwTx.ToString());
                                System.GC.Collect();
                                System.GC.WaitForPendingFinalizers();
                                File.Delete(xmlfile);
                                result = "Web API send Ok";
                                thisResult.Processed = true;
                                thisResult.FileName = xmlfile;
                            }
                        }
                        catch (Exception ex)
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Web API Send Error Found: " + ex.Message + ". " + "", Color.Red);
                            string err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                            using (MailModule mail = new MailModule(_mailServer))
                            {
                                mail.SendMsg(xmlfile, Globals.AlertsTo, "Web API Failed to send (" + prof.P_DESCRIPTION + ")", "The Web API Proces failed to send the file." + Environment.NewLine +
                                    "Error Message was :" + result);
                            }
                        }


                        break;
                    }
            }
            return thisResult;
        }

        private ProcessResult ProcessXUA(string xmlfile, string originalFile)
        {
            FileInfo processingFile = new FileInfo(xmlfile);
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            XUA.UniversalInterchange xua = new XUA.UniversalInterchange();
            using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(XUA.UniversalInterchange));
                xua = (XUA.UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            if (xua.Body.BodyField == null)
            {
                return ProcessXUT(xmlfile, originalFile);
            }
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            cwRx++;
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());
            string senderID = string.Empty;
            string recipientID = string.Empty;
            string reasonCode = string.Empty;
            string eventCode = string.Empty;
            string msgDir = "";
            var trRef = new NodeData.Models.TransReference();
            List<XUS.DataSource> dscoll = new List<XUS.DataSource>();
            XUA.DataContext dc = new XUA.DataContext();
            dc = xua.Body.BodyField.Activity.DataContext;
            try
            {

                senderID = xua.Header.SenderID;
                recipientID = xua.Header.RecipientID;
                try
                {
                    reasonCode = xua.Body.BodyField.Activity.DataContext.ActionPurpose.Code.ToString();
                }
                catch (Exception)
                {
                    reasonCode = string.Empty;

                }
                if (xua.Body.BodyField.Activity.DataContext.EventType != null)
                {
                    eventCode = xua.Body.BodyField.Activity.DataContext.EventType.Code;
                }
                else
                {
                    eventCode = string.Empty;
                }
                var fp = new FileProcessor(xmlfile, connMgr.ConnString);
                ApiLog cNodeLog = fp.CheckApiLog(originalFile);
                ProfileOperations profOps = new ProfileOperations(connMgr.ConnString);
                var custProfileRecord = profOps.GetProfile(senderID, recipientID, reasonCode, eventCode, cNodeLog.F_SUBJECT, cNodeLog.F_APPCODE, cNodeLog.F_CWFILENAME);


                if (custProfileRecord != null)
                {
                    //Is Client Active and not on hold
                    if (custProfileRecord.C_IS_ACTIVE == "Y" || custProfileRecord.C_ON_HOLD != "Y")
                    {
                        string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfileRecord.C_PATH, "Held"));
                        if (f.StartsWith("Warning"))
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                        }
                        else
                        {
                            xmlfile = f;
                        }
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_NAME + " is not Active or is on Hold. Moving to Hold and skipping" + "");
                        thisResult.Processed = true;
                        thisResult.FileName = xmlfile;
                        return thisResult;
                    }
                    else
                    {
                        if (custProfileRecord.P_ACTIVE == "N")
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Customer profile: (" + custProfileRecord.C_CODE + ")" + custProfileRecord.P_DESCRIPTION + " is Not active. Skipping.", Color.Orange);
                            thisResult.Processed = true;
                            thisResult.FileName = xmlfile;
                            return thisResult;

                        }
                        CreateProfPath(custProfileRecord.C_PATH + "\\Processing", recipientID + reasonCode);
                        //Send file to Archive folder
                        //archiveFile = Globals.ArchiveFile(custProfileRecord.C_PATH + "\\Archive", xmlfile);
                        string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfileRecord.C_PATH, "Processing", recipientID + reasonCode));
                        if (f.StartsWith("Warning"))
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                        }
                        else
                        {
                            xmlfile = f;
                        }
                        Guid remID = Guid.NewGuid();
                        remID = AddTodo(custProfileRecord.P_ID, xmlfile, "Add_Transaction", DateTime.Now);
                        AddTransaction(custProfileRecord.C_ID, custProfileRecord.P_ID, !string.IsNullOrEmpty(cNodeLog.F_FILENAME) ? cNodeLog.F_FILENAME : Path.GetFileName(xmlfile), msgDir, true, trRef);
                        if (custProfileRecord.P_DTS == "Y")
                        {
                            remID = AddTodo(custProfileRecord.P_ID, xmlfile, "DTS_Processing", DateTime.Now);
                            DataTransformService DTS = new DataTransformService(Globals.DbConn);
                            DTS.Transform(custProfileRecord.P_ID, xmlfile, _mailServer);

                        }
                        if (custProfileRecord.P_MSGTYPE.Trim() == "Conversion")
                        {
                            // Rename the Cargowise XML File to the new Global XML                           
                            thisResult = ConvertCWFile(custProfileRecord, xmlfile);
                        }
                        //Create the Delete from TODO list ready to be removed
                        string ftpSend = string.Empty;
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Profile : " + custProfileRecord.P_DESCRIPTION);
                        thisResult = ProcessFileDelivery(custProfileRecord, xmlfile, cNodeLog);
                    }

                }
                else
                {
                    // No customer Profile found so will move to Root Path for Pickup. 
                    CreateProfPath(GetCustPath(senderID) + "\\Processing", recipientID + reasonCode);
                    string cwxml = string.Empty;
                    string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfileRecord.C_PATH, "Processing", recipientID + reasonCode));
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    {
                        xmlfile = f;
                    }
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_NAME + " Does not have a Document Profile. Moving to Root Path" + "");
                    AddTransactionLog(custProfileRecord.P_ID, senderID, xmlfile, "Customer " + custProfileRecord.C_NAME + " Recipient found " + recipientID + ". Moving to Folder to retrieval", DateTime.Now, "Y", custProfileRecord.P_BILLTO, recipientID);
                    AddTransaction(custProfileRecord.C_ID, custProfileRecord.P_ID, string.IsNullOrEmpty(originalFile) ? Path.GetFileName(xmlfile) : originalFile, msgDir, true, trRef);
                    //addSummRecord(DateTime.Now, recipientID, senderID, "Missing Profile", trRef, xmlfile, "Profile not found");
                    ProcessingErrors procerror = new ProcessingErrors
                    {
                        ErrorCode = new CTCErrorCode
                        {
                            Code = NodeError.e100,
                            Description = "customer profile not found. Move to retry folder: ",
                            Severity = "Warning"
                        },
                        FileName = xmlfile,
                        ProcId = custProfileRecord.P_ID,
                        RecipientId = recipientID,
                        SenderId = senderID
                    };

                    AddProcessingError(procerror);
                    thisResult.FolderLoc = Globals.FailLoc;
                    thisResult.Processed = false;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }
            }
            catch (Exception ex)
            {
                string srEx = ex.GetType().Name;

            }
            return thisResult;



        }

        private ProcessResult ProcessXUT(string xmlfile, string originalFile)
        {
            FileInfo processingFile = new FileInfo(xmlfile);
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            XUT.UniversalInterchange xutHeader = new XUT.UniversalInterchange();
            using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(XUT.UniversalInterchange));
                xutHeader = (XUT.UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            if (xutHeader.Body.BodyField.TransactionInfo == null)
            {
                return null;
            }
            XUT.TransactionInfo xut = xutHeader.Body.BodyField.TransactionInfo;
            ProcessResult result = new ProcessResult();
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            cwRx++;
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());
            string senderID = string.Empty;
            string recipientID = string.Empty;
            string reasonCode = string.Empty;
            string eventCode = string.Empty;
            senderID = xutHeader.Header.SenderID;
            recipientID = xutHeader.Header.RecipientID;
            var trRef = new NodeData.Models.TransReference();
            List<XUT.DataSource> dscoll = new List<XUT.DataSource>();
            XUT.DataContext dc = new XUT.DataContext();
            dc = xut.DataContext;

            if (dc.DataSourceCollection == null)
            {
                return null;
            }

            foreach (var ds in dc.DataSourceCollection)
            {

            }
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());

            if (xut.DataContext.EventType != null)
            {
                eventCode = xut.DataContext.EventType.Code;
            }
            else
            {
                eventCode = string.Empty;
            }
            if (xut.DataContext.ActionPurpose != null)
            {
                reasonCode = xut.DataContext.ActionPurpose.Code;
            }
            else
            {
                reasonCode = string.Empty;
            }
            var fp = new FileProcessor(xmlfile, connMgr.ConnString);
            ApiLog cNodeLog = fp.CheckApiLog(originalFile);
            string msgDir = "";
            ProfileOperations profOps = new ProfileOperations(connMgr.ConnString);

            vw_CustomerProfile custProfileRecord = profOps.GetProfile(senderID, recipientID, reasonCode, eventCode, cNodeLog.F_SUBJECT, cNodeLog.F_APPCODE, cNodeLog.F_CWFILENAME);
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Sender ID: " + senderID + " Recipient ID:" + recipientID + "(" + eventCode + "|" + reasonCode + ")", Color.Blue);

            Guid remID = Guid.NewGuid();
            if (custProfileRecord != null)
            {
                //Is Client Active and not on hold
                if (custProfileRecord.C_IS_ACTIVE != "Y" || custProfileRecord.C_ON_HOLD == "Y")
                {

                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_NAME + " is not Active or is on Hold. Moving to Hold and skipping" + "");
                    thisResult.Processed = true;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }
                else
                {
                    if (custProfileRecord.P_ACTIVE == "N")
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Customer profile: (" + custProfileRecord.C_CODE + ")" + custProfileRecord.P_DESCRIPTION + " is Not active. Skipping.", Color.Orange);
                        thisResult.Processed = true;
                        thisResult.FileName = xmlfile;
                        return thisResult;

                    }
                    remID = AddTodo(custProfileRecord.P_ID, xmlfile, "Add_Transaction", DateTime.Now);
                    AddTransaction(custProfileRecord.C_ID, custProfileRecord.P_ID, string.IsNullOrEmpty(originalFile) ? Path.GetFileName(xmlfile) : originalFile, msgDir, true, trRef);
                    if (custProfileRecord.P_DTS == "Y")
                    {
                        remID = AddTodo(custProfileRecord.P_ID, xmlfile, "DTS_Processing", DateTime.Now);
                        DataTransformService DTS = new DataTransformService(Globals.DbConn);
                        DTS.Transform(custProfileRecord.P_ID, xmlfile, _mailServer);
                    }
                    if (!string.IsNullOrEmpty(custProfileRecord.P_MSGTYPE))
                    {
                        if (custProfileRecord.P_MSGTYPE.Trim() == "Conversion")
                        {
                            // Rename the Cargowise XML File to the new Global XML
                            //TODO: Check the Accounting issue here. Doubling Accounting charge.
                            thisResult = ConvertCWFile(custProfileRecord, xmlfile);
                            //AddTransaction(custProfileRecord.C_ID, custProfileRecord.P_ID, Path.GetFileName(xmlfile), msgDir, true, trRef, archiveFile);
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Conversion for Profile : " + custProfileRecord.P_DESCRIPTION + " complete.");
                            return thisResult;
                        }
                    }

                    //Create the Delete from TODO list ready to be removed
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Profile : " + custProfileRecord.P_DESCRIPTION);
                    thisResult = ProcessFileDelivery(custProfileRecord, xmlfile, cNodeLog);
                    if (thisResult.Processed)
                    {
                        RemoveFromTodo(remID);
                    }
                    //addSummRecord(DateTime.Now, recipientID, senderID, custProfileRecord.P_MSGTYPE, trRef, xmlfile, result);

                }

            }
            else
            {
                // No customer Profile found so will move to Root Path for Pickup. 

                string cwxml = string.Empty;
                string custpath = Path.Combine(GetCustPath(senderID), "Failed");

                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_NAME + " Does not have a Document Profile. Moving to Root Path" + "");
                using (MailModule mail = new MailModule(_mailServer))
                {
                    mail.SendMsg(xmlfile, "andy.duggan@ctcorp.com.au", "Missing profile for " + senderID, "SenderID:" + senderID + Environment.NewLine +
                                    "Recipient:" + recipientID + Environment.NewLine +
                                    "Purpose Code:" + reasonCode + Environment.NewLine +
                                    "Event Code: " + eventCode + Environment.NewLine +
                                    "Moving File to : " + custpath);
                }
                if (!Directory.Exists(custpath) || (custpath == "Failed"))
                {
                    try
                    {
                        Directory.CreateDirectory(custpath);
                    }
                    catch (IOException)
                    {
                        using (MailModule mail = new MailModule(_mailServer))
                        {
                            mail.SendMsg(xmlfile, "andy.duggan@ctcorp.com.au", "Unable to create Path", "Unable to Create Fail Path :" + custpath);
                        }
                        thisResult.FolderLoc = Globals.FailLoc;
                        thisResult.Processed = false;
                        thisResult.FileName = xmlfile;
                        return thisResult;
                    }
                }
                NodeResources.MoveFile(xmlfile, custpath);

                ProcessingErrors procerror = new ProcessingErrors
                {
                    ErrorCode = new CTCErrorCode
                    {
                        Code = NodeError.e100,
                        Description = "Customer Profile Not Found. Move to Retry folder"
                    },
                    FileName = xmlfile,
                    ProcId = custProfileRecord.P_ID,
                    RecipientId = recipientID,
                    SenderId = senderID
                };
                AddProcessingError(procerror);
                thisResult.FolderLoc = Globals.FailLoc;
                thisResult.Processed = false;
                thisResult.FileName = xmlfile;
                return thisResult;
            }
            return thisResult;
        }


        private void ListSatellites()
        {
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Refreshing Satellites");
            if (grdSatelliteStatus.InvokeRequired)
            {
                grdSatelliteStatus.BeginInvoke(new Action(delegate { ListSatellites(); }));
                return;
            }
            grdSatelliteStatus.AutoGenerateColumns = false;
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
            {

                BindingSource bsSatList = new BindingSource();
                var satList = (from heartbeats in uow.HeartBeats.GetAll()
                               join cust in uow.Customers.GetAll() on heartbeats.HB_C equals cust.C_ID
                               where cust.C_IS_ACTIVE == "Y"
                               select new vw_HeartBeat
                               {
                                   C_NAME = cust.C_NAME,
                                   HB_NAME = heartbeats.HB_NAME,
                                   HB_LASTOPERATION = heartbeats.HB_LASTOPERATION,
                                   HB_LASTCHECKIN = heartbeats.HB_LASTCHECKIN,
                                   HB_OPENED = heartbeats.HB_OPENED,
                                   HB_PID = heartbeats.HB_PID,
                                   HB_ID = heartbeats.HB_ID
                               }).ToList();

                if (satList.Count > 0)
                {
                    bsSatList.DataSource = new SortableBindingList<vw_HeartBeat>(satList);
                    grdSatelliteStatus.DataSource = bsSatList.DataSource;

                }
            }
            isDataBound = true;
        }

        //private OrganizationAddress GetTransportAddressOrg(string shipRef, Shipment shipment, string v)
        //{
        //    OrganizationAddress result = new OrganizationAddress();
        //    Shipment transportShipment = new Shipment();
        //    result = null;
        //    transportShipment = GetShipment(shipment.SubShipmentCollection, shipRef);
        //    foreach (OrganizationAddress oa in transportShipment.OrganizationAddressCollection)
        //    {
        //        if (oa.AddressType == v)
        //        {
        //            result = oa;
        //            break;
        //        }
        //    }
        //    return result;
        //}

        private XUS.Shipment GetShipment(XUS.Shipment[] subShipColl, string shipRef)
        {
            XUS.Shipment result = new XUS.Shipment();
            bool found = false;

            foreach (XUS.Shipment ship in subShipColl)
            {
                foreach (XUS.DataSource ds in ship.DataContext.DataSourceCollection)
                {
                    if (ds.Key == shipRef)
                    {
                        result = ship;
                        found = true;
                        //break;
                    }
                    if (found)
                    {
                        if (ship.WayBillType.Code == "MWB")
                        {
                            result = GetShipment(ship.SubShipmentCollection, shipRef);
                        }

                        found = false;
                        break;
                    }
                }

            }
            return result;
        }

        public NodeData.Models.TransReference ContainerChainToCW(string xmlFile, vw_CustomerProfile cust)
        {
            var result = new NodeData.Models.TransReference();
            var ext = Path.GetExtension(xmlFile);
            var expExt = !string.IsNullOrEmpty(cust.P_FILETYPE) ? cust.P_FILETYPE.Trim() : "*";
            if (ext.ToUpper() == expExt || expExt == "*")
            {

                MaximasConversion mc = new MaximasConversion(connMgr.ConnString, xmlFile, cust);
                mc.OutPath = Globals.XMLLoc;
                var ccEvent = mc.CreateEvents();
                if (ccEvent.Contains("Error"))
                {
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + mc.ErrMsg, Color.Red);
                }
                else
                {
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + ccEvent, Color.Green);
                }
            }
            else
            {
                using (MailModule mail = new MailModule(_mailServer))
                {
                    mail.SendMsg(xmlFile, Globals.AlertsTo, "Unknown FileType found for " + cust.P_DESCRIPTION, "File :" + xmlFile + " is not the accepted File Type. File is attached and has been removed from the Queue. ");
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Failed to process file: " + Path.GetFileName(xmlFile) + ". Wrong File Type.", Color.Red);
                    result.Ref3Type = NodeData.Models.RefType.Error;
                    result.Reference3 = "Wrong File Type";
                    NodeResources.MoveFile(xmlFile, Globals.FailLoc);
                    return result;
                }
            }


            result.Reference1 = xmlFile;
            result.Ref1Type = NodeData.Models.RefType.Event;

            return result;

        }

        public NodeData.Models.TransReference CargowiseToFreightTracker(string xmlFile, vw_CustomerProfile cust)
        {
            var result = new NodeData.Models.TransReference();
            XMLLocker.Cargowise.XUS.UniversalInterchange cwUXML = new XMLLocker.Cargowise.XUS.UniversalInterchange();
            if (!File.Exists(xmlFile))
                return null;

            using (FileStream fStream = new FileStream(xmlFile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(XMLLocker.Cargowise.XUS.UniversalInterchange));
                cwUXML = (XMLLocker.Cargowise.XUS.UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            var cwConsol = cwUXML.Body.BodyField.Shipment;

            var cwConsolNo = (from c in cwConsol.DataContext.DataSourceCollection
                              where c.Type == "ForwardingConsol"
                              select c.Key).FirstOrDefault();

            var cwDecNo = (from c in cwConsol.DataContext.DataSourceCollection
                           where c.Type == "CustomsDeclaration"
                           select c.Key).FirstOrDefault();
            var cwShipmentNo = (from s in cwConsol.DataContext.DataSourceCollection
                                where s.Type == "ForwardingShipment"
                                select s.Key).FirstOrDefault();
            NodeFileTimeSlotRequest commonTSR = new NodeFileTimeSlotRequest();
            NodeData.Models.vw_CustomerProfile custProfile = new NodeData.Models.vw_CustomerProfile();
            // Copy between different vw_Profile objects. 
            var srcProf = cust.GetType();
            var dstProf = custProfile.GetType();
            foreach (var fld in srcProf.GetProperties())
            {
                var dstFld = dstProf.GetProperty(fld.Name);
                if (dstFld == null)
                {
                    continue;

                }
                dstFld.SetValue(custProfile, fld.GetValue(cust));
            }
            //
            Booking booking = new Booking();
            CWToCommon cwToCommon = new CWToCommon(Globals.AppLogPath, custProfile);
            if (cwConsol.DataContext.DataSourceCollection.Count() == 1 || (string.IsNullOrEmpty(cwShipmentNo) && !string.IsNullOrEmpty(cwDecNo)))
            {
                // Job is a Customs Declaration Only
                var jobType = (from f in cwConsol.DataContext.DataSourceCollection
                               select f.Type).FirstOrDefault();
                switch (jobType)
                {
                    case "CustomsDeclaration":
                        commonTSR = cwToCommon.GetTimeSlotFromDeclaration(cwConsol);
                        break;
                    case "LocalTransport": break;
                    case "PortTransport": break;
                    case "ForwardingShipment":
                        //commonTSR = cwToCommon.GetTimeSlotFromShipment(cwConsol);
                        break;
                }
                // commonTSR = cwToCommon.GetTimeSlotFromDeclaration(cwConsol);
                if (commonTSR == null)
                {
                    using (MailModule mail = new MailModule(_mailServer))
                    {
                        mail.SendMsg(xmlFile, Globals.AlertsTo, "Failed to Create TSR.", "Error: " + cwToCommon.ErrorString);
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Failed to Create TSR." + cwToCommon.ErrorString, Color.Red);
                        result.Ref3Type = NodeData.Models.RefType.Error;
                        result.Reference3 = "No containers Found";
                        NodeResources.MoveFile(xmlFile, Globals.FailLoc);
                        return result;
                    }
                }
                if (commonTSR.Containers != null)
                {
                    if (commonTSR.Containers.Length == 0)
                    {
                        result.Ref3Type = NodeData.Models.RefType.Error;
                        result.Reference3 = "No containers Found";
                        return result;
                    }
                    if (cwConsol.CustomizedFieldCollection != null)
                    {
                        var custRef = (from x in cwConsol.CustomizedFieldCollection
                                       where x.Key == "KN Tracking"
                                       select x.Value).FirstOrDefault();
                        if (!string.IsNullOrEmpty(custRef))
                        {
                            commonTSR.BookingReference = custRef;
                            commonTSR.CustomerReference.RefValue = !string.IsNullOrEmpty(cwConsol.AgentsReference) ? cwConsol.AgentsReference : commonTSR.CustomerReference.RefValue;
                        }
                        else
                        {
                            commonTSR.BookingReference = cwDecNo;
                            if (cwConsol.WayBillType != null)
                            {
                                if (cwConsol.WayBillType.Code == "MWB")
                                {
                                    commonTSR.CustomerReference = new CustomerReferenceElement { RefValue = cwConsol.WayBillNumber };
                                };
                            }
                        }
                    }
                }

                CommonToFreightTracker ctoF = new CommonToFreightTracker(custProfile);
                booking = ctoF.CreateBooking(commonTSR);

                if (booking != null)
                {
                    result.Ref1Type = NodeData.Models.RefType.Brokerage;
                    result.Reference1 = cwDecNo;
                    result.Ref2Type = NodeData.Models.RefType.Master;
                    result.Reference2 = "";

                    //Check Profile for Parameters and update the Timeslot references prior to creating the file. 
                    // Do it here as the function is working correctly for Shipment type jobs. 

                    var profile = new ProfileOperations(connMgr.ConnString);
                    {
                        var custRefParam = profile.GetParamValue(cust.P_PARAMLIST, "CustomerReference");
                        if (!string.IsNullOrEmpty(custRefParam))
                        {
                            switch (custRefParam)
                            {
                                case "MWB":
                                    booking.CustomerReference = string.IsNullOrEmpty(cwConsol.WayBillNumber) ? booking.CustomerReference : cwConsol.WayBillNumber;
                                    break;
                                case "CustomsDeclaration":
                                    booking.CustomerReference = cwDecNo;
                                    break;
                            }
                        }
                        var bookingRefParam = profile.GetParamValue(cust.P_PARAMLIST, "BookingReference");
                        if (!string.IsNullOrEmpty(bookingRefParam))
                        {
                            switch (bookingRefParam)
                            {
                                case "MWB":
                                    booking.BookingReference = string.IsNullOrEmpty(cwConsol.WayBillNumber) ? booking.BookingReference : cwConsol.WayBillNumber;
                                    break;
                                case "CustomsDeclaration":
                                    booking.BookingReference = cwDecNo;
                                    break;
                            }
                        }

                    }
                }

            }
            else
            {
                // A Shipment exists
                string cwMaster = cwConsol.WayBillNumber;
                if (cwConsol.SubShipmentCollection != null)
                {
                    foreach (var shipment in cwConsol.SubShipmentCollection)
                    {
                        commonTSR = cwToCommon.GetTimeSlotFromShipment(shipment);
                        if (commonTSR == null)
                        {
                            using (MailModule mail = new MailModule(_mailServer))
                            {
                                mail.SendMsg(xmlFile, Globals.AlertsTo, "Failed to Create TSR.", "Error: " + cwToCommon.ErrorString);
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Failed to Create TSR." + cwToCommon.ErrorString, Color.Red);
                                result.Ref3Type = NodeData.Models.RefType.Error;
                                result.Reference3 = "No containers Found";
                                NodeResources.MoveFile(xmlFile, Globals.FailLoc);
                                return result;
                            }
                        }
                        commonTSR.BookingReference = cwConsolNo;

                        if (shipment.CustomizedFieldCollection != null)
                        {
                            var custRef = (from x in shipment.CustomizedFieldCollection
                                           where x.Key == "KN Tracking"
                                           select x.Value).FirstOrDefault();
                            if (!string.IsNullOrEmpty(custRef))
                            {
                                commonTSR.BookingReference = custRef;
                                commonTSR.CustomerReference.RefValue = !string.IsNullOrEmpty(shipment.AgentsReference) ? shipment.AgentsReference : commonTSR.CustomerReference.RefValue;
                            }
                        }
                        CommonToFreightTracker ctoF = new CommonToFreightTracker(custProfile);
                        booking = ctoF.CreateBooking(commonTSR);
                        if (booking != null)
                        {
                            result.Ref1Type = NodeData.Models.RefType.Housebill;
                            result.Reference1 = shipment.WayBillNumber;
                            result.Ref2Type = NodeData.Models.RefType.Master;
                        }
                    }
                }
            }
            booking.CustomerCode = cust.P_SENDERID;
            FileBuilder fileBuilder = new FileBuilder(Path.Combine(cust.C_PATH, "Processing"));
            booking.BookingReference = string.IsNullOrEmpty(booking.BookingReference) ? booking.CustomerReference : booking.BookingReference;
            var bookingFile = fileBuilder.BuildXmlFile<Booking>(new XmlSerializerNamespaces(), "http://freighttracker.com.au/Schemas/ImportBooking", booking, cust, cust.P_SENDERID + "-FTRACKER-" + booking.BookingReference, ".xml");
            result.Ref3Type = NodeData.Models.RefType.FileName;
            result.Reference3 = bookingFile;
            var paramlist = cust.P_PARAMLIST.Split('|').ToList();
            if (paramlist.Count > 0)
            {
                var pList = (from param in paramlist
                             where param != null || !param.Equals(string.Empty)
                             select param).ToList();
                if (pList.Count > 0)
                {
                    foreach (var param in pList)
                    {
                        var pod = param.Split(',');
                        if (pod.Length == 5)
                        {
                            var vardischarge = pod[0].Split(':');
                            if (vardischarge[1] == commonTSR.DischargePort)
                            {
                                var ArrServer = pod[1].Split(':');
                                var ArrFolder = pod[2].Split(':');
                                var ArrUsername = pod[3].Split(':');
                                var ArrPassword = pod[4].Split(':');
                                int port = int.Parse(cust.P_PORT);
                                IFtpHelper ftp = new FtpHelper(bookingFile, Globals.WinSCPLocation);
                                var ftpResult = ftp.FtpSend(bookingFile, ArrServer[1], port, ArrFolder[1], ArrUsername[1], ArrPassword[1]);
                                if (ftpResult.Contains("File Sent Ok"))
                                {
                                    File.Delete(bookingFile);
                                    File.Delete(xmlFile);
                                    filesTx++;
                                    NodeResources.AddLabel(lblFilesTx, filesTx.ToString());
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "File " + Path.GetFileName(bookingFile) + " Sent. Response: " + ftpResult, Color.Green);

                                }
                                else
                                {
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "File " + Path.GetFileName(bookingFile) + "Failed to Send. Response: " + ftpResult, Color.Red);
                                    using (MailModule mail = new MailModule(_mailServer))
                                    {
                                        mail.SendMsg(bookingFile, Globals.AlertsTo, "FTP Failed to send (" + Path.GetFileName(bookingFile) + ")", "The FTP Proces failed to send the file." + Environment.NewLine +
                                            "Error Message was :" + result);
                                    }
                                }

                            }
                        }
                    }
                }

            }
            return result;
        }



        private ProcessResult ConvertCWFile(vw_CustomerProfile cust, string file)
        {
            ProcessResult result = new ProcessResult();
            result.FileName = file;

            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Beginning Cargowise Conversion: " + file);
            string ftpSend = string.Empty;
            if (!string.IsNullOrEmpty(cust.P_METHOD) && (cust.P_LIBNAME != "COMMON"))
            {
                string custMethod;
                string custParams;
                custMethod = cust.P_METHOD;
                custParams = cust.P_PARAMLIST;
                Type custType = this.GetType();
                MethodInfo custMethodToRun = custType.GetMethod(custMethod);
                try
                {
                    // Move Cargowise files to Customer Folder for Satellite processing. 
                    if (custMethod == "MoveToProcessing")
                    {

                        string newPath = cust.C_PATH;
                        if (!string.IsNullOrEmpty(custParams))
                        {
                            var paramlist = custParams.Split('|').ToList();
                            foreach (var p in paramlist)
                            {
                                if (!string.IsNullOrEmpty(p.Trim()))
                                {
                                    DirectoryInfo dir = new DirectoryInfo(Path.GetFullPath(paramlist[0]));
                                    if (dir.Exists)
                                    {
                                        newPath = p.Trim();
                                        break;
                                    }
                                }
                            }
                        }

                        var varResult = custMethodToRun.Invoke(this, new object[] { file, Path.Combine(newPath, "Processing"), "", cust.P_DESCRIPTION });
                        if (varResult != null)
                        {
                            result.Processed = true;
                        }
                    }
                    else
                    {
                        var ts = new NodeData.Models.TransReference();
                        // Convert FROM Cargowise Files
                        if (!string.IsNullOrEmpty(cust.P_LIBNAME))
                        {
                            // Conversion via DLL
                            string libtoLoad = Path.Combine(cust.C_PATH + "\\Lib", cust.P_LIBNAME);
                            Assembly custLib = Assembly.LoadFile(libtoLoad);
                            Type convertMethod = custLib.GetTypes().ToList().FirstOrDefault(f => f.Name == custMethod);
                            dynamic CustomerXMLLibrary = Activator.CreateInstance(convertMethod);
                            var conversionResult = CustomerXMLLibrary.DoConvert(file, cust.P_SENDERID, cust.P_RECIPIENTID, Globals.XMLLoc);
                            if (conversionResult != null)
                            {
                                bool bSuccess = false;
                                switch (conversionResult.Ref3Type)
                                {
                                    case NodeData.Models.RefType.Error:
                                        NodeResources.MoveFile(file, Globals.FailLoc);
                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Error in Custom Library: " + cust.P_LIBNAME + ". Moving to Failed", Color.Red);
                                        using (MailModule mail = new MailModule(_mailServer))
                                        {
                                            mail.SendMsg(file, Globals.AlertsTo, "Error in Custom Library " + cust.P_LIBNAME, "Error message: " + conversionResult.Reference3);
                                        }
                                        bSuccess = false;
                                        result.Processed = true;
                                        result.FolderLoc = Globals.FailLoc;
                                        result.FileName = Path.GetFileName(conversionResult.Reference3);
                                        break;

                                    case NodeData.Models.RefType.FileName:
                                        // Globals.ArchiveFile(Globals.ArchLoc, conversionResult.Reference3);
                                        bSuccess = true;
                                        //canDelete = true;
                                        result.FileName = file;
                                        result.Processed = true;
                                        break;
                                }
                                AddTransaction(cust.C_ID, cust.P_ID, file, cust.P_DIRECTION, bSuccess, conversionResult);
                            }

                            else
                            {
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Conversion failed :" + ts.Reference3);
                                NodeResources.MoveFile(file, Globals.FailLoc);
                                result.Processed = true;
                            }
                        }
                        else
                        {
                            var varResult = custMethodToRun.Invoke(this, new object[] { file, cust });
                            if (varResult.GetType().Name == "TransReference")
                            {
                                var tr = (NodeData.Models.TransReference)varResult;
                                result.Processed = true;
                                if (tr.Ref3Type == NodeData.Models.RefType.FileName || File.Exists(tr.Reference3))
                                {
                                    file = tr.Reference3;
                                }
                            }

                        }
                        result = ProcessFileDelivery(cust, file, null);


                        if (result.Processed)
                        {
                            //AddTransaction((Guid)dr["C_ID"], (Guid)dr["P_ID"], xmlFile, msgDir, true, (TransReference)varResult, archiveFile);

                            result.FileName = file;
                            result.Processed = true;
                            System.GC.Collect();
                            System.GC.WaitForPendingFinalizers();
                            File.Delete(file);
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name;
                    strEx += " " + ex.Message;
                }
            }
            return result;
        }
        public ProcessResult ProcessXUE(string xmlfile, string originalFile, vw_CustomerProfile profile)
        {
            ProcessResult thisResult = new ProcessResult();
            FileInfo processingFile = new FileInfo(xmlfile);
            thisResult.Processed = true;
            string senderID = string.Empty;
            string recipientID = string.Empty;
            string reasonCode = string.Empty;
            string eventCode = string.Empty;
            totfilesRx++;
            string msgDir = "";
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            cwRx++;
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());
            var trRef = new NodeData.Models.TransReference();

            XUE.UniversalInterchange ue = new XUE.UniversalInterchange();

            using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(XUE.UniversalInterchange));
                ue = (XUE.UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            senderID = ue.Header.SenderID;
            recipientID = ue.Header.RecipientID;
            reasonCode = ue.Body.BodyField.Event.DataContext.ActionPurpose != null ? ue.Body.BodyField.Event.DataContext.ActionPurpose.Code : string.Empty;
            var fp = new FileProcessor(xmlfile, connMgr.ConnString);
            ApiLog cNodeLog = fp.CheckApiLog(originalFile);
            eventCode = ue.Body.BodyField.Event.EventType;
            if (profile == null)
            {
                ProfileOperations profOps = new ProfileOperations(connMgr.ConnString);
                profile = profOps.GetProfile(senderID, recipientID, reasonCode, eventCode, cNodeLog.F_SUBJECT, cNodeLog.F_APPCODE, cNodeLog.F_CWFILENAME);
                if (profile == null)
                {
                    ProcessingErrors procerror = new ProcessingErrors
                    {
                        ErrorCode = new CTCErrorCode
                        {
                            Code = NodeError.e100,
                            Description = "Customer profile not found. Move to retry folder",
                            Severity = "Warning"
                        },
                        FileName = xmlfile,
                        RecipientId = recipientID,
                        SenderId = senderID
                    };
                    // No customer Profile found so will move to Root Path for Pickup. 
                    CreateProfPath(GetCustPath(senderID) + "\\Processing", recipientID + reasonCode);
                    string f = NodeResources.MoveFile(xmlfile, Path.Combine(profile.C_PATH, "Processing", recipientID + reasonCode));
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlfile = f; }
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + profile.C_NAME + " Does not have a Document Profile. Moving to Root Path" + "");
                    AddTransactionLog(profile.P_ID, senderID, xmlfile, "Customer " + profile.C_NAME + " Recipient found " + recipientID + ". Moving to Folder to retrieval", DateTime.Now, "Y", profile.P_BILLTO, recipientID);
                    AddTransaction(profile.C_ID, profile.P_ID, Path.GetFileName(xmlfile), msgDir, true, trRef);
                    //addSummRecord(DateTime.Now, recipientID, senderID, "Missing Profile", trRef, xmlfile, "Profile not found");
                    AddProcessingError(procerror);
                    thisResult.FolderLoc = Globals.FailLoc;
                    thisResult.Processed = false;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }
            }
            try
            {

                string archiveFile = string.Empty;
                if (profile.C_IS_ACTIVE != "Y")
                {
                    string cwxml = string.Empty;
                    string f;
                    if (string.IsNullOrEmpty(profile.C_ON_HOLD))
                    {
                        f = NodeResources.MoveFile(xmlfile, Globals.FailLoc);
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Event File Found without Profile. Moving to Failed and skipping" + "");
                    }
                    else
                    {

                        f = NodeResources.MoveFile(xmlfile, Path.Combine(profile.C_PATH, "Held"));
                        if (f.StartsWith("Warning"))
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                        }
                        else
                        { xmlfile = f; }

                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + profile.C_NAME + " is not Active or is on Hold. Moving to Hold and skipping" + "");

                    }
                    thisResult.Processed = true;
                    thisResult.FileName = xmlfile;
                    return thisResult;

                }
                else
                {
                    if (profile.P_ACTIVE == "N")
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Customer profile: (" + profile.C_CODE + ")" + profile.P_DESCRIPTION + " is Not active. Skipping.", Color.Orange);
                        thisResult.Processed = true;
                        thisResult.FileName = xmlfile;
                        return thisResult;

                    }
                    CreateProfPath(profile.C_PATH + "\\Processing", recipientID + reasonCode);
                    //Send file to Archive folder
                    // archiveFile = Globals.ArchiveFile(custProfileRecord.C_PATH + "\\Archive", xmlfile);
                    string f = NodeResources.MoveFile(xmlfile, Path.Combine(profile.C_PATH, "Processing", recipientID + reasonCode));
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlfile = f; }
                    Guid remID = Guid.NewGuid();

                    remID = AddTodo(profile.P_ID, xmlfile, "Add_Transaction", DateTime.Now);
                    AddTransaction(profile.C_ID, profile.P_ID, string.IsNullOrEmpty(originalFile) ? Path.GetFileName(xmlfile) : originalFile, msgDir, true, trRef);
                    if (profile.P_DTS == "Y")
                    {
                        remID = AddTodo(profile.P_ID, xmlfile, "DTS_Processing", DateTime.Now);
                        DataTransformService DTS = new DataTransformService(Globals.DbConn);
                        DTS.Transform(profile.P_ID, xmlfile, _mailServer);
                    }
                    if (profile.P_MSGTYPE.Trim() == "Conversion")
                    {
                        // Rename the Cargowise XML File to the new Global XML
                        ConvertCWFile(profile, xmlfile);
                        AddTransaction(profile.C_ID, profile.P_ID, string.IsNullOrEmpty(originalFile) ? Path.GetFileName(xmlfile) : originalFile, msgDir, true, trRef);
                    }
                    //Create the Delete from TODO list ready to be removed
                    string ftpSend = string.Empty;
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Profile : " + profile.P_DESCRIPTION);
                    thisResult = ProcessFileDelivery(profile, xmlfile, cNodeLog);
                }
                return thisResult;
            }
            catch (Exception ex)
            {
                // CreateProfPath(getCustPath(senderID) + "\\Processing", recipientID + reasonCode);
                string f = NodeResources.MoveFile(xmlfile, Path.Combine(profile.C_PATH, "Processing", recipientID + reasonCode));
                if (f.StartsWith("Warning"))
                {
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                }
                else
                { xmlfile = f; }

                string strEx = ex.GetType().Name;
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error found. Process XUE failed with Error: " + ex.Message);
                string errMsg = "Recipient ID:" + recipientID + Environment.NewLine +
                                "Sender ID:" + senderID + Environment.NewLine +
                                "Event Code:" + eventCode;
                using (MailModule mail = new MailModule(_mailServer))
                {
                    mail.SendMsg(xmlfile, Globals.AlertsTo, "Error Processing XUE Message", errMsg);
                }
                ProcessingErrors procerror = new ProcessingErrors
                {
                    ErrorCode = new CTCErrorCode
                    {
                        Code = NodeError.e112,
                        Description = "Customer profile not found. Move to retry folder",
                        Severity = "Warning"
                    },
                    FileName = xmlfile,
                    ProcId = profile != null ? profile.P_ID : Guid.Empty,
                    RecipientId = recipientID,
                    SenderId = senderID
                };

                AddProcessingError(procerror);
                thisResult.FolderLoc = Globals.FailLoc;
                thisResult.Processed = false;
                thisResult.FileName = xmlfile;

                return thisResult;

            }




        }

        private ProcessResult ProcessXMS(string xmlFile, string originalFile, TaskList task)
        {

            string senderID = string.Empty;
            string recipientID = string.Empty;
            string actionPurpose = string.Empty;
            string eventCode = string.Empty;

            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File " + Path.GetFileName(xmlFile) + " is Cargowise XMS.");

            FileProcessor fp = new FileProcessor(xmlFile, connMgr.ConnString);
            FileInfo processingFile = new FileInfo(xmlFile);
            ProcessResult processResult = new ProcessResult();
            ProcessResult thisResult = processResult;
            thisResult.Processed = true;

            // Get reason and event codes - no event code in Legacy
            //string legEventCode = CWFunctions.GetCodefromCW(Path.Combine(task.TL_Path, task.TL_FileName), "Purpose");
            eventCode = !string.IsNullOrEmpty(eventCode) ? eventCode : "";
            actionPurpose = CWFunctions.GetCodefromCW(Path.Combine(task.TL_Path, task.TL_FileName), "ActionPurpose");
            // If event type null or error then check legacy code type Purpose
            if (CWFunctions.GetXMLType(Path.Combine(task.TL_Path, task.TL_FileName)) == "XMS")
            {
                string legEventCode = CWFunctions.GetCodefromCW(Path.Combine(task.TL_Path, task.TL_FileName), "Purpose");
                actionPurpose = !string.IsNullOrEmpty(legEventCode) ? legEventCode : "";
            }

            //add api log check to get adapter switches
            //var profile = GetProfileFromAPiLog(ApiLog, originalFile);
            ApiLog cNodeLog = fp.CheckApiLog(originalFile);
            ProfileOperations profOps = new ProfileOperations(connMgr.ConnString);
            vw_CustomerProfile custProfileRecord = profOps.GetProfile(task.TL_SenderID, task.TL_RecipientID, actionPurpose, eventCode, cNodeLog.F_SUBJECT, cNodeLog.F_APPCODE, cNodeLog.F_CWFILENAME);

            if (custProfileRecord != null)
            {
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Profile : " + custProfileRecord.P_DESCRIPTION);
                // old check some reason ?? if (fp.ProcessXMS(xmlFile, originalFile))
                if (custProfileRecord.C_IS_ACTIVE == "Y")
                {
                    //NodeResources.AddText(edLog, fp.Log, Color.Green);
                    processResult = new ProcessResult
                    {
                        FileName = xmlFile,
                        FolderLoc = processingFile.DirectoryName,
                        Processed = true
                    };

                    thisResult = ProcessFileDelivery(custProfileRecord, xmlFile, cNodeLog);
                    filesTx++;
                    cwTx++;
                }
            }
            else
            {
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + fp.ErrorLog, Color.Red);
                using (MailModule mail = new MailModule(_mailServer))
                {
                    mail.SendMsg(xmlFile, Globals.AlertsTo, "Processing Legacy XML Error", "Error: " + fp.ErrorLog);
                }

                processResult = new ProcessResult
                {
                    FileName = xmlFile,
                    FolderLoc = processingFile.DirectoryName,
                    Processed = true
                };
            }

            return processResult;
        }

        private ProcessResult ProcessNDM(string xmlfile, string originalFile)
        {
            ProcessResult thisResult = new ProcessResult();
            FileInfo processingFile = new FileInfo(xmlfile);
            thisResult.Processed = true;
            string senderID = string.Empty;
            string recipientID = string.Empty;
            string reasonCode = string.Empty;
            string eventCode = string.Empty;
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            cwRx++;
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());
            var trRef = new NodeData.Models.TransReference();
            XDocument xDoc;
            string msgDir = "";
            XElement datasource;
            XElement nativeType;
            string nativeString = string.Empty;
            XElement native;
            XNamespace ns = "http://www.cargowise.com/Schemas/Universal/2011/11";
            XNamespace nsa = "http://www.cargowise.com/Schemas/Native/2011/11";
            try
            {
                xDoc = XDocument.Load(xmlfile);
                var elSenderID = xDoc.Descendants().Where(n => n.Name == ns + "SenderID").FirstOrDefault();
                if (elSenderID != null)
                {
                    senderID = elSenderID.Value;
                }
                else
                {

                }
                var elRecipientID = xDoc.Descendants().Where(n => n.Name == ns + "RecipientID").FirstOrDefault();
                if (elRecipientID != null)
                {
                    recipientID = elRecipientID.Value;
                }
                else
                {

                }
                native = xDoc.Descendants().Where(n => n.Name == nsa + "Native").FirstOrDefault();
                if (native != null)
                {
                    datasource = xDoc.Descendants().Where(n => n.Name == ns + "DataSource").FirstOrDefault();
                    if (datasource != null)
                    {
                        nativeType = datasource.Element(ns + "Type");
                        nativeString = nativeType.Value;
                    }


                }
                try
                {
                    var pc = native.Descendants().Where(n => n.Name == ns + "ActionPurpose").FirstOrDefault();
                    if (pc != null)
                    {
                        reasonCode = pc.Element(ns + "Code").Value;
                    }

                }
                catch (Exception e)
                {
                    ProcessingErrors procerror = new ProcessingErrors
                    {
                        ErrorCode = new CTCErrorCode
                        {
                            Code = NodeError.e110,
                            Description = "Error processing NBN file: " + e.Message,
                            Severity = "Warning"
                        },
                        FileName = xmlfile,
                        RecipientId = recipientID,
                        SenderId = senderID
                    };

                    AddProcessingError(procerror);
                    thisResult.FolderLoc = Globals.FailLoc;
                    thisResult.Processed = false;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }

            }
            catch (Exception ex)
            {
                ProcessingErrors procerror = new ProcessingErrors
                {
                    ErrorCode = new CTCErrorCode
                    {
                        Code = NodeError.e110,
                        Description = "Error processing NDM file: " + ex.Message,
                        Severity = "Warning"
                    },
                    FileName = xmlfile,

                    RecipientId = recipientID,
                    SenderId = senderID
                };
                AddProcessingError(procerror);
                thisResult.FolderLoc = Globals.FailLoc;
                thisResult.Processed = false;
                thisResult.FileName = xmlfile;
                return thisResult;
            }
            var fp = new FileProcessor(xmlfile, connMgr.ConnString);
            ApiLog cNodeLog = fp.CheckApiLog(originalFile);
            ProfileOperations profOps = new ProfileOperations(connMgr.ConnString);
            vw_CustomerProfile custProfileRecord = profOps.GetProfile(senderID, recipientID, reasonCode, eventCode, cNodeLog.F_SUBJECT, cNodeLog.F_APPCODE, cNodeLog.F_CWFILENAME);

            if (custProfileRecord != null)
            {
                //Is Client Active and not on hold
                if (custProfileRecord.C_IS_ACTIVE != "Y" || custProfileRecord.C_ON_HOLD == "Y")
                {
                    string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfileRecord.C_PATH, "Held"));
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlfile = f; }

                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_NAME + " is not Active or is on Hold. Moving to Hold and skipping" + "");
                    thisResult.Processed = true;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }
                else
                {
                    if (custProfileRecord.P_ACTIVE == "N")
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Customer profile: (" + custProfileRecord.C_CODE + ")" + custProfileRecord.P_DESCRIPTION + " is Not active. Skipping.", Color.Orange);
                        thisResult.Processed = true;
                        thisResult.FileName = xmlfile;
                        return thisResult;
                    }
                    CreateProfPath(custProfileRecord.C_PATH + "\\Processing", recipientID + reasonCode);
                    //Send file to Archive folder
                    // archiveFile = Globals.ArchiveFile(custProfileRecord.C_PATH + "\\Archive", xmlfile);
                    string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfileRecord.C_PATH, "Processing", recipientID + reasonCode));
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlfile = f; }

                    //Add Initial file to TODO list. 
                    Guid remID = Guid.NewGuid();
                    remID = AddTodo(custProfileRecord.P_ID, xmlfile, "Add_Transaction", DateTime.Now);
                    AddTransaction(custProfileRecord.C_ID, custProfileRecord.P_ID, Path.GetFileName(xmlfile), msgDir, true, trRef);
                    if (custProfileRecord.P_DTS == "Y")
                    {
                        remID = AddTodo(custProfileRecord.P_ID, xmlfile, "DTS_Processing", DateTime.Now);
                        DataTransformService DTS = new DataTransformService(Globals.DbConn);
                        DTS.Transform(custProfileRecord.P_ID, xmlfile, _mailServer);
                    }
                    string result = "";
                    string strSuccess = "N";
                    //Message type will determine whether the message is sending to eAdapter (New Native file Imported/Converted) or sent to customer service. 
                    switch (custProfileRecord.P_MSGTYPE.Trim())
                    {
                        case "Conversion":
                            thisResult = ConvertCWFile(custProfileRecord, xmlfile);
                            break;
                        case "eAdapter":
                            try
                            {
                                remID = AddTodo(custProfileRecord.P_ID, xmlfile, "Added to todo List", DateTime.Now);
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_NAME + " Recipient found " + recipientID + ".Sending file via CTC Adapter" + "");
                                string sendlog = CTCAdapter.SendMessage(custProfileRecord.P_SERVER, xmlfile, recipientID, custProfileRecord.P_USERNAME, custProfileRecord.P_PASSWORD, null);
                                // Test without sending

                                //String sendlog = "Send OK";
                                if (sendlog.Contains("SEND OK"))
                                {
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Transmitted Ok. ");
                                    cwTx++;
                                    NodeResources.AddLabel(lblCWTx, cwTx.ToString());
                                    strSuccess = "Y";
                                    AddTransaction(custProfileRecord.C_ID, custProfileRecord.P_ID, Path.GetFileName(xmlfile), msgDir, false, trRef);
                                    System.GC.Collect();
                                    System.GC.WaitForPendingFinalizers();
                                    File.Delete(xmlfile);
                                    result = "CTC Adapter send Ok";
                                }
                                else
                                {
                                    result = "CTC Adapter send Failed: " + sendlog;
                                    ProcessingErrors procerror = new ProcessingErrors
                                    {
                                        ErrorCode = new CTCErrorCode
                                        {
                                            Code = NodeError.e104,
                                            Description = "CTC Adapter send Failed: " + sendlog,
                                            Severity = "Warning"
                                        },
                                        FileName = xmlfile,
                                        ProcId = custProfileRecord.P_ID,
                                        RecipientId = recipientID,
                                        SenderId = senderID
                                    };

                                    AddProcessingError(procerror);
                                    thisResult.FolderLoc = Globals.FailLoc;
                                    thisResult.Processed = false;
                                    thisResult.FileName = xmlfile;
                                }
                                thisResult.FileName = xmlfile;
                                AddTransactionLog(custProfileRecord.P_ID, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via CTC Adapter with the Result: " + sendlog, DateTime.Now, strSuccess, custProfileRecord.P_BILLTO, recipientID);
                            }
                            catch (Exception ex)
                            {
                                ProcessingErrors procerror = new ProcessingErrors
                                {
                                    ErrorCode = new CTCErrorCode
                                    {
                                        Code = NodeError.e104,
                                        Description = "CTC Adapter send Failed: " + ex.Message
                                        ,
                                        Severity = "Warning"
                                    },
                                    FileName = xmlfile,
                                    ProcId = custProfileRecord.P_ID,
                                    RecipientId = recipientID,
                                    SenderId = senderID
                                };
                                AddProcessingError(procerror);
                                thisResult.FolderLoc = Globals.FailLoc;
                                thisResult.Processed = false;
                                thisResult.FileName = xmlfile;
                                AddTransactionLog(custProfileRecord.P_ID, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via CTC Adapter Failed with the Result: " + ex.Message, DateTime.Now, strSuccess, custProfileRecord.P_BILLTO, recipientID);
                                result = "CTC Adapter send failed: " + ex.Message;
                            }

                            break;

                        case "Forward":
                            break;

                        case "Original":
                            break;

                        case "Response":
                            switch (nativeString)
                            {
                                case "Product":


                                    break;
                            }

                            break;
                            // Rename the Cargowise XML File to the new Global XML


                    }
                    //Create the Delete from TODO list ready to be removed
                    string ftpSend = string.Empty;
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Profile : " + custProfileRecord.P_DESCRIPTION);
                }
            }
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            File.Delete(xmlfile);
            return thisResult;
        }

        private ProcessResult ProcessNon(string xmlfile)
        {
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = false;
            return thisResult;
        }



        public bool CheckIfStopped()
        {
            if (btnStart.Text == "&Start")
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        private ProcessResult ProcessCargowiseXMLFile(string xmlfile, TaskList task)
        {
            // Default XML Identification Process
            ProcessResult canDelete = new ProcessResult();
            FileInfo processingFile = new FileInfo(xmlfile);
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Found " + Path.GetFileName(xmlfile) + ". Checking for Cargowise File.");

            var ns = CWFunctions.GetNameSpace(xmlfile);
            if (ns == "InUse")
            {
                return canDelete;
            }

            string senderID = string.Empty;
            string recipientID = string.Empty;
            string reasonCode = string.Empty;

            string transactionFile = string.Empty;
            if (task != null)
            {
                transactionFile = task.TL_OrginalFileName;
            }
            //This function returns the Cargowise APPCODE where
            // XMS = Legacy Verbose/Light XXML Export - the old XML with eDocs 
            // UDM = Universal Data Message. These are the CW1 Messages (XUS, XUE, XUA, XUT)
            // NDM = Native Data Message. Orgs, Products, Rates.
            // InUSE = When another process or function has locked the file Access. It will retry next time
            // All else it is not a Carogwise file and therefore will use the ProcessCustomFiles method
            string appCode = CWFunctions.GetXMLType(xmlfile);
            switch (appCode)
            {
                case "XMS":
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File is Cargowise (XMS) XML File :" + xmlfile + ". " + "");

                    canDelete = ProcessXMS(xmlfile, transactionFile, task);

                    break;
                case "UDM":  //Or XUA
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File is Cargowise (UDM) XML File :" + xmlfile + ". " + "");
                    try
                    {
                        ProfileOperations profOps = new ProfileOperations(connMgr.ConnString);
                        var prof = profOps.GetProfile(task);
                        //If the Task has a profile from when it as imported then pass the profile instead of re-querying
                        if (CTCAdapter.GetXMLType(xmlfile) == "UniversalEvent")
                        {
                            canDelete = ProcessXUE(xmlfile, transactionFile, prof);
                        }
                        else
                        {

                            canDelete = ProcessUDM(xmlfile, transactionFile, prof);
                        }
                    }
                    catch (IOException ex)
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File " + xmlfile + " is in use by another process will try again in next cycle . ", Color.Orange);
                    }
                    catch (Exception ex)
                    {
                        string exString = ex.GetType().Name;
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File " + xmlfile + " processing error: Error: " + exString, Color.Red);
                        using (MailModule mail = new MailModule(_mailServer))
                        {
                            mail.SendMsg(xmlfile, Globals.AlertsTo, "Processing XML(UDM) Message Error", "Error: " + exString + Environment.NewLine + ex.Message + Environment.NewLine + ex.InnerException.Message);
                        }
                    }

                    break;
                case "NDM":
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File is Cargowise (NDM) XML File :" + xmlfile + ". " + "");
                    canDelete = ProcessNDM(xmlfile, transactionFile);
                    break;
                case "InUse":
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File " + xmlfile + " is use will try again next Cycle . ", Color.Orange);

                    break;
                default:
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File is not a valid Cargowise XML File :" + xmlfile + ". Checking Client Specific formats" + "");
                    //Process Custom is where it determines what the file is and how to handle it. 
                    canDelete = ProcessCustomFiles(xmlfile, transactionFile);
                    break;
            }
            if (canDelete.FileName != null)
            {
                //CanDelete is a summary of the action. Contains a File name and whether it can be deleted or retry next cycle. 
                if (canDelete.Processed)
                {
                    // Whatever processing was marked as complete.
                    //Clean up any resouces hold the file open from this thread. 
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    try
                    {
                        // This is checking if it exists and to delete it. Only fires if I am running from multiple machines simultaneously. 
                        File.Delete(canDelete.FileName);
                        if (File.Exists(xmlfile))
                        {
                            File.Delete(xmlfile);
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                    // -------------------------     Dont Delete API LOG as may need to re process
                    //DeleteApiLog(xmlfile);
                }

            }
            return canDelete;
        }

        private string CheckFileName(TaskList task)
        {
            throw new NotImplementedException();
        }

        private void DeleteApiLog(string xmlfile)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
            {
                string fileName = Path.GetFileName(xmlfile);
                var apilog = uow.ApiLogs.Find(x => x.F_FILENAME == fileName).FirstOrDefault();
                if (apilog == null)
                {
                    FileProcessor fp = new FileProcessor(xmlfile, connMgr.ConnString);
                    apilog = fp.CheckApiLog(string.Empty, xmlfile);
                    apilog = uow.ApiLogs.Get(apilog.F_TRACKINGID);

                }
                if (apilog != null)
                {
                    uow.ApiLogs.Remove(apilog);
                    uow.Complete();
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void CreateProfPath(string path, string profile)
        {
            try
            {
                string profilePath = Path.Combine(path, profile);
                Directory.CreateDirectory(profilePath);
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("You do not have access to create a folder here.Please Check", "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (IOException exIO)
            {
                MessageBox.Show("Error Creating Directory: " + exIO.Message, "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void loadSettings()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);


            var path = Path.Combine(appDataPath, "CTCNode");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            Globals.AppPath = path;
            Globals.AppConfig = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".XML";
            Globals.AppConfig = Path.Combine(path, Globals.AppConfig);
            FrmSettings settings = new FrmSettings();


            if (!File.Exists(Globals.AppConfig))
            {
                XDocument xmlConfig = new XDocument(
                            new XDeclaration("1.0", "UTF-8", "Yes"),
                            new XElement("CNode",
                            new XElement("Database"),
                            new XElement("Transmod"),
                            new XElement("HeartBeat"),
                            new XElement("Config")));
                xmlConfig.Save(Globals.AppConfig);
                settings.ShowDialog();
            }
            else
            {
                try
                {
                    bool loadconfig = false;
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(Globals.AppConfig);
                    XmlNodeList nodelist = xmlConfig.SelectNodes("/CNode/Database");
                    XmlNode xmlCatalog = nodelist[0].SelectSingleNode("Catalog");
                    XmlNode xnode;
                    if (xmlCatalog != null)
                    {
                        Globals.DataBase = xmlCatalog.InnerText;
                    }
                    else
                    {
                        loadconfig = true;
                    }

                    XmlNode xmlServer = nodelist[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.ServerName = xmlServer.InnerText;
                    }
                    else
                    {
                        loadconfig = true;
                    }

                    XmlNode xmlUser = nodelist[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.UserName = xmlUser.InnerText;
                    }
                    else
                    {
                        loadconfig = true;
                    }

                    XmlNode xmlPassword = nodelist[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.Password = xmlPassword.InnerText;
                    }
                    else
                    {
                        loadconfig = true;
                    }

                    nodelist = xmlConfig.SelectNodes("/CNode/TransMod");
                    XmlNode xmlTransCatalog = nodelist[0].SelectSingleNode("Catalog");
                    if (xmlTransCatalog != null)
                    {
                        Globals.TransDatabase = xmlTransCatalog.InnerText;
                    }
                    else
                    {
                        loadconfig = true;
                    }

                    XmlNode xmlTransServer = nodelist[0].SelectSingleNode("Servername");
                    if (xmlTransServer != null)
                    {
                        Globals.TransServer = xmlTransServer.InnerText;
                    }
                    else
                    {
                        loadconfig = true;
                    }

                    XmlNode xmlTransUser = nodelist[0].SelectSingleNode("Username");
                    if (xmlTransUser != null)
                    {
                        Globals.TransUsername = xmlTransUser.InnerText;
                    }
                    else
                    {
                        loadconfig = true;
                    }

                    XmlNode xmlTransPassword = nodelist[0].SelectSingleNode("Password");
                    if (xmlTransPassword != null)
                    {
                        Globals.TransPassword = xmlTransPassword.InnerText;
                    }
                    else
                    {
                        loadconfig = true;
                    }

                    XmlNode xmlAlertsTo = nodelist[0].SelectSingleNode("AlertsTo");
                    if (xmlAlertsTo != null)
                    {
                        Globals.AlertsTo = xmlAlertsTo.InnerText;
                    }
                    else
                    {
                        loadconfig = true;
                    }

                    nodelist = null;
                    nodelist = xmlConfig.SelectNodes("/CNode/Config");
                    XmlNode xPollingtime = nodelist[0].SelectSingleNode("PollingTime");
                    if (xPollingtime != null)
                    {
                        Globals.PollTime = Convert.ToInt16(xPollingtime.InnerText) == 0 ? 1 : Convert.ToInt16(xPollingtime.InnerText);

                    }
                    else
                    {
                        Globals.PollTime = 2;
                    }
                    XmlNode xGuardianPollTime = nodelist[0].SelectSingleNode("GuardianPollTime");
                    if (xGuardianPollTime != null)
                    {
                        Globals.GuardianPollTime = Convert.ToInt16(xGuardianPollTime.InnerText) == 0 ? 1 : Convert.ToInt16(xGuardianPollTime.InnerText);
                    }
                    else
                    {
                        Globals.GuardianPollTime = 10;
                    }

                    XmlNode xConfig = nodelist[0].SelectSingleNode("XmlLocation");
                    if (xConfig != null)
                    {
                        Globals.XMLLoc = xConfig.InnerText;

                    }
                    XmlNode xmlCyclecount = nodelist[0].SelectSingleNode("CycleCount");
                    if (xmlCyclecount == null)
                    {
                        Globals.CycleCount = 100;
                    }
                    else
                    {
                        Globals.CycleCount = int.Parse(xmlCyclecount.InnerText);
                    }

                    XmlNode xmlMainTimer = nodelist[0].SelectSingleNode("MainTimer");
                    if (xmlMainTimer == null)
                    {
                        Globals.MainTimerInt = 2;
                    }
                    else
                    {
                        Globals.MainTimerInt = int.Parse(xmlMainTimer.InnerText);
                    }

                    xConfig = nodelist[0].SelectSingleNode("ReportLocation");
                    if (xConfig != null)
                    {
                        Globals.ReportLoc = xConfig.InnerText;
                    }
                    xConfig = nodelist[0].SelectSingleNode("CustomerRootPath");
                    if (xConfig != null)
                    {
                        Globals.RootPath = xConfig.InnerText;
                    }
                    xConfig = nodelist[0].SelectSingleNode("CustomReceivePath");
                    if (xConfig != null)
                    {
                        Globals.CustomReceive = xConfig.InnerText;
                    }
                    xConfig = nodelist[0].SelectSingleNode("FailPath");
                    if (xConfig != null)
                    {
                        Globals.FailLoc = xConfig.InnerText;
                    }
                    xConfig = nodelist[0].SelectSingleNode("TestingLocation");
                    if (xConfig != null)
                    {
                        Globals.TestingLoc = xConfig.InnerText;
                    }
                    else
                    {
                        loadconfig = true;
                    }
                    xConfig = nodelist[0].SelectSingleNode("Pdf2XmlLoc");
                    if (xConfig != null)
                    {
                        Globals.Pdf2Xml = xConfig.InnerText;
                    }
                    else
                    {
                        loadconfig = true;
                    }

                    xConfig = nodelist[0].SelectSingleNode("WinscpLocation");
                    if (xConfig != null)
                    {
                        Globals.WinSCPLocation = xConfig.InnerText;
                    }
                    else
                    {
                        loadconfig = true;
                    }
                    _mailServer = new MailServerSettings();
                    XmlNodeList xSmtp = nodelist[0].SelectNodes("SMTP");
                    if (xSmtp[0] != null)
                    {
                        xnode = xSmtp[0].SelectSingleNode("SMTPServer");
                        if (xnode != null)
                        {
                            _mailServer.Server = xnode.InnerText;

                            _mailServer.Port = string.IsNullOrEmpty(xnode.Attributes[0].InnerXml) ? 0 : Convert.ToInt16(xnode.Attributes[0].InnerXml);
                        }
                        else
                        {
                            loadconfig = true;
                        }

                        xnode = xSmtp[0].SelectSingleNode("EmailAddress");
                        if (xnode != null)
                        { _mailServer.Email = xnode.InnerText; }
                        else
                        {
                            loadconfig = true;
                        }

                        xnode = xSmtp[0].SelectSingleNode("SMTPUsername");
                        if (xnode != null)
                        {
                            _mailServer.UserName = xnode.InnerText;
                        }
                        else
                        {
                            loadconfig = true;
                        }

                        xnode = xSmtp[0].SelectSingleNode("SMTPPassword");
                        if (xnode != null)
                        {
                            _mailServer.Password = xnode.InnerText;
                        }
                        else
                        {
                            loadconfig = true;
                        }
                    }
                    xConfig = nodelist[0].SelectSingleNode("XmlLocation");
                    nodelist = null;

                    nodelist = xmlConfig.SelectNodes("/CNode/Config/Archive");
                    xnode = nodelist[0].SelectSingleNode("ArchiveLocation");
                    if (xnode != null)
                    {
                        Globals.ArchLoc = xnode.InnerText;
                    }
                    else
                    {
                        loadconfig = true;
                    }
                    // OLD Archive routine.. to be removed
                    //xnode = nodelist[0].SelectSingleNode("ArchiveFrequency");
                    //if (xnode != null)
                    //{
                    //    Globals.ArcFrequency = xnode.InnerText;
                    //}
                    //else
                    //{
                    //    loadconfig = true;
                    //}

                    if (loadconfig)
                    {
                        throw new System.Exception("Mandatory Settings missing");
                    }

                    connMgr = new NodeData.ConnectionManager(Globals.ServerName, Globals.DataBase, Globals.UserName, Globals.Password);
                    Globals.DbConn = connMgr.ConnString;

                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name + ": " + ex.Message;
                    MessageBox.Show("There was an error loading the Config files. Please check the settings", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FrmSettings FormSettings = new FrmSettings();
                    FormSettings.MailServerSettings = _mailServer;
                    FormSettings.ShowDialog();
                    DialogResult dr = FormSettings.DialogResult;
                    if (dr == DialogResult.OK)
                    {
                        loadSettings();
                    }


                }

            }
            settings.Dispose();

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (btnStart.Text == "&Stop")
            {
                btnStart_Click(btnStart, null);
            }

            FrmSettings FormSettings = new FrmSettings();
            FormSettings.MailServerSettings = _mailServer;
            FormSettings.ShowDialog();
            DialogResult dr = FormSettings.DialogResult;
            if (dr == DialogResult.OK)
            {
                loadSettings();
            }
        }

        private void customerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool started = false;
            if (btnStart.Text == "&Stop")
            {
                btnStart_Click(btnStart, null);
                started = true;
            }


            FrmCustomerProfile custProfile = new FrmCustomerProfile();
            custProfile.ShowDialog();
            if (started)
            {
                btnStart.BackColor = Color.LightCoral;
                btnStart.Text = "&Stop";
                ProcessInboundQueue();
                tmrMain.Start();
                tslMain.Text = "Timed process running";

            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            isDataBound = false;
            tslMain.Text = "Loading form settings....";
            loadSettings();
            tmrMain = new System.Timers.Timer();
            string appLog = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + "-Log.XML";
            Globals.AppLogPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                System.Reflection.Assembly.GetExecutingAssembly().GetName().Name, appLog);
            tmrMain.Interval = (int)TimeSpan.FromMinutes(Globals.MainTimerInt).TotalMilliseconds;
            tmrHeartBeat.Elapsed += new ElapsedEventHandler(tmrHead_Elapsed);
            tmrHeartBeat.Interval = (int)TimeSpan.FromMinutes(Globals.GuardianPollTime).TotalMilliseconds;
            tmrHeartBeat.Start();
            tmrMain.Elapsed += new ElapsedEventHandler(tmrMain_Elapsed);

            tslMode.Text = "Production";
            productionToolStripMenuItem.Checked = true;

            lblCwRx.Text = "0";
            cbInbound.Checked = true;
            cbOutbound.Checked = true;
            lblFilesRx.Text = "0";
            lblCWTx.Text = "0";
            lblFilesTx.Text = "0";
            lblTotFilesRx.Text = "0";
            lblTotFilesTx.Text = "0";
            filesRx = 0;
            filesTx = 0;
            totfilesRx = 0;
            totfilesTx = 0;
            cwRx = 0;
            cwTx = 0;
            tslMain.Text = "Listing Satellites...";
            ListSatellites();
            try
            {
                string[] args = Environment.GetCommandLineArgs();

                if (args.Length > 0)
                {
                    for (int i = 0; i < args.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(args[i]))
                        {
                            string[] param = args[i].Split(':');
                            if (param.Length > 1)
                            {
                                switch (param[0].ToUpper())
                                {
                                    case "-O":
                                        if (param[1].ToUpper() == "STOP")
                                        {
                                        }
                                        else
                                        {
                                        }
                                        break;
                                    case "-M":
                                        if (param[1].ToUpper() == "PRODUCTION")
                                        {
                                            tslMode.Text = "Production";
                                            productionToolStripMenuItem.Checked = true;
                                        }
                                        else
                                        {
                                            tslMode.Text = "Testing";
                                            testingToolStripMenuItem.Checked = true;
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception)
            {

            }
            tslMain.Text = "Loaded...";
            tslMain.Width = this.Width / 2;
            tslSpacer.Width = this.Width / 2 - (tslCmbMode.Width + productionToolStripMenuItem.Width);

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            prevHour = DateTime.Now.Hour;
            if (((Button)sender).Text == "&Start" || isRestart)
            {
                tslMain.Text = "Starting....";
                ((Button)sender).Text = "&Stop";
                ((Button)sender).BackColor = Color.LightCoral;
                eventStopWatch = new Stopwatch();
                //SetButtonText(btnStart, "&Stop");
                startTime = string.Format("{0:g} ", DateTime.Now);
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Timed processes running.");
                //if (totfilesRx > 0 | totfilesTx > 0)
                //{
                //    DialogResult dr = new DialogResult();
                //    dr = MessageBox.Show("Do you wish to reset the Counters as well?", "Start Process", MessageBoxButtons.YesNo);
                //    if (dr == DialogResult.Yes)
                //    {
                //        lblCwRx.Text = "0";
                //        lblFilesRx.Text = "0";
                //        lblCWTx.Text = "0";
                //        lblFilesTx.Text = "0";
                //        lblTotFilesRx.Text = "0";
                //        lblTotFilesTx.Text = "0";
                //        filesRx = 0;
                //        filesTx = 0;
                //        totfilesRx = 0;
                //        totfilesTx = 0;
                //        cwRx = 0;
                //        cwTx = 0;
                //        dsMain.Tables["CurrentSummary"].Clear();
                //    }
                //}
                if (cbInbound.Checked)
                {
                    ProcessInboundQueue();
                }

                if (cbOutbound.Checked)
                {
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Scan Folders " + Globals.XMLLoc);
                    ScanXMLFolders();
                    ProcessOutboundFolder();
                }

                tmrMain.Start();
                tslMain.Text = "Timed process running";
                this.tslSpacer.Padding = new Padding(this.Size.Width - (195), 0, 0, 0);
                isRestart = false;
            }
            else
            {
                tslMain.Text = "Stopping...";
                ((Button)sender).BackColor = Color.LightGreen;
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Timed processes stopped.");
                tmrMain.Stop();
                tslMain.Text = "Timed process stopped";
                if (isStop)
                {
                    isRestart = true;
                    btnStart_Click((Button)sender, null);
                }

                if (isStop)
                {
                    isStop = false;
                    ((Button)sender).Text = "&Stop";
                }
                else
                {
                    ((Button)sender).Text = "&Start";
                }
            }
        }


        private void button1_Click_1(object sender, EventArgs e)
        {
            var systemPath = System.Environment.
                            GetFolderPath(
                                Environment.SpecialFolder.MyDocuments
                            );
            File.WriteAllText(Path.Combine(systemPath, "CTCNodeBE-Log.txt"), edLog.Text + Environment.NewLine);
            edLog.Text = "";

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (btnStart.Text == "&Stop")
            {
                btnStart_Click(null, null);
            }

            frmAbout About = new frmAbout();
            About.ShowDialog();
        }

        private void AddProcessingError(ProcessingErrors procError)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
            {
                uow.ProcErrors.Add(new Processing_Error
                {
                    E_ERRORCODE = procError.ErrorCode.Code.ToString(),
                    E_ERRORDESC = procError.ErrorCode.Description,
                    E_FILENAME = procError.FileName,
                    E_RECIPIENTID = procError.RecipientId,
                    E_SENDERID = procError.SenderId,
                    E_P = procError.ProcId,
                    E_PROCDATE = DateTime.Now,
                    E_PK = Guid.NewGuid()
                });
                uow.Complete();
            }


        }



        private void monthlyReportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void testXMLTransferToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CWTestModule testModule = new CWTestModule();
            testModule.Show();

        }



        private void scratchPadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Scratchpad scratchpad = new Scratchpad();
            scratchpad.ShowDialog();

        }

        private void cargowiseFieldMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCargowiseValues cargowiseValues = new frmCargowiseValues();
            cargowiseValues.Show();
        }


        private void testingToolStringMenuItem_Click(object sender, EventArgs e)
        {
            if (testingToolStripMenuItem.Checked)
            {
                productionToolStripMenuItem.Checked = false;
                tslMode.Text = "Testing";
            }
            else
            {
                productionToolStripMenuItem.Checked = true;
                tslMode.Text = "Production";
            }
        }

        private void productionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (productionToolStripMenuItem.Checked)
            {
                testingToolStripMenuItem.Checked = false;
                tslMode.Text = "Production";
            }
            else
            {
                testingToolStripMenuItem.Checked = true;
                tslMode.Text = "Testing";
            }
        }

        private void RemoveFromTodo(Guid id)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connMgr.ConnString)))
            {
                var todoID = uow.ToDos.Get(id);
                if (todoID != null)
                {
                    uow.ToDos.Remove(todoID);
                    uow.Complete();
                }

            }


        }

        private void rpttestToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void FrmMain_Resize(object sender, EventArgs e)
        {
            tslSpacer.Width = this.Width / 2 - (tslCmbMode.Width + productionToolStripMenuItem.Width);
        }

        private void grdSatelliteStatus_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (e.RowIndex >= 0 && isDataBound)
            {
                bool bOpened = false;
                if (grdSatelliteStatus.Rows[e.RowIndex].Cells["SatOpened"].Value != null)
                {
                    if (!string.IsNullOrEmpty(grdSatelliteStatus.Rows[e.RowIndex].Cells["SatOpened"].Value.ToString()))

                    {
                        bOpened = true;
                        //     imgSat = new Icon("Resources\\16Ok.ico");

                        DateTime lastCheckin;
                        if (DateTime.TryParse(grdSatelliteStatus.Rows[e.RowIndex].Cells["SatLastCheckin"].Value.ToString(), out lastCheckin))
                        {
                            TimeSpan ts = DateTime.Now - lastCheckin;
                            if (ts.Minutes > Globals.GuardianPollTime)
                            {
                                grdSatelliteStatus.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Coral;
                            }
                            else
                            {
                                grdSatelliteStatus.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LawnGreen;
                            }

                        }

                    }
                    else
                    {
                        bOpened = false;
                    }
                }

                DataGridViewCheckBoxCell cell = (DataGridViewCheckBoxCell)grdSatelliteStatus.Rows[e.RowIndex].Cells["SatRunning"];
                if (!bOpened)
                {
                    cell.Value = false;
                    grdSatelliteStatus.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.DimGray;
                }
                else
                {
                    cell.Value = true;

                }


            }


        }

        private void grdSatelliteStatus_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void grdSatelliteStatus_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            isDataBound = true;
        }

        private void logger_Tick(object sender, EventArgs e)
        {
            MonitorLogs();
        }
    }
}







