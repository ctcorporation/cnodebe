﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CNodeBE.Forms
{


    public partial class frmProfileParam : Form
    {
        List<ProfParam> profParams = new List<ProfParam>();
        public string Param { get; set; }
        public frmProfileParam(string profParam)
        {
            Param = profParam;
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmProfileParam_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Param))
            {
                var multi = Param.Split(',');
                if (multi.Length == 0)
                {
                    txtSingle.Text = Param;
                }
                else
                {

                    foreach (var entry in multi)
                    {
                        var pairs = entry.Split(':');
                        if (pairs.Length == 0)
                        {
                            profParams.Add(new ProfParam { Key = entry });
                        }
                        else
                        {
                            profParams.Add(new ProfParam { Key = pairs[0], Val = pairs[1] });
                        }
                    }
                    FillGrid();
                }


            }
        }

        private void FillGrid()
        {
            BindingSource bsProfileList = new BindingSource();

            if (profParams.Count > 0)
            {
                //   grdParams.AutoGenerateColumns = false;
                bsProfileList.DataSource = new SortableBindingList<ProfParam>(profParams);
                grdParams.DataSource = bsProfileList.DataSource;

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            profParams.Add(new ProfParam
            {
                Key = txtKey.Text,
                Val = txtValue.Text
            });
            FillGrid();
            txtValue.Text = "";
            txtKey.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            profParams.Add(new ProfParam
            {
                Key = txtSingle.Text
            });
            FillGrid();
        }

        private void frmProfileParam_FormClosing(object sender, FormClosingEventArgs e)
        {
            string x = string.Empty;
            foreach (var param in profParams)
            {
                x += param.Key;
                x += !string.IsNullOrEmpty(param.Val) ? ":" + param.Val : string.Empty;
                x += ",";
            }
            Param = x;
        }
    }
    public class ProfParam
    {
        public string Key { get; set; }
        public string Val { get; set; }

    }
}
