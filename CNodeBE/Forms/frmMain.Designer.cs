﻿namespace CNodeBE
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.btnClose = new System.Windows.Forms.Button();
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profilesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.systemToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargowiseFieldMappingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testXMLTransferToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scratchPadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslMain = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslSpacer = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslMode = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslCmbMode = new System.Windows.Forms.ToolStripDropDownButton();
            this.testingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnStart = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.grdSatelliteStatus = new System.Windows.Forms.DataGridView();
            this.SatCustomer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SatelliteName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SatRunning = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SatID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SatOpened = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SatLastCheckin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SatLastOp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cxSatelliteList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.stopToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reStartToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.lblFilesTx = new System.Windows.Forms.Label();
            this.lblCWTx = new System.Windows.Forms.Label();
            this.lblTotFilesTx = new System.Windows.Forms.Label();
            this.lblFilesRx = new System.Windows.Forms.Label();
            this.lblCwRx = new System.Windows.Forms.Label();
            this.lblTotFilesRx = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tbCurrent = new System.Windows.Forms.TabPage();
            this.edLog = new System.Windows.Forms.RichTextBox();
            this.tbSummary = new System.Windows.Forms.TabPage();
            this.dgCurrent = new System.Windows.Forms.DataGridView();
            this.dateInDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.recipientDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.senderDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.msgTypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ref1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ref1TypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ref2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ref2TypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ref3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ref3TypeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.filenameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dsMain = new System.Data.DataSet();
            this.CurrentSummary = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.tbOperationSummary = new System.Windows.Forms.TabPage();
            this.cmProcessingError = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.reProcessToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteLineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gbPRocessQueues = new System.Windows.Forms.GroupBox();
            this.cbInbound = new System.Windows.Forms.CheckBox();
            this.cbOutbound = new System.Windows.Forms.CheckBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.logger = new System.Windows.Forms.Timer(this.components);
            this.mnuMain.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdSatelliteStatus)).BeginInit();
            this.cxSatelliteList.SuspendLayout();
            this.tcMain.SuspendLayout();
            this.tbCurrent.SuspendLayout();
            this.tbSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCurrent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentSummary)).BeginInit();
            this.tbOperationSummary.SuspendLayout();
            this.cmProcessingError.SuspendLayout();
            this.gbPRocessQueues.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClose.Location = new System.Drawing.Point(1075, 447);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(89, 27);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "&Close";
            this.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.button1_Click);
            // 
            // mnuMain
            // 
            this.mnuMain.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.profilesToolStripMenuItem,
            this.systemToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Padding = new System.Windows.Forms.Padding(5, 1, 0, 1);
            this.mnuMain.Size = new System.Drawing.Size(1173, 26);
            this.mnuMain.TabIndex = 1;
            this.mnuMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(116, 26);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // profilesToolStripMenuItem
            // 
            this.profilesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.customerToolStripMenuItem});
            this.profilesToolStripMenuItem.Name = "profilesToolStripMenuItem";
            this.profilesToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.profilesToolStripMenuItem.Text = "&Profiles";
            // 
            // customerToolStripMenuItem
            // 
            this.customerToolStripMenuItem.Name = "customerToolStripMenuItem";
            this.customerToolStripMenuItem.Size = new System.Drawing.Size(208, 26);
            this.customerToolStripMenuItem.Text = "Customer &Profiles";
            this.customerToolStripMenuItem.Click += new System.EventHandler(this.customerToolStripMenuItem_Click);
            // 
            // systemToolStripMenuItem
            // 
            this.systemToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.cargowiseFieldMappingToolStripMenuItem,
            this.testXMLTransferToolStripMenuItem,
            this.scratchPadToolStripMenuItem});
            this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
            this.systemToolStripMenuItem.Size = new System.Drawing.Size(70, 24);
            this.systemToolStripMenuItem.Text = "&System";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(261, 26);
            this.settingsToolStripMenuItem.Text = "S&ettings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // cargowiseFieldMappingToolStripMenuItem
            // 
            this.cargowiseFieldMappingToolStripMenuItem.Name = "cargowiseFieldMappingToolStripMenuItem";
            this.cargowiseFieldMappingToolStripMenuItem.Size = new System.Drawing.Size(261, 26);
            this.cargowiseFieldMappingToolStripMenuItem.Text = "Cargowise Field Mapping";
            this.cargowiseFieldMappingToolStripMenuItem.Click += new System.EventHandler(this.cargowiseFieldMappingToolStripMenuItem_Click);
            // 
            // testXMLTransferToolStripMenuItem
            // 
            this.testXMLTransferToolStripMenuItem.Name = "testXMLTransferToolStripMenuItem";
            this.testXMLTransferToolStripMenuItem.Size = new System.Drawing.Size(261, 26);
            this.testXMLTransferToolStripMenuItem.Text = "Test XML Transfer";
            this.testXMLTransferToolStripMenuItem.Click += new System.EventHandler(this.testXMLTransferToolStripMenuItem_Click);
            // 
            // scratchPadToolStripMenuItem
            // 
            this.scratchPadToolStripMenuItem.Name = "scratchPadToolStripMenuItem";
            this.scratchPadToolStripMenuItem.Size = new System.Drawing.Size(261, 26);
            this.scratchPadToolStripMenuItem.Text = "XML Update";
            this.scratchPadToolStripMenuItem.Click += new System.EventHandler(this.scratchPadToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(55, 24);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(133, 26);
            this.aboutToolStripMenuItem.Text = "&About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslMain,
            this.tslSpacer,
            this.tslMode,
            this.tslCmbMode});
            this.statusStrip1.Location = new System.Drawing.Point(0, 494);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 12, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1173, 30);
            this.statusStrip1.TabIndex = 2;
            // 
            // tslMain
            // 
            this.tslMain.AutoSize = false;
            this.tslMain.Name = "tslMain";
            this.tslMain.Size = new System.Drawing.Size(39, 24);
            this.tslMain.Text = "Status";
            this.tslMain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tslSpacer
            // 
            this.tslSpacer.AutoSize = false;
            this.tslSpacer.Name = "tslSpacer";
            this.tslSpacer.Padding = new System.Windows.Forms.Padding(50, 0, 0, 0);
            this.tslSpacer.Size = new System.Drawing.Size(50, 24);
            // 
            // tslMode
            // 
            this.tslMode.Name = "tslMode";
            this.tslMode.Size = new System.Drawing.Size(48, 24);
            this.tslMode.Text = "Mode";
            this.tslMode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tslCmbMode
            // 
            this.tslCmbMode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tslCmbMode.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.testingToolStripMenuItem,
            this.productionToolStripMenuItem});
            this.tslCmbMode.Image = ((System.Drawing.Image)(resources.GetObject("tslCmbMode.Image")));
            this.tslCmbMode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tslCmbMode.MergeAction = System.Windows.Forms.MergeAction.Insert;
            this.tslCmbMode.Name = "tslCmbMode";
            this.tslCmbMode.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.tslCmbMode.Padding = new System.Windows.Forms.Padding(0, 0, 15, 0);
            this.tslCmbMode.Size = new System.Drawing.Size(53, 28);
            this.tslCmbMode.Text = "toolStripDropDownButton2";
            // 
            // testingToolStripMenuItem
            // 
            this.testingToolStripMenuItem.CheckOnClick = true;
            this.testingToolStripMenuItem.Name = "testingToolStripMenuItem";
            this.testingToolStripMenuItem.Size = new System.Drawing.Size(164, 26);
            this.testingToolStripMenuItem.Text = "Testing";
            this.testingToolStripMenuItem.Click += new System.EventHandler(this.testingToolStringMenuItem_Click);
            // 
            // productionToolStripMenuItem
            // 
            this.productionToolStripMenuItem.Checked = true;
            this.productionToolStripMenuItem.CheckOnClick = true;
            this.productionToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.productionToolStripMenuItem.Name = "productionToolStripMenuItem";
            this.productionToolStripMenuItem.Size = new System.Drawing.Size(164, 26);
            this.productionToolStripMenuItem.Text = "Production";
            this.productionToolStripMenuItem.Click += new System.EventHandler(this.productionToolStripMenuItem_Click);
            // 
            // btnStart
            // 
            this.btnStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStart.BackColor = System.Drawing.Color.LightGreen;
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStart.Location = new System.Drawing.Point(949, 447);
            this.btnStart.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(89, 27);
            this.btnStart.TabIndex = 3;
            this.btnStart.Text = "&Start";
            this.btnStart.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.grdSatelliteStatus);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(729, 134);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(435, 308);
            this.panel1.TabIndex = 6;
            // 
            // grdSatelliteStatus
            // 
            this.grdSatelliteStatus.AllowUserToAddRows = false;
            this.grdSatelliteStatus.AllowUserToDeleteRows = false;
            this.grdSatelliteStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdSatelliteStatus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SatCustomer,
            this.SatelliteName,
            this.SatRunning,
            this.SatID,
            this.SatOpened,
            this.SatLastCheckin,
            this.SatLastOp});
            this.grdSatelliteStatus.ContextMenuStrip = this.cxSatelliteList;
            this.grdSatelliteStatus.Location = new System.Drawing.Point(9, 25);
            this.grdSatelliteStatus.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.grdSatelliteStatus.Name = "grdSatelliteStatus";
            this.grdSatelliteStatus.ReadOnly = true;
            this.grdSatelliteStatus.RowHeadersVisible = false;
            this.grdSatelliteStatus.RowHeadersWidth = 51;
            this.grdSatelliteStatus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdSatelliteStatus.Size = new System.Drawing.Size(419, 279);
            this.grdSatelliteStatus.TabIndex = 1;
            this.grdSatelliteStatus.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdSatelliteStatus_CellContentClick);
            this.grdSatelliteStatus.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.grdSatelliteStatus_CellFormatting);
            this.grdSatelliteStatus.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.grdSatelliteStatus_DataBindingComplete);
            // 
            // SatCustomer
            // 
            this.SatCustomer.DataPropertyName = "C_NAME";
            this.SatCustomer.HeaderText = "Customer";
            this.SatCustomer.MinimumWidth = 6;
            this.SatCustomer.Name = "SatCustomer";
            this.SatCustomer.ReadOnly = true;
            this.SatCustomer.Width = 125;
            // 
            // SatelliteName
            // 
            this.SatelliteName.DataPropertyName = "HB_NAME";
            this.SatelliteName.HeaderText = "Satellite/App";
            this.SatelliteName.MinimumWidth = 6;
            this.SatelliteName.Name = "SatelliteName";
            this.SatelliteName.ReadOnly = true;
            this.SatelliteName.Width = 125;
            // 
            // SatRunning
            // 
            this.SatRunning.HeaderText = "Status";
            this.SatRunning.MinimumWidth = 6;
            this.SatRunning.Name = "SatRunning";
            this.SatRunning.ReadOnly = true;
            this.SatRunning.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.SatRunning.Width = 125;
            // 
            // SatID
            // 
            this.SatID.DataPropertyName = "HB_ID";
            this.SatID.HeaderText = "ID";
            this.SatID.MinimumWidth = 6;
            this.SatID.Name = "SatID";
            this.SatID.ReadOnly = true;
            this.SatID.Visible = false;
            this.SatID.Width = 125;
            // 
            // SatOpened
            // 
            this.SatOpened.DataPropertyName = "HB_OPENED";
            this.SatOpened.HeaderText = "Opened";
            this.SatOpened.MinimumWidth = 6;
            this.SatOpened.Name = "SatOpened";
            this.SatOpened.ReadOnly = true;
            this.SatOpened.Width = 125;
            // 
            // SatLastCheckin
            // 
            this.SatLastCheckin.DataPropertyName = "HB_LASTCHECKIN";
            this.SatLastCheckin.HeaderText = "Last Checkin";
            this.SatLastCheckin.MinimumWidth = 6;
            this.SatLastCheckin.Name = "SatLastCheckin";
            this.SatLastCheckin.ReadOnly = true;
            this.SatLastCheckin.Width = 125;
            // 
            // SatLastOp
            // 
            this.SatLastOp.DataPropertyName = "HB_LASTOPERATION";
            this.SatLastOp.HeaderText = "Last Operation";
            this.SatLastOp.MinimumWidth = 6;
            this.SatLastOp.Name = "SatLastOp";
            this.SatLastOp.ReadOnly = true;
            this.SatLastOp.Width = 125;
            // 
            // cxSatelliteList
            // 
            this.cxSatelliteList.ImageScalingSize = new System.Drawing.Size(18, 18);
            this.cxSatelliteList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stopToolStripMenuItem,
            this.startToolStripMenuItem,
            this.reStartToolStripMenuItem});
            this.cxSatelliteList.Name = "cxSatelliteList";
            this.cxSatelliteList.Size = new System.Drawing.Size(133, 76);
            // 
            // stopToolStripMenuItem
            // 
            this.stopToolStripMenuItem.Name = "stopToolStripMenuItem";
            this.stopToolStripMenuItem.Size = new System.Drawing.Size(132, 24);
            this.stopToolStripMenuItem.Text = "Stop";
            // 
            // startToolStripMenuItem
            // 
            this.startToolStripMenuItem.Name = "startToolStripMenuItem";
            this.startToolStripMenuItem.Size = new System.Drawing.Size(132, 24);
            this.startToolStripMenuItem.Text = "Start";
            // 
            // reStartToolStripMenuItem
            // 
            this.reStartToolStripMenuItem.Name = "reStartToolStripMenuItem";
            this.reStartToolStripMenuItem.Size = new System.Drawing.Size(132, 24);
            this.reStartToolStripMenuItem.Text = "Re-Start";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Satellite Status";
            // 
            // lblFilesTx
            // 
            this.lblFilesTx.AutoSize = true;
            this.lblFilesTx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilesTx.Location = new System.Drawing.Point(221, 241);
            this.lblFilesTx.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFilesTx.Name = "lblFilesTx";
            this.lblFilesTx.Size = new System.Drawing.Size(17, 17);
            this.lblFilesTx.TabIndex = 12;
            this.lblFilesTx.Text = "0";
            // 
            // lblCWTx
            // 
            this.lblCWTx.AutoSize = true;
            this.lblCWTx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCWTx.Location = new System.Drawing.Point(221, 207);
            this.lblCWTx.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCWTx.Name = "lblCWTx";
            this.lblCWTx.Size = new System.Drawing.Size(17, 17);
            this.lblCWTx.TabIndex = 11;
            this.lblCWTx.Text = "0";
            // 
            // lblTotFilesTx
            // 
            this.lblTotFilesTx.AutoSize = true;
            this.lblTotFilesTx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotFilesTx.Location = new System.Drawing.Point(221, 172);
            this.lblTotFilesTx.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotFilesTx.Name = "lblTotFilesTx";
            this.lblTotFilesTx.Size = new System.Drawing.Size(17, 17);
            this.lblTotFilesTx.TabIndex = 10;
            this.lblTotFilesTx.Text = "0";
            // 
            // lblFilesRx
            // 
            this.lblFilesRx.AutoSize = true;
            this.lblFilesRx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFilesRx.Location = new System.Drawing.Point(221, 138);
            this.lblFilesRx.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFilesRx.Name = "lblFilesRx";
            this.lblFilesRx.Size = new System.Drawing.Size(17, 17);
            this.lblFilesRx.TabIndex = 9;
            this.lblFilesRx.Text = "0";
            // 
            // lblCwRx
            // 
            this.lblCwRx.AutoSize = true;
            this.lblCwRx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCwRx.Location = new System.Drawing.Point(221, 103);
            this.lblCwRx.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCwRx.Name = "lblCwRx";
            this.lblCwRx.Size = new System.Drawing.Size(17, 17);
            this.lblCwRx.TabIndex = 8;
            this.lblCwRx.Text = "0";
            // 
            // lblTotFilesRx
            // 
            this.lblTotFilesRx.AutoSize = true;
            this.lblTotFilesRx.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotFilesRx.Location = new System.Drawing.Point(221, 69);
            this.lblTotFilesRx.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotFilesRx.Name = "lblTotFilesRx";
            this.lblTotFilesRx.Size = new System.Drawing.Size(17, 17);
            this.lblTotFilesRx.TabIndex = 7;
            this.lblTotFilesRx.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(36, 241);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(152, 17);
            this.label8.TabIndex = 6;
            this.label8.Text = "Files to Non Cargowise";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(168, 17);
            this.label7.TabIndex = 5;
            this.label7.Text = "Files to Cargowise Clients";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 17);
            this.label6.TabIndex = 4;
            this.label6.Text = "Total Files Sent";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(164, 17);
            this.label5.TabIndex = 3;
            this.label5.Text = "Customer Files Received";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Cargowise Files";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 17);
            this.label3.TabIndex = 1;
            this.label3.Text = "Total Files Received";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "Operation Summary";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(476, 324);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(155, 28);
            this.button1.TabIndex = 7;
            this.button1.Text = "Save and Clear Log";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // tcMain
            // 
            this.tcMain.Controls.Add(this.tbCurrent);
            this.tcMain.Controls.Add(this.tbSummary);
            this.tcMain.Controls.Add(this.tbOperationSummary);
            this.tcMain.Location = new System.Drawing.Point(15, 47);
            this.tcMain.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(685, 411);
            this.tcMain.TabIndex = 8;
            // 
            // tbCurrent
            // 
            this.tbCurrent.Controls.Add(this.edLog);
            this.tbCurrent.Location = new System.Drawing.Point(4, 25);
            this.tbCurrent.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbCurrent.Name = "tbCurrent";
            this.tbCurrent.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbCurrent.Size = new System.Drawing.Size(677, 382);
            this.tbCurrent.TabIndex = 0;
            this.tbCurrent.Text = "Current Operation";
            this.tbCurrent.UseVisualStyleBackColor = true;
            // 
            // edLog
            // 
            this.edLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.edLog.Location = new System.Drawing.Point(8, 7);
            this.edLog.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.edLog.Name = "edLog";
            this.edLog.Size = new System.Drawing.Size(657, 363);
            this.edLog.TabIndex = 0;
            this.edLog.Text = "";
            // 
            // tbSummary
            // 
            this.tbSummary.Controls.Add(this.dgCurrent);
            this.tbSummary.Location = new System.Drawing.Point(4, 25);
            this.tbSummary.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbSummary.Name = "tbSummary";
            this.tbSummary.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbSummary.Size = new System.Drawing.Size(677, 382);
            this.tbSummary.TabIndex = 1;
            this.tbSummary.Text = "Summary";
            this.tbSummary.UseVisualStyleBackColor = true;
            // 
            // dgCurrent
            // 
            this.dgCurrent.AllowUserToAddRows = false;
            this.dgCurrent.AllowUserToDeleteRows = false;
            this.dgCurrent.AllowUserToOrderColumns = true;
            this.dgCurrent.AllowUserToResizeRows = false;
            this.dgCurrent.AutoGenerateColumns = false;
            this.dgCurrent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCurrent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dateInDataGridViewTextBoxColumn,
            this.recipientDataGridViewTextBoxColumn,
            this.senderDataGridViewTextBoxColumn,
            this.msgTypeDataGridViewTextBoxColumn,
            this.ref1DataGridViewTextBoxColumn,
            this.ref1TypeDataGridViewTextBoxColumn,
            this.ref2DataGridViewTextBoxColumn,
            this.ref2TypeDataGridViewTextBoxColumn,
            this.ref3DataGridViewTextBoxColumn,
            this.ref3TypeDataGridViewTextBoxColumn,
            this.filenameDataGridViewTextBoxColumn});
            this.dgCurrent.DataMember = "CurrentSummary";
            this.dgCurrent.DataSource = this.dsMain;
            this.dgCurrent.Location = new System.Drawing.Point(8, 7);
            this.dgCurrent.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgCurrent.Name = "dgCurrent";
            this.dgCurrent.RowHeadersWidth = 51;
            this.dgCurrent.Size = new System.Drawing.Size(659, 327);
            this.dgCurrent.TabIndex = 0;
            // 
            // dateInDataGridViewTextBoxColumn
            // 
            this.dateInDataGridViewTextBoxColumn.DataPropertyName = "DateIn";
            this.dateInDataGridViewTextBoxColumn.HeaderText = "Date In";
            this.dateInDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.dateInDataGridViewTextBoxColumn.Name = "dateInDataGridViewTextBoxColumn";
            this.dateInDataGridViewTextBoxColumn.Width = 125;
            // 
            // recipientDataGridViewTextBoxColumn
            // 
            this.recipientDataGridViewTextBoxColumn.DataPropertyName = "Recipient";
            this.recipientDataGridViewTextBoxColumn.HeaderText = "Recipient";
            this.recipientDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.recipientDataGridViewTextBoxColumn.Name = "recipientDataGridViewTextBoxColumn";
            this.recipientDataGridViewTextBoxColumn.Width = 125;
            // 
            // senderDataGridViewTextBoxColumn
            // 
            this.senderDataGridViewTextBoxColumn.DataPropertyName = "Sender";
            this.senderDataGridViewTextBoxColumn.HeaderText = "Sender";
            this.senderDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.senderDataGridViewTextBoxColumn.Name = "senderDataGridViewTextBoxColumn";
            this.senderDataGridViewTextBoxColumn.Width = 125;
            // 
            // msgTypeDataGridViewTextBoxColumn
            // 
            this.msgTypeDataGridViewTextBoxColumn.DataPropertyName = "MsgType";
            this.msgTypeDataGridViewTextBoxColumn.HeaderText = "Message Type";
            this.msgTypeDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.msgTypeDataGridViewTextBoxColumn.Name = "msgTypeDataGridViewTextBoxColumn";
            this.msgTypeDataGridViewTextBoxColumn.Width = 125;
            // 
            // ref1DataGridViewTextBoxColumn
            // 
            this.ref1DataGridViewTextBoxColumn.DataPropertyName = "Ref1";
            this.ref1DataGridViewTextBoxColumn.HeaderText = "Reference";
            this.ref1DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.ref1DataGridViewTextBoxColumn.Name = "ref1DataGridViewTextBoxColumn";
            this.ref1DataGridViewTextBoxColumn.Width = 125;
            // 
            // ref1TypeDataGridViewTextBoxColumn
            // 
            this.ref1TypeDataGridViewTextBoxColumn.DataPropertyName = "Ref1Type";
            this.ref1TypeDataGridViewTextBoxColumn.HeaderText = "Type";
            this.ref1TypeDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.ref1TypeDataGridViewTextBoxColumn.Name = "ref1TypeDataGridViewTextBoxColumn";
            this.ref1TypeDataGridViewTextBoxColumn.Width = 125;
            // 
            // ref2DataGridViewTextBoxColumn
            // 
            this.ref2DataGridViewTextBoxColumn.DataPropertyName = "Ref2";
            this.ref2DataGridViewTextBoxColumn.HeaderText = "Reference";
            this.ref2DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.ref2DataGridViewTextBoxColumn.Name = "ref2DataGridViewTextBoxColumn";
            this.ref2DataGridViewTextBoxColumn.Width = 125;
            // 
            // ref2TypeDataGridViewTextBoxColumn
            // 
            this.ref2TypeDataGridViewTextBoxColumn.DataPropertyName = "ref2Type";
            this.ref2TypeDataGridViewTextBoxColumn.HeaderText = "Type";
            this.ref2TypeDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.ref2TypeDataGridViewTextBoxColumn.Name = "ref2TypeDataGridViewTextBoxColumn";
            this.ref2TypeDataGridViewTextBoxColumn.Width = 125;
            // 
            // ref3DataGridViewTextBoxColumn
            // 
            this.ref3DataGridViewTextBoxColumn.DataPropertyName = "ref3";
            this.ref3DataGridViewTextBoxColumn.HeaderText = "Reference";
            this.ref3DataGridViewTextBoxColumn.MinimumWidth = 6;
            this.ref3DataGridViewTextBoxColumn.Name = "ref3DataGridViewTextBoxColumn";
            this.ref3DataGridViewTextBoxColumn.Width = 125;
            // 
            // ref3TypeDataGridViewTextBoxColumn
            // 
            this.ref3TypeDataGridViewTextBoxColumn.DataPropertyName = "Ref3Type";
            this.ref3TypeDataGridViewTextBoxColumn.HeaderText = "Type";
            this.ref3TypeDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.ref3TypeDataGridViewTextBoxColumn.Name = "ref3TypeDataGridViewTextBoxColumn";
            this.ref3TypeDataGridViewTextBoxColumn.Width = 125;
            // 
            // filenameDataGridViewTextBoxColumn
            // 
            this.filenameDataGridViewTextBoxColumn.DataPropertyName = "filename";
            this.filenameDataGridViewTextBoxColumn.HeaderText = "Filename";
            this.filenameDataGridViewTextBoxColumn.MinimumWidth = 6;
            this.filenameDataGridViewTextBoxColumn.Name = "filenameDataGridViewTextBoxColumn";
            this.filenameDataGridViewTextBoxColumn.Width = 125;
            // 
            // dsMain
            // 
            this.dsMain.DataSetName = "NewDataSet";
            this.dsMain.Tables.AddRange(new System.Data.DataTable[] {
            this.CurrentSummary});
            // 
            // CurrentSummary
            // 
            this.CurrentSummary.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12});
            this.CurrentSummary.TableName = "CurrentSummary";
            // 
            // dataColumn1
            // 
            this.dataColumn1.Caption = "Date";
            this.dataColumn1.ColumnName = "DateIn";
            this.dataColumn1.DataType = typeof(System.DateTime);
            // 
            // dataColumn2
            // 
            this.dataColumn2.Caption = "Recipient";
            this.dataColumn2.ColumnName = "Recipient";
            // 
            // dataColumn3
            // 
            this.dataColumn3.Caption = "Sender";
            this.dataColumn3.ColumnName = "Sender";
            // 
            // dataColumn4
            // 
            this.dataColumn4.Caption = "Message Type";
            this.dataColumn4.ColumnName = "MsgType";
            // 
            // dataColumn5
            // 
            this.dataColumn5.Caption = "Reference";
            this.dataColumn5.ColumnName = "Ref1";
            // 
            // dataColumn6
            // 
            this.dataColumn6.Caption = "Type";
            this.dataColumn6.ColumnName = "Ref1Type";
            // 
            // dataColumn7
            // 
            this.dataColumn7.Caption = "Reference";
            this.dataColumn7.ColumnName = "Ref2";
            // 
            // dataColumn8
            // 
            this.dataColumn8.Caption = "Type";
            this.dataColumn8.ColumnName = "ref2Type";
            // 
            // dataColumn9
            // 
            this.dataColumn9.Caption = "Reference";
            this.dataColumn9.ColumnName = "ref3";
            // 
            // dataColumn10
            // 
            this.dataColumn10.Caption = "Type";
            this.dataColumn10.ColumnName = "Ref3Type";
            // 
            // dataColumn11
            // 
            this.dataColumn11.Caption = "File Name";
            this.dataColumn11.ColumnName = "filename";
            // 
            // dataColumn12
            // 
            this.dataColumn12.Caption = "Result";
            this.dataColumn12.ColumnName = "Result";
            // 
            // tbOperationSummary
            // 
            this.tbOperationSummary.Controls.Add(this.lblFilesTx);
            this.tbOperationSummary.Controls.Add(this.label2);
            this.tbOperationSummary.Controls.Add(this.button1);
            this.tbOperationSummary.Controls.Add(this.lblCWTx);
            this.tbOperationSummary.Controls.Add(this.label3);
            this.tbOperationSummary.Controls.Add(this.lblTotFilesTx);
            this.tbOperationSummary.Controls.Add(this.label4);
            this.tbOperationSummary.Controls.Add(this.lblFilesRx);
            this.tbOperationSummary.Controls.Add(this.label5);
            this.tbOperationSummary.Controls.Add(this.lblCwRx);
            this.tbOperationSummary.Controls.Add(this.label6);
            this.tbOperationSummary.Controls.Add(this.lblTotFilesRx);
            this.tbOperationSummary.Controls.Add(this.label7);
            this.tbOperationSummary.Controls.Add(this.label8);
            this.tbOperationSummary.Location = new System.Drawing.Point(4, 25);
            this.tbOperationSummary.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbOperationSummary.Name = "tbOperationSummary";
            this.tbOperationSummary.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbOperationSummary.Size = new System.Drawing.Size(677, 382);
            this.tbOperationSummary.TabIndex = 3;
            this.tbOperationSummary.Text = "Operation Summary";
            this.tbOperationSummary.UseVisualStyleBackColor = true;
            // 
            // cmProcessingError
            // 
            this.cmProcessingError.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmProcessingError.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.reProcessToolStripMenuItem,
            this.deleteLineToolStripMenuItem});
            this.cmProcessingError.Name = "cmProcessingError";
            this.cmProcessingError.Size = new System.Drawing.Size(170, 52);
            // 
            // reProcessToolStripMenuItem
            // 
            this.reProcessToolStripMenuItem.Name = "reProcessToolStripMenuItem";
            this.reProcessToolStripMenuItem.Size = new System.Drawing.Size(169, 24);
            this.reProcessToolStripMenuItem.Text = "Re-Process";
            // 
            // deleteLineToolStripMenuItem
            // 
            this.deleteLineToolStripMenuItem.Name = "deleteLineToolStripMenuItem";
            this.deleteLineToolStripMenuItem.Size = new System.Drawing.Size(169, 24);
            this.deleteLineToolStripMenuItem.Text = "Delete Line(s)";
            // 
            // gbPRocessQueues
            // 
            this.gbPRocessQueues.Controls.Add(this.cbInbound);
            this.gbPRocessQueues.Controls.Add(this.cbOutbound);
            this.gbPRocessQueues.Location = new System.Drawing.Point(729, 59);
            this.gbPRocessQueues.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbPRocessQueues.Name = "gbPRocessQueues";
            this.gbPRocessQueues.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbPRocessQueues.Size = new System.Drawing.Size(435, 55);
            this.gbPRocessQueues.TabIndex = 9;
            this.gbPRocessQueues.TabStop = false;
            this.gbPRocessQueues.Text = "Process Queues";
            // 
            // cbInbound
            // 
            this.cbInbound.AutoSize = true;
            this.cbInbound.Location = new System.Drawing.Point(215, 22);
            this.cbInbound.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbInbound.Name = "cbInbound";
            this.cbInbound.Size = new System.Drawing.Size(135, 21);
            this.cbInbound.TabIndex = 1;
            this.cbInbound.Text = "Inbound Queues";
            this.cbInbound.UseVisualStyleBackColor = true;
            // 
            // cbOutbound
            // 
            this.cbOutbound.AutoSize = true;
            this.cbOutbound.Location = new System.Drawing.Point(24, 22);
            this.cbOutbound.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbOutbound.Name = "cbOutbound";
            this.cbOutbound.Size = new System.Drawing.Size(148, 21);
            this.cbOutbound.TabIndex = 0;
            this.cbOutbound.Text = "OutBound Queues";
            this.cbOutbound.UseVisualStyleBackColor = true;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(28, 466);
            this.lblVersion.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(54, 17);
            this.lblVersion.TabIndex = 10;
            this.lblVersion.Text = "version";
            // 
            // logger
            // 
            this.logger.Interval = 60000;
            this.logger.Tick += new System.EventHandler(this.logger_Tick);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1173, 524);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.gbPRocessQueues);
            this.Controls.Add(this.tcMain);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.mnuMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mnuMain;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmMain";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "CTC Node (BE) Admin";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.Resize += new System.EventHandler(this.FrmMain_Resize);
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdSatelliteStatus)).EndInit();
            this.cxSatelliteList.ResumeLayout(false);
            this.tcMain.ResumeLayout(false);
            this.tbCurrent.ResumeLayout(false);
            this.tbSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCurrent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CurrentSummary)).EndInit();
            this.tbOperationSummary.ResumeLayout(false);
            this.tbOperationSummary.PerformLayout();
            this.cmProcessingError.ResumeLayout(false);
            this.gbPRocessQueues.ResumeLayout(false);
            this.gbPRocessQueues.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem systemToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profilesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslMain;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblFilesTx;
        private System.Windows.Forms.Label lblCWTx;
        private System.Windows.Forms.Label lblTotFilesTx;
        private System.Windows.Forms.Label lblFilesRx;
        private System.Windows.Forms.Label lblCwRx;
        private System.Windows.Forms.Label lblTotFilesRx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tbCurrent;
        private System.Windows.Forms.TabPage tbSummary;
        private System.Windows.Forms.DataGridView dgCurrent;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateInDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn recipientDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn senderDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn msgTypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ref1DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ref1TypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ref2DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ref2TypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ref3DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ref3TypeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn filenameDataGridViewTextBoxColumn;
        private System.Data.DataSet dsMain;
        private System.Data.DataTable CurrentSummary;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Windows.Forms.ContextMenuStrip cmProcessingError;
        private System.Windows.Forms.ToolStripMenuItem reProcessToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteLineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testXMLTransferToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scratchPadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cargowiseFieldMappingToolStripMenuItem;
        private System.Windows.Forms.RichTextBox edLog;
        private System.Windows.Forms.ToolStripStatusLabel tslSpacer;
        private System.Windows.Forms.ToolStripStatusLabel tslMode;
        private System.Windows.Forms.ToolStripDropDownButton tslCmbMode;
        private System.Windows.Forms.ToolStripMenuItem testingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productionToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tbOperationSummary;
        private System.Windows.Forms.DataGridView grdSatelliteStatus;
        private System.Windows.Forms.ContextMenuStrip cxSatelliteList;
        private System.Windows.Forms.ToolStripMenuItem stopToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reStartToolStripMenuItem;
        private System.Windows.Forms.GroupBox gbPRocessQueues;
        private System.Windows.Forms.CheckBox cbInbound;
        private System.Windows.Forms.CheckBox cbOutbound;
        private System.Windows.Forms.DataGridViewTextBoxColumn SatCustomer;
        private System.Windows.Forms.DataGridViewTextBoxColumn SatelliteName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn SatRunning;
        private System.Windows.Forms.DataGridViewTextBoxColumn SatID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SatOpened;
        private System.Windows.Forms.DataGridViewTextBoxColumn SatLastCheckin;
        private System.Windows.Forms.DataGridViewTextBoxColumn SatLastOp;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Timer logger;
    }
}

