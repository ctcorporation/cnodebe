﻿using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;


namespace CNodeBE
{


    public partial class FrmSettings : Form
    {
        public IMailServerSettings MailServerSettings { get; set; }

        public FrmSettings()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            XmlDocument xmlConfig = new XmlDocument();
            xmlConfig.Load(Globals.AppConfig);
            XmlNodeList nodelist = xmlConfig.SelectNodes("/CNode/Database");
            XmlNode xmlCatalog = nodelist[0].SelectSingleNode("Catalog");
            XmlNode xnode;
            if (xmlCatalog == null)
            {
                xnode = xmlConfig.CreateElement("Catalog");
                xnode.InnerText = edCTCDatabase.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlCatalog.InnerText = edCTCDatabase.Text;
            }

            XmlNode xmlServer = nodelist[0].SelectSingleNode("Servername");
            if (xmlServer == null)
            {
                xnode = xmlConfig.CreateElement("Servername");
                xnode.InnerText = edCTCServer.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlServer.InnerText = edCTCServer.Text;
            }

            XmlNode xmlUser = nodelist[0].SelectSingleNode("Username");
            if (xmlUser == null)
            {
                xnode = xmlConfig.CreateElement("Username");
                xnode.InnerText = edCTCUserName.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlUser.InnerText = edCTCUserName.Text;
            }

            XmlNode xmlPassword = nodelist[0].SelectSingleNode("Password");
            if (xmlUser == null)
            {
                xnode = xmlConfig.CreateElement("Password");
                xnode.InnerText = edCTCPassword.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlPassword.InnerText = edCTCPassword.Text;
            }

            nodelist = xmlConfig.SelectNodes("/CNode/TransMod");
            if (nodelist.Count == 0)
            {
                XmlNode xRoot = xmlConfig.SelectSingleNode("/CNode");
                xnode = xmlConfig.CreateElement("TransMod");
                xRoot.AppendChild(xnode);
                nodelist = xmlConfig.SelectNodes("/CNode/TransMod");
            }
            xmlCatalog = null;
            xmlCatalog = nodelist[0].SelectSingleNode("Catalog");

            xnode = null;
            if (xmlCatalog == null)
            {
                xnode = xmlConfig.CreateElement("Catalog");
                xnode.InnerText = edTransDatabase.Text;
                Globals.TransDatabase = edTransDatabase.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.TransDatabase = edTransDatabase.Text;
                xmlCatalog.InnerText = edTransDatabase.Text;
            }
            xmlServer = null;
            xmlServer = nodelist[0].SelectSingleNode("Servername");
            if (xmlServer == null)
            {
                xnode = xmlConfig.CreateElement("Servername");
                xnode.InnerText = edTransServer.Text;
                Globals.TransServer = edTransServer.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlServer.InnerText = edTransServer.Text;
                Globals.TransServer = edTransServer.Text;
            }

            xmlUser = null;

            xmlUser = nodelist[0].SelectSingleNode("Username");
            if (xmlUser == null)
            {
                xnode = xmlConfig.CreateElement("Username");
                xnode.InnerText = edTransUserName.Text;
                Globals.TransUsername = edCTCUserName.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlUser.InnerText = edTransUserName.Text;
                Globals.TransUsername = edTransUserName.Text;
            }

            xmlPassword = null;
            xmlPassword = nodelist[0].SelectSingleNode("Password");
            if (xmlUser == null)
            {
                xnode = xmlConfig.CreateElement("Password");
                xnode.InnerText = edTransPassword.Text;
                Globals.TransPassword = edTransPassword.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                Globals.TransPassword = edTransPassword.Text;
                xmlPassword.InnerText = edTransPassword.Text;
            }


            XmlNode xmlAlerts = nodelist[0].SelectSingleNode("AlertsTo");
            if (xmlAlerts == null)
            {
                xnode = xmlConfig.CreateElement("AlertsTo");
                xnode.InnerText = edAlerts.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlAlerts.InnerText = edAlerts.Text;
            }

            nodelist = xmlConfig.SelectNodes("/CNode/Config");
            if (nodelist[0] == null)
            {
                xnode = xmlConfig.CreateElement("Config");
                XmlNode xConfig = xmlConfig.SelectSingleNode("/CNode");
                xConfig.AppendChild(xnode);
                nodelist = xmlConfig.SelectNodes("/CNode/Config");
            }
            XmlNode xmlPollTime = nodelist[0].SelectSingleNode("PollingTime");
            if (xmlPollTime == null)
            {
                xnode = xmlConfig.CreateElement("PollingTime");
                xnode.InnerText = numPollingTime.Value.ToString();
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlPollTime.InnerText = numPollingTime.Value.ToString();
            }

            XmlNode xmlGuardianTime = nodelist[0].SelectSingleNode("GuardianPollTime");
            if (xmlGuardianTime == null)
            {
                xnode = xmlConfig.CreateElement("GuardianPollTime");
                xnode.InnerText = numGuardian.Value.ToString();
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlGuardianTime.InnerText = numGuardian.Value.ToString();
            }
            XmlNode xmlCyclecount = nodelist[0].SelectSingleNode("CycleCount");
            if (xmlCyclecount == null)
            {
                xnode = xmlConfig.CreateElement("CycleCount");
                xnode.InnerText = edCyclecount.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlCyclecount.InnerText = edCyclecount.Text;
            }

            XmlNode xmlMainTimer = nodelist[0].SelectSingleNode("MainTimer");
            if (xmlMainTimer == null)
            {
                xnode = xmlConfig.CreateElement("MainTimer");
                xnode.InnerText = edMainTimer.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlMainTimer.InnerText = edMainTimer.Text;
            }
            XmlNode xmlXMLLoc = nodelist[0].SelectSingleNode("XmlLocation");
            if (xmlXMLLoc == null)
            {
                xnode = xmlConfig.CreateElement("XmlLocation");
                xnode.InnerText = edXMLLocation.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlXMLLoc.InnerText = edXMLLocation.Text;
            }
            XmlNode xmlPdfXmlLoc = nodelist[0].SelectSingleNode("Pdf2XmlLoc");
            if (xmlPdfXmlLoc == null)
            {
                xnode = xmlConfig.CreateElement("Pdf2XmlLoc");
                xnode.InnerText = edPdf2XMLLoc.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlPdfXmlLoc.InnerText = edPdf2XMLLoc.Text;
            }

            XmlNode xmlReportLoc = nodelist[0].SelectSingleNode("WinscpLocation");
            if (xmlReportLoc == null)
            {
                xnode = xmlConfig.CreateElement("WinscpLocation");
                xnode.InnerText = edWinscpLocation.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlReportLoc.InnerText = edWinscpLocation.Text;
            }

            XmlNode xmlRootPath = nodelist[0].SelectSingleNode("CustomerRootPath");
            if (xmlRootPath == null)
            {
                xnode = xmlConfig.CreateElement("CustomerRootPath");
                xnode.InnerText = edCustRoot.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlRootPath.InnerText = edCustRoot.Text;
            }

            XmlNode xmlFailPath = nodelist[0].SelectSingleNode("FailPath");
            if (xmlFailPath == null)
            {
                xnode = xmlConfig.CreateElement("FailPath");
                xnode.InnerText = edFailedLoc.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlFailPath.InnerText = edFailedLoc.Text;
            }

            XmlNode xmlCustomReceivePath = nodelist[0].SelectSingleNode("CustomReceivePath");
            if (xmlCustomReceivePath == null)
            {
                xnode = xmlConfig.CreateElement("CustomReceivePath");
                xnode.InnerText = edCustomReceive.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlCustomReceivePath.InnerText = edCustomReceive.Text;
            }

            XmlNode xmlTestLoc = nodelist[0].SelectSingleNode("TestingLocation");
            if (xmlTestLoc == null)
            {
                xnode = xmlConfig.CreateElement("TestingLocation");
                xnode.InnerText = edTestingLocation.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlTestLoc.InnerText = edTestingLocation.Text;
            }

            nodelist = xmlConfig.SelectNodes("/CNode/Config/SMTP");
            if (nodelist[0] == null)
            {
                xnode = xmlConfig.CreateElement("SMTP");
                XmlNode xConfig = xmlConfig.SelectSingleNode("/CNode/Config");
                xConfig.AppendChild(xnode);
                nodelist = xmlConfig.SelectNodes("/CNode/Config/SMTP");
            }
            //HERE I AM
            XmlNode xmlSMTP = nodelist[0].SelectSingleNode("SMTPServer");
            if (xmlSMTP == null)
            {
                xnode = xmlConfig.CreateElement("SMTPServer");
                xnode.InnerText = edSMTPServer.Text;
                XmlAttribute xPort = xmlConfig.CreateAttribute("Port");
                xPort.InnerXml = edSMTPPort.Text;
                xnode.Attributes.Append(xPort);
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlSMTP.InnerText = edSMTPServer.Text;
                xmlSMTP.Attributes[0].InnerXml = edSMTPPort.Text;
            }
            xmlSMTP = nodelist[0].SelectSingleNode("EmailAddress");
            if (xmlSMTP == null)
            {
                xnode = xmlConfig.CreateElement("EmailAddress");
                xnode.InnerText = edEmail.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlSMTP.InnerText = edEmail.Text;
            }
            xmlSMTP = nodelist[0].SelectSingleNode("SMTPUsername");
            if (xmlSMTP == null)
            {
                xnode = xmlConfig.CreateElement("SMTPUsername");
                xnode.InnerText = edSMTPUsername.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlSMTP.InnerText = edSMTPUsername.Text;
            }
            xmlSMTP = nodelist[0].SelectSingleNode("SMTPPassword");
            if (xmlSMTP == null)
            {
                xnode = xmlConfig.CreateElement("SMTPPassword");
                xnode.InnerText = edSMTPPassword.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlSMTP.InnerText = edSMTPPassword.Text;
            }

            nodelist = xmlConfig.SelectNodes("/CNode/Config/Archive");
            if (nodelist[0] == null)
            {
                xnode = xmlConfig.CreateElement("Archive");
                XmlNode xConfig = xmlConfig.SelectSingleNode("/CNode/Config");
                xConfig.AppendChild(xnode);
                nodelist = xmlConfig.SelectNodes("/CNode/Config/Archive");

            }
            XmlNode xmlArchiveLoc = nodelist[0].SelectSingleNode("ArchiveLocation");
            if (xmlArchiveLoc == null)
            {
                xnode = xmlConfig.CreateElement("ArchiveLocation");
                xnode.InnerText = txtArchiveLoc.Text;
                nodelist[0].AppendChild(xnode);
            }
            else
            {
                xmlArchiveLoc.InnerText = txtArchiveLoc.Text;
            }




            //XmlNode xmlArcOptions = nodelist[0].SelectSingleNode("ArchiveFrequency");
            //string RbOptions = string.Empty;
            //foreach (RadioButton rb in gbArchive.Controls)
            //{
            //    if (rb.Checked)
            //    {
            //        RbOptions = rb.Text;
            //        break;
            //    }
            //}
            //if (xmlArcOptions == null)
            //{
            //    xnode = xmlConfig.CreateElement("ArchiveFrequency");
            //    xnode.InnerText = RbOptions;
            //    nodelist[0].AppendChild(xnode);
            //}
            //else
            //{
            //    xmlArcOptions.InnerText = RbOptions;
            //}

            xmlConfig.Save(Globals.AppConfig);
            MessageBox.Show("CTC Node Settings saved", "CTC Node Configuration", MessageBoxButtons.OK, MessageBoxIcon.Information);

        }

        private void FrmSettings_Load(object sender, EventArgs e)
        {
            try
            {
                edCTCDatabase.Text = Globals.DataBase;
                edCTCServer.Text = Globals.ServerName;
                edCTCUserName.Text = Globals.UserName;
                edCTCPassword.Text = Globals.Password;
                edTransServer.Text = Globals.TransServer;
                edTransDatabase.Text = Globals.TransDatabase;
                edTransPassword.Text = Globals.TransPassword;
                edTransUserName.Text = Globals.TransUsername;
                edXMLLocation.Text = Globals.XMLLoc;
                edPdf2XMLLoc.Text = Globals.Pdf2Xml;
                txtArchiveLoc.Text = Globals.ArchLoc;
                edWinscpLocation.Text = Globals.WinSCPLocation;
                edCustRoot.Text = Globals.RootPath;
                edCyclecount.Text = Globals.CycleCount.ToString();
                edMainTimer.Text = Globals.MainTimerInt.ToString();
                if (MailServerSettings != null)
                {
                    edSMTPServer.Text = MailServerSettings.Server;
                    edSMTPPort.Text = MailServerSettings.Port.ToString();
                    edEmail.Text = MailServerSettings.Email;
                    edSMTPUsername.Text = MailServerSettings.UserName;
                    edSMTPPassword.Text = MailServerSettings.Password;
                }

                edAlerts.Text = Globals.AlertsTo;

                edCustomReceive.Text = Globals.CustomReceive;
                edFailedLoc.Text = Globals.FailLoc;
                edTestingLocation.Text = Globals.TestingLoc;
                numGuardian.Value = Globals.GuardianPollTime;
                try
                {
                    numPollingTime.Value = Globals.PollTime;
                }
                catch (Exception)
                {
                    numPollingTime.Value = 2;
                }
                edCustomReceive.Text = Globals.CustomReceive;
            }
            catch (Exception)
            {

            }

        }

        private void btnTestConnection_Click(object sender, EventArgs e)
        {
            edCTCResults.Clear();
            edCTCResults.Text = "Data Source=" + edCTCServer.Text + ";Initial Catalog=" + edCTCDatabase.Text + ";User ID = " + edCTCUserName.Text + ";Password=" + edCTCPassword.Text + ";" + Environment.NewLine;
            edCTCResults.Text += "Testing Connection" + Environment.NewLine;
            SqlConnection sqlconn = new SqlConnection();
            sqlconn.ConnectionString = "Data Source=" + edCTCServer.Text + ";Initial Catalog=" + edCTCDatabase.Text + ";User ID = " + edCTCUserName.Text + ";Password=" + edCTCPassword.Text + ";";
            try
            {
                sqlconn.Open();
                edCTCResults.Text += " Connected Successfully." + Environment.NewLine;
            }
            catch (SqlException ex)
            {
                edCTCResults.Text += "Error found :" + ex.Message + Environment.NewLine;
            }


        }

        private void tbSecurity_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
        }

        private void btnDirXml_Click(object sender, EventArgs e)
        {

            try
            {
                fdInLocation.SelectedPath = edXMLLocation.Text;
            }
            catch (Exception)
            {
            }
            fdInLocation.ShowDialog();
            edXMLLocation.Text = fdInLocation.SelectedPath;
        }


        private void bbCustRoot_Click(object sender, EventArgs e)
        {
            try
            {
                fdInLocation.SelectedPath = edCustRoot.Text;
            }
            catch (Exception)
            { }

            fdInLocation.ShowDialog();
            edCustRoot.Text = fdInLocation.SelectedPath;
        }

        private void bbCustomReceive_Click(object sender, EventArgs e)
        {
            try
            {
                fdInLocation.SelectedPath = edCustomReceive.Text;
            }
            catch (Exception)
            { }

            fdInLocation.ShowDialog();
            edCustomReceive.Text = fdInLocation.SelectedPath;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                fdInLocation.SelectedPath = edFailedLoc.Text;
            }
            catch (Exception)
            { }
            fdInLocation.ShowDialog();
            edFailedLoc.Text = fdInLocation.SelectedPath;
        }

        private void btnTransTest_Click(object sender, EventArgs e)
        {
            rtbTransResults.Clear();
            rtbTransResults.Text = "Data Source=" + edTransServer.Text + ";Initial Catalog=" + edTransDatabase.Text + ";User ID = " + edTransUserName.Text + ";Password=" + edTransPassword.Text + ";" + Environment.NewLine;
            rtbTransResults.Text += "Testing Connection" + Environment.NewLine;
            SqlConnection sqlconn = new SqlConnection();
            sqlconn.ConnectionString = "Data Source=" + edTransServer.Text + ";Initial Catalog=" + edTransDatabase.Text + ";User ID = " + edTransUserName.Text + ";Password=" + edTransPassword.Text + ";";
            try
            {
                sqlconn.Open();
                NodeResources.AddRTBText(rtbTransResults, " Connected Successfully." + Environment.NewLine, Color.Black);
            }
            catch (SqlException ex)
            {
                string strEx = ex.GetType().Name;
                NodeResources.AddRTBText(rtbTransResults, strEx + " Error found :" + ex.Message + Environment.NewLine, Color.Red);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edTestingLocation.Text;
            fd.ShowDialog();
            edTestingLocation.Text = fd.SelectedPath;

        }



        private void btnWinscpLocaiton_Click(object sender, EventArgs e)
        {
            try
            {
                fdInLocation.SelectedPath = edWinscpLocation.Text;
            }
            catch (Exception)
            { }
            fdInLocation.ShowDialog();
            edWinscpLocation.Text = fdInLocation.SelectedPath;
        }

        private void btnWinscpLocaiton_Click_1(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.SelectedPath = edWinscpLocation.Text;
            fd.ShowDialog();
            edWinscpLocation.Text = fd.SelectedPath;
        }

        private void bbPDF2Xml_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog
            {
                SelectedPath = edPdf2XMLLoc.Text
            };
            fd.ShowDialog();
            edPdf2XMLLoc.Text = fd.SelectedPath;

        }

        private void btnArchiveLoc_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog
            {
                SelectedPath = txtArchiveLoc.Text
            };
            fd.ShowDialog();
            txtArchiveLoc.Text = fd.SelectedPath;
        }
    }
}
