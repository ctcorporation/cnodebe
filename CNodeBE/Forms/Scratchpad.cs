﻿using NodeResources;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;

namespace CNodeBE
{
    public partial class Scratchpad : Form
    {
        public Scratchpad()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public bool ConvertToCW()
        {
            bool result = false;

            DataSet dsProduct = new DataSet();
            dsProduct.ReadXml(textBox1.Text);
            return result;

        }

        public static void AddText(RichTextBox rtb, string value, Color color)
        {
            if (rtb.InvokeRequired)
            {
                rtb.BeginInvoke(new System.Action(delegate { AddText(rtb, value, color); }));
                return;
            }
            string[] str = value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            rtb.DeselectAll();
            rtb.SelectionColor = color;
            rtb.AppendText(value + Environment.NewLine);
            rtb.SelectionFont = new Font(rtb.SelectionFont, FontStyle.Regular);
            rtb.SelectionColor = Color.Black;
            rtb.ScrollToCaret();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            fd.ShowDialog();
            textBox1.Text = fd.SelectedPath;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DoFileUpdate();

        }

        private void DoFileUpdate()
        {
            bool sendFile = false;
            DialogResult dr = MessageBox.Show("Do you wish to send to CTC-Node", "XML Bulk Update", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                sendFile = true;
            }
            edLog.Text = "";
            foreach (var file in new DirectoryInfo(textBox1.Text).GetFiles())
            {

                if (file.Extension.ToLower() == ".xml")
                {
                    XDocument xdoc = XDocument.Load(file.FullName);
                    var ns = xdoc.Root.Name.Namespace;
                    AddText(edLog, "Searching File: " + file.FullName, Color.Black);
                    var el = xdoc.Descendants().Elements().Where(x => x.Name.LocalName == edXmlTag.Text).FirstOrDefault();
                    if (el != null)
                    {
                        if (el.Value == edFindValue.Text)
                        {
                            el.Value = edReplaceValue.Text;
                            AddText(edLog, file.FullName + " Modified. Writing changes.", Color.Green);
                            xdoc.Save(file.FullName);
                            if (sendFile)
                            {
                                CTCSendFile(file.FullName);
                            }
                            
                        }
                        else
                        {
                            AddText(edLog, "XML Tag found but incorrect value. Skipping", Color.Orange);
                        }


                    }
                    else
                    {
                        AddText(edLog, "XML Tag not found in File: " + file.FullName, Color.Gray);
                    }
                }
            }
        }

        private void CTCSendFile(string fullName)
        {
            try
            {   
                string sendlog = string.Empty;
                        sendlog = CTCAdapter.SendMessage(@"https://adapter.ctc.net.au/cnodestreamed.svc", fullName, "CTCSYD", "CTCSYD", "", null);

                // Test without sending. UnComment the following
                //String sendlog = "Send OK";
                if (sendlog.Contains("SEND OK"))
                {
                    AddText(edLog," File Transmitted Ok. ",Color.Blue);                    

                }
                else
                {
                    AddText(edLog,"CTC Adapter send Failed: " + sendlog,Color.Red);
                    
                }
            }
            catch (Exception ex)
            {
                AddText(edLog, ex.Message, Color.Red);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
