﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace CNodeBE
{
    public class TransactionLogMaintenance : IDisposable
    {
        public string ConnString
        {
            get { return _connString; }
            set { _connString = value; }
        }

        string _connString { get; set; }
        public TransactionLogMaintenance()
        {

        }

        public TransactionLogMaintenance(string connstring)
        {
            _connString = connstring;
        }
        public void RemoveIncomingLog(string fileToBeRemoved)
        {
            SqlCommand cmdDeleteFromLog = new SqlCommand();
            cmdDeleteFromLog.Connection = new SqlConnection(_connString);
            cmdDeleteFromLog.CommandType = System.Data.CommandType.StoredProcedure;
            cmdDeleteFromLog.CommandText = "DeleteFromIncomingFileLog";
            cmdDeleteFromLog.Parameters.AddWithValue("@FileName", fileToBeRemoved);
            cmdDeleteFromLog.Connection.Open();
            cmdDeleteFromLog.ExecuteNonQuery();
            cmdDeleteFromLog.Connection.Close();
        }

        public void PruneTransactions(string DateToPurge)
        {
            SqlCommand cmdDeleteFromLog = new SqlCommand();
            cmdDeleteFromLog.Connection = new SqlConnection(_connString);
            cmdDeleteFromLog.CommandType = System.Data.CommandType.StoredProcedure;
            cmdDeleteFromLog.CommandText = "DeleteFromIncomingFileLog";
            cmdDeleteFromLog.Parameters.AddWithValue("@DateLimit", DateToPurge);
            cmdDeleteFromLog.Connection.Open();
            cmdDeleteFromLog.ExecuteNonQuery();
            cmdDeleteFromLog.Connection.Close();

        }

        bool disposed = false;
        // Instantiate a SafeHandle instance.
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                handle.Dispose();
                // Free any other managed objects here.
                //
            }

            disposed = true;
        }
    }
}
