﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;
using WinSCP;
using CWShipment = CNodeBE.Shipment;

namespace CNodeBE
{


    public partial class FrmMain : Form
    {

        public System.Timers.Timer tmrMain;
        public SqlConnection sqlConn;
        public Boolean isLocked;
        public int totfilesRx;
        public int totfilesTx;
        public int cwRx;
        public int cwTx;
        public int filesRx;
        public int filesTx;

        public FrmMain()
        {
            InitializeComponent();
            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
        }

        //TODO: Update Order(Excel) to PCFS and put into Library. 
        //TODO: Create EMOTrans CW -> Global XML routine
        //TODO: Update EMOTrans CW -> Global to put into Library. 

        private Guid FindTransactionID(TransReference tr)
        {
            Guid transactionID = new Guid();
            SqlCommand sqlTransaction = new SqlCommand("returnTransactionID", sqlConn);
            sqlTransaction.CommandType = CommandType.StoredProcedure;
            SqlParameter tranId = sqlTransaction.Parameters.Add("@TID", SqlDbType.UniqueIdentifier);
            tranId.Direction = ParameterDirection.Output;
            SqlParameter ref1 = sqlTransaction.Parameters.Add("@Ref1", SqlDbType.NVarChar, 50);
            SqlParameter ref2 = sqlTransaction.Parameters.Add("@Ref2", SqlDbType.NVarChar, 50);
            SqlParameter ref3 = sqlTransaction.Parameters.Add("@Ref3", SqlDbType.NVarChar, 50);
            ref1.Value = tr.Reference1;
            if (String.IsNullOrEmpty(tr.Reference2))
                ref2.Value = DBNull.Value;
            else
                ref2.Value = tr.Reference2;
            if (String.IsNullOrEmpty(tr.Reference3))
                ref3.Value = DBNull.Value;
            else
                ref3.Value = tr.Reference3;
            if (sqlConn.State == ConnectionState.Open)
                sqlConn.Close();
            sqlConn.Open();
            sqlTransaction.ExecuteNonQuery();
            if (tranId.Value == DBNull.Value)
                transactionID = Guid.Empty;
            else
                transactionID = (Guid)tranId.Value;
            return transactionID;
        }

        public void ProcessTodoList()
        {
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Sending and Cleanup Phase Started.");
            SqlDataAdapter daTodo = new SqlDataAdapter("SELECT L_ID, L_FILENAME, L_DATE, P_Recipientid, P_REASONCODE, P_SENDERID, P_EVENTCODE FROM vw_TodoList ORDER BY C_CODE", sqlConn);
            DataSet dsTodo = new DataSet();
            SqlCommand sqlDeleteTodo = new SqlCommand("Delete from Todo where L_ID = @L_ID", sqlConn);
            SqlParameter L_ID = sqlDeleteTodo.Parameters.Add("@L_ID", SqlDbType.UniqueIdentifier);
            try
            {
                sqlConn.Close();

            }
            catch (SqlException)
            {

            }
            catch (Exception ex)
            {
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Process Todo List Failed: " + ex.Message);
                String err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                MailModule.sendMsg("", Globals.AlertsTo, "CTC Node Exception Found: Process TODO List", err);
            }
            try
            {
                if (sqlConn.State != ConnectionState.Closed)
                {
                    sqlConn.Close();
                }
                sqlConn.ConnectionString = Globals.connString();
                sqlConn.Open();
                daTodo.Fill(dsTodo, "Todo");
                sqlConn.Close();
                String ctcMessage = String.Empty;
                String strSuccess = "N";
                foreach (DataRow drTodo in dsTodo.Tables["Todo"].Rows)
                {

                    CustProfileRecord custProfileRecord = GetCustomerProfile(drTodo["P_SENDERID"].ToString(), drTodo["P_recipientID"].ToString(), drTodo["P_reasonCode"].ToString(), drTodo["P_eventCode"].ToString());

                    if (File.Exists(drTodo["L_FILENAME"].ToString()))
                    {
                        switch (custProfileRecord.P_delivery)
                        {
                            case "C":  //CTC Adapter Connection
                                ctcMessage = CTCAdapter.SendMessage(custProfileRecord.P_server, drTodo["L_FILENAME"].ToString(), custProfileRecord.P_Recipientid, custProfileRecord.P_username, custProfileRecord.P_password);

                                if (!ctcMessage.Contains("CTC Adapter Failed:"))
                                {
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + ctcMessage + ". " + "");
                                    Globals.ArchiveFile(Path.Combine(custProfileRecord.C_path, "Archive"), drTodo["L_FILENAME"].ToString());
                                    System.GC.Collect();
                                    System.GC.WaitForPendingFinalizers();
                                    File.Delete(drTodo["L_FILENAME"].ToString());
                                    L_ID.Value = drTodo["L_ID"];
                                    if (sqlConn.State == ConnectionState.Open)
                                    {
                                        sqlConn.Close();

                                    }
                                    sqlConn.Open();
                                    sqlDeleteTodo.ExecuteNonQuery();
                                    cwTx++;

                                    NodeResources.AddLabel(lblCWTx, cwTx.ToString());
                                    totfilesTx++;
                                    //lblTotFilesTx.Text = totfilesTx.ToString();
                                    NodeResources.AddLabel(lblTotFilesTx, totfilesTx.ToString());
                                    ctcMessage = "Send File " + drTodo["L_FILENAME"].ToString() + " via CTC Adapter with result: " + ctcMessage;
                                    strSuccess = "Y";
                                }
                                else
                                {
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Error Found: " + ctcMessage + ". " + "", Color.Red, FontStyle.Bold);
                                    ctcMessage = "Send File " + drTodo["L_FILENAME"].ToString() + " via CTC Adapter with result: " + ctcMessage;
                                    strSuccess = "N";
                                }

                                break;
                            case "F":  //FTP Connection
                                totfilesTx++;
                                NodeResources.AddLabel(lblTotFilesTx, totfilesTx.ToString());
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_name + " Recipient found " + custProfileRecord.P_Recipientid + ".Sending file via FTP" + "");
                                string ftpResult = NodeResources.FtpSend(drTodo["L_FILENAME"].ToString(), custProfileRecord);
                                if (ftpResult.Contains("File Sent Ok"))
                                {
                                    sqlConn.ConnectionString = Globals.connString();
                                    sqlConn.Open();
                                    L_ID.Value = drTodo["L_ID"];
                                    sqlDeleteTodo.ExecuteNonQuery();
                                    sqlConn.Close();
                                    filesTx++;
                                    NodeResources.AddLabel(lblFilesTx, filesTx.ToString());
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "File " + Path.GetFileName(drTodo["L_FILENAME"].ToString()) + " Sent. Response: " + ftpResult);
                                    System.GC.Collect();
                                    System.GC.WaitForPendingFinalizers();
                                    File.Delete(drTodo["L_FILENAME"].ToString());
                                    ctcMessage = "Send File " + drTodo["L_FILENAME"].ToString() + " via FTP with result: " + ftpResult;
                                    strSuccess = "Y";

                                }
                                else
                                {
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Transfer Result is invalid: ");
                                }

                                break;
                            case "E":  //Email Connection
                                {

                                    totfilesTx++;
                                    NodeResources.AddLabel(lblTotFilesTx, totfilesTx.ToString());
                                    break;
                                }
                            case "R": //Receive Only - Check File Exists. Delete Todo. 
                                {
                                    L_ID.Value = drTodo["L_ID"];
                                    if (sqlConn.State == ConnectionState.Open)
                                    {
                                        sqlConn.Close();
                                    }
                                    sqlConn.Open();
                                    sqlDeleteTodo.ExecuteNonQuery();
                                    sqlConn.Close();
                                    break;
                                }
                        }
                    }
                    else
                    {
                        //File no longer found
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Not Found: " + drTodo["L_FILENAME"].ToString() + ". " + "");
                        ctcMessage = " Error File Not Found: " + drTodo["L_FILENAME"].ToString() + " .";
                        strSuccess = "N";
                        L_ID.Value = drTodo["L_ID"];
                        if (sqlConn.State == ConnectionState.Open)
                        {
                            sqlConn.Close();
                        }
                        sqlConn.Open();
                        sqlDeleteTodo.ExecuteNonQuery();
                        // AddTransactionLog((Guid)drTodo["P_ID"], drTodo["C_CODE"].ToString(), drTodo["L_FILENAME"].ToString(), ctcMessage, DateTime.Now, strSuccess);
                    }
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                    sqlConn.Open();
                    if (drTodo["P_DIRECTION"].ToString() == "S")
                    {
                    }
                    else
                    {
                    }
                    AddTransactionLog((Guid)drTodo["P_ID"], drTodo["C_CODE"].ToString(), drTodo["L_FILENAME"].ToString(), ctcMessage, DateTime.Now, strSuccess, drTodo["P_BILLTO"].ToString(), drTodo["P_Recipientid"].ToString());
                }
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Sending and Cleanup Phase Finished.");
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error found.Process TODO Error Found: " + ex.Message, Color.Red, FontStyle.Bold);
                String err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                MailModule.sendMsg("", Globals.AlertsTo, "CTC Node Exception Found", err);
            }
        }

        private void tmrMain_Elapsed(object sender, ElapsedEventArgs e)
        {
            tmrMain.Stop();
            RetrieveEmails();
            RetrieveCustomerFiles();

            ScanXMLFolders();
            // ScanCustomFolders();
            tslMain.Text = "Next Process at " + DateTime.Now.AddMinutes(5);
            System.Windows.Forms.Application.DoEvents();
            if (btnStart.Text == "&Stop")
            {
                tmrMain.Start();
                GetProcErrors();
            }

        }

        public void RetrieveEmails()
        {
            //Create a list of Clients work are lookinng for Email
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Customer Email Queues.");
            SqlDataAdapter sqlReceiveList = new SqlDataAdapter("SELECT C_NAME, C_CODE, C_PATH, P_EMAILADDRESS, P_PORT, CAST(CASE P_SSL WHEN 'Y' THEN 1 ELSE 0 END AS BIT) as SSL, P_FILETYPE, P_ACTIVE, P_DIRECTION, P_DELIVERY, C_ID, P_ID, P_USERNAME, " +
                                                        "P_PASSWORD, P_SERVER, P_SENDEREMAIL, P_SENDERID, P_Recipientid,  P_SUBJECT, P_MSGTYPE, P_MESSAGEDESCR, P_CUSTOMERCOMPANYNAME FROM vw_CustomerProfile where P_DELIVERY='E' and P_DIRECTION='R'", sqlConn);
            try
            {
                if (sqlConn.State == ConnectionState.Open)
                    sqlConn.Close();
            }
            catch (Exception)
            { }
            sqlConn.Open();
            DataSet dsReceiveEmailList = new DataSet();
            sqlReceiveList.Fill(dsReceiveEmailList, "RxList");
            foreach (DataRow dr in dsReceiveEmailList.Tables["RxList"].Rows)
            {
                try
                {
                    List<MsgSummary> msgTodo = MailModule.GetMail(dr["P_SERVER"].ToString(), int.Parse(dr["P_PORT"].ToString()), Boolean.Parse(dr["SSL"].ToString()), dr["P_USERNAME"].ToString(), dr["P_PASSWORD"].ToString());
                    if (msgTodo.Count > 0)
                    {
                        foreach (MsgSummary msg in msgTodo)
                        {
                            try
                            {
                                if (msg.MsgFile != null)
                                {
                                    foreach (MsgAtt att in msg.MsgFile)
                                    {

                                        if (MsgValid(msg, att, dr["P_EMAILADDRESS"].ToString(), dr["P_SENDEREMAIL"].ToString(), dr["P_SUBJECT"].ToString(), att.AttFilename.Extension))
                                        {
                                            // TODO - Must replace the PCFS link. 


                                            try
                                            {
                                                string f = NodeResources.MoveFile(att.AttFilename.FullName, Path.Combine(dr["C_PATH"].ToString(), "Processing"));
                                                AddTransaction((Guid)dr["C_ID"], (Guid)dr["P_ID"], att.AttFilename.FullName, "R", true, null, "");
                                            }
                                            catch (Exception ex)
                                            {
                                                string strEx = ex.GetType().Name;
                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error. Moving Attachment " + att.AttFilename.FullName + " : " + ex.Message, Color.Red);
                                            }

                                            //MoveFile(att.AttFilename.FullName, Path.Combine(dr["C_PATH"].ToString(), "Processing"));

                                        }
                                        else
                                        {
                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Invalid File found in email (" + msg.MsgFrom + "/" + msg.MsgTo + ": " + msg.MsgSubject + " Att:" + Path.GetFileName(att.AttFilename.FullName));
                                            try
                                            {
                                                System.GC.Collect();
                                                System.GC.WaitForPendingFinalizers();
                                                File.Delete(att.AttFilename.FullName);
                                            }
                                            catch (Exception ex)
                                            {
                                                string strEx = ex.GetType().Name;
                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error. Deleting Invalid file : " + ex.Message, Color.Red);
                                                MailModule.sendMsg("", Globals.AlertsTo, "Error Deleting Invalid file", "Error Retrieving Email for " + dr["P_USERNAME"].ToString() + ": " + ex.Message);
                                            }
                                        }
                                    }
                                }

                            }
                            catch (Exception ex)
                            {
                                string strEx = ex.GetType().Name;
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error. Saving Attachments " + dr["P_USERNAME"].ToString() + ": " + ex.Message, Color.Red);
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name;
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error. Retrieving Email for " + dr["P_USERNAME"].ToString() + ": " + ex.Message, Color.Red);
                    MailModule.sendMsg("", Globals.AlertsTo, "Error Retrieving Email for " + dr["P_USERNAME"].ToString(), "Error Retrieving Email for " + dr["P_USERNAME"].ToString() + ": " + ex.Message);
                }

            }
        }

        //private string MoveFile(string att, string folder)
        //{

        //    int icount = 1;

        //    string filename = Path.GetFileNameWithoutExtension(att);
        //    string ext = Path.GetExtension(att);
        //    string newfile = Path.Combine(folder, Path.GetFileName(att));
        //    while (File.Exists(newfile))
        //    {
        //        newfile = Path.Combine(folder, filename + icount + ext);
        //        icount++;

        //    }

        //    try
        //    {
        //        File.Move(att, Path.Combine(folder,newfile));
        //    }
        //    catch (Exception ex)
        //    {
        //        string strEx = ex.GetType().Name;
        //        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error found while moving " + filename + ". Error details: " + ex.Message, Color.Red, FontStyle.Bold);
        //    }
        //    return newfile;
        //}


        public TransReference MoveToProcessing(string xmlfile, string folder, string Ref1, string Ref2)
        {
            TransReference tRef = new TransReference();
            tRef.Ref1Type = RefType.Custom;
            tRef.Reference1 = Ref1;
            tRef.Ref2Type = RefType.Operation;
            tRef.Reference2 = Ref2;
            tRef.Ref3Type = RefType.Transaction;
            string f = string.Empty;
            f = NodeResources.MoveFile(xmlfile, folder);
            tRef.Reference3 = f;
            if (!f.StartsWith("Warning"))
            {

            }
            return tRef;
        }
        private ProcessResult ProcessTransportImport(string xmlfile)
        {
            SqlConnection sqlTransConn = new SqlConnection();
            sqlTransConn.ConnectionString = Globals.connTransString();
            ProcessResult result = new ProcessResult();
            FileInfo processingFile = new FileInfo(xmlfile);
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            UniversalInterchange cwFile = new UniversalInterchange();
            using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
                cwFile = (UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            result.FileName = xmlfile;
            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            String eventCode = String.Empty;
            string transportMode = string.Empty;

            int legNo = 0;
            TransReference trRef = new TransReference();
            List<DataSource> dscoll = new List<DataSource>();
            DataContext dc = new DataContext();
            dc = cwFile.Body.BodyField.Shipment.DataContext;
            List<DataSource> dsColl = new List<DataSource>();
            dsColl = dc.DataSourceCollection.ToList();
            string jobNo = string.Empty;
            bool tJob = false;
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Cargowise Transport Job Found. Importing");

            foreach (DataSource ds in dsColl)
            {
                switch (ds.Type)
                {
                    case "LocalTransport":
                        jobNo = ds.Key;
                        tJob = true;
                        break;
                }
            }
            if (tJob)
            {
                try
                {
                    UniversalInterchangeHeader header = new UniversalInterchangeHeader();
                    header = cwFile.Header;
                    senderID = header.SenderID;


                    SqlCommand sqlValCust = new SqlCommand("ISVALIDCustomer", sqlTransConn);
                    sqlValCust.CommandType = CommandType.StoredProcedure;
                    SqlParameter oCode = sqlValCust.Parameters.Add("@O_CODE", SqlDbType.NChar, 10);
                    SqlParameter isValid = sqlValCust.Parameters.Add("@ISVALID", SqlDbType.Bit);
                    SqlParameter oid = sqlValCust.Parameters.Add("@O_ID", SqlDbType.UniqueIdentifier);
                    isValid.Direction = ParameterDirection.Output;
                    oid.Direction = ParameterDirection.Output;
                    oCode.Value = senderID;
                    if (sqlTransConn.State == ConnectionState.Open)
                    {
                        sqlTransConn.Close();
                    }
                    sqlTransConn.Open();
                    sqlValCust.ExecuteNonQuery();

                    if ((bool)isValid.Value)
                    {
                        CWShipment shipment = new CWShipment();
                        shipment = cwFile.Body.BodyField.Shipment;
                        OrganizationAddress legOrg = new OrganizationAddress();
                        int delPack = 0;
                        string delUOM = string.Empty;
                        string dropMode = string.Empty;
                        string jobType = string.Empty;
                        string legType = string.Empty;
                        string trnTemplate = string.Empty;
                        string containerNumber = string.Empty;
                        string containerMode = "N/A";
                        string estDelDate = string.Empty;
                        int totPcs = 0;
                        decimal totWgt = 0;
                        decimal totVol = 0;
                        string shipRef = string.Empty;
                        string haz = "N";
                        transportMode = shipment.TransportMode.Code;
                        legType = shipment.TransportBookingDirection.Code;
                        trnTemplate = shipment.LocalTransportJobType.Code.Substring(0, 2);
                        switch (trnTemplate)
                        {
                            case "DR": //Destination Delivery 
                                jobType = "DELIVERY";
                                break;
                            case "OR": //Origin Pickup
                                jobType = "PICKUP";
                                break;
                            case "ES": //Export Sea Pickup
                                jobType = "PICKUP";
                                break;
                            case "EA": //Export Air Pickup
                                jobType = "PICKUP";
                                break;
                            case "IS": //Import Sea Delivery
                                jobType = "DELIVERY";
                                break;
                            case "IA": //Import Air Delivery
                                jobType = "DELIVERY";
                                break;

                        }
                        delPack = shipment.OuterPacks;
                        dropMode = shipment.LocalTransportEquipmentNeeded.Code;
                        delUOM = shipment.OuterPacksPackageType.Code;
                        totPcs = shipment.TotalNoOfPieces;
                        totWgt = shipment.TotalWeight;
                        totVol = shipment.TotalVolume;
                        if (shipment.IsHazardous)
                        {
                            haz = "Y";
                        }
                        else
                        {

                        }
                        containerMode = shipment.ContainerMode.Code;
                        //shipRef = GetRef(shp.AdditionalReferenceCollection.AdditionalReference.ToList(), "BPR");
                        SqlCommand addJob = new SqlCommand("CreateJob", sqlTransConn);
                        addJob.CommandType = CommandType.StoredProcedure;
                        addJob.Parameters.AddWithValue("@O_CUST", oid.Value);
                        addJob.Parameters.AddWithValue("@items", delPack);
                        addJob.Parameters.AddWithValue("@TJobno", jobNo);
                        addJob.Parameters.AddWithValue("@DropMode", dropMode);
                        addJob.Parameters.AddWithValue("@TotalPcs", totPcs);
                        addJob.Parameters.AddWithValue("@TotalWgt", totWgt);
                        addJob.Parameters.AddWithValue("@TotalVol", totVol);
                        addJob.Parameters.AddWithValue("@JobType", jobType);
                        addJob.Parameters.AddWithValue("@DelDate", estDelDate);
                        addJob.Parameters.AddWithValue("@CONTMODE", containerMode);
                        addJob.Parameters.AddWithValue("@REFERENCE", shipRef);
                        addJob.Parameters.AddWithValue("@HAZARDOUS", haz);
                        addJob.Parameters.AddWithValue("@STATUS", "Pending");
                        addJob.Parameters.AddWithValue("@uOM", delUOM);
                        SqlParameter jid = addJob.Parameters.Add("@ID", SqlDbType.UniqueIdentifier);
                        jid.Direction = ParameterDirection.Output;
                        if (sqlTransConn.State == ConnectionState.Open)
                        {
                            sqlTransConn.Close();
                        }
                        sqlTransConn.Open();
                        addJob.ExecuteNonQuery();
                        if (shipment.ContainerCollection != null)
                        {

                        }
                        List<LegAddress> instructionAddress = new List<LegAddress>();
                        if (shipment.InstructionCollection != null)
                        {
                            foreach (ShipmentInstruction instruction in shipment.InstructionCollection)
                            {
                                LegAddress ca = new LegAddress();
                                dropMode = string.Empty;
                                dropMode = instruction.DropMode.Code;
                                legNo = instruction.Sequence;
                                ca.LegType = instruction.Type.Code;
                                ca.AddressId = GetOrgAddress(instruction.Address, (Guid)oid.Value);
                                ca.DropMode = instruction.DropMode.Code;
                                ca.LegNo = instruction.Sequence;
                                if (instruction.InstructionPackingLineLinkCollection != null)
                                {
                                    foreach (ShipmentInstructionInstructionPackingLineLink pl in instruction.InstructionPackingLineLinkCollection)
                                    {
                                        ca.PackSet = GetPackingSetFromLink(cwFile, pl.PackingLineLink);
                                        foreach (Confirmation conf in pl.ConfirmationCollection)
                                        {
                                            ca.DateType = conf.DateDescription;
                                            ca.LegDate = conf.EstimatedDate;
                                            instructionAddress.Add(ca);
                                        }
                                    }
                                }
                                if (instruction.InstructionContainerLinkCollection != null)
                                {
                                    foreach (ShipmentInstructionInstructionContainerLink cl in instruction.InstructionContainerLinkCollection)
                                    {

                                        ca.ContainerNo = GetContainerNoFromLink(cwFile, cl.ContainerLink);

                                        foreach (Confirmation conf in cl.ConfirmationCollection)
                                        {
                                            ca.DateType = conf.DateDescription;
                                            ca.LegDate = conf.EstimatedDate;
                                            instructionAddress.Add(ca);
                                        }
                                    }
                                }



                            }
                            // TODO - Foreach in Instruction Address. Loop and add Transport Legs. 
                            foreach (LegAddress la in instructionAddress)
                            {
                                SqlCommand addLeg = new SqlCommand("AddTransportLeg", sqlTransConn);
                                addLeg.CommandType = CommandType.StoredProcedure;
                                addLeg.Parameters.AddWithValue("@JID", jid.Value);
                                addLeg.Parameters.AddWithValue("@AID", la.AddressId);
                                addLeg.Parameters.AddWithValue("@TJobno", jobNo);
                                addLeg.Parameters.AddWithValue("@DropMode", la.DropMode);
                                if (la.PackSet != null)
                                {
                                    addLeg.Parameters.AddWithValue("@Pcs", la.PackSet.Pcs);
                                    addLeg.Parameters.AddWithValue("@Wgt", la.PackSet.Weight);
                                    addLeg.Parameters.AddWithValue("@Vol", la.PackSet.Volume);
                                    addLeg.Parameters.AddWithValue("@uOM", la.PackSet.PackType);
                                }
                                addLeg.Parameters.AddWithValue("@LegNo", la.LegNo);
                                addLeg.Parameters.AddWithValue("@LegType", la.LegType);
                                addLeg.Parameters.AddWithValue("@ContMode", "");
                                addLeg.Parameters.AddWithValue("@ContainerNo", la.ContainerNo);
                                addLeg.Parameters.AddWithValue("@RequiredDate", la.LegDate);

                                if (legOrg == null)
                                {
                                    continue;
                                }
                                SqlParameter Legno = addJob.Parameters.AddWithValue("@LegNo", legNo);
                                SqlParameter legStatus = addJob.Parameters.AddWithValue("@STATUS", "Created");
                                SqlParameter l_id = addJob.Parameters.Add("@ID", SqlDbType.UniqueIdentifier);
                                l_id.Direction = ParameterDirection.Output;
                                if (la.AddressId != Guid.Empty)
                                {
                                    if (sqlTransConn.State == ConnectionState.Open)
                                    {
                                        sqlTransConn.Close();
                                    }
                                    sqlTransConn.Open();
                                    addLeg.ExecuteNonQuery();
                                }

                            }


                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Transport Job (" + jobNo + ") has been imported.", Color.DarkGreen);
                        }

                        result.FileName = xmlfile;
                        result.Processed = true;


                    }
                    else
                    {
                        // Log error
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Transport Customer " + senderID + " not valid. Please confirm", Color.DarkGreen);
                        result.FileName = xmlfile;
                        result.Processed = false;
                    }

                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name;
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + "Exception found in file " + Path.GetFileName(xmlfile) + ". Error Message " + ex.Message, Color.Red, FontStyle.Bold);
                    string f = NodeResources.MoveFile(xmlfile, Globals.FailLoc);
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlfile = f; }
                    //xmlfile = MoveFile(xmlfile, Globals.FailLoc);
                }


            }
            else
            {

            }
            return result;

        }

        private PackingSet GetPackingSetFromLink(UniversalInterchange cwFile, int packingLineLink)
        {
            PackingSet result = new PackingSet();

            foreach (PackingLine pl in cwFile.Body.BodyField.Shipment.PackingLineCollection)
            {
                if (pl.Link == packingLineLink)
                {
                    result.Pcs = pl.PackQty;
                    result.Weight = pl.Weight;
                    result.Volume = pl.Volume;
                    result.PackType = pl.PackType.Code;
                }
            }
            return result;

        }

        private string GetContainerNoFromLink(UniversalInterchange cwFile, int containerLink)
        {
            string result = string.Empty;
            foreach (Container cont in cwFile.Body.BodyField.Shipment.ContainerCollection.Container)
            {
                if (cont.Link == containerLink)
                    result = cont.ContainerNumber;
            }
            return result;
        }

        public Boolean MsgValid(MsgSummary MsgtoValid, MsgAtt Attachment, String Recipient, String SenderEmail, String Subject, String FileType)
        {
            String strValid;
            strValid = "";
            Boolean bValid = false;
            SqlCommand sqlVal = new SqlCommand("ValidateEmailMessage", sqlConn);
            sqlVal.CommandType = CommandType.StoredProcedure;
            SqlParameter retProfileId = sqlVal.Parameters.Add("@P_ID", SqlDbType.UniqueIdentifier);
            retProfileId.Direction = ParameterDirection.Output;
            SqlParameter parSender = sqlVal.Parameters.Add("@P_SENDEREMAIL", SqlDbType.VarChar, 100);
            if (!String.IsNullOrEmpty(SenderEmail))
                parSender.Value = SenderEmail;
            SqlParameter parSubject = sqlVal.Parameters.Add("@P_SUBJECT", SqlDbType.VarChar, 100);
            if (!String.IsNullOrEmpty(Subject))
                parSubject.Value = Subject;
            SqlParameter parEmail = sqlVal.Parameters.Add("@P_EMAILADDRESS", SqlDbType.VarChar, 100);
            if (!String.IsNullOrEmpty(Recipient))
                parEmail.Value = Recipient;
            try
            {
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.Open();
                sqlVal.ExecuteNonQuery();
                strValid = retProfileId.Value.ToString();
                if (String.IsNullOrEmpty(strValid))
                {
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Unable to match Receive profile:  Sender:" + SenderEmail + " Recipient:" + Recipient + " Subject:" + Subject);
                    bValid = false;
                }
                else
                {
                    SqlCommand sqlFileType = new SqlCommand("SELECT P_FILETYPE from Profile where P_ID = '" + strValid + "'", sqlConn);
                    try
                    {
                        if (sqlConn.State == ConnectionState.Open)
                        {
                            sqlConn.Close();
                        }
                        sqlConn.Open();
                    }
                    catch (Exception)
                    {
                        bValid = false;
                    }
                    SqlDataReader dr;
                    dr = sqlFileType.ExecuteReader(CommandBehavior.CloseConnection);
                    if (dr.HasRows)
                    {
                        dr.Read();
                        int intPos = FileType.IndexOf(dr["P_FILETYPE"].ToString().ToLower().Trim());
                        if (intPos > -1)
                            bValid = true;
                        else
                            bValid = false;
                    }

                }

            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error Locating Receive profile:  " + ex.Message);
                bValid = false;
            }

            return bValid;

        }

        public void RetrieveCustomerFiles()
        {

            SqlDataAdapter ftpPollProfiles = new SqlDataAdapter("Select C_Code, C_ID, C_PATH, P_ID, P_DESCRIPTION, P_Server, CAST(CASE P_SSL WHEN 'Y' THEN 1 ELSE 0 END AS BIT) as SSL, P_SUBJECT as P_KEYPATH, P_EMAILADDRESS as P_FINGERPRINT, P_DELIVERY, P_UserName, P_Password, P_Recipientid, P_Port, P_Path, P_FILETYPE, P_Direction from VW_CustomerProfile where P_ACTIVE='Y'and ( P_Delivery ='F' or P_DELIVERY = 'P' ) Order by P_Server", sqlConn);
            //  ftpPollProfiles.SelectCommand.CommandType = CommandType.Text;
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            sqlConn.ConnectionString = Globals.connString();
            sqlConn.Open();
            DataSet dsFtp = new DataSet();
            ftpPollProfiles.Fill(dsFtp);
            string filesPath = string.Empty;
            if (tslMode.Text == "Production")
            {
                filesPath = Globals.XMLLoc;
            }
            else
            {
                filesPath = Globals.TestingLoc;
            }
            foreach (DataRow dr in dsFtp.Tables[0].Rows)
            {

                if (dr["P_DELIVERY"].ToString() == "F" && dr["P_DIRECTION"].ToString() == "R")
                {
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Beginning FTP Polling Process for " + dr["P_DESCRIPTION"].ToString());
                    SessionOptions sessionOptions = new SessionOptions
                    {
                        HostName = dr["P_SERVER"].ToString(),
                        UserName = dr["P_USERNAME"].ToString(),
                        Password = dr["P_PASSWORD"].ToString()
                    };

                    if ((bool)dr["SSL"])
                    {
                        sessionOptions.Protocol = Protocol.Sftp;
                        sessionOptions.SshPrivateKeyPath = dr["P_KEYPATH"].ToString();
                        sessionOptions.SshHostKeyFingerprint = dr["P_FINGERPRINT"].ToString();
                    }
                    else
                    {
                        sessionOptions.Protocol = Protocol.Ftp;
                    }
                    sessionOptions.PortNumber = int.Parse(dr["P_PORT"].ToString().Trim());
                    using (Session session = new Session())
                    {
                        session.Open(sessionOptions);
                        TransferOptions trOptions = new TransferOptions();
                        trOptions.TransferMode = TransferMode.Automatic;
                        //trOptions.FileMask = "*" + dr["P_FILETYPE"].ToString();
                        trOptions.FileMask = "|*/";
                        trOptions.OverwriteMode = OverwriteMode.Overwrite;
                        trOptions.ResumeSupport.State = TransferResumeSupportState.Off;
                        TransferOperationResult trResult;
                        string remoteFolder = string.Format("/{0}/*", dr["P_PATH"].ToString().Trim());
                        trResult = session.GetFiles(remoteFolder, Path.Combine(dr["C_PATH"].ToString(), "Processing") + "\\", true, trOptions);
                        trResult.Check();
                        if (trResult.Transfers.Count > 0)
                        {
                            foreach (TransferEventArgs transfer in trResult.Transfers)
                            {
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Downloaded " + transfer.FileName + " -> " + filesPath, Color.Green);
                                AddTransaction((Guid)dr["C_ID"], (Guid)dr["P_ID"], transfer.FileName, "R", true, null, "");
                            }
                        }
                        if (trResult.Failures.Count > 0)
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + trResult.Failures);
                        }



                    }


                }
                if (dr["P_DELIVERY"].ToString() == "P" && dr["P_DIRECTION"].ToString() == "R")
                {
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Scanning Inbound FTP Folders for  " + dr["P_DESCRIPTION"].ToString());
                    DirectoryInfo diFtpReceive = new DirectoryInfo(dr["P_PATH"].ToString().Trim());
                    foreach (var fileName in diFtpReceive.GetFiles("*." + dr["P_FILETYPE"].ToString().Trim()))
                    {

                        ProcessResult thisResult = new ProcessResult();
                        string cwXML = string.Empty;
                        //cwXML = MoveFile(fileName.FullName, Path.Combine(dr["C_PATH"].ToString(), "Processing"));
                        string f = NodeResources.MoveFile(fileName.FullName, Path.Combine(dr["C_PATH"].ToString(), "Processing"));

                        if (f.StartsWith("Warning"))
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                        }
                        else
                        {
                            cwXML = f;
                            try
                            {
                                thisResult = ProcessCustomFiles(cwXML, dr["C_CODE"].ToString());
                                if (thisResult.Processed)
                                {
                                    System.GC.Collect();
                                    System.GC.WaitForPendingFinalizers();
                                    File.Delete(cwXML);
                                }
                            }
                            catch (IOException exIO)
                            {
                                string strEx = exIO.GetType().Name;
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + exIO + " Error while downloading. Error was " + exIO.Message, Color.Red);
                                String err = exIO.Message + Environment.NewLine + exIO.Source + exIO.StackTrace;
                                MailModule.sendMsg("", Globals.AlertsTo, "CTC Node Exception Found: Retrieving Customer Files (FileMove)", err);
                            }
                        }



                    }
                }
            }
        }


        public void ScanXMLFolders()
        {
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Scanning for All XML Files...");
            string sFilesPath = string.Empty;
            if (tslMode.Text == "Production")
            {
                sFilesPath = Globals.XMLLoc;
            }
            else
            {
                sFilesPath = Globals.TestingLoc;
            }
            DirectoryInfo cargowiseXMLFolder = new DirectoryInfo(sFilesPath);
            tmrMain.Stop();
            if (Directory.Exists(sFilesPath))
            {
                foreach (var xmlFile in cargowiseXMLFolder.GetFiles("*.XML"))
                {
                    if (CheckIfStopped())
                    {
                        return;
                    }
                    else
                    {
                        try
                        {
                            ProcessCargowiseXMLFile(xmlFile.FullName);
                        }
                        catch (IOException exIO)
                        {
                            if (exIO.Message.Contains("Could not find file"))
                            {
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " WARNING. File Name no longer found. Skipping");
                            }
                        }
                        catch (Exception ex)
                        {
                            string strEx = ex.GetType().Name;
                            MailModule.sendMsg("", Globals.AlertsTo, "Error while Scanning Folders", "There was an error during the Scanprocess. Error message is : " + ex.Message);

                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Please note that the XML Folder does not exist. Please check setup", "Error locating Folder", MessageBoxButtons.OK, MessageBoxIcon.Error);
                FrmSettings FormSettings = new FrmSettings();
                FormSettings.ShowDialog();
                DialogResult dr = FormSettings.DialogResult;
                if (dr == DialogResult.OK)
                {
                    loadSettings();
                }
            }
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " No more XML Files found.");
            ProcessTodoList();
            tmrMain.Start();
        }

        private ProcessResult ProcessCustomFiles(String xmlFile, string custCode)
        {
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            thisResult.FileName = xmlFile;
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File " + Path.GetFileName(xmlFile) + " Is not a Cargowise File. Checking for Against Customer.");
            FileInfo processingFile = new FileInfo(xmlFile);
            filesRx++;
            NodeResources.AddLabel(lblFilesRx, filesRx.ToString());
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            String msgDir = "";
            TransReference trRef = new TransReference();
            string archiveFile;
            string recipientID = "";
            string senderID = "";
            string custPath = Globals.FailLoc;
            string filesPath = string.Empty;

            if (tslMode.Text == "Production")
            {
                filesPath = Globals.XMLLoc;
            }
            else
            {
                filesPath = Globals.TestingLoc;
            }
            if (processingFile.Extension.ToUpper() == ".XML")
            {
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " New XML file found " + Path.GetFileName(processingFile.FullName) + ". Processing" + "");
                SqlDataAdapter daXML = new SqlDataAdapter("Select C_ID, P_ID, C_NAME, C_IS_ACTIVE, C_ON_HOLD, C_PATH, P_EVENTCODE, P_LIBNAME," +
                                                            "C_FTP_CLIENT, P_REASONCODE, P_SERVER, P_USERNAME, P_DELIVERY, P_METHOD, P_PARAMLIST," +
                                                            "P_PATH, P_PASSWORD, P_MSGTYPE, P_Recipientid, P_SENDERID, P_EMAILADDRESS, P_SUBJECT," +
                                                            "P_DESCRIPTION, P_NOTIFY, P_BILLTO, P_PORT, P_DIRECTION, C_CODE, P_XSD, CAST(CASE P_DTS WHEN 'Y' THEN 1 ELSE 0 END AS BIT) as DTS, P_ACTIVE, CAST(CASE P_SSL WHEN 'Y' THEN 1 ELSE 0 END AS BIT) as SSL " +
                                                            "from VW_CustomerProfile where P_XSD is not null and P_ACTIVE='Y' and (@C_CODE is Null or C_CODE = @C_CODE)", sqlConn);
                SqlParameter cCode = daXML.SelectCommand.Parameters.Add("@C_CODE", SqlDbType.VarChar, 15);
                if (string.IsNullOrEmpty(custCode))
                {
                    cCode.Value = DBNull.Value;
                }
                else
                {
                    cCode.Value = custCode;

                }
                DataSet dsXML = new DataSet();
                daXML.Fill(dsXML, "XMLFile");
                Boolean processed = false;

                //   FileStream fsFile = new FileStream(xmlFile, FileMode.Open, FileAccess.ReadWrite);
                //   StreamReader sr = new StreamReader(fsFile);
                while (!processed)
                {
                    foreach (DataRow dr in dsXML.Tables[0].Rows)
                    {

                        try
                        {
                            CustProfileRecord cust = new CustProfileRecord
                            {
                                C_code = dr["C_CODE"].ToString(),
                                C_id = (Guid)dr["C_ID"],
                                C_ftp_client = dr["c_FTP_CLIENT"].ToString(),
                                C_is_active = dr["C_IS_ACTIVE"].ToString(),
                                C_name = dr["C_NAME"].ToString(),
                                P_Recipientid = dr["P_Recipientid"].ToString(),
                                P_Senderid = dr["P_SENDERID"].ToString(),
                                C_on_hold = dr["C_ON_HOLD"].ToString(),
                                C_path = dr["c_PATH"].ToString(),
                                P_id = (Guid)dr["p_id"],
                                P_delivery = dr["P_DELIVERY"].ToString(),
                                P_description = dr["P_DESCRIPTION"].ToString(),
                                P_Direction = dr["P_DIRECTION"].ToString(),
                                P_DTS = (bool)dr["DTS"],
                                P_Method = dr["P_METHOD"].ToString(),
                                P_password = dr["P_PASSWORD"].ToString(),
                                P_Params = dr["P_PARAMLIST"].ToString(),
                                P_path = dr["P_PATH"].ToString(),
                                P_port = dr["P_PORT"].ToString(),
                                P_server = dr["P_SERVER"].ToString(),
                                P_username = dr["P_USERNAME"].ToString(),
                                P_Subject = dr["P_SUBJECT"].ToString(),
                                P_ssl = (bool)dr["SSL"],
                                P_XSD = dr["P_XSD"].ToString(),
                                P_EmailAddress = dr["P_EMAILADDRESS"].ToString(),
                                P_NotifyEmail = dr["P_NOTIFY"].ToString(),
                                P_Library = dr["P_LIBNAME"].ToString()

                            };


                            string schemaFile = Path.Combine(cust.C_path, "Lib", cust.P_XSD);
                            XmlTextReader sXsd = new XmlTextReader(schemaFile);
                            XmlSchema schema = new XmlSchema();
                            string _byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                            schema = XmlSchema.Read(sXsd, CustomValidateHandler.HandlerErrors);
                            XmlReaderSettings settings = new XmlReaderSettings();
                            settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                            settings.ValidationType = ValidationType.Schema;
                            settings.Schemas.Add(schema);
                            settings.ValidationEventHandler += (o, ea) =>
                            {
                                throw new XmlSchemaValidationException(
                                    string.Format("Schema Not Found: {0}",
                                                  ea.Message),
                                    ea.Exception);
                            };
                            //XmlSchemaSet schemas = new XmlSchemaSet();
                            //schemas.Add(schema);
                            //XDocument doc = XDocument.Load(xmlFile);
                            //string msg = "";
                            //doc.Validate(schemas, (o, e) =>
                            //{
                            //    msg += e.Message + Environment.NewLine;
                            //});

                            using (var stream = new FileStream(xmlFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                            using (var cusXML = XmlReader.Create(stream, settings))
                            {
                                while (cusXML.Read())
                                {

                                }
                                stream.Close();
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Schema File Found (" + schemaFile + "). Continue processing Profile.");
                                stream.Dispose();
                                cusXML.Close();
                                cusXML.Dispose();
                                string custMethod = cust.P_Method;
                                string custParams = cust.P_Params;

                                archiveFile = Globals.ArchiveFile(cust.C_path + "\\Archive", xmlFile);
                                if (!String.IsNullOrEmpty(custMethod))
                                {
                                    Type custType = this.GetType();
                                    MethodInfo custMethodToRun = custType.GetMethod(custMethod);
                                    try
                                    {
                                        //       sr.Close();
                                        //       sr.Dispose();
                                    }
                                    catch (Exception)
                                    {

                                    }

                                    if (custMethod == "MoveToProcessing")
                                    {
                                        var varResult = custMethodToRun.Invoke(this, new Object[] { xmlFile, Path.Combine(cust.C_path, "Processing"), cust.P_XSD, cust.P_description });
                                        if (varResult != null)
                                        {
                                            AddTransaction(cust.C_id, cust.P_id, xmlFile, cust.P_Direction, true, (TransReference)varResult, archiveFile);
                                            processed = true;
                                            thisResult.FileName = null;
                                            thisResult.Processed = true;


                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(cust.P_Library))
                                        {
                                            String libtoLoad = Path.Combine(cust.C_path + "\\Lib", cust.P_Library);
                                            Assembly custLib = Assembly.LoadFile(libtoLoad);
                                            Type convertMethod = custLib.GetTypes().ToList().FirstOrDefault(f => f.Name == "ConvertToCW");
                                            dynamic CustomerXMLLibrary = Activator.CreateInstance(convertMethod);
                                            var conversionResult = CustomerXMLLibrary.DoConvert(xmlFile, cust.P_Senderid, cust.P_Recipientid, Globals.XMLLoc);
                                            string tr;
                                            tr = conversionResult.ToString();
                                            TransReference ts = new TransReference();
                                            ts = ArrtoRef(tr.Split('*'));

                                            if (ts.Reference3 == "Conversion Complete")
                                            {
                                                AddTransaction(cust.C_id, cust.P_id, xmlFile, msgDir, true, ts, archiveFile);
                                                processed = true;
                                                thisResult.Processed = true;
                                            }

                                            else
                                            {
                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Conversion failed :" + ts.Reference3);
                                                NodeResources.MoveFile(xmlFile, Globals.FailLoc);
                                                processed = true;
                                                thisResult.Processed = true;


                                            }
                                        }
                                        else
                                        {
                                            var varResult = custMethodToRun.Invoke(this, new Object[] { xmlFile });
                                            if (varResult != null)
                                            {
                                                AddTransaction(cust.C_id, cust.P_id, xmlFile, msgDir, true, (TransReference)varResult, archiveFile);
                                                processed = true;
                                                thisResult.Processed = true;
                                            }
                                        }


                                        //   var conversionResult = libTest.DoConvert(xmlFile, dr["P_Recipientid"].ToString(), dr["C_CODE"].ToString(), Path.Combine(dr["C_PATH"].ToString() + "\\Processing\\"));


                                    }



                                    //AddTransactionLog((Guid)dr["P_ID"], dr["C_CODE"].ToString(), xmlFile, "Custom Conversion to Universal XML:" + conversionResult, DateTime.Now, "Y", dr["P_BILLTO"].ToString(), dr["P_Recipientid"].ToString());
                                }
                                else
                                {
                                    string strSuccess = "";
                                    string result = "";
                                    string ftpSend = string.Empty;
                                    switch (cust.P_Direction)
                                    {
                                        case "S":
                                            switch (cust.P_delivery)
                                            {
                                                case ("R"):
                                                    {
                                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + cust.C_name + " Recipient found " + recipientID + ". Moving to Folder");
                                                        AddTransactionLog(cust.P_id, senderID, xmlFile, "Customer " + cust.C_name + " Recipient found " + recipientID + ". Moving to Folder to retrieval", DateTime.Now, "Y", cust.P_BillTo, recipientID);
                                                        // AddTransaction(cust.C_id, cust.P_id, xmlFile, msgDir, true, (TransReference)varResult, archiveFile);
                                                        NodeResources.MoveFile(xmlFile, Path.Combine(cust.C_path, "Processing"));
                                                        thisResult.Processed = true;
                                                        thisResult.FileName = null;
                                                        break;
                                                    }
                                                case ("D"):
                                                    {
                                                        if (File.Exists(xmlFile))
                                                        {
                                                            try
                                                            {
                                                                //CTCFtp ctcftp = new CTCFtp("ftp://"+cust.P_server.Trim(), cust.P_username, cust.P_password);
                                                                //ctcftp.upload(cust.P_path + "/" + Path.GetFileName(xmlFile), xmlFile);

                                                                SessionOptions sessionOptions = new SessionOptions
                                                                {
                                                                    HostName = cust.P_server.Trim(),
                                                                    PortNumber = int.Parse(cust.P_port.Trim()),
                                                                    UserName = cust.P_username.Trim(),
                                                                    Password = cust.P_password.Trim()
                                                                };
                                                                if (cust.P_ssl)
                                                                {
                                                                    sessionOptions.Protocol = Protocol.Sftp;
                                                                    sessionOptions.SshPrivateKeyPath = Path.Combine(cust.C_path, "Lib", cust.P_Subject);
                                                                    sessionOptions.SshHostKeyFingerprint = cust.P_EmailAddress;


                                                                }
                                                                else
                                                                {
                                                                    sessionOptions.Protocol = Protocol.Ftp;
                                                                }
                                                                //try
                                                                //{
                                                                TransferOperationResult tr;
                                                                using (Session session = new Session())
                                                                {
                                                                    session.Open(sessionOptions);
                                                                    if (session.Opened)
                                                                    {
                                                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Connected to " + cust.P_server + ". Sending file  :" + xmlFile + Environment.NewLine + " TO " + sessionOptions.HostName + "/" + cust.P_path + "/" + Path.GetFileName(xmlFile));
                                                                        TransferOptions tOptions = new TransferOptions();
                                                                        tOptions.OverwriteMode = OverwriteMode.Overwrite;
                                                                        tr = session.PutFiles(xmlFile, "/" + cust.P_path + "/" + Path.GetFileName(xmlFile), true, tOptions);
                                                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Results: ");
                                                                        if (tr.IsSuccess)
                                                                        {
                                                                            if (sqlConn.State == ConnectionState.Open)
                                                                                sqlConn.Close();
                                                                            sqlConn.Open();
                                                                            //remTodo.ExecuteNonQuery();
                                                                            sqlConn.Close();
                                                                            filesTx++;
                                                                        }
                                                                        else
                                                                        {
                                                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Transfer Result is invalid: ");
                                                                        }
                                                                        NodeResources.AddLabel(lblFilesTx, filesTx.ToString());
                                                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "File " + Path.GetFileName(xmlFile) + " Sent. Response: " + ftpSend);
                                                                        AddTransaction(cust.C_id, cust.P_id, Path.GetFileName(xmlFile), msgDir, false, trRef, archiveFile);
                                                                        strSuccess = "Y";
                                                                        result = "File Sent Ok";
                                                                        try
                                                                        {
                                                                            System.GC.Collect();
                                                                            System.GC.WaitForPendingFinalizers();
                                                                            File.Delete(xmlFile);
                                                                        }
                                                                        catch (Exception ex)
                                                                        {
                                                                            result = "Failed: " + ex.Message;
                                                                        }



                                                                    }
                                                                    else
                                                                    {

                                                                    }
                                                                }

                                                            }
                                                            catch (InvalidOperationException ex)
                                                            {
                                                                string strEx = ex.Message;
                                                            }
                                                            catch (ArgumentException ex)
                                                            {
                                                                string strEx = ex.Message;
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                // remID.Value = AddTodo((Guid)cust.P_id, xmlFile, ftpSend, DateTime.Now);
                                                                ProcessingErrors procerror = new ProcessingErrors();
                                                                procerror.ErrorCode.Code = NodeError.e104;
                                                                procerror.SenderId = senderID;
                                                                procerror.RecipientId = recipientID;
                                                                procerror.ErrorCode.Description = "Cargowise FTP Send Error Found: " + ex.Message + ". ";
                                                                procerror.FileName = xmlFile;
                                                                procerror.ProcId = cust.P_id;
                                                                AddProcessingError(procerror);
                                                                thisResult.FolderLoc = cust.C_path + "\\Processing\\";
                                                                thisResult.Processed = false;
                                                                thisResult.FileName = xmlFile;
                                                                //AddTransactionLog(Guid.Empty, senderID, xmlFile, "Customer Found but no Profile found. Move to Failed Location for retry.", DateTime.Now, "Y", senderID, recipientID);
                                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Cargowise FTP Send Error Found: " + ex.Message + ". " + "");
                                                                AddTransactionLog(cust.P_id, senderID, xmlFile, "File " + Path.GetFileName(xmlFile) + " sent via FTP with the Result: " + ftpSend, DateTime.Now, strSuccess, cust.P_BillTo, recipientID);
                                                                String err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                                                                return thisResult;

                                                            }
                                                            finally
                                                            {
                                                                sqlConn.Close();
                                                            }
                                                        }
                                                        break;
                                                    }
                                                case ("C"):
                                                    {
                                                        try
                                                        {
                                                            //remID.Value = AddTodo((Guid)cust.P_id, xmlFile, "Added to todo List", DateTime.Now);
                                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + cust.C_name + " Recipient found " + recipientID + ".Sending file via CTC Adapter" + "");
                                                            String sendlog = CTCAdapter.SendMessage(cust.P_server, xmlFile, recipientID, cust.P_username, cust.P_password);
                                                            // Test without sending

                                                            //String sendlog = "Send OK";
                                                            if (sendlog.Contains("SEND OK"))
                                                            {
                                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Transmitted Ok. ");
                                                                if (sqlConn.State == ConnectionState.Open)
                                                                {
                                                                    sqlConn.Close();
                                                                }
                                                                sqlConn.ConnectionString = Globals.connString();
                                                                sqlConn.Open();
                                                                //remTodo.ExecuteNonQuery();
                                                                sqlConn.Close();
                                                                cwTx++;
                                                                NodeResources.AddLabel(lblCWTx, cwTx.ToString());
                                                                strSuccess = "Y";
                                                                //msgType = "Original";
                                                                //ToSend = true;
                                                                AddTransaction(cust.C_id, cust.P_id, Path.GetFileName(xmlFile), msgDir, false, trRef, archiveFile);
                                                                System.GC.Collect();
                                                                System.GC.WaitForPendingFinalizers();
                                                                File.Delete(xmlFile);
                                                                result = "CTC Adapter send Ok";
                                                            }
                                                            else
                                                            {
                                                                result = "CTC Adapter send Failed: " + sendlog;
                                                                ProcessingErrors procerror = new ProcessingErrors();
                                                                procerror.ErrorCode.Code = NodeError.e104;
                                                                procerror.SenderId = senderID;
                                                                procerror.RecipientId = recipientID;
                                                                procerror.ErrorCode.Description = "CTC Adapter send Failed: " + sendlog;
                                                                procerror.ProcId = cust.P_id;
                                                                procerror.FileName = xmlFile;
                                                                AddProcessingError(procerror);
                                                                thisResult.FolderLoc = Globals.FailLoc;
                                                                thisResult.Processed = false;
                                                                thisResult.FileName = xmlFile;
                                                            }
                                                            AddTransactionLog(cust.P_id, senderID, xmlFile, "File " + Path.GetFileName(xmlFile) + " sent via CTC Adapter with the Result: " + sendlog, DateTime.Now, strSuccess, cust.P_BillTo, recipientID);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            if (sqlConn.State == ConnectionState.Open)
                                                            {
                                                                sqlConn.Close();
                                                            }
                                                            ProcessingErrors procerror = new ProcessingErrors();
                                                            procerror.ErrorCode.Code = NodeError.e104;
                                                            procerror.SenderId = senderID;
                                                            procerror.RecipientId = recipientID;
                                                            procerror.ErrorCode.Description = "CTC Adapter send Failed: " + ex.Message;
                                                            procerror.FileName = xmlFile;
                                                            procerror.ProcId = cust.P_id;
                                                            AddProcessingError(procerror);
                                                            thisResult.FolderLoc = Globals.FailLoc;
                                                            thisResult.Processed = false;
                                                            thisResult.FileName = xmlFile;
                                                            AddTransactionLog(cust.P_id, senderID, xmlFile, "File " + Path.GetFileName(xmlFile) + " sent via CTC Adapter Failed with the Result: " + ex.Message, DateTime.Now, strSuccess, cust.P_BillTo, recipientID);
                                                            //     result = "CTC Adapter send failed: " + ex.Message;
                                                        }
                                                    }
                                                    break;
                                                case "E":
                                                    {
                                                        //  remID.Value = AddTodo((Guid)cust.P_id, xmlFile, "Added to todo List", DateTime.Now);
                                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + cust.C_name + " Recipient found " + recipientID + ".Sending file via Email" + "");
                                                        string mailResult = string.Empty;
                                                        mailResult = MailModule.sendMsg(xmlFile, cust.P_EmailAddress, cust.P_Subject, "Cargowise File Attached");
                                                        // mailResult = "Message Sent OK";

                                                        if (mailResult == "Message Sent OK")
                                                        {
                                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Transmitted Ok. ");
                                                            sqlConn.ConnectionString = Globals.connString(); sqlConn.Open();
                                                            //    remTodo.ExecuteNonQuery();
                                                            sqlConn.Close();
                                                            cwTx++;
                                                            NodeResources.AddLabel(lblCWTx, cwTx.ToString());
                                                            AddTransaction(cust.C_id, cust.P_id, Path.GetFileName(xmlFile), msgDir, false, trRef, archiveFile);
                                                            //  result = mailResult;
                                                            thisResult.Processed = true;
                                                            thisResult.FileName = xmlFile;
                                                        }
                                                        else
                                                        {
                                                            //   remID.Value = AddTodo((Guid)cust.P_id, xmlFile, "Message send failed. Error message was: " + mailResult, DateTime.Now);
                                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Message send failed. Error message was: " + mailResult);
                                                            //   result = "Email send failed: " + mailResult;
                                                            ProcessingErrors procerror = new ProcessingErrors();
                                                            procerror.ErrorCode.Code = NodeError.e105;
                                                            procerror.SenderId = senderID;
                                                            procerror.RecipientId = recipientID;
                                                            procerror.ErrorCode.Description = "Email message failed to send " + mailResult;
                                                            procerror.ProcId = cust.P_id;
                                                            procerror.FileName = xmlFile;
                                                            AddProcessingError(procerror);
                                                            thisResult.FolderLoc = cust.C_path + "\\Processing\\";
                                                            thisResult.Processed = false;
                                                            thisResult.FileName = xmlFile;

                                                        }
                                                        AddTransactionLog(cust.P_id, senderID, xmlFile, "File " + Path.GetFileName(xmlFile) + " sent via Email with the Result: " + mailResult, DateTime.Now, strSuccess, cust.P_BillTo, recipientID);
                                                        break;
                                                    }

                                            }
                                            break;
                                        case "R":

                                            break;
                                    }
                                }



                            }
                            // sr.Close();
                            //  sr.Dispose();
                            recipientID = cust.P_Recipientid;
                            senderID = cust.P_Senderid;
                            archiveFile = Globals.ArchiveFile(cust.C_path + "\\Archive", xmlFile);
                            CreateProfPath(cust.C_path + "\\Processing", cust.C_code + "XSD");
                            msgDir = "S";
                            // AddTransaction((Guid)dr["C_ID"], (Guid)dr["P_ID"], xmlFile, msgDir, false, trRef, archiveFile);
                            FillTodoList(cust.C_path + "\\Processing\\", cust.P_id);
                            Globals.ArchiveFile(cust.C_path + "\\Archive", xmlFile);
                            processed = true;
                            thisResult.Processed = true;
                            break;
                        }

                        catch (XmlSchemaValidationException ex)
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Invalid Schema found. Continue Scanning for matching Customer Schema ");
                            if (dr["P_NOTIFY"].ToString() == "Y")
                            {
                                MailModule.sendMsg("", Globals.AlertsTo, "XML Schema Validation issue", "Invalid schema definition error:" + ex.Message);
                            }


                        }
                        catch (XmlException ex)
                        {
                            ProcessingErrors procerror = new ProcessingErrors();
                            CTCErrorCode error = new CTCErrorCode();
                            error.Code = NodeError.e111;
                            error.Description = "Customer XML Error :" + ex.Message;
                            error.Severity = "Warning";
                            procerror.ErrorCode = error;
                            procerror.SenderId = senderID;
                            procerror.RecipientId = recipientID;
                            procerror.FileName = xmlFile;
                            AddProcessingError(procerror);
                            thisResult.FolderLoc = Globals.FailLoc;
                            thisResult.Processed = false;
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node (XML) Exception Found: Custom Processing:" + ex.Message + ". " + "");
                            //   return thisResult;

                        }
                        catch (Exception ex)
                        {
                            ProcessingErrors procerror = new ProcessingErrors();
                            CTCErrorCode error = new CTCErrorCode();
                            error.Code = NodeError.e111;
                            error.Description = "Customer XML Error :" + ex.Message;
                            error.Severity = "Warning";
                            procerror.ErrorCode = error;
                            procerror.SenderId = senderID;
                            procerror.RecipientId = recipientID;
                            procerror.FileName = xmlFile;
                            AddProcessingError(procerror);
                            thisResult.FolderLoc = Globals.FailLoc;
                            thisResult.Processed = false;
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "CTC Node (Trap All) Exception Found: Custom Processing:" + ex.Message + ". " + "");
                            //   return thisResult;
                        }
                    }
                    if (!processed)
                    {
                        ProcessingErrors procerror = new ProcessingErrors();
                        CTCErrorCode error = new CTCErrorCode();
                        error.Code = NodeError.e111;
                        error.Description = "Unable to process " + Path.GetFileName(xmlFile) + "No matching routines found. Moving to Failed.";
                        error.Severity = "Warning";
                        procerror.ErrorCode = error;
                        procerror.SenderId = senderID;
                        procerror.RecipientId = recipientID;
                        procerror.FileName = xmlFile;
                        AddProcessingError(procerror);
                        thisResult.FolderLoc = Globals.FailLoc;
                        thisResult.Processed = false;
                        NodeResources.MoveFile(Path.GetFileName(xmlFile), Path.Combine(Path.Combine(filesPath, "Failed")));
                        //File.Move(xmlFile, Path.Combine(Path.Combine(Globals.XMLLoc, "Failed"), Path.GetFileName(xmlFile)));
                        AddProcessingError(procerror);
                        thisResult.Processed = false;
                        thisResult.FolderLoc = custPath;
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Unable to process " + Path.GetFileName(xmlFile) + "No matching routines found. " + "");
                        processed = true;
                        //  return thisResult;
                    }

                }
            }
            return thisResult;
        }

        private TransReference ArrtoRef(string[] v)
        {
            string[] r3;
            TransReference result = new TransReference();
            switch (v.Count())
            {
                case 1:
                    r3 = v[0].Split('|');
                    result.Ref3Type = RefType.Operation;
                    result.Reference3 = r3[1];
                    break;
                case 3:
                    string[] r1 = v[0].Split('|');
                    result.Ref1Type = (RefType)Enum.Parse(typeof(RefType), r1[0]);
                    result.Reference1 = r1[1];
                    string[] r2 = v[1].Split('|');
                    result.Ref2Type = (RefType)Enum.Parse(typeof(RefType), r2[0]);
                    result.Reference2 = r2[1];
                    r3 = v[2].Split('|');
                    result.Ref3Type = (RefType)Enum.Parse(typeof(RefType), r3[0]);
                    result.Reference3 = r3[1];

                    break;

            }

            return result;
        }

        private void FillTodoList(String pathToScan, Guid profileID)
        {
            SqlCommand sqlTodo = new SqlCommand("Add_Todo", sqlConn);
            sqlTodo.CommandType = CommandType.StoredProcedure;
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            SqlParameter r_id = sqlTodo.Parameters.Add("@R_ID", SqlDbType.UniqueIdentifier);
            r_id.Direction = ParameterDirection.Output;
            SqlParameter r_status = sqlTodo.Parameters.Add("@R_STATUS", SqlDbType.Char, 1);
            r_status.Direction = ParameterDirection.Output;
            SqlParameter p_id = sqlTodo.Parameters.Add("@P_ID", SqlDbType.UniqueIdentifier);
            SqlParameter filename = sqlTodo.Parameters.Add("@FILENAME", SqlDbType.VarChar, 200);
            SqlParameter lastdate = sqlTodo.Parameters.Add("@LASTDATE", SqlDbType.DateTime);
            SqlParameter lastresult = sqlTodo.Parameters.Add("@LASTRESULT", SqlDbType.Char, 1);

            DirectoryInfo diTodo = new DirectoryInfo(pathToScan);
            foreach (var fileTodo in diTodo.GetFiles())
            {
                p_id.Value = profileID;
                filename.Value = fileTodo.FullName;
                lastdate.Value = DateTime.Now;
                lastresult.Value = "Added to Todo List";
                sqlConn.ConnectionString = Globals.connString(); sqlConn.Open();
                sqlTodo.ExecuteNonQuery();
                sqlConn.Close();
            }
        }

        private String getCustPath(String senderID)
        {
            SqlCommand sqlCustPath = new SqlCommand("SELECT @C_PATH=C_PATH from CUSTOMER where C_CODE = @C_CODE", sqlConn);
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            sqlCustPath.Parameters.AddWithValue("@C_CODE", senderID);
            SqlParameter CUSTPATH = sqlCustPath.Parameters.Add("@C_PATH", SqlDbType.VarChar, -1);
            CUSTPATH.Direction = ParameterDirection.Output;
            sqlCustPath.CommandType = CommandType.Text;
            sqlCustPath.ExecuteNonQuery();
            if (CUSTPATH.Value != DBNull.Value)
            {
                return CUSTPATH.Value.ToString();
            }
            else
                return String.Empty;
        }

        private Guid getCustID(String senderID)
        {
            SqlCommand sqlCustID = new SqlCommand("SELECT @C_ID=C_ID from CUSTOMER where C_CODE = @C_CODE", sqlConn);
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            sqlCustID.Parameters.AddWithValue("@C_CODE", senderID);
            SqlParameter CUSTID = sqlCustID.Parameters.Add("@C_ID", SqlDbType.UniqueIdentifier);
            CUSTID.Direction = ParameterDirection.Output;
            sqlCustID.CommandType = CommandType.Text;
            sqlCustID.ExecuteNonQuery();
            if (CUSTID.Value != DBNull.Value)
            {
                return (Guid)CUSTID.Value;
            }
            else
                return Guid.Empty;
        }



        private Guid AddTodo(Guid p_id, String fileName, String lastresult, DateTime lastdate)
        {
            SqlCommand addTodo = new SqlCommand("ADD_TODO", sqlConn);
            addTodo.CommandType = CommandType.StoredProcedure;
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;

            Guid todoid = new Guid();
            try
            {
                SqlParameter l_id = addTodo.Parameters.Add("@P_ID", SqlDbType.UniqueIdentifier);
                SqlParameter l_filename = addTodo.Parameters.Add("@FILENAME", SqlDbType.VarChar, 200);
                SqlParameter l_date = addTodo.Parameters.Add("@LASTDATE", SqlDbType.DateTime);
                SqlParameter l_lastResult = addTodo.Parameters.Add("@LASTRESULT", SqlDbType.VarChar, 200);
                SqlParameter r_TODOID = addTodo.Parameters.Add("@R_ID", SqlDbType.UniqueIdentifier);
                r_TODOID.Direction = ParameterDirection.Output;
                SqlParameter r_Status = addTodo.Parameters.Add("R_STATUS", SqlDbType.Char, 1);
                r_Status.Direction = ParameterDirection.Output;
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.ConnectionString = Globals.connString();

                sqlConn.Open();
                l_id.Value = p_id;
                l_filename.Value = fileName;
                l_date.Value = lastdate;
                l_lastResult.Value = lastresult;
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.Open();
                addTodo.ExecuteNonQuery();
                todoid = (Guid)r_TODOID.Value;
            }
            catch (SqlException ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();
                CTCErrorCode error = new CTCErrorCode();
                error.Code = NodeError.e105;
                error.Description = "SQL Connection Error :" + ex.Message;
                error.Severity = "Fatal";
                procerror.ErrorCode = error;
                procerror.FileName = fileName;
                thisResult.FolderLoc = Globals.FailLoc;
                thisResult.Processed = false;
                AddProcessingError(procerror);
                thisResult.Processed = false;
                procerror.ProcId = p_id;
                AddProcessingError(procerror);
            }
            finally
            {
                sqlConn.Close();
            }


            return todoid;

        }

        private void AddTransaction(Guid custid, Guid profile, string fileName, string direction, bool original, TransReference trRefs, string archive)
        {
            SqlCommand sqlTransaction = new SqlCommand();
            sqlTransaction.CommandType = CommandType.StoredProcedure;
            sqlTransaction.CommandText = "Add_Transaction";
            sqlTransaction.Connection = sqlConn;
            try
            {
                sqlTransaction.Parameters.AddWithValue("@T_C", custid);
                sqlTransaction.Parameters.AddWithValue("@T_P", profile);
                sqlTransaction.Parameters.AddWithValue("@T_FILENAME", Path.GetFileName(fileName));
                sqlTransaction.Parameters.AddWithValue("@T_DATETIME", DateTime.Now);
                sqlTransaction.Parameters.AddWithValue("@T_TRIAL", "N");
                sqlTransaction.Parameters.AddWithValue("@DIRECTION", direction);
                if (original)
                    sqlTransaction.Parameters.AddWithValue("@ORIGINAL", 1);
                else
                    sqlTransaction.Parameters.AddWithValue("@ORIGINAL", 0);
                if (trRefs != null)
                {
                    sqlTransaction.Parameters.AddWithValue("T_REF1", trRefs.Reference1);
                    sqlTransaction.Parameters.AddWithValue("T_REF2", trRefs.Reference2);
                    sqlTransaction.Parameters.AddWithValue("T_REF3", trRefs.Reference3);
                    sqlTransaction.Parameters.AddWithValue("T_REF1TYPE", trRefs.Ref1Type.ToString());
                    sqlTransaction.Parameters.AddWithValue("T_REF2TYPE", trRefs.Ref2Type.ToString());
                    sqlTransaction.Parameters.AddWithValue("T_REF3TYPE", trRefs.Ref3Type.ToString());
                }
                if (!string.IsNullOrEmpty(archive))
                    sqlTransaction.Parameters.AddWithValue("T_ARCHIVE", Path.GetFileName(archive));

                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.ConnectionString = Globals.connString();
                sqlConn.Open();
                sqlTransaction.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();
                CTCErrorCode error = new CTCErrorCode();
                error.Code = NodeError.e105;
                error.Description = "SQL Connection Error:" + ex.Message;
                error.Severity = "Fatal";
                procerror.ErrorCode = error;
                procerror.FileName = fileName;
                procerror.ProcId = profile;
                AddProcessingError(procerror);
            }
            finally
            {
                sqlConn.Close();
            }


        }

        private void AddTransactionLog(Guid p_id, String senderid, String filename, String lastresult, DateTime lastdate, String success, String cust, String recipientid)
        {
            Guid custid = Guid.Empty;
            try
            {
                SqlCommand addTodo = new SqlCommand("ADD_TransactionLog", sqlConn);
                addTodo.CommandType = CommandType.StoredProcedure;
                SqlParameter l_id = addTodo.Parameters.Add("@P_ID", SqlDbType.UniqueIdentifier);
                SqlParameter l_filename = addTodo.Parameters.Add("@FILENAME", SqlDbType.VarChar, 200);
                SqlParameter l_date = addTodo.Parameters.Add("@LASTDATE", SqlDbType.DateTime);
                SqlParameter l_lastResult = addTodo.Parameters.Add("@LASTRESULT", SqlDbType.VarChar, 200);
                SqlParameter l_success = addTodo.Parameters.Add("@SUCCESS", SqlDbType.Char, 1);
                SqlParameter c_id = addTodo.Parameters.Add("@C_ID", SqlDbType.UniqueIdentifier);
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.ConnectionString = Globals.connString();
                sqlConn.Open();
                l_id.Value = p_id;
                l_filename.Value = Path.GetFileName(filename);
                l_date.Value = lastdate;
                l_lastResult.Value = lastresult;
                custid = getCustID(cust);
                if (custid != Guid.Empty)
                {
                    c_id.Value = custid;
                }
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.Open();
                addTodo.ExecuteNonQuery();
                sqlConn.Close();
            }
            catch (SqlException exSql)
            {
                CTCErrorCode error = new CTCErrorCode();
                ProcessingErrors procerror = new ProcessingErrors();
                if (custid != Guid.Empty)
                {
                    error.Code = NodeError.e106;
                }
                else
                {
                    error.Code = NodeError.e107;
                }
                error.Description = "SQL Connection Error:" + exSql.Message;
                procerror.ErrorCode = error;
                procerror.SenderId = senderid;
                procerror.RecipientId = cust;
                procerror.FileName = filename;
                procerror.ProcId = p_id;
                AddProcessingError(procerror);
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Error updating Transaction Log: " + exSql.Message);

            }
            catch (Exception ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();
                CTCErrorCode error = new CTCErrorCode();
                if (custid != Guid.Empty)
                {
                    error.Code = NodeError.e106;
                }
                else
                {
                    error.Code = NodeError.e111;
                }
                error.Description = ex.Message;
                procerror.ErrorCode = error;
                procerror.SenderId = senderid;
                procerror.RecipientId = cust;
                procerror.ErrorCode.Description = ex.Message;
                procerror.FileName = filename;
                procerror.ProcId = p_id;
                AddProcessingError(procerror);
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Error updating Transaction Log(2): " + ex.Message);
                // String file = Path.GetFileName(filename);
                // File.Move(filename, Path.Combine(Globals.FailLoc, file));

            }
            finally
            {
                sqlConn.Close();
            }
        }

        private XmlNode FindNode(XmlNodeList list, String nodeName)
        {
            if (list.Count > 0)
            {
                foreach (XmlNode node in list)
                {
                    if (node.Name.Equals(nodeName)) return node;
                    if (node.HasChildNodes)
                    {
                        XmlNode nodeFound = FindNode(node.ChildNodes, nodeName);
                        if (nodeFound != null)
                        {
                            return nodeFound;
                        }
                    }
                }
            }
            return null;
        }

        //private ProcessResult ProcessXMS(String xmlfile)
        //{
        //    FileInfo processingFile = new FileInfo(xmlfile);
        //    ProcessResult thisResult = new ProcessResult();
        //    thisResult.Processed = true;
        //    XmlDocument xUXMLShipment = new XmlDocument();
        //    var nsMgr = new XmlNamespaceManager(xUXMLShipment.NameTable);
        //    nsMgr.AddNamespace("CW", "http://www.edi.com.au/EnterpriseService/");
        //    StreamReader sr = new StreamReader(processingFile.FullName);
        //    xUXMLShipment.Load(sr);
        //    sr.Close();
        //    sr.Dispose();
        //    totfilesRx++;
        //    NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
        //    String senderID = String.Empty;
        //    String recipientID = String.Empty;
        //    String Cartageid = String.Empty;
        //    String reasonCode = String.Empty;
        //    String msgDir = "";
        //    TransReference trRef = new TransReference();

        //    String archiveFile;
        //    try
        //    {
        //        XmlNode xNode;
        //        XmlNodeList xList = xUXMLShipment.SelectNodes("//CW:Source", nsMgr);
        //        if (xList.Count > 0)
        //        {
        //            try
        //            {
        //                xNode = xList[0].SelectSingleNode("//CW:EnterpriseCode", nsMgr);
        //                senderID = xNode.InnerText;
        //                xNode = xList[0].SelectSingleNode("//CW:CompanyCode", nsMgr);
        //                senderID += xNode.InnerText;
        //                xNode = xList[0].SelectSingleNode("//CW:OriginServer", nsMgr);
        //                senderID += xNode.InnerText;
        //                xNode = xList[0].SelectSingleNode("//CW:Purpose", nsMgr);
        //                reasonCode = xNode.InnerText;
        //                if (String.IsNullOrEmpty(reasonCode))
        //                {
        //                    MailModule.sendMsg(xmlfile, Globals.AlertsTo, "Legacy XML missing Reason Code", "The File Attached is missing the Reason Code");
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                string strEx = ex.GetType().Name;
        //                ProcessingErrors procerror = new ProcessingErrors();
        //                procerror.ErrorCode.Code = NodeError.e110;
        //                procerror.SenderId = senderID;
        //                procerror.RecipientId = recipientID;
        //                procerror.ErrorCode.Description = strEx + " Legacy XML missing Reason Code.";
        //                procerror.FileName = xmlfile;
        //                AddProcessingError(procerror);
        //                thisResult.FolderLoc = Globals.FailLoc;
        //                thisResult.Processed = false;
        //                thisResult.FileName = xmlfile;

        //                return thisResult;

        //            }
        //        }
        //        if (reasonCode == "PEG")
        //        {
        //            xNode = xUXMLShipment.SelectSingleNode("//CW:EDIOrganisation", nsMgr);
        //        }
        //        else
        //        {
        //            xList = xUXMLShipment.SelectNodes("//CW:CartageCompany", nsMgr);
        //            xNode = null;
        //            if (xList != null)
        //            {
        //                xNode = xUXMLShipment.SelectSingleNode("//CW:CartageCompany", nsMgr);
        //                xNode = FindNode(xList, "CartageCompany");
        //                if (xNode == null)
        //                {
        //                    xList = xUXMLShipment.SelectNodes("//CW:CartageJob", nsMgr);
        //                    xNode = xUXMLShipment.SelectSingleNode("//CW:CartageOrg", nsMgr);
        //                    if (xNode == null)
        //                    {
        //                        xNode = xUXMLShipment.SelectSingleNode("//CW:EDIOrganisation", nsMgr);

        //                    }

        //                }

        //            }
        //        }
        //        recipientID = xNode.Attributes["EDICode"].Value.ToString();
        //        xList = xUXMLShipment.SelectNodes("//CW:DataSource");
        //        msgDir = "R";
        //        cwRx++;
        //        NodeResources.AddLabel(lblCwRx, cwRx.ToString());
        //        SqlCommand sqlCustProfile = new SqlCommand("Select C_ID, P_ID, C_NAME, C_IS_ACTIVE, C_ON_HOLD, C_PATH, C_FTP_CLIENT, P_REASONCODE, P_SERVER, P_USERNAME, P_DELIVERY, P_PATH, P_PASSWORD, P_Recipientid, P_SENDERID, P_DESCRIPTION, P_PORT, C_CODE from vw_CustomerProfile WHERE P_SENDERID = @SENDERID and P_Recipientid =@RECIPIENTID ", sqlConn);
        //        sqlCustProfile.Parameters.AddWithValue("@SENDERID", senderID);
        //        sqlCustProfile.Parameters.AddWithValue("@RECIPIENTID", recipientID);
        //        CustProfileRecord custProfileRecord = new CustProfileRecord();
        //        custProfileRecord = GetCustomerProfile(senderID, recipientID, reasonCode, eventCode);
        //        if (sqlConn.State == ConnectionState.Open)
        //        {
        //            sqlConn.Close();
        //        }
        //        sqlConn.ConnectionString = Globals.connString();
        //        sqlConn.Open();
        //        //SqlDataReader drCustProfile;
        //        List<CustProfileRecord> custProfile = new List<CustProfileRecord>();
        //        using (SqlDataReader drCustProfile = sqlCustProfile.ExecuteReader(CommandBehavior.CloseConnection))
        //        {
        //            if (drCustProfile.HasRows)
        //            {

        //                while (drCustProfile.Read())
        //                {
        //                    CustProfileRecord result = new CustProfileRecord();
        //                    result.C_id = (Guid)drCustProfile["C_ID"];
        //                    result.P_id = (Guid)drCustProfile["P_ID"];
        //                    result.C_name = drCustProfile["C_NAME"].ToString();
        //                    result.C_is_active = drCustProfile["C_IS_ACTIVE"].ToString();
        //                    result.C_on_hold = drCustProfile["C_ON_HOLD"].ToString();
        //                    result.C_path = drCustProfile["C_PATH"].ToString();
        //                    result.C_ftp_client = drCustProfile["C_FTP_CLIENT"].ToString();
        //                    result.P_reasoncode = drCustProfile["P_REASONCODE"].ToString();
        //                    result.P_server = drCustProfile["P_SERVER"].ToString();
        //                    result.P_username = drCustProfile["P_USERNAME"].ToString();
        //                    result.P_delivery = drCustProfile["P_DELIVERY"].ToString();
        //                    result.P_password = drCustProfile["P_PASSWORD"].ToString();
        //                    result.P_Recipientid = drCustProfile["P_Recipientid"].ToString();
        //                    result.P_description = drCustProfile["P_DESCRIPTION"].ToString();
        //                    result.P_Senderid = drCustProfile["P_SENDERID"].ToString();
        //                    result.P_path = drCustProfile["P_PATH"].ToString();
        //                    result.P_port = drCustProfile["P_PORT"].ToString();
        //                    result.C_code = drCustProfile["C_CODE"].ToString();
        //                    result.P_BillTo = drCustProfile["P_BILLTO"].ToString();
        //                    custProfile.Add(result);
        //                }
        //            }
        //            else
        //            {

        //                SqlCommand execAdd = new SqlCommand();
        //                execAdd.Connection = sqlConn;
        //                execAdd.CommandText = "Add_Profile";
        //                execAdd.CommandType = CommandType.StoredProcedure;
        //                SqlParameter PC = execAdd.Parameters.Add("@P_C", SqlDbType.UniqueIdentifier);
        //                PC.Value = getCustID(senderID);
        //                SqlParameter PReasonCode = execAdd.Parameters.Add("@P_REASONCODE", SqlDbType.Char, 3);
        //                PReasonCode.Value = reasonCode;
        //                SqlParameter PServer = execAdd.Parameters.Add("@P_SERVER", SqlDbType.VarChar, 50);
        //                PServer.Value = "";
        //                SqlParameter PUsername = execAdd.Parameters.Add("@P_USERNAME", SqlDbType.VarChar, 50);
        //                PUsername.Value = "";
        //                SqlParameter PPath = execAdd.Parameters.Add("@P_PATH", SqlDbType.VarChar, 100);
        //                PPath.Value = "";
        //                SqlParameter PPassword = execAdd.Parameters.Add("@P_PASSWORD", SqlDbType.VarChar, 50);
        //                PPassword.Value = "";
        //                string sDelivery = String.Empty;
        //                sDelivery = "R";
        //                SqlParameter pDelivery = execAdd.Parameters.Add("@P_DELIVERY", SqlDbType.Char, 1);
        //                pDelivery.Value = sDelivery;
        //                SqlParameter pMSGType = execAdd.Parameters.Add("@MSGTYPE", SqlDbType.VarChar, 20);
        //                pMSGType.Value = "Original";
        //                SqlParameter pBillType = execAdd.Parameters.Add("@BILLTO", SqlDbType.VarChar, 15);
        //                pBillType.Value = senderID;
        //                SqlParameter pChargeable = execAdd.Parameters.Add("@CHARGEABLE", SqlDbType.Char, 1);
        //                pChargeable.Value = "Y";
        //                SqlParameter PPort = execAdd.Parameters.Add("@P_PORT", SqlDbType.Char, 10);
        //                PPort.Value = "";
        //                SqlParameter PDescription = execAdd.Parameters.Add("@P_DESCRIPTION", SqlDbType.VarChar, 100);
        //                PDescription.Value = "No Transport Profile found. Store in Folder";
        //                SqlParameter PRecipientId = execAdd.Parameters.Add("@P_Recipientid", SqlDbType.VarChar, 15);
        //                PRecipientId.Value = recipientID;
        //                SqlParameter PSenderId = execAdd.Parameters.Add("@P_SENDERID", SqlDbType.VarChar, 15);
        //                PSenderId.Value = senderID;
        //                SqlParameter p_id = execAdd.Parameters.Add("@R_ID", SqlDbType.UniqueIdentifier);
        //                p_id.Direction = ParameterDirection.Output;
        //                SqlParameter r_Action = execAdd.Parameters.Add("@R_ACTION", SqlDbType.Char, 1);
        //                r_Action.Direction = ParameterDirection.Output;
        //                if (sqlConn.State == ConnectionState.Open)
        //                {
        //                    sqlConn.Close();
        //                }
        //                sqlConn.Open();
        //                execAdd.ExecuteNonQuery();
        //                sqlConn.Close();

        //                //Customer found but no Customer/Transport profile found. Move to default folder. 
        //                CreateProfPath(getCustPath(senderID) + "\\Processing", recipientID + reasonCode);
        //                ProcessingErrors procerror = new ProcessingErrors();
        //                procerror.ErrorCode.Code = NodeError.e100;
        //                procerror.SenderId = senderID;
        //                procerror.RecipientId = recipientID;
        //                procerror.ErrorCode.Description = "Customer Found but no Profile Found.";
        //                procerror.FileName = xmlfile;
        //                thisResult.FolderLoc = Globals.FailLoc;
        //                thisResult.Processed = false;
        //                thisResult.FileName = xmlfile;

        //                AddProcessingError(procerror);
        //                AddTransactionLog(Guid.Empty, senderID, xmlfile, "Customer Found but no Profile found. Move to default collection folder.", DateTime.Now, "Y", senderID, recipientID);
        //                return thisResult;

        //            }
        //            drCustProfile.Close();
        //        }
        //        SqlCommand remTodo = new SqlCommand("DELETE FROM TODO WHERE L_ID =@ID", sqlConn);
        //        SqlParameter remID = remTodo.Parameters.Add("@ID", SqlDbType.UniqueIdentifier);
        //        if (custProfile.Count > 0)
        //        {
        //            //Is Client Active and not on hold
        //            if (custProfile[0].C_is_active == "N" | custProfile[0].C_on_hold == "Y")
        //            {
        //                string cwXML = string.Empty;
        //                string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfile[0].C_path, "Held"));
        //                if (f.StartsWith("Warning"))
        //                {
        //                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
        //                }
        //                else
        //                { xmlfile = f; }
        //                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfile[0].C_name + " is not Active or is on Hold. Moving to Hold and skipping" + "");
        //            }
        //            else
        //            {

        //                remID.Value = AddTodo(custProfile[0].P_id, xmlfile, "Add_Transaction: ", DateTime.Now);
        //                archiveFile = Globals.ArchiveFile(custProfile[0].C_path + "\\Archive", xmlfile);
        //                //Create the Delete from TODO list ready to be removed
        //                if (sqlConn.State == ConnectionState.Open)
        //                {
        //                    sqlConn.Close();
        //                }
        //                CreateProfPath(custProfile[0].C_path + "\\Processing", recipientID + reasonCode);
        //                //Send file to Archive folder

        //                string cwxml = string.Empty;
        //                string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfile[0].C_path, "Processing", recipientID + reasonCode));
        //                if (f.StartsWith("Warning"))
        //                {
        //                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
        //                }
        //                else
        //                { xmlfile = f; }

        //                xmlfile = cwxml;

        //                //xmlfile = Path.Combine(custProfile[0].C_path + "\\Processing\\" + recipientID + reasonCode, Path.GetFileName(xmlfile));
        //                String ftpSend = String.Empty;
        //                String strSuccess = "N";
        //                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Profile : " + custProfile[0].P_description);
        //                AddTransaction(custProfile[0].C_id, custProfile[0].P_id, xmlfile, msgDir, true, trRef, archiveFile);
        //                thisResult.Processed = true;
        //                thisResult.FileName = xmlfile;

        //                switch (custProfile[0].P_delivery)
        //                {
        //                    case ("R"):
        //                        {
        //                            //File.Move(xmlfile, Path.Combine(drProf["C_PATH"].ToString() + "\\Processing\\" + recipientID + reasonCode, Path.GetFileName(xmlfile)));
        //                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfile[0].C_name + " Recipient found " + recipientID + ". Moving to Folder");
        //                            AddTransactionLog(custProfile[0].P_id, senderID, xmlfile, "Customer " + custProfile[0].C_name + " Recipient found " + recipientID + ". Moving to Folder to retrieval", DateTime.Now, "Y", custProfile[0].P_BillTo, recipientID);
        //                            msgDir = "R";

        //                            break;
        //                        }
        //                    case ("F"):
        //                        {
        //                            try
        //                            {
        //                                string ftpResult = NodeResources.FtpSend(xmlfile, custProfile);
        //                                if (ftpResult.Contains("File Sent Ok"))
        //                                {
        //                                    if (sqlConn.State == ConnectionState.Open)
        //                                    {
        //                                        sqlConn.Close();
        //                                    }
        //                                    // sqlConn.ConnectionString = Globals.connString();
        //                                    sqlConn.Open();
        //                                    remTodo.ExecuteNonQuery();
        //                                    filesTx++;
        //                                    NodeResources.AddLabel(lblFilesTx, filesTx.ToString());
        //                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "File " + Path.GetFileName(xmlfile) + " Sent. Response: " + ftpSend);
        //                                    msgDir = "S";
        //                                    AddTransaction(custProfile[0].C_id, custProfile[0].P_id, xmlfile, msgDir, false, trRef, archiveFile);
        //                                    strSuccess = "Y";


        //                                    remID.Value = AddTodo(custProfile[0].P_id, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via FTP with the Result: " + ftpSend, DateTime.Now);
        //                                    AddTransactionLog(custProfile[0].P_id, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via FTP with the Result: " + ftpSend, DateTime.Now, strSuccess, custProfile[0].P_BillTo, recipientID);
        //                                }
        //                                else
        //                                {
        //                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Unable to Send FTP File " + Path.GetFileName(xmlfile) + ". Error was " + ftpSend);
        //                                }


        //                            }
        //                            catch (Exception ex)
        //                            {
        //                                string strEx = ex.GetType().Name;
        //                                ProcessingErrors procerror = new ProcessingErrors();
        //                                CTCErrorCode error = new CTCErrorCode();
        //                                error.Code = NodeError.e104;
        //                                error.Description = "Customer Exists but no profile Found.";
        //                                error.Severity = "Warning";
        //                                procerror.ErrorCode = error;
        //                                procerror.SenderId = senderID;
        //                                procerror.RecipientId = recipientID;
        //                                procerror.FileName = xmlfile;
        //                                AddProcessingError(procerror);
        //                                thisResult.FolderLoc = Globals.FailLoc;
        //                                thisResult.Processed = false;
        //                                thisResult.FileName = xmlfile;

        //                                remID.Value = AddTodo(custProfile[0].P_id, xmlfile, ftpSend, DateTime.Now);
        //                                if (sqlConn.State == ConnectionState.Open)
        //                                {
        //                                    sqlConn.Close();
        //                                }
        //                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error. Cargowise FTP Send Error Found: " + ex.Message + ". " + "");
        //                                String err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
        //                                AddTransactionLog(custProfile[0].P_id, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via FTP with the Result: " + ftpSend, DateTime.Now, strSuccess, custProfile[0].P_BillTo, recipientID);

        //                            }
        //                            break;
        //                        }
        //                    case ("C"):
        //                        {
        //                            try
        //                            {
        //                                remID.Value = AddTodo(custProfile[0].P_id, xmlfile, "Added to todo List", DateTime.Now);
        //                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfile[0].C_name + " Recipient found " + recipientID + ".Sending file via CTC Adapter" + "");
        //                                String sendlog = CTCAdapter.SendMessage(custProfile[0].P_server, xmlfile, recipientID, custProfile[0].P_username, custProfile[0].P_password);
        //                                if (sendlog == "Send Ok")
        //                                {
        //                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Transmitted Ok. ");
        //                                    if (sqlConn.State == ConnectionState.Open)
        //                                    {
        //                                        sqlConn.Close();
        //                                    }
        //                                    sqlConn.ConnectionString = Globals.connString();
        //                                    sqlConn.Open();
        //                                    try
        //                                    {
        //                                        remTodo.ExecuteNonQuery();
        //                                    }
        //                                    catch (Exception)
        //                                    {
        //                                    }
        //                                    sqlConn.Close();

        //                                    cwTx++;
        //                                    NodeResources.AddLabel(lblCWTx, cwTx.ToString());
        //                                    strSuccess = "Y";
        //                                }
        //                                AddTransactionLog(custProfile[0].P_id, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via CTC Adapter with the Result: " + sendlog, DateTime.Now, strSuccess, custProfile[0].P_BillTo, recipientID);
        //                                msgDir = "S";
        //                                AddTransaction(custProfile[0].C_id, custProfile[0].P_id, xmlfile, msgDir, false, trRef, archiveFile);
        //                            }
        //                            catch (Exception ex)
        //                            {
        //                                if (sqlConn.State == ConnectionState.Open)
        //                                {
        //                                    sqlConn.Close();
        //                                }
        //                                ProcessingErrors procerror = new ProcessingErrors();
        //                                CTCErrorCode error = new CTCErrorCode();
        //                                error.Code = NodeError.e108;
        //                                error.Description = "Customer Exists but no profile Found.";
        //                                error.Severity = "Warning";
        //                                procerror.ErrorCode = error;
        //                                procerror.SenderId = senderID;
        //                                procerror.RecipientId = recipientID;
        //                                procerror.FileName = xmlfile;
        //                                AddProcessingError(procerror);
        //                                thisResult.FolderLoc = Globals.FailLoc;
        //                                thisResult.Processed = false;
        //                                thisResult.FileName = xmlfile;

        //                                string strEx = ex.GetType().Name;
        //                                remID.Value = AddTodo(custProfile[0].P_id, xmlfile, ex.Message, DateTime.Now);
        //                                AddTransactionLog(custProfile[0].P_id, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via CTC Adapter Failed with the Result: " + ex.Message, DateTime.Now, strSuccess, custProfile[0].P_BillTo, recipientID);

        //                                String err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;

        //                            }
        //                        }
        //                        break;
        //                    case "E":
        //                        {
        //                            remID.Value = AddTodo(custProfile[0].P_id, xmlfile, "Added to todo List", DateTime.Now);
        //                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfile[0].C_name + " Recipient found " + recipientID + ".Sending file via Email" + "");
        //                            String mailResult = MailModule.sendMsg(xmlfile, custProfile[0].P_server, custProfile[0].P_description, "Cargowise File Attached");

        //                            if (mailResult == "Message Sent OK")
        //                            {
        //                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Transmitted Ok. ");
        //                                if (sqlConn.State == ConnectionState.Open)
        //                                {
        //                                    sqlConn.Close();
        //                                }
        //                                sqlConn.ConnectionString = Globals.connString();
        //                                sqlConn.Open();
        //                                try
        //                                {
        //                                    remTodo.ExecuteNonQuery();
        //                                }
        //                                catch (Exception)
        //                                {
        //                                }
        //                                sqlConn.Close();
        //                                cwTx++;
        //                                NodeResources.AddLabel(lblCWTx, cwTx.ToString());
        //                                msgDir = "S";
        //                                AddTransaction(custProfile[0].C_id, custProfile[0].P_id, xmlfile, msgDir, false, trRef, archiveFile);
        //                                strSuccess = "Y";
        //                            }
        //                            else
        //                            {
        //                                ProcessingErrors procerror = new ProcessingErrors();
        //                                CTCErrorCode error = new CTCErrorCode();
        //                                error.Code = NodeError.e105;
        //                                error.Description = "Message send failed. Error message was:" + mailResult;
        //                                error.Severity = "Warning";
        //                                procerror.ErrorCode = error;
        //                                procerror.SenderId = senderID;
        //                                procerror.RecipientId = recipientID;
        //                                procerror.FileName = xmlfile;
        //                                AddProcessingError(procerror);
        //                                thisResult.FolderLoc = Globals.FailLoc;
        //                                thisResult.Processed = false;
        //                                thisResult.FolderLoc = custProfile[0].C_path + "\\Processing\\";
        //                                remID.Value = AddTodo(custProfile[0].P_id, xmlfile, "Message send failed. Error message was: " + mailResult, DateTime.Now);
        //                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Message send failed. Error message was: " + mailResult);
        //                                thisResult.Processed = false;
        //                                thisResult.FolderLoc = custProfile[0].C_path + "\\Processing\\";
        //                                thisResult.FileName = xmlfile;

        //                            }
        //                            AddTransactionLog(custProfile[0].P_id, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via Email with the Result: " + mailResult, DateTime.Now, strSuccess, custProfile[0].P_BillTo, recipientID);
        //                            break;
        //                        }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            // No customer Profile found so will move to Root Path for Pickup.
        //            ProcessingErrors procerror = new ProcessingErrors();
        //            CTCErrorCode error = new CTCErrorCode();
        //            error.Code = NodeError.e100;
        //            error.Description = "Missing Customer Profile. Moving to pickup folder.";
        //            error.Severity = "Warning";
        //            procerror.ErrorCode = error;
        //            procerror.SenderId = senderID;
        //            procerror.RecipientId = recipientID;
        //            procerror.FileName = xmlfile;
        //            AddProcessingError(procerror);
        //            thisResult.FolderLoc = Globals.FailLoc;
        //            thisResult.FileName = xmlfile;

        //            CreateProfPath(getCustPath(senderID) + "\\Processing", recipientID + reasonCode);

        //            string cwxml = string.Empty;

        //            string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfile[0].C_path, "Processing", recipientID + reasonCode));
        //            if (f.StartsWith("Warning"))
        //            {
        //                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
        //            }
        //            else
        //            { xmlfile = f; }
        //            xmlfile = cwxml;


        //            thisResult.Processed = false;
        //            thisResult.FileName = xmlfile;

        //            thisResult.FolderLoc = custProfile[0].C_path + "\\Processing\\";
        //            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfile[0].C_name + " Does not have a Document Profile. Moving to Root Path" + "");
        //            AddTransactionLog(custProfile[0].P_id, senderID, xmlfile, "Customer " + custProfile[0].C_name + " Recipient found " + recipientID + ". Moving to Folder to retrieval", DateTime.Now, "Y", custProfile[0].P_BillTo, recipientID);
        //            // AddTransaction((Guid)custProfile[0].C_id, (Guid)custProfile[0].P_id, xmlfile, msgDir, true, trRef, "N/A");
        //        }
        //        return thisResult;
        //    }
        //    catch (System.InvalidOperationException ex)
        //    {
        //        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Cargowise File Failed: " + ex.Message);
        //        if (sqlConn.State == ConnectionState.Open)
        //        {
        //            sqlConn.Close();
        //            sqlConn.ConnectionString = Globals.connString();
        //            sqlConn.Open();
        //        }
        //        ProcessingErrors procerror = new ProcessingErrors();
        //        CTCErrorCode error = new CTCErrorCode();
        //        error.Code = NodeError.e111;
        //        error.Description = "Customer Exists but no profile Found.";
        //        error.Severity = "Warning";
        //        procerror.ErrorCode = error;
        //        procerror.SenderId = senderID;
        //        procerror.RecipientId = recipientID;
        //        procerror.FileName = xmlfile;
        //        AddProcessingError(procerror);
        //        thisResult.FolderLoc = Globals.FailLoc;
        //        thisResult.Processed = false;
        //        thisResult.FileName = xmlfile;

        //        return thisResult;

        //        // AddTransactionLog(Guid.Empty, senderID, xmlfile, "Processing Cargowise File Failed: " + ex.Message, DateTime.Now,"N");
        //    }
        //    catch (IOException)
        //    {
        //        ProcessingErrors procerror = new ProcessingErrors();
        //        CTCErrorCode error = new CTCErrorCode();
        //        error.Code = NodeError.e109;
        //        error.Description = "File I/O Error found.";
        //        error.Severity = "Warning";
        //        procerror.ErrorCode = error;
        //        procerror.SenderId = senderID;
        //        procerror.RecipientId = recipientID;
        //        procerror.FileName = xmlfile;
        //        AddProcessingError(procerror);
        //        thisResult.FolderLoc = Globals.FailLoc;
        //        thisResult.Processed = false;
        //        thisResult.FileName = xmlfile;

        //        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Process Cargowise XML. File Already Exists. Skipping Move");
        //        return thisResult;


        //    }
        //    catch (Exception ex)
        //    {
        //        ProcessingErrors procerror = new ProcessingErrors();
        //        CTCErrorCode error = new CTCErrorCode();
        //        error.Code = NodeError.e111;
        //        error.Description = "Cargowise XML Error Found.";
        //        error.Severity = "Warning";
        //        procerror.ErrorCode = error;
        //        procerror.SenderId = senderID;
        //        procerror.RecipientId = recipientID;
        //        procerror.FileName = xmlfile;
        //        AddProcessingError(procerror);
        //        thisResult.FolderLoc = Globals.FailLoc;
        //        thisResult.Processed = false;
        //        thisResult.FileName = xmlfile;

        //        string strEx = ex.GetType().Name;
        //        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Process Cargowise XML." + strEx + " Error Found: " + ex.Message + ". " + "", Color.Red, FontStyle.Bold);
        //        return thisResult;
        //    }

        //}

        private ProcessResult ProcessUDM(String xmlfile)
        {
            FileInfo processingFile = new FileInfo(xmlfile);
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            UniversalInterchange cwFile = new UniversalInterchange();
            using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
                cwFile = (UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            if (cwFile.Body.BodyField == null)
            {
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Cargowise File found but is not UDM. Attempting NDM: ");
                thisResult = ProcessNDM(xmlfile);
                return thisResult;
            }
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            cwRx++;
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());
            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            String eventCode = String.Empty;
            String msgDir = "";
            TransReference trRef = new TransReference();
            List<DataSource> dscoll = new List<DataSource>();
            DataContext dc = new DataContext();
            dc = cwFile.Body.BodyField.Shipment.DataContext;
            int iS = 0;
            int iB = 0;
            int iC = 0;
            int iO = 0;
            int iT = 0;
            int iL = 0;

            if (dc.DataSourceCollection != null)
            {
                try
                {
                    //dscoll = cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection.ToList();
                    dscoll = cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection.ToList();
                }
                catch (Exception)
                {

                    NodeResources.AddText(edLog, "XML File Missing DataSourceCollection tag");
                    ProcessingErrors procerror = new ProcessingErrors();
                    CTCErrorCode error = new CTCErrorCode();
                    error.Code = NodeError.e111;
                    error.Description = "DataSource Collection not found:";
                    error.Severity = "Fatal";
                    procerror.ErrorCode = error;
                    procerror.SenderId = senderID;
                    procerror.RecipientId = recipientID;
                    procerror.FileName = xmlfile;
                    procerror.ProcId = Guid.Empty;
                    AddProcessingError(procerror);
                    thisResult.FolderLoc = Globals.FailLoc;
                    thisResult.Processed = false;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }

                try
                {
                    foreach (DataSource ds in dscoll)
                    {
                        switch (ds.Type)
                        {
                            case "ForwardingShipment":
                                iS++;
                                break;
                            case "CustomsDeclaration":
                                iB++;
                                break;
                            case "ForwardingConsol":
                                iC++;
                                break;
                            case "WarehouseOrder":
                                iO++;
                                break;
                            case "LocalTransport":
                                iL++;
                                break;
                            case "TransportBooking":
                                iT++;
                                break;

                        }
                    }
                    if (iC > 0 | iS > 0 | iB > 0)
                    {
                        try
                        {
                            if (cwFile.Body.BodyField.Shipment.WayBillType != null)
                            {
                                switch (cwFile.Body.BodyField.Shipment.WayBillType.Code)
                                {
                                    case "MWB":

                                        trRef.Ref1Type = RefType.Master;
                                        break;
                                    case "HWB":
                                        trRef.Ref1Type = RefType.Housebill;
                                        break;
                                }
                                trRef.Reference1 = cwFile.Body.BodyField.Shipment.WayBillNumber;
                            }
                            else
                            {
                                trRef.Reference1 = cwFile.Body.BodyField.Shipment.AgentsReference;
                                trRef.Ref1Type = RefType.AgentRef;
                            }
                        }
                        catch (Exception)
                        {
                            ProcessingErrors procerror = new ProcessingErrors();
                            CTCErrorCode error = new CTCErrorCode();
                            error.Code = NodeError.e110;
                            error.Description = "Error Processing UDM File. Error locating WayBill Number:";
                            error.Severity = "Warning";
                            procerror.ErrorCode = error;
                            procerror.SenderId = senderID;
                            procerror.RecipientId = recipientID;
                            procerror.FileName = xmlfile;
                            AddProcessingError(procerror);
                            thisResult.FolderLoc = Globals.FailLoc;
                            thisResult.Processed = false;
                            thisResult.FileName = xmlfile;
                            return thisResult;
                        }
                    }
                    if (iC == 1 && iS == 0)
                    {
                        foreach (DataSource ds in dscoll)
                        {
                            if (ds.Type == "ForwardingConsol")
                            {
                                trRef.Ref2Type = RefType.Consol;
                                trRef.Reference2 = ds.Key;
                                if (cwFile.Body.BodyField.Shipment.SubShipmentCollection[0].WayBillType.Code == "HWB")
                                {
                                    trRef.Reference3 = cwFile.Body.BodyField.Shipment.SubShipmentCollection[0].WayBillNumber;
                                    trRef.Ref3Type = RefType.Housebill;
                                }
                            }
                        }
                    }
                    else if (iS > 1)
                    {
                        foreach (DataSource ds in dscoll)
                        {
                            if (ds.Type == "ForwardingConsol")
                            {
                                trRef.Ref2Type = RefType.Consol;
                                trRef.Reference2 = ds.Key;
                            }
                        }
                    }
                    else if (iB == 1 && iS == 0)
                    {
                        trRef.Ref2Type = RefType.Brokerage;
                        trRef.Reference2 = cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection[0].Key;
                        trRef.Reference3 = cwFile.Body.BodyField.Shipment.OwnerRef;
                        trRef.Ref3Type = RefType.Order;
                    }
                    else if (iS == 1)
                    {
                        trRef.Ref2Type = RefType.Shipment;

                        if (cwFile.Body.BodyField.Shipment.SubShipmentCollection != null)
                        {
                            trRef.Reference2 = cwFile.Body.BodyField.Shipment.SubShipmentCollection[0].DataContext.DataSourceCollection[0].Key;
                            if (cwFile.Body.BodyField.Shipment.SubShipmentCollection[0].WayBillType.Code == "HWB")
                            {
                                if (!string.IsNullOrEmpty(cwFile.Body.BodyField.Shipment.SubShipmentCollection[0].WayBillNumber))
                                {
                                    trRef.Reference3 = cwFile.Body.BodyField.Shipment.SubShipmentCollection[0].WayBillNumber;
                                    trRef.Ref3Type = RefType.Housebill;
                                }
                            }
                        }
                        else
                        {
                            trRef.Reference2 = dscoll[0].Key;
                            if (!string.IsNullOrEmpty(cwFile.Body.BodyField.Shipment.WayBillNumber))
                            {
                                trRef.Reference3 = cwFile.Body.BodyField.Shipment.WayBillNumber;
                                trRef.Ref3Type = RefType.Housebill;
                            }

                        }

                    }
                    else if (iO == 1)
                    {
                        if (string.IsNullOrEmpty(cwFile.Body.BodyField.Shipment.DataContext.DataSourceCollection[0].Key.ToString()))
                        {
                            trRef.Reference1 = "NEW";
                        }
                        trRef.Ref1Type = RefType.Order;
                        trRef.Ref2Type = RefType.Order;
                        trRef.Reference2 = cwFile.Body.BodyField.Shipment.Order.OrderNumber;
                        List<OrganizationAddress> orgColl = new List<OrganizationAddress>();
                        orgColl = cwFile.Body.BodyField.Shipment.OrganizationAddressCollection.ToList();
                        foreach (OrganizationAddress oa in orgColl)
                        {
                            if (oa.AddressType == "ConsignorDocumentaryAddress")
                            {
                                trRef.Reference3 = oa.OrganizationCode;
                            }
                        }
                    }

                }
                catch (Exception)
                {
                    ProcessingErrors procerror = new ProcessingErrors();
                    CTCErrorCode error = new CTCErrorCode();
                    error.Code = NodeError.e110;
                    error.Description = "Error Processing UDM File. Error locating WayBill Number:";
                    error.Severity = "Warning";
                    procerror.ErrorCode = error;
                    procerror.SenderId = senderID;
                    procerror.RecipientId = recipientID;
                    procerror.FileName = xmlfile;
                    AddProcessingError(procerror);
                    thisResult.FolderLoc = Globals.FailLoc;
                    thisResult.Processed = false;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }
            }
            else
            {
                if (dc.DataTargetCollection == null)
                {

                }
            }

            String archiveFile;

            try
            {
                cwRx++;
                NodeResources.AddLabel(lblCwRx, cwRx.ToString());
                senderID = cwFile.Header.SenderID;
                recipientID = cwFile.Header.RecipientID;
                if (cwFile.Body.BodyField.Shipment.DataContext.EventType != null)
                {
                    eventCode = cwFile.Body.BodyField.Shipment.DataContext.EventType.Code;
                }
                else
                {
                    eventCode = string.Empty;
                }
                if (cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose != null)
                {
                    reasonCode = cwFile.Body.BodyField.Shipment.DataContext.ActionPurpose.Code;
                }
                else
                {
                    reasonCode = string.Empty;
                }
                if (iL > 0)
                {
                    ProcessResult proc = new ProcessResult();
                    Globals.ArchiveFile(Globals.ArchLoc, xmlfile);

                    proc = ProcessTransportImport(xmlfile);
                    return (proc);
                }
                if (iT > 0)
                {
                    ProcessResult proc = new ProcessResult();
                    Globals.ArchiveFile(Globals.ArchLoc, xmlfile);
                    proc = ProcessTransportBookingImport(xmlfile);
                    return (proc);
                }
                CustProfileRecord custProfileRecord = new CustProfileRecord();
                custProfileRecord = GetCustomerProfile(senderID, recipientID, reasonCode, eventCode);

                SqlCommand remTodo = new SqlCommand("DELETE FROM TODO WHERE L_ID =@ID", sqlConn);
                SqlParameter remID = remTodo.Parameters.Add("@ID", SqlDbType.UniqueIdentifier);
                if (custProfileRecord != null)
                {
                    //Is Client Active and not on hold
                    if (custProfileRecord.C_is_active == "N" | custProfileRecord.C_on_hold == "Y")
                    {

                        string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfileRecord.C_path, "Held"));
                        if (f.StartsWith("Warning"))
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                        }
                        else
                        { xmlfile = f; }
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_name + " is not Active or is on Hold. Moving to Hold and skipping" + "");
                        thisResult.Processed = true;
                        thisResult.FileName = xmlfile;
                        return thisResult;
                    }
                    else
                    {
                        if (sqlConn.State == ConnectionState.Open)
                        {
                            sqlConn.Close();
                        }
                        CreateProfPath(custProfileRecord.C_path + "\\Processing", recipientID + reasonCode);
                        //Send file to Archive folder
                        archiveFile = Globals.ArchiveFile(custProfileRecord.C_path + "\\Archive", xmlfile);

                        string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfileRecord.C_path, "Processing", recipientID + reasonCode));
                        if (f.StartsWith("Warning"))
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                        }
                        else
                        { xmlfile = f; }
                        remID.Value = AddTodo(custProfileRecord.P_id, xmlfile, "Add_Transaction", DateTime.Now);
                        AddTransaction(custProfileRecord.C_id, custProfileRecord.P_id, Path.GetFileName(xmlfile), msgDir, true, trRef, archiveFile);
                        if (custProfileRecord.P_DTS)
                        {
                            remID.Value = AddTodo(custProfileRecord.P_id, xmlfile, "DTS_Processing", DateTime.Now);
                            DTS(custProfileRecord.P_id, xmlfile);
                        }
                        if (custProfileRecord.P_MsgType.Trim() == "Conversion")
                        {
                            // Rename the Cargowise XML File to the new Global XML
                            //TODO: Check the Accounting issue here. Doubling Accounting charge.
                            thisResult = ConvertCWFile(custProfileRecord, xmlfile);
                            AddTransaction(custProfileRecord.C_id, custProfileRecord.P_id, Path.GetFileName(xmlfile), msgDir, true, trRef, archiveFile);
                        }
                        //Create the Delete from TODO list ready to be removed
                        String ftpSend = String.Empty;
                        String strSuccess = "N";
                        string result = "";
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Profile : " + custProfileRecord.P_description);
                        switch (custProfileRecord.P_delivery)
                        {
                            case ("R"):
                                {
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_name + " Recipient found " + recipientID + ". Moving to Folder");
                                    AddTransactionLog(custProfileRecord.P_id, senderID, xmlfile, "Customer " + custProfileRecord.C_name + " Recipient found " + recipientID + ". Moving to Folder to retrieval", DateTime.Now, "Y", custProfileRecord.P_BillTo, recipientID);
                                    result = "File Saved";
                                    NodeResources.MoveFile(xmlfile, Path.Combine(custProfileRecord.C_path, "Processing"));
                                    thisResult.Processed = true;
                                    thisResult.FileName = null;
                                    break;
                                }
                            case ("D"):
                                {
                                    if (File.Exists(xmlfile))
                                    {
                                        try
                                        {
                                            string ftpResult = NodeResources.FtpSend(xmlfile, custProfileRecord);
                                            if (ftpResult.Contains("File Sent Ok"))
                                            {
                                                if (sqlConn.State == ConnectionState.Open)
                                                    sqlConn.Close();
                                                sqlConn.Open();
                                                remTodo.ExecuteNonQuery();
                                                sqlConn.Close();
                                                filesTx++;
                                                NodeResources.AddLabel(lblFilesTx, filesTx.ToString());
                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "File " + Path.GetFileName(xmlfile) + " Sent. Response: " + ftpSend);
                                                // AddTransaction((Guid)custProfileRecord.C_id, (Guid)custProfileRecord.P_id, Path.GetFileName(xmlfile), msgDir, false, trRef, archiveFile);
                                                strSuccess = "Y";
                                                result = ftpResult;
                                                try
                                                {
                                                    System.GC.Collect();
                                                    System.GC.WaitForPendingFinalizers();
                                                    File.Delete(xmlfile);
                                                }
                                                catch (Exception ex)
                                                {
                                                    result = "Failed: " + ex.Message;
                                                }

                                            }
                                            else
                                            {
                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Transfer Result is invalid: ");
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            remID.Value = AddTodo(custProfileRecord.P_id, xmlfile, ftpSend, DateTime.Now);
                                            ProcessingErrors procerror = new ProcessingErrors();
                                            procerror.ErrorCode.Code = NodeError.e104;
                                            procerror.SenderId = senderID;
                                            procerror.RecipientId = recipientID;
                                            procerror.ErrorCode.Description = "Cargowise FTP Send Error Found: " + ex.Message + ". ";
                                            procerror.FileName = xmlfile;
                                            procerror.ProcId = custProfileRecord.P_id;
                                            AddProcessingError(procerror);
                                            thisResult.FolderLoc = custProfileRecord.C_path + "\\Processing\\";
                                            thisResult.Processed = false;
                                            thisResult.FileName = xmlfile;
                                            //AddTransactionLog(Guid.Empty, senderID, xmlfile, "Customer Found but no Profile found. Move to Failed Location for retry.", DateTime.Now, "Y", senderID, recipientID);
                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Cargowise FTP Send Error Found: " + ex.Message + ". " + "");
                                            AddTransactionLog(custProfileRecord.P_id, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via FTP with the Result: " + ftpSend, DateTime.Now, strSuccess, custProfileRecord.P_BillTo, recipientID);
                                            String err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                                            return thisResult;
                                        }
                                        finally
                                        {
                                            sqlConn.Close();
                                        }
                                    }
                                    break;
                                }
                            case ("C"):
                                {
                                    try
                                    {
                                        remID.Value = AddTodo(custProfileRecord.P_id, xmlfile, "Added to todo List", DateTime.Now);
                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_name + " Recipient found " + recipientID + ".Sending file via CTC Adapter" + "");
                                        String sendlog = CTCAdapter.SendMessage(custProfileRecord.P_server, xmlfile, recipientID, custProfileRecord.P_username, custProfileRecord.P_password);
                                        // Test without sending

                                        //String sendlog = "Send OK";
                                        if (sendlog.Contains("SEND OK"))
                                        {
                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Transmitted Ok. ");
                                            if (sqlConn.State == ConnectionState.Open)
                                            {
                                                sqlConn.Close();
                                            }
                                            sqlConn.ConnectionString = Globals.connString();
                                            sqlConn.Open();
                                            remTodo.ExecuteNonQuery();
                                            sqlConn.Close();
                                            cwTx++;
                                            NodeResources.AddLabel(lblCWTx, cwTx.ToString());
                                            strSuccess = "Y";
                                            AddTransaction(custProfileRecord.C_id, custProfileRecord.P_id, Path.GetFileName(xmlfile), msgDir, false, trRef, archiveFile);
                                            System.GC.Collect();
                                            System.GC.WaitForPendingFinalizers();
                                            File.Delete(xmlfile);
                                            result = "CTC Adapter send Ok";
                                        }
                                        else
                                        {
                                            result = "CTC Adapter send Failed: " + sendlog;
                                            ProcessingErrors procerror = new ProcessingErrors();
                                            procerror.ErrorCode.Code = NodeError.e104;
                                            procerror.SenderId = senderID;
                                            procerror.RecipientId = recipientID;
                                            procerror.ErrorCode.Description = "CTC Adapter send Failed: " + sendlog;
                                            procerror.ProcId = custProfileRecord.P_id;
                                            procerror.FileName = xmlfile;
                                            AddProcessingError(procerror);
                                            thisResult.FolderLoc = Globals.FailLoc;
                                            thisResult.Processed = false;
                                            thisResult.FileName = xmlfile;
                                        }
                                        AddTransactionLog(custProfileRecord.P_id, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via CTC Adapter with the Result: " + sendlog, DateTime.Now, strSuccess, custProfileRecord.P_BillTo, recipientID);
                                    }
                                    catch (Exception ex)
                                    {
                                        if (sqlConn.State == ConnectionState.Open)
                                        {
                                            sqlConn.Close();
                                        }
                                        ProcessingErrors procerror = new ProcessingErrors();
                                        procerror.ErrorCode.Code = NodeError.e104;
                                        procerror.SenderId = senderID;
                                        procerror.RecipientId = recipientID;
                                        procerror.ErrorCode.Description = "CTC Adapter send Failed: " + ex.Message;
                                        procerror.FileName = xmlfile;
                                        procerror.ProcId = custProfileRecord.P_id;
                                        AddProcessingError(procerror);
                                        thisResult.FolderLoc = Globals.FailLoc;
                                        thisResult.Processed = false;
                                        thisResult.FileName = xmlfile;
                                        AddTransactionLog(custProfileRecord.P_id, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via CTC Adapter Failed with the Result: " + ex.Message, DateTime.Now, strSuccess, custProfileRecord.P_BillTo, recipientID);
                                        result = "CTC Adapter send failed: " + ex.Message;
                                    }
                                }
                                break;
                            case "E":
                                {
                                    remID.Value = AddTodo(custProfileRecord.P_id, xmlfile, "Added to todo List", DateTime.Now);
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_name + " Recipient found " + recipientID + ".Sending file via Email" + "");
                                    string mailResult = string.Empty;
                                    mailResult = MailModule.sendMsg(xmlfile, custProfileRecord.P_EmailAddress, custProfileRecord.P_Subject, "Cargowise File Attached");
                                    // mailResult = "Message Sent OK";

                                    if (mailResult == "Message Sent OK")
                                    {
                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Transmitted Ok. ");
                                        sqlConn.ConnectionString = Globals.connString(); sqlConn.Open();
                                        remTodo.ExecuteNonQuery();
                                        sqlConn.Close();
                                        cwTx++;
                                        NodeResources.AddLabel(lblCWTx, cwTx.ToString());
                                        strSuccess = "Y";
                                        AddTransaction(custProfileRecord.C_id, custProfileRecord.P_id, Path.GetFileName(xmlfile), msgDir, false, trRef, archiveFile);
                                        result = mailResult;
                                        thisResult.Processed = true;
                                        thisResult.FileName = xmlfile;
                                    }
                                    else
                                    {
                                        remID.Value = AddTodo(custProfileRecord.P_id, xmlfile, "Message send failed. Error message was: " + mailResult, DateTime.Now);
                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Message send failed. Error message was: " + mailResult);
                                        result = "Email send failed: " + mailResult;
                                        ProcessingErrors procerror = new ProcessingErrors();
                                        procerror.ErrorCode.Code = NodeError.e105;
                                        procerror.SenderId = senderID;
                                        procerror.RecipientId = recipientID;
                                        procerror.ErrorCode.Description = "Email message failed to send " + mailResult;
                                        procerror.ProcId = custProfileRecord.P_id;
                                        procerror.FileName = xmlfile;
                                        AddProcessingError(procerror);
                                        thisResult.FolderLoc = custProfileRecord.C_path + "\\Processing\\";
                                        thisResult.Processed = false;
                                        thisResult.FileName = xmlfile;

                                    }
                                    AddTransactionLog(custProfileRecord.P_id, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via Email with the Result: " + mailResult, DateTime.Now, strSuccess, custProfileRecord.P_BillTo, recipientID);
                                    break;
                                }

                        }
                        addSummRecord(DateTime.Now, recipientID, senderID, custProfileRecord.P_MsgType, trRef, xmlfile, result);

                    }

                }
                else
                {
                    // No customer Profile found so will move to Root Path for Pickup. 
                    CreateProfPath(getCustPath(senderID) + "\\Processing", recipientID + reasonCode);
                    string cwxml = string.Empty;
                    string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfileRecord.C_path, "Processing", recipientID + reasonCode));
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlfile = f; }
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_name + " Does not have a Document Profile. Moving to Root Path" + "");
                    AddTransactionLog(custProfileRecord.P_id, senderID, xmlfile, "Customer " + custProfileRecord.C_name + " Recipient found " + recipientID + ". Moving to Folder to retrieval", DateTime.Now, "Y", custProfileRecord.P_BillTo, recipientID);
                    AddTransaction(custProfileRecord.C_id, custProfileRecord.P_id, Path.GetFileName(xmlfile), msgDir, true, trRef, "N/A");
                    addSummRecord(DateTime.Now, recipientID, senderID, "Missing Profile", trRef, xmlfile, "Profile not found");
                    ProcessingErrors procerror = new ProcessingErrors();
                    procerror.ErrorCode.Code = NodeError.e100;
                    procerror.SenderId = senderID;
                    procerror.RecipientId = recipientID;
                    procerror.ErrorCode.Description = "Customer Profile Not Found. Move to Retry folder";
                    procerror.ProcId = custProfileRecord.P_id;
                    procerror.FileName = xmlfile;
                    AddProcessingError(procerror);
                    thisResult.FolderLoc = Globals.FailLoc;
                    thisResult.Processed = false;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }
                return thisResult;
            }
            catch (System.InvalidOperationException ex)
            {
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Processing Cargowise File Failed: " + ex.Message);
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                    sqlConn.ConnectionString = Globals.connString();
                    sqlConn.Open();
                }
                String err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                ProcessingErrors procerror = new ProcessingErrors();
                procerror.ErrorCode.Code = NodeError.e111;
                procerror.SenderId = senderID;
                procerror.RecipientId = recipientID;
                procerror.ErrorCode.Description = "Processing Cargowise File Failed: " + ex.Message;
                procerror.FileName = xmlfile;
                AddProcessingError(procerror);
                thisResult.FolderLoc = Globals.FailLoc;
                thisResult.FileName = xmlfile;
                thisResult.Processed = false;
                return thisResult;

            }
            catch (IOException ex)
            {
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Process Cargowise XML Error Found: " + ex.Message + ". " + "");
                String err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                ProcessingErrors procerror = new ProcessingErrors();
                procerror.ErrorCode.Code = NodeError.e109;
                procerror.SenderId = senderID;
                procerror.RecipientId = recipientID;
                procerror.ErrorCode.Description = "Processing Cargowise File Failed: " + ex.Message;
                procerror.FileName = xmlfile;
                AddProcessingError(procerror);
                thisResult.FolderLoc = Globals.FailLoc;
                thisResult.Processed = false;
                thisResult.FileName = xmlfile;
                return thisResult;
            }
            catch (Exception ex)
            {
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Process Cargowise XML Error Found: " + ex.Message + ". " + "");
                String err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                ProcessingErrors procerror = new ProcessingErrors();
                procerror.ErrorCode.Code = NodeError.e111;
                procerror.SenderId = senderID;
                procerror.RecipientId = recipientID;
                procerror.ErrorCode.Description = "Processing Cargowise File Failed: " + ex.Message;
                procerror.FileName = xmlfile;
                AddProcessingError(procerror);
                thisResult.FolderLoc = Globals.FailLoc;
                thisResult.Processed = false;
                thisResult.FileName = xmlfile;
                return thisResult;
            }

        }

        private CustProfileRecord GetCustomerProfile(string senderID, string recipientID, string reasonCode, string eventCode)
        {
            CustProfileRecord result = new CustProfileRecord();

            SqlCommand sqlCustProfile = new SqlCommand("Select C_ID, P_ID, C_NAME, C_IS_ACTIVE, C_ON_HOLD, C_PATH, P_EVENTCODE, " +
                                                             "C_FTP_CLIENT, P_REASONCODE, P_SERVER, P_USERNAME, P_DELIVERY, P_METHOD, P_PARAMLIST," +
                                                             "P_PATH, P_PASSWORD, P_MSGTYPE, P_Recipientid, P_SENDERID, P_EMAILADDRESS, P_SUBJECT," +
                                                             "P_DESCRIPTION, P_BILLTO, P_PORT, P_DIRECTION, C_CODE, P_DTS, P_ACTIVE, P_SSL " +
                                                             "from vw_CustomerProfile WHERE P_SENDERID = @SENDERID and P_Recipientid= @RECIPIENTID and P_ACTIVE='Y' " +
                                                             "Order by P_REASONCODE desc, P_EVENTCODE DESC ", sqlConn);

            sqlCustProfile.Parameters.AddWithValue("@SENDERID", senderID);
            sqlCustProfile.Parameters.AddWithValue("@RECIPIENTID", recipientID);

            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            sqlConn.ConnectionString = Globals.connString();
            sqlConn.Open();
            List<CustProfileRecord> custProfile = new List<CustProfileRecord>();
            using (SqlDataReader drCustProfile = sqlCustProfile.ExecuteReader(CommandBehavior.CloseConnection))
            {
                if (drCustProfile.HasRows)
                {
                    while (drCustProfile.Read())
                    {
                        result = new CustProfileRecord();
                        result.C_id = (Guid)drCustProfile["C_ID"];
                        result.P_id = (Guid)drCustProfile["P_ID"];
                        result.C_name = drCustProfile["C_NAME"].ToString().Trim();
                        result.C_is_active = drCustProfile["C_IS_ACTIVE"].ToString().Trim();
                        result.C_on_hold = drCustProfile["C_ON_HOLD"].ToString().Trim();
                        result.C_path = drCustProfile["C_PATH"].ToString().Trim();
                        result.C_ftp_client = drCustProfile["C_FTP_CLIENT"].ToString().Trim();
                        result.P_reasoncode = drCustProfile["P_REASONCODE"].ToString().Trim();
                        result.P_EventCode = drCustProfile["P_EVENTCODE"].ToString().Trim();
                        result.P_server = drCustProfile["P_SERVER"].ToString().Trim();
                        result.P_username = drCustProfile["P_USERNAME"].ToString().Trim();
                        result.P_delivery = drCustProfile["P_DELIVERY"].ToString().Trim();
                        result.P_password = drCustProfile["P_PASSWORD"].ToString().Trim();
                        result.P_Recipientid = drCustProfile["P_Recipientid"].ToString().Trim();
                        result.P_Senderid = drCustProfile["P_SENDERID"].ToString().Trim();
                        result.P_description = drCustProfile["P_DESCRIPTION"].ToString().Trim();
                        result.P_path = drCustProfile["P_PATH"].ToString().Trim();
                        result.P_port = drCustProfile["P_PORT"].ToString().Trim();
                        result.C_code = drCustProfile["C_CODE"].ToString().Trim();
                        result.P_BillTo = drCustProfile["P_BILLTO"].ToString().Trim();
                        result.P_Direction = drCustProfile["P_DIRECTION"].ToString().Trim();
                        result.P_MsgType = drCustProfile["P_MSGTYPE"].ToString().Trim();
                        result.P_Subject = drCustProfile["P_SUBJECT"].ToString().Trim();
                        result.P_EmailAddress = drCustProfile["P_EMAILADDRESS"].ToString().Trim();
                        result.P_Method = drCustProfile["P_METHOD"].ToString().Trim();
                        result.P_Params = drCustProfile["P_PARAMLIST"].ToString().Trim();
                        if (drCustProfile["P_SSL"].ToString().Trim() == "Y")
                        {
                            result.P_ssl = true;
                        }
                        else
                        {
                            result.P_ssl = false;
                        }

                        if (drCustProfile["P_DTS"].ToString().Trim() == "Y")
                        {
                            result.P_DTS = true;
                        }
                        else
                        {
                            result.P_DTS = false;
                        }
                        custProfile.Add(result);
                    }
                }
                else
                {
                    result = new CustProfileRecord();
                    SqlCommand execAdd = new SqlCommand();
                    execAdd.Connection = sqlConn;
                    execAdd.CommandText = "Add_Profile";
                    execAdd.CommandType = CommandType.StoredProcedure;
                    SqlParameter PC = execAdd.Parameters.Add("@P_C", SqlDbType.UniqueIdentifier);
                    PC.Value = getCustID(senderID);
                    SqlParameter PReasonCode = execAdd.Parameters.Add("@P_REASONCODE", SqlDbType.Char, 3);
                    PReasonCode.Value = reasonCode.ToUpper();
                    SqlParameter PEventCode = execAdd.Parameters.Add("@P_EVENTCODE", SqlDbType.Char, 3);
                    PEventCode.Value = eventCode.ToUpper();
                    SqlParameter PServer = execAdd.Parameters.Add("@P_SERVER", SqlDbType.VarChar, 50);
                    PServer.Value = "";
                    SqlParameter PUsername = execAdd.Parameters.Add("@P_USERNAME", SqlDbType.VarChar, 50);
                    PUsername.Value = "";
                    SqlParameter PPath = execAdd.Parameters.Add("@P_PATH", SqlDbType.VarChar, 100);
                    PPath.Value = "";
                    SqlParameter PPassword = execAdd.Parameters.Add("@P_PASSWORD", SqlDbType.VarChar, 50);
                    PPassword.Value = "";
                    string sDelivery = String.Empty;
                    sDelivery = "R";
                    SqlParameter pDelivery = execAdd.Parameters.Add("@P_DELIVERY", SqlDbType.Char, 1);
                    pDelivery.Value = sDelivery;
                    SqlParameter pMSGType = execAdd.Parameters.Add("@MSGTYPE", SqlDbType.VarChar, 20);
                    pMSGType.Value = "Original";
                    SqlParameter pBillType = execAdd.Parameters.Add("@BILLTO", SqlDbType.VarChar, 15);
                    pBillType.Value = senderID;
                    SqlParameter pChargeable = execAdd.Parameters.Add("@CHARGEABLE", SqlDbType.Char, 1);
                    pChargeable.Value = "Y";
                    SqlParameter PPort = execAdd.Parameters.Add("@P_PORT", SqlDbType.Char, 10);
                    PPort.Value = "";
                    SqlParameter PDescription = execAdd.Parameters.Add("@P_DESCRIPTION", SqlDbType.VarChar, 100);
                    PDescription.Value = "No Transport Profile found. Store in Folder";
                    SqlParameter PRecipientId = execAdd.Parameters.Add("@P_Recipientid", SqlDbType.VarChar, 15);
                    PRecipientId.Value = recipientID;
                    SqlParameter PSenderID = execAdd.Parameters.Add("P_SENDERID", SqlDbType.VarChar, 15);
                    PSenderID.Value = senderID;
                    SqlParameter p_id = execAdd.Parameters.Add("@R_ID", SqlDbType.UniqueIdentifier);
                    SqlParameter PDirection = execAdd.Parameters.Add("@P_DIRECTION", SqlDbType.Char, 1);
                    PDirection.Value = "R";
                    p_id.Direction = ParameterDirection.Output;
                    SqlParameter r_Action = execAdd.Parameters.Add("@R_ACTION", SqlDbType.Char, 1);
                    r_Action.Direction = ParameterDirection.Output;
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                    sqlConn.Open();
                    execAdd.ExecuteNonQuery();
                    sqlConn.Close();
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Create New Profile Sender ID: " + senderID + " Recipient ID: " + recipientID + " Purpose Code: " + reasonCode + " Event Code:" + eventCode);

                }
                foreach (CustProfileRecord cp in custProfile)
                {
                    if (cp.P_reasoncode == reasonCode)
                    {
                        if (cp.P_EventCode == eventCode)
                        {
                            result = cp;
                            break;
                        }
                        else if (cp.P_EventCode == "*")
                        {
                            result = cp;
                            break;
                        }
                    }
                    else if (cp.P_reasoncode == "*")
                    {
                        if (cp.P_EventCode == eventCode)
                        {
                            result = cp;
                            break;
                        }
                        else if (cp.P_EventCode == "*")
                        {
                            result = cp;
                            break;
                        }
                    }

                }

            }
            return result;
        }


        private ProcessResult ProcessTransportBookingImport(string xmlfile)
        {
            SqlConnection sqlTransConn = new SqlConnection();
            sqlTransConn.ConnectionString = Globals.connTransString();
            ProcessResult result = new ProcessResult();
            FileInfo processingFile = new FileInfo(xmlfile);
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = true;
            UniversalInterchange cwFile = new UniversalInterchange();
            using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
            {
                XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
                cwFile = (UniversalInterchange)cwConvert.Deserialize(fStream);
                fStream.Close();
            }
            result.FileName = xmlfile;
            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            String eventCode = String.Empty;
            int legNo = 0;
            TransReference trRef = new TransReference();
            List<DataSource> dscoll = new List<DataSource>();
            DataContext dc = new DataContext();
            dc = cwFile.Body.BodyField.Shipment.DataContext;
            List<DataSource> dsColl = new List<DataSource>();
            dsColl = dc.DataSourceCollection.ToList();
            string jobNo = string.Empty;
            bool tJob = false;
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Cargowise Transport Booking Job Found. Importing");
            foreach (DataSource ds in dsColl)
            {
                switch (ds.Type)
                {
                    case "TransportBooking":
                        jobNo = ds.Key;
                        tJob = true;
                        break;
                }
            }
            if (tJob)
            {
                try
                {
                    UniversalInterchangeHeader header = new UniversalInterchangeHeader();
                    header = cwFile.Header;
                    senderID = header.SenderID;
                    SqlCommand sqlValCust = new SqlCommand("ISVALIDCustomer", sqlTransConn);
                    sqlValCust.CommandType = CommandType.StoredProcedure;
                    SqlParameter oCode = sqlValCust.Parameters.Add("@O_CODE", SqlDbType.NChar, 10);
                    SqlParameter isValid = sqlValCust.Parameters.Add("@ISVALID", SqlDbType.Bit);
                    SqlParameter oid = sqlValCust.Parameters.Add("@O_ID", SqlDbType.UniqueIdentifier);
                    isValid.Direction = ParameterDirection.Output;
                    oid.Direction = ParameterDirection.Output;
                    oCode.Value = senderID;
                    if (sqlTransConn.State == ConnectionState.Open)
                    {
                        sqlTransConn.Close();
                    }
                    sqlTransConn.Open();
                    sqlValCust.ExecuteNonQuery();


                    if ((bool)isValid.Value)
                    {
                        OrganizationAddress legOrg = new OrganizationAddress();
                        int packingLineLink = 0;
                        int containerLink = 0;
                        int delPack = 0;
                        string delUOM = string.Empty;
                        decimal delWgt = 0;
                        decimal delVol = 0;
                        string dropMode = string.Empty;
                        string jobType = string.Empty;
                        string legType = string.Empty;
                        string shipRef = string.Empty;
                        string descr = string.Empty;
                        string haz = "N";
                        string containerNumber = string.Empty;
                        string containerMode = "N/A";
                        string transportMode = string.Empty;
                        string trnTemplate = string.Empty;
                        string estDelDate = string.Empty;
                        string requiredDelivery = string.Empty;
                        int totPcs = 0;
                        decimal totWgt = 0;
                        decimal totVol = 0;

                        foreach (CNodeBE.Shipment shp in cwFile.Body.BodyField.Shipment.SubShipmentCollection)
                        {
                            foreach (DataSource ds in shp.DataContext.DataSourceCollection)
                            {
                                if (ds.Type == "TransportBooking")
                                {
                                    jobNo = ds.Key;
                                    transportMode = shp.TransportMode.Code;
                                    legType = shp.TransportBookingDirection.Code;
                                    trnTemplate = shp.LocalTransportJobType.Code.Substring(0, 2);
                                    if (shp.LocalTransportJobType.Code.StartsWith("E"))
                                    {
                                        jobType = "PICKUP";
                                        switch (trnTemplate)
                                        {
                                            case "EF": //FCL
                                                break;
                                            case "EL": //LCL
                                                break;
                                            case "EE": //Empty
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        jobType = "DELIVERY";
                                        switch (trnTemplate)
                                        {
                                            case "IF": //FCL
                                                break;
                                            case "IL": //LCL
                                                break;
                                            case "IE": //Empty
                                                break;
                                        }
                                    }
                                    //Get Reference Shipment
                                    delPack = shp.OuterPacks;
                                    try
                                    {
                                        dropMode = shp.LocalTransportEquipmentNeeded.Code;
                                    }
                                    catch
                                    { }


                                    descr = shp.GoodsDescription;
                                    if (shp.IsHazardous)
                                    {
                                        haz = "Y";
                                    }
                                    foreach (PackingLine pl in cwFile.Body.BodyField.Shipment.PackingLineCollection)
                                    {
                                        totPcs += pl.PackQty;
                                        totVol += pl.Volume;
                                        totWgt += pl.Weight;
                                        delUOM = pl.PackType.Code;
                                    }

                                    shipRef = GetRef(shp.AdditionalReferenceCollection.AdditionalReference.ToList(), "BPR");
                                    SqlCommand addJob = new SqlCommand("CreateJob", sqlTransConn);
                                    addJob.CommandType = CommandType.StoredProcedure;
                                    SqlParameter O_Cust = addJob.Parameters.AddWithValue("@O_CUST", oid.Value);
                                    SqlParameter ItemCount = addJob.Parameters.AddWithValue("@items", delPack);
                                    SqlParameter TJobno = addJob.Parameters.AddWithValue("@TJobno", jobNo);
                                    SqlParameter DropMode = addJob.Parameters.AddWithValue("@DropMode", dropMode);
                                    SqlParameter TotalPcs = addJob.Parameters.AddWithValue("@TotalPcs", totPcs);
                                    SqlParameter TotalWgt = addJob.Parameters.AddWithValue("@TotalWgt", totWgt);
                                    SqlParameter TotalVol = addJob.Parameters.AddWithValue("@TotalVol", totVol);
                                    SqlParameter JobType = addJob.Parameters.AddWithValue("@JobType", jobType);
                                    SqlParameter contMode = addJob.Parameters.AddWithValue("@CONTMODE", containerMode);
                                    SqlParameter jobReference = addJob.Parameters.AddWithValue("@REFERENCE", shipRef);
                                    SqlParameter jobstatus = addJob.Parameters.AddWithValue("@STATUS", "Pending");
                                    SqlParameter DelDate = addJob.Parameters.AddWithValue("@DelDate", estDelDate);
                                    SqlParameter Description = addJob.Parameters.AddWithValue("@DESCRIPTION", descr);
                                    SqlParameter Hazardous = addJob.Parameters.AddWithValue("@HAZARDOUS", haz);
                                    SqlParameter uOM = addJob.Parameters.AddWithValue("@uOM", delUOM);
                                    SqlParameter jid = addJob.Parameters.Add("@ID", SqlDbType.UniqueIdentifier);
                                    jid.Direction = ParameterDirection.Output;
                                    if (sqlTransConn.State == ConnectionState.Open)
                                    {
                                        sqlTransConn.Close();
                                    }
                                    sqlTransConn.Open();
                                    addJob.ExecuteNonQuery();


                                    List<PackingLine> plColl = new List<PackingLine>();
                                    plColl = cwFile.Body.BodyField.Shipment.PackingLineCollection.ToList();
                                    //Loop through each of the instruction Types (ie MLT has many PIC and DLV in one job. )
                                    Guid addressID = new Guid();
                                    foreach (ShipmentInstruction instruction in shp.InstructionCollection)
                                    {
                                        int legPcs = 0;
                                        decimal legWgt = 0;
                                        decimal legVol = 0;
                                        dropMode = string.Empty;
                                        dropMode = instruction.DropMode.Code;
                                        legNo = instruction.Sequence;
                                        legType = instruction.Type.Code;

                                        switch (instruction.Type.Code)
                                        {
                                            case "DLV":

                                                legOrg = GetTransportAddressOrg(shipRef, cwFile.Body.BodyField.Shipment, "ConsignorPickupDeliveryAddress");
                                                addressID = GetOrgAddress(legOrg, (Guid)oid.Value);
                                                //                                        dropMode = instruction.DropMode.Code;

                                                if (instruction.InstructionPackingLineLinkCollection != null)
                                                {
                                                    foreach (ShipmentInstructionInstructionPackingLineLink pl in instruction.InstructionPackingLineLinkCollection)
                                                    {
                                                        packingLineLink = pl.PackingLineLink;
                                                        legPcs = plColl[packingLineLink].PackQty;
                                                        legVol = plColl[packingLineLink].Volume;
                                                        legWgt = plColl[packingLineLink].Weight;

                                                        foreach (Confirmation conf in pl.ConfirmationCollection)
                                                        {
                                                            if (conf.DateDescription == "DLV")
                                                            {
                                                                estDelDate = conf.EstimatedDate;
                                                                requiredDelivery = conf.RequiredToDate;
                                                            }

                                                        }

                                                    }
                                                }
                                                legType = "DELIVERY";

                                                break;
                                            case "MLT":
                                                legOrg = instruction.Address;
                                                dropMode = instruction.DropMode.Code;
                                                if (instruction.InstructionPackingLineLinkCollection != null)
                                                {
                                                    foreach (ShipmentInstructionInstructionPackingLineLink pl in instruction.InstructionPackingLineLinkCollection)
                                                    {
                                                        packingLineLink = pl.PackingLineLink;
                                                        foreach (Confirmation conf in pl.ConfirmationCollection)
                                                        {
                                                            if (conf.LegLink == instruction.Sequence)
                                                            {

                                                                switch (conf.DateDescription)
                                                                {
                                                                    case "PIC":
                                                                        estDelDate = conf.EstimatedDate;
                                                                        break;

                                                                    case "DLV":
                                                                        estDelDate = conf.EstimatedDate;
                                                                        break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                legType = "MULTI";
                                                break;
                                            case "PIC":
                                                legOrg = GetTransportAddressOrg(shipRef, cwFile.Body.BodyField.Shipment, "ConsigneePickupDeliveryAddress");
                                                addressID = GetOrgAddress(legOrg, (Guid)oid.Value);
                                                dropMode = instruction.DropMode.Code;
                                                if (instruction.InstructionPackingLineLinkCollection != null)
                                                {
                                                    foreach (ShipmentInstructionInstructionPackingLineLink pl in instruction.InstructionPackingLineLinkCollection)
                                                    {
                                                        packingLineLink = pl.PackingLineLink;

                                                        foreach (Confirmation conf in pl.ConfirmationCollection)
                                                        {
                                                            if (conf.DateDescription == "PIC")
                                                            {
                                                                estDelDate = conf.EstimatedDate;
                                                                requiredDelivery = conf.RequiredFromDate;
                                                            }

                                                        }
                                                    }
                                                }
                                                legType = "PICKUP";
                                                break;
                                        }



                                        try
                                        {

                                            if (instruction.InstructionContainerLinkCollection != null)
                                            {

                                                foreach (ShipmentInstructionInstructionContainerLink cl in instruction.InstructionContainerLinkCollection)
                                                {
                                                    containerLink = cl.ContainerLink;
                                                }

                                                ShipmentContainerCollection containerColl = new ShipmentContainerCollection();
                                                foreach (Container cn in cwFile.Body.BodyField.Shipment.ContainerCollection.Container)
                                                {
                                                    if (cn.Link == containerLink)
                                                    {
                                                        containerNumber = cn.ContainerNumber;

                                                    }
                                                }
                                            }

                                        }
                                        catch (Exception ex)
                                        {
                                            string strEx = ex.GetType().Name;
                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error Found. Transport Job (" + jobNo + ") Error in Instruction Collection.", Color.Red);
                                            string f = NodeResources.MoveFile(xmlfile, Globals.FailLoc);
                                            if (f.StartsWith("Warning"))
                                            {
                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                                            }
                                            else
                                            { xmlfile = f; }
                                            // xmlfile = MoveFile(xmlfile, Globals.FailLoc);

                                        }

                                        SqlCommand addLeg = new SqlCommand("AddTransportLeg", sqlTransConn);
                                        addLeg.CommandType = CommandType.StoredProcedure;
                                        addLeg.Parameters.AddWithValue("@JID", jid.Value);
                                        addLeg.Parameters.AddWithValue("@AID", addressID);
                                        addLeg.Parameters.AddWithValue("@items", delPack);
                                        addLeg.Parameters.AddWithValue("@TJobno", jobNo);
                                        addLeg.Parameters.AddWithValue("@DropMode", dropMode);
                                        addLeg.Parameters.AddWithValue("@Pcs", delPack);
                                        addLeg.Parameters.AddWithValue("@Wgt", delWgt);
                                        addLeg.Parameters.AddWithValue("@Vol", delVol);
                                        addLeg.Parameters.AddWithValue("@LegType", legType);
                                        addLeg.Parameters.AddWithValue("@ContMode", "");
                                        addLeg.Parameters.AddWithValue("@ContainerNo", containerNumber);
                                        addLeg.Parameters.AddWithValue("@DelDate", estDelDate);
                                        addLeg.Parameters.AddWithValue("@uOM", delUOM);
                                        if (legOrg == null)
                                        {
                                            continue;
                                        }
                                        SqlParameter Legno = addJob.Parameters.AddWithValue("@LegNo", legNo);
                                        SqlParameter legStatus = addJob.Parameters.AddWithValue("@STATUS", "Created");
                                        SqlParameter l_id = addJob.Parameters.Add("@ID", SqlDbType.UniqueIdentifier);
                                        l_id.Direction = ParameterDirection.Output;
                                        if (sqlTransConn.State == ConnectionState.Open)
                                        {
                                            sqlTransConn.Close();
                                        }
                                        sqlTransConn.Open();
                                        addLeg.ExecuteNonQuery();
                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Transport Job (" + jobNo + ") has been imported.", Color.DarkGreen);

                                    }
                                }
                                List<AdditionalReference> arcoll = new List<AdditionalReference>();


                            }



                        }


                        result.FileName = xmlfile;
                        result.Processed = true;


                    }
                    else
                    {
                        // Log error
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Transport Customer " + senderID + " not valid. Please confirm", Color.DarkGreen);
                        result.FileName = xmlfile;
                        result.Processed = false;
                    }

                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name;
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + "Exception found in file " + Path.GetFileName(xmlfile) + ". Error Message " + ex.Message, Color.Red, FontStyle.Bold);
                    string f = NodeResources.MoveFile(xmlfile, Globals.FailLoc);
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlfile = f; }
                    //xmlfile = MoveFile(xmlfile, Globals.FailLoc);
                }


            }
            else
            {

            }
            return result;

        }

        private Guid GetOrgAddress(OrganizationAddress legOrg, Guid custid)
        {
            Guid result = new Guid();
            if (legOrg != null)
            {
                SqlConnection sqlTransConn = new SqlConnection();
                sqlTransConn.ConnectionString = Globals.connTransString();

                SqlCommand getOrgAddress = new SqlCommand("GetOrgAddress", sqlTransConn);
                getOrgAddress.CommandType = CommandType.StoredProcedure;
                SqlParameter Company = getOrgAddress.Parameters.AddWithValue("@Company", legOrg.CompanyName);
                SqlParameter ShortCode = getOrgAddress.Parameters.AddWithValue("@ShortCode", legOrg.AddressShortCode);
                SqlParameter CompanyState = getOrgAddress.Parameters.AddWithValue("@State", legOrg.State);
                SqlParameter Address1 = getOrgAddress.Parameters.AddWithValue("@Add1", legOrg.Address1);
                SqlParameter Address2 = getOrgAddress.Parameters.AddWithValue("@Add2", legOrg.Address2);
                if (legOrg.Country != null)
                {
                    SqlParameter Address3 = getOrgAddress.Parameters.AddWithValue("@Add3", legOrg.Country.Code);
                }

                SqlParameter Sub = getOrgAddress.Parameters.AddWithValue("@Sub", legOrg.City);
                SqlParameter PostCode = getOrgAddress.Parameters.AddWithValue("@PostCode", legOrg.Postcode);
                SqlParameter Ph = getOrgAddress.Parameters.AddWithValue("@PH", legOrg.Phone);
                SqlParameter AddressType = getOrgAddress.Parameters.AddWithValue("@AddressType", legOrg.AddressType);
                SqlParameter CompanyCode = getOrgAddress.Parameters.AddWithValue("@Code", legOrg.OrganizationCode);
                SqlParameter customerID = getOrgAddress.Parameters.AddWithValue("@O_CUST", custid);
                SqlParameter id = getOrgAddress.Parameters.Add("@AID", SqlDbType.UniqueIdentifier);
                id.Direction = ParameterDirection.Output;
                if (sqlTransConn.State == ConnectionState.Open)
                    sqlTransConn.Close();
                sqlTransConn.Open();
                getOrgAddress.ExecuteNonQuery();
                result = (Guid)id.Value;
            }
            else
                result = Guid.Empty;
            return result;
        }

        private OrganizationAddress GetTransportAddressOrg(string shipRef, Shipment shipment, string v)
        {
            OrganizationAddress result = new OrganizationAddress();
            Shipment transportShipment = new Shipment();
            transportShipment = GetShipment(shipment.SubShipmentCollection, shipRef);
            foreach (OrganizationAddress oa in transportShipment.OrganizationAddressCollection)
            {
                if (oa.AddressType == v)
                {
                    result = oa;
                    break;
                }
            }
            return result;
        }

        private CWShipment GetShipment(Shipment[] subShipColl, string shipRef)
        {
            Shipment result = new Shipment();
            bool found = false;
            foreach (Shipment ship in subShipColl)
            {
                foreach (DataSource ds in ship.DataContext.DataSourceCollection)
                {
                    if (ds.Key == shipRef)
                    {
                        result = ship;
                        found = true;
                        //break;
                    }
                    if (found)
                    {
                        if (ship.WayBillType.Code == "MWB")
                            result = GetShipment(ship.SubShipmentCollection, shipRef);
                        found = false;
                        break;
                    }
                }

            }
            return result;
        }

        private string GetRef(List<AdditionalReference> additionalReferenceCollection, string v)
        {
            string result = string.Empty;

            foreach (AdditionalReference ar in additionalReferenceCollection)
            {
                if (ar.Type.Code == v)
                {
                    result = ar.ReferenceNumber;
                }
            }
            return result;

        }


        private ProcessResult ConvertCWFile(CustProfileRecord cust, String file)
        {
            ProcessResult result = new ProcessResult();
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Beginning Cargowise Conversion: " + file);
            string archiveFile = string.Empty;
            archiveFile = Globals.ArchiveFile(cust.C_path + "\\Archive", file);
            string strSuccess = "";
            string strResult = "";
            string ftpSend = string.Empty;
            if (!String.IsNullOrEmpty(cust.P_Method))
            {
                string custMethod;
                string custParams;
                custMethod = cust.P_Method;
                custParams = cust.P_Params;
                Type custType = this.GetType();
                MethodInfo custMethodToRun = custType.GetMethod(custMethod);
                try
                {
                    // Move Cargowise files to Customer Folder for Satellite processing. 
                    if (custMethod == "MoveToProcessing")
                    {

                        string newPath = cust.C_path;
                        if (!string.IsNullOrEmpty(custParams))
                        {
                            string[] paramlist = custParams.Split('|');
                            DirectoryInfo dir = new DirectoryInfo(Path.GetFullPath(paramlist[0]));
                            if (dir.Exists)
                            {
                                newPath = paramlist[0];
                            }
                        }

                        var varResult = custMethodToRun.Invoke(this, new Object[] { file, Path.Combine(newPath, "Processing"), "", cust.P_description });
                        if (varResult != null)
                        {

                        }
                    }
                    else
                    {
                        TransReference ts = new TransReference();
                        // Convert FROM Cargowise Files
                        if (!string.IsNullOrEmpty(cust.P_Library))
                        {
                            // Conversion via DLL
                            String libtoLoad = Path.Combine(cust.C_path + "\\Lib", cust.P_Library);
                            Assembly custLib = Assembly.LoadFile(libtoLoad);
                            Type convertMethod = custLib.GetTypes().ToList().FirstOrDefault(f => f.Name == "ConvertToCW");
                            dynamic CustomerXMLLibrary = Activator.CreateInstance(convertMethod);
                            TransReference conversionResult = CustomerXMLLibrary.DoConvert(file, cust.P_Senderid, cust.P_Recipientid, Globals.XMLLoc);
                            if (conversionResult != null)
                            {
                                switch (conversionResult.Ref3Type)
                                {
                                    case RefType.Error:
                                        NodeResources.MoveFile(file, Globals.FailLoc);
                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Error in Custom Library: " + cust.P_Library + ". Moving to Failed", Color.Red);
                                        MailModule.sendMsg(file, Globals.AlertsTo, "Error in Custom Library " + cust.P_Library, "Error message: " + conversionResult.Reference3);
                                        result.Processed = true;
                                        result.FolderLoc = Globals.FailLoc;
                                        result.FileName = Path.GetFileName(conversionResult.Reference3);
                                        break;

                                    case RefType.FileName:
                                        Globals.ArchiveFile(Globals.ArchLoc, conversionResult.Reference3);
                                        AddTransaction(cust.C_id, cust.P_id, file, cust.P_Direction, true, ts, archiveFile);
                                        //canDelete = true;
                                        result.FileName = file;
                                        result.Processed = true;
                                        break;
                                }
                            }

                            else
                            {
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Conversion failed :" + ts.Reference3);
                                NodeResources.MoveFile(file, Globals.FailLoc);
                                result.Processed = true;


                            }
                        }
                        else
                        {
                            var varResult = custMethodToRun.Invoke(this, new Object[] { file, cust });
                        }
                        switch (cust.P_Direction)
                        {
                            case "S":
                                switch (cust.P_delivery)
                                {
                                    case ("R"):
                                        {
                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + cust.C_name + " Recipient found " + cust.P_Recipientid + ". Moving to Folder");
                                            AddTransactionLog(cust.P_id, cust.P_Senderid, file, "Customer " + cust.C_name + " Recipient found " + cust.P_Recipientid + ". Moving to Folder to retrieval", DateTime.Now, "Y", cust.P_BillTo, cust.P_Recipientid);

                                            NodeResources.MoveFile(file, Path.Combine(cust.C_path, "Processing"));
                                            result.Processed = true;
                                            result.FileName = null;
                                            break;
                                        }
                                    case ("D"):
                                        {
                                            if (File.Exists(file))
                                            {
                                                try
                                                {
                                                    //CTCFtp ctcftp = new CTCFtp("ftp://"+cust.P_server.Trim(), cust.P_username, cust.P_password);
                                                    //ctcftp.upload(cust.P_path + "/" + Path.GetFileName(file), file);

                                                    SessionOptions sessionOptions = new SessionOptions
                                                    {
                                                        HostName = cust.P_server.Trim(),
                                                        PortNumber = int.Parse(cust.P_port.Trim()),
                                                        UserName = cust.P_username.Trim(),
                                                        Password = cust.P_password.Trim()
                                                    };
                                                    if (cust.P_ssl)
                                                    {
                                                        sessionOptions.Protocol = Protocol.Sftp;
                                                        sessionOptions.SshPrivateKeyPath = Path.Combine(cust.C_path, "Lib", cust.P_Subject);
                                                        sessionOptions.SshHostKeyFingerprint = cust.P_EmailAddress;


                                                    }
                                                    else
                                                    {
                                                        sessionOptions.Protocol = Protocol.Ftp;
                                                    }
                                                    //try
                                                    //{
                                                    TransferOperationResult tr;
                                                    using (Session session = new Session())
                                                    {
                                                        session.Open(sessionOptions);
                                                        if (session.Opened)
                                                        {
                                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Connected to " + cust.P_server + ". Sending file  :" + file + Environment.NewLine + " TO " + sessionOptions.HostName + "/" + cust.P_path + "/" + Path.GetFileName(file));
                                                            TransferOptions tOptions = new TransferOptions();
                                                            tOptions.OverwriteMode = OverwriteMode.Overwrite;
                                                            tr = session.PutFiles(file, "/" + cust.P_path + "/" + Path.GetFileName(file), true, tOptions);
                                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Results: ");
                                                            if (tr.IsSuccess)
                                                            {
                                                                if (sqlConn.State == ConnectionState.Open)
                                                                    sqlConn.Close();
                                                                sqlConn.Open();
                                                                //remTodo.ExecuteNonQuery();
                                                                sqlConn.Close();
                                                                filesTx++;
                                                            }
                                                            else
                                                            {
                                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Transfer Result is invalid: ");
                                                            }
                                                            NodeResources.AddLabel(lblFilesTx, filesTx.ToString());
                                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "File " + Path.GetFileName(file) + " Sent. Response: " + ftpSend);
                                                            //AddTransaction((Guid)cust.C_id, (Guid)cust.P_id, Path.GetFileName(file), cust.P_Direction, false, ts, archiveFile);
                                                            strSuccess = "Y";
                                                            strResult = "File Sent Ok";
                                                            try
                                                            {
                                                                System.GC.Collect();
                                                                System.GC.WaitForPendingFinalizers();
                                                                File.Delete(file);
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                strResult = "Failed: " + ex.Message;
                                                            }



                                                        }
                                                        else
                                                        {

                                                        }
                                                    }

                                                }
                                                catch (InvalidOperationException ex)
                                                {
                                                    string strEx = ex.Message;
                                                }
                                                catch (ArgumentException ex)
                                                {
                                                    string strEx = ex.Message;
                                                }
                                                catch (Exception ex)
                                                {
                                                    // remID.Value = AddTodo((Guid)cust.P_id, file, ftpSend, DateTime.Now);
                                                    ProcessingErrors procerror = new ProcessingErrors();
                                                    procerror.ErrorCode.Code = NodeError.e104;
                                                    procerror.SenderId = cust.P_Senderid;
                                                    procerror.RecipientId = cust.P_Recipientid;
                                                    procerror.ErrorCode.Description = "Cargowise FTP Send Error Found: " + ex.Message + ". ";
                                                    procerror.FileName = file;
                                                    procerror.ProcId = cust.P_id;
                                                    AddProcessingError(procerror);
                                                    result.FolderLoc = cust.C_path + "\\Processing\\";
                                                    result.Processed = false;
                                                    result.FileName = file;
                                                    //AddTransactionLog(Guid.Empty, cust.P_Senderid, file, "Customer Found but no Profile found. Move to Failed Location for retry.", DateTime.Now, "Y", cust.P_Senderid, cust.P_Recipientid);
                                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Cargowise FTP Send Error Found: " + ex.Message + ". " + "");
                                                    AddTransactionLog(cust.P_id, cust.P_Senderid, file, "File " + Path.GetFileName(file) + " sent via FTP with the Result: " + ftpSend, DateTime.Now, strSuccess, cust.P_BillTo, cust.P_Recipientid);
                                                    String err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                                                    return result;

                                                }
                                                finally
                                                {
                                                    sqlConn.Close();
                                                }
                                            }
                                            break;
                                        }
                                    case ("C"):
                                        {
                                            try
                                            {
                                                //remID.Value = AddTodo((Guid)cust.P_id, file, "Added to todo List", DateTime.Now);
                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + cust.C_name + " Recipient found " + cust.P_Recipientid + ".Sending file via CTC Adapter" + "");
                                                String sendlog = CTCAdapter.SendMessage(cust.P_server, file, cust.P_Recipientid, cust.P_username, cust.P_password);
                                                // Test without sending

                                                //String sendlog = "Send OK";
                                                if (sendlog.Contains("SEND OK"))
                                                {
                                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Transmitted Ok. ");
                                                    if (sqlConn.State == ConnectionState.Open)
                                                    {
                                                        sqlConn.Close();
                                                    }
                                                    sqlConn.ConnectionString = Globals.connString();
                                                    sqlConn.Open();
                                                    //remTodo.ExecuteNonQuery();
                                                    sqlConn.Close();
                                                    cwTx++;
                                                    NodeResources.AddLabel(lblCWTx, cwTx.ToString());
                                                    strSuccess = "Y";
                                                    //msgType = "Original";
                                                    //ToSend = true;
                                                    // AddTransaction((Guid)cust.C_id, (Guid)cust.P_id, Path.GetFileName(file), cust.P_Senderid, false, ts, archiveFile);
                                                    System.GC.Collect();
                                                    System.GC.WaitForPendingFinalizers();
                                                    File.Delete(file);
                                                    strResult = "CTC Adapter send Ok";
                                                }
                                                else
                                                {
                                                    strResult = "CTC Adapter send Failed: " + sendlog;
                                                    ProcessingErrors procerror = new ProcessingErrors();
                                                    procerror.ErrorCode.Code = NodeError.e104;
                                                    procerror.SenderId = cust.P_Senderid;
                                                    procerror.RecipientId = cust.P_Recipientid;
                                                    procerror.ErrorCode.Description = "CTC Adapter send Failed: " + sendlog;
                                                    procerror.ProcId = cust.P_id;
                                                    procerror.FileName = file;
                                                    AddProcessingError(procerror);
                                                    result.FolderLoc = Globals.FailLoc;
                                                    result.Processed = false;
                                                    result.FileName = file;
                                                }
                                                AddTransactionLog(cust.P_id, cust.P_Senderid, file, "File " + Path.GetFileName(file) + " sent via CTC Adapter with the Result: " + sendlog, DateTime.Now, strSuccess, cust.P_BillTo, cust.P_Recipientid);
                                            }
                                            catch (Exception ex)
                                            {
                                                if (sqlConn.State == ConnectionState.Open)
                                                {
                                                    sqlConn.Close();
                                                }
                                                ProcessingErrors procerror = new ProcessingErrors();
                                                procerror.ErrorCode.Code = NodeError.e104;
                                                procerror.SenderId = cust.P_Senderid;
                                                procerror.RecipientId = cust.P_Recipientid;
                                                procerror.ErrorCode.Description = "CTC Adapter send Failed: " + ex.Message;
                                                procerror.FileName = file;
                                                procerror.ProcId = cust.P_id;
                                                AddProcessingError(procerror);
                                                result.FolderLoc = Globals.FailLoc;
                                                result.Processed = false;
                                                result.FileName = file;
                                                AddTransactionLog(cust.P_id, cust.P_Senderid, file, "File " + Path.GetFileName(file) + " sent via CTC Adapter Failed with the Result: " + ex.Message, DateTime.Now, strSuccess, cust.P_BillTo, cust.P_Recipientid);
                                                //     result = "CTC Adapter send failed: " + ex.Message;
                                            }
                                        }
                                        break;
                                    case "E":
                                        {
                                            //  remID.Value = AddTodo((Guid)cust.P_id, file, "Added to todo List", DateTime.Now);
                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + cust.C_name + " Recipient found " + cust.P_Recipientid + ".Sending file via Email" + "");
                                            string mailResult = string.Empty;
                                            mailResult = MailModule.sendMsg(file, cust.P_EmailAddress, cust.P_Subject, "Cargowise File Attached");
                                            // mailResult = "Message Sent OK";

                                            if (mailResult == "Message Sent OK")
                                            {
                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Transmitted Ok. ");
                                                sqlConn.ConnectionString = Globals.connString(); sqlConn.Open();
                                                //    remTodo.ExecuteNonQuery();
                                                sqlConn.Close();
                                                cwTx++;
                                                NodeResources.AddLabel(lblCWTx, cwTx.ToString());
                                                // AddTransaction((Guid)cust.C_id, (Guid)cust.P_id, Path.GetFileName(file), cust.P_Direction, false, ts, archiveFile);
                                                //  result = mailResult;
                                                result.Processed = true;
                                                result.FileName = file;
                                            }
                                            else
                                            {
                                                //   remID.Value = AddTodo((Guid)cust.P_id, file, "Message send failed. Error message was: " + mailResult, DateTime.Now);
                                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Message send failed. Error message was: " + mailResult);
                                                //   result = "Email send failed: " + mailResult;
                                                ProcessingErrors procerror = new ProcessingErrors();
                                                procerror.ErrorCode.Code = NodeError.e105;
                                                procerror.SenderId = cust.P_Senderid;
                                                procerror.RecipientId = cust.P_Recipientid;
                                                procerror.ErrorCode.Description = "Email message failed to send " + mailResult;
                                                procerror.ProcId = cust.P_id;
                                                procerror.FileName = file;
                                                AddProcessingError(procerror);
                                                result.FolderLoc = cust.C_path + "\\Processing\\";
                                                result.Processed = false;
                                                result.FileName = file;

                                            }
                                            AddTransactionLog(cust.P_id, cust.P_Senderid, file, "File " + Path.GetFileName(file) + " sent via Email with the Result: " + mailResult, DateTime.Now, strSuccess, cust.P_BillTo, cust.P_Recipientid);
                                            break;
                                        }

                                }
                                break;
                            case "R":

                                break;
                        }
                        if (result.Processed)
                        {
                            //AddTransaction((Guid)dr["C_ID"], (Guid)dr["P_ID"], xmlFile, msgDir, true, (TransReference)varResult, archiveFile);

                            result.FileName = file;
                            result.Processed = true;
                            System.GC.Collect();
                            System.GC.WaitForPendingFinalizers();
                            File.Delete(file);
                        }
                    }


                }
                catch (Exception ex)
                {
                    string strEx = ex.GetType().Name;
                    strEx += " " + ex.Message;
                }


            }
            return result;


        }


        //public void EmoTransGateway(String file, CustProfileRecord cust)
        //{
        //    UniversalInterchange toConvert;
        //    string result = "";
        //    string shipmentNo = "";
        //    using (FileStream fStream = new FileStream(file, FileMode.Open))
        //    {
        //        XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
        //        toConvert = (UniversalInterchange)cwConvert.Deserialize(fStream);
        //        fStream.Close();
        //    }
        //    CWShipment consol = new CWShipment();
        //    String senderID = toConvert.Header.SenderID;
        //    consol = toConvert.Body.BodyField.Shipment;
        //    CNodeBE.Shipment[] shipments = toConvert.Body.BodyField.Shipment.SubShipmentCollection;
        //    EMOTransGW.Shipment EmoShipment = new EMOTransGW.Shipment();

        //    if (shipments == null)
        //    {
        //        List<CNodeBE.Shipment> shp = new List<CWShipment>();
        //        shp.Add(toConvert.Body.BodyField.Shipment);
        //        shipments = shp.ToArray();
        //    }

        //    try
        //    {
        //        foreach (CNodeBE.Shipment shipment in shipments)
        //        {

        //            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Converting Cargowise to Gateway file: " + file + Environment.NewLine);
        //            EmoShipment.SiteId = cust.P_Senderid;
        //            List<CNodeBE.DataSource> dataSourceColl = shipment.DataContext.DataSourceCollection.ToList();
        //            foreach (DataSource dataSource in dataSourceColl)
        //            {
        //                if (dataSource.Type == "ForwardingShipment")
        //                    shipmentNo = dataSource.Key;
        //            }
        //            EmoShipment.FileNumber = shipmentNo;
        //            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Processing Shipment" + shipmentNo + Environment.NewLine);
        //            //EmoShipment.MessageDateTime = DateTime.UtcNow.ToUniversalTime();
        //            DateTime procTime = new DateTime();
        //            procTime = DateTime.Now;
        //            EmoShipment.MessageDateTime = DateTime.Parse(DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'sszzz"));
        //            EmoShipment.TransactionDateTime = DateTime.Parse(DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'sszzz"));
        //            EmoShipment.TransactionId = shipmentNo;
        //            TransReference trId = new TransReference();
        //            trId.Ref1Type = TransReference.RefType.Shipment;
        //            trId.Reference1 = shipmentNo;
        //            if (consol.WayBillType.Code == "MBL")
        //                trId.Reference2 = shipment.WayBillNumber;
        //            trId.Ref2Type = TransReference.RefType.Master;
        //            EmoShipment.MasterBillNumber = consol.WayBillNumber;
        //            if (shipment.WayBillType.Code == "HWB")
        //            {
        //                EmoShipment.HouseBillNumber = shipment.WayBillNumber;
        //                trId.Reference2 = shipment.WayBillNumber;
        //                trId.Ref2Type = TransReference.RefType.Housebill;
        //            }
        //            Guid tID = new Guid();
        //            tID = FindTransactionID(trId);
        //            if (tID == Guid.Empty)
        //            {
        //                EmoShipment.TransactionSetPurpose = EMOTransGW.TransactionSetPurpose.Add;
        //                trId.Reference3 = DateTime.Now.ToString("yyyyMMddhhmmssfff");
        //                trId.Ref3Type = TransReference.RefType.Transaction;
        //                EmoShipment.MessageId = trId.Reference3;
        //            }
        //            else
        //            {
        //                SqlCommand sqlTrans = new SqlCommand("SELECT T_REF3 from Transactions where T_ID = '" + tID.ToString() + "'", sqlConn);
        //                if (sqlConn.State == ConnectionState.Open)
        //                    sqlConn.Close();
        //                sqlConn.Open();
        //                SqlDataReader dr;
        //                dr = sqlTrans.ExecuteReader(CommandBehavior.CloseConnection);
        //                if (dr.HasRows)
        //                {
        //                    dr.Read();
        //                    EmoShipment.MessageId = dr["T_REF3"].ToString();
        //                    dr.Close();
        //                    EmoShipment.TransactionSetPurpose = EMOTransGW.TransactionSetPurpose.Replace;
        //                }
        //                else
        //                {
        //                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "ConvertCWFile error: Unable to find Transaction Reference. " + shipmentNo + Environment.NewLine);
        //                    string msg;
        //                    msg = "Error locating Transaction Reference Number but found first two References" + Environment.NewLine
        //                            + "T_REF1 = " + trId.Reference1 + " (" + trId.Ref1Type + ")" + Environment.NewLine
        //                            + "T_REF2 = " + trId.Reference2 + " (" + trId.Ref2Type + ")" + Environment.NewLine;
        //                    MailModule.sendMsg("", Globals.AlertsTo, "ConvertCWFile Error: Unable to find Transaction Reference", msg);
        //                    return;
        //                }

        //            }
        //            EmoShipment.ShipmentType = EMOTransGW.ShipmentType.House;
        //            string paymentMethod = string.Empty;
        //            try
        //            {
        //                if (!string.IsNullOrEmpty(consol.PaymentMethod.Description))
        //                {
        //                    paymentMethod = consol.PaymentMethod.Description;
        //                }
        //                else
        //                {
        //                    if (!string.IsNullOrEmpty(consol.PaymentMethod.Description))
        //                        paymentMethod = shipment.PaymentMethod.Description;
        //                }
        //                switch (paymentMethod)
        //                {
        //                    case "Prepaid":
        //                        EmoShipment.PaymentMethod = EMOTransGW.PaymentMethod.Prepaid;
        //                        break;
        //                    case "Collect":
        //                        EmoShipment.PaymentMethod = EMOTransGW.PaymentMethod.Collect;
        //                        break;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Payment Method is null. Skipping." + Environment.NewLine);
        //            }

        //            if (shipment.ShipmentType.Code != null)
        //            {
        //                switch (shipment.ShipmentType.Code)
        //                {
        //                    case "STD":
        //                        EmoShipment.ShipmentType = EMOTransGW.ShipmentType.House;
        //                        break;
        //                    case "AGT":
        //                        EmoShipment.ShipmentType = EMOTransGW.ShipmentType.Floating;
        //                        break;
        //                    case "CLD":
        //                        EmoShipment.ShipmentType = EMOTransGW.ShipmentType.Master;
        //                        break;
        //                    default:
        //                        EmoShipment.ShipmentType = EMOTransGW.ShipmentType.House;
        //                        break;
        //                }
        //            }
        //            //Loop through Date Collection
        //            List<CNodeBE.Date> dtColl = shipment.DateCollection.Cast<CNodeBE.Date>().ToList();
        //            NodeResources.AddRTBText(edLog, string.Format("{0:g} ", DateTime.Now) + "Adding Dates" + Environment.NewLine);
        //            foreach (CNodeBE.Date dt in dtColl)
        //            {
        //                try
        //                {
        //                    DateTime dateTest;
        //                    switch (dt.Type)
        //                    {
        //                        case DateType.Departure:
        //                            if (DateTime.TryParse(dt.Value, out dateTest))
        //                                EmoShipment.DepartureDateTime = dateTest;
        //                            break;
        //                        case DateType.Arrival:
        //                            if (DateTime.TryParse(dt.Value, out dateTest))
        //                                EmoShipment.ArrivalDateTime = dateTest;
        //                            break;
        //                        case DateType.FirstArrivalInCountry:
        //                            if (DateTime.TryParse(dt.Value, out dateTest))
        //                                EmoShipment.InlandDateTime = dateTest;
        //                            break;
        //                        case DateType.EntryAuthorisation:
        //                            if (DateTime.TryParse(dt.Value, out dateTest))
        //                                EmoShipment.CustomsReleaseDate = dateTest;
        //                            break;
        //                        case DateType.Pickup:
        //                            if (DateTime.TryParse(dt.Value, out dateTest))
        //                                EmoShipment.PickupDateTime = dateTest;
        //                            break;
        //                        case DateType.Delivery:
        //                            if (DateTime.TryParse(dt.Value, out dateTest))
        //                                EmoShipment.DeliveryDateTime = dateTest;
        //                            break;
        //                        case DateType.CutOffDate:
        //                            if (DateTime.TryParse(dt.Value, out dateTest))
        //                                EmoShipment.CutoffDateTime = dateTest;
        //                            break;
        //                    }
        //                }
        //                catch (ArgumentNullException ex)
        //                {

        //                }
        //                catch (Exception ex)
        //                {
        //                    MailModule.sendMsg("", Globals.AlertsTo, "Error Convertying dates", ex.Message);
        //                }
        //            }
        //            EmoShipment.GrossWeight = consol.TotalWeight;
        //            switch (consol.TotalWeightUnit.Code)
        //            {
        //                case "KG":
        //                    EmoShipment.WeightUnit = EMOTransGW.WeightUnit.KG;
        //                    break;
        //                case "LB":
        //                    EmoShipment.WeightUnit = EMOTransGW.WeightUnit.LB;
        //                    break;
        //            }
        //            EmoShipment.Volume = consol.TotalVolume;
        //            switch (consol.TotalVolumeUnit.Code) // They do not have the same Codes 
        //            {
        //                case "M3":
        //                    EmoShipment.VolumeUnit = EMOTransGW.VolumeUnit.CBM;
        //                    break;
        //                case "CF":
        //                    EmoShipment.VolumeUnit = EMOTransGW.VolumeUnit.CFT;
        //                    break;
        //            }
        //            EmoShipment.Currency = consol.FreightRateCurrency.Code;
        //            EmoShipment.TransportationTerms = shipment.ShipmentIncoTerm.Code;
        //            //Loop through Organisations to create list of Organisations (Parties). 
        //            List<EMOTransGW.Party> partyColl = new List<EMOTransGW.Party>();
        //            EMOTransGW.Party party = new EMOTransGW.Party();
        //            List<CNodeBE.OrganizationAddress> orgAddColl = new List<OrganizationAddress>();
        //            orgAddColl = shipment.OrganizationAddressCollection.Cast<CNodeBE.OrganizationAddress>().ToList();
        //            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Adding Organisations (Parties)" + Environment.NewLine);
        //            foreach (CNodeBE.OrganizationAddress OrgAdd in orgAddColl)
        //            {
        //                switch (OrgAdd.AddressType)
        //                {
        //                    case "ShippingLineAddress":
        //                        party.PartyType = EMOTransGW.PartyType.Carrier;
        //                        break;
        //                    case "ImporterDocumentaryAddress":
        //                        party.PartyType = EMOTransGW.PartyType.Importer;
        //                        break;
        //                    case "SupplierDocumentaryAddress":
        //                        party.PartyType = EMOTransGW.PartyType.Supplier;
        //                        break;
        //                    case "ConsigneeDocumentaryAddress":
        //                        party.PartyType = EMOTransGW.PartyType.Consignee;
        //                        break;
        //                    case "ImportBroker":
        //                        party.PartyType = EMOTransGW.PartyType.Broker;
        //                        break;
        //                    case "NotifyParty":
        //                        party.PartyType = EMOTransGW.PartyType.Notify;
        //                        break;
        //                    case "SendersLocalClient":
        //                        break;
        //                    case "SendersOverseasAgent":
        //                        party.PartyType = EMOTransGW.PartyType.Agent;
        //                        break;
        //                    case "ReceivingForwarderAddress":
        //                        party.PartyType = EMOTransGW.PartyType.Forwarder;
        //                        break;
        //                    case "SendingForwarderAddress":
        //                        party.PartyType = EMOTransGW.PartyType.Forwarder;
        //                        break;
        //                }
        //                party.Name = OrgAdd.CompanyName;
        //                party.PartyCode = OrgAdd.OrganizationCode;
        //                party.Address1 = OrgAdd.Address1;
        //                party.Address2 = OrgAdd.Address2;
        //                party.CityName = OrgAdd.City;
        //                party.CountryCode = OrgAdd.Country.Code;
        //                party.PostalCode = OrgAdd.Postcode;
        //                party.StateOrProvinceCode = OrgAdd.State;
        //                partyColl.Add(party);
        //            }
        //            EMOTransGW.Parties shipParties = new EMOTransGW.Parties();
        //            shipParties.Party = partyColl.ToArray();
        //            EmoShipment.Parties = shipParties;
        //            //Loop through Orders to create Order Array (References PO).
        //            List<EMOTransGW.Reference> refCollection = new List<EMOTransGW.Reference>();
        //            EMOTransGW.Reference reference = new EMOTransGW.Reference();
        //            CNodeBE.ShipmentLocalProcessing locProc = new ShipmentLocalProcessing();
        //            locProc = shipment.LocalProcessing;
        //            List<CNodeBE.ShipmentLocalProcessingOrderNumber> ordCollection = new List<ShipmentLocalProcessingOrderNumber>();
        //            if (locProc.OrderNumberCollection != null)
        //            {
        //                ordCollection = locProc.OrderNumberCollection.Cast<CNodeBE.ShipmentLocalProcessingOrderNumber>().ToList();
        //                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Looping through Orders" + Environment.NewLine);
        //                foreach (CNodeBE.ShipmentLocalProcessingOrderNumber order in ordCollection)
        //                {
        //                    reference.ReferenceNumber = order.OrderReference;
        //                    reference.Qualifier = "PO";
        //                    reference.Name = "Purchase Order Number";
        //                    refCollection.Add(reference);
        //                }
        //                EMOTransGW.References shipRefs = new EMOTransGW.References();
        //                shipRefs.Reference = refCollection.ToArray();
        //                EmoShipment.References = shipRefs;
        //            }
        //            //Loop through UNLOCODE to create Locations array
        //            List<EMOTransGW.Location> locationColl = new List<EMOTransGW.Location>();
        //            EMOTransGW.Location location = new EMOTransGW.Location();
        //            if (consol.PlaceOfDelivery != null)
        //            {
        //                location.LocationType = EMOTransGW.LocationType.PlaceOfDelivery;
        //                location.LocationName = consol.PlaceOfDelivery.Name;
        //                location.LocationIdQualifier = EMOTransGW.LocationIdQualifier.UN;
        //                location.LocationId = consol.PlaceOfDelivery.Code;
        //                location.CountryCode = consol.PlaceOfDelivery.Code.Substring(0, 2);
        //                locationColl.Add(location);
        //            }
        //            if (consol.PortOfDischarge != null)
        //            {
        //                location.LocationType = EMOTransGW.LocationType.PortOfDischarge;
        //                location.LocationName = consol.PortOfDischarge.Name;
        //                location.LocationIdQualifier = EMOTransGW.LocationIdQualifier.UN;
        //                location.LocationId = consol.PortOfDischarge.Code;
        //                location.CountryCode = consol.PortOfDischarge.Code.Substring(0, 2);
        //                locationColl.Add(location);
        //            }
        //            if (consol.PortOfLoading != null)
        //            {
        //                location.LocationType = EMOTransGW.LocationType.PortOfLoad;
        //                location.LocationName = consol.PortOfLoading.Name;
        //                location.LocationIdQualifier = EMOTransGW.LocationIdQualifier.UN;
        //                location.LocationId = consol.PortOfLoading.Code;
        //                location.CountryCode = consol.PortOfLoading.Code.Substring(0, 2);
        //                locationColl.Add(location);
        //            }
        //            EMOTransGW.Locations shpLocs = new EMOTransGW.Locations();
        //            shpLocs.Location = locationColl.ToArray();
        //            EmoShipment.Locations = shpLocs;
        //            //Loop through transport legs to create Routing array
        //            List<EMOTransGW.ShipmentRoutingSegment> routeCollection = new List<EMOTransGW.ShipmentRoutingSegment>();
        //            EMOTransGW.ShipmentRoutingSegment route = new EMOTransGW.ShipmentRoutingSegment();
        //            try
        //            {
        //                List<CNodeBE.TransportLeg> TransportColl = shipment.TransportLegCollection.Cast<CNodeBE.TransportLeg>().ToList();
        //                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Adding Transport Legs" + Environment.NewLine);
        //                foreach (CNodeBE.TransportLeg leg in TransportColl)
        //                {
        //                    route.Sequence = leg.LegOrder;
        //                    route.VesselName = leg.VesselName;
        //                    route.VoyageFlightNumber = leg.VoyageFlightNo;
        //                    route.DepartureLocation = leg.PortOfLoading.Code;
        //                    route.DepartureDateTime = DateTime.Parse(leg.EstimatedDeparture);
        //                    route.ArrivalLocation = leg.PortOfDischarge.Code;
        //                    route.ArrivalDateTime = DateTime.Parse(leg.EstimatedArrival);
        //                    route.DepartureLocationQualifier = EMOTransGW.DepartureLocationQualifier.UN;
        //                    route.ArrivalLocationQualifier = EMOTransGW.ArrivalLocationQualifier.UN;
        //                    routeCollection.Add(route);
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "No Transport Legs found. Skipping." + Environment.NewLine);
        //            }

        //            EMOTransGW.ShipmentRouting shpRoutes = new EMOTransGW.ShipmentRouting();
        //            shpRoutes.Segment = routeCollection.ToArray();
        //            EmoShipment.Routing = shpRoutes;
        //            String orignCountry = toConvert.Body.BodyField.Shipment.DataContext.Company.Country.Code;
        //            //Switch between Airfreight and SeaFreight
        //            switch (consol.TransportMode.Code)
        //            {
        //                case "SEA":
        //                    if (shipment.PortOfDestination.Code == orignCountry)
        //                    {
        //                        EmoShipment.TransportationMethod = EMOTransGW.TransportationMethod.OceanImport;
        //                    }
        //                    else
        //                    {
        //                        EmoShipment.TransportationMethod = EMOTransGW.TransportationMethod.OceanExport;
        //                    }
        //                    int contCount = consol.ContainerCount;
        //                    //Loop through Container Collection (looking for TEU and Sea Freight Container Data
        //                    List<EMOTransGW.Container> containerColl = new List<EMOTransGW.Container>();
        //                    EMOTransGW.Container container = new EMOTransGW.Container();
        //                    List<EMOTransGW.ContainerContentsContent> contentColl = new List<EMOTransGW.ContainerContentsContent>();
        //                    EMOTransGW.ContainerContentsContent content = new EMOTransGW.ContainerContentsContent();
        //                    List<CNodeBE.Container> cwContainerColl = new List<CNodeBE.Container>();
        //                    cwContainerColl = shipment.ContainerCollection.Container.Cast<CNodeBE.Container>().ToList();
        //                    List<CNodeBE.PackingLine> packColl = new List<PackingLine>();
        //                    packColl = shipment.PackingLineCollection.Cast<PackingLine>().ToList();
        //                    int TEU = 0;
        //                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Adding Containers" + Environment.NewLine);
        //                    foreach (CNodeBE.Container cwContainer in cwContainerColl)
        //                    {
        //                        switch (cwContainer.ContainerType.Code.Substring(1, 2))
        //                        {
        //                            case "20":
        //                                TEU += 1;
        //                                break;
        //                            case "40":
        //                                TEU += 2;
        //                                break;
        //                        }
        //                        container.EquipmentInitial = cwContainer.ContainerNumber.Substring(0, 4);
        //                        container.EquipmentNumber = cwContainer.ContainerNumber.Substring(4, 7);
        //                        container.EquipmentTypeCode = cwContainer.ContainerType.Code;
        //                        container.QuantityShipped = cwContainer.ContainerCount;
        //                        container.GrossWeight = cwContainer.GrossWeight;

        //                        switch (cwContainer.VolumeUnit.Code)
        //                        {
        //                            case "M3":
        //                                container.VolumeUnit = EMOTransGW.VolumeUnit.CBM;
        //                                break;
        //                            case "CF":
        //                                container.VolumeUnit = EMOTransGW.VolumeUnit.CFT;
        //                                break;
        //                        }
        //                        switch (cwContainer.WeightUnit.Code)
        //                        {
        //                            case "KG":
        //                                container.WeightUnit = EMOTransGW.WeightUnit.KG;
        //                                break;
        //                            case "LB":
        //                                container.WeightUnit = EMOTransGW.WeightUnit.LB;
        //                                break;
        //                        }

        //                        PackingLine packCont = packColl.Find(x => x.ContainerNumber == cwContainer.ContainerNumber);
        //                        // If Packing Line Collection Exists.
        //                        if (packCont != null)
        //                        {
        //                            List<EMOTransGW.ContainerContentsContent> containerContents = new List<EMOTransGW.ContainerContentsContent>();
        //                            EMOTransGW.ContainerContentsContent containerContent = new EMOTransGW.ContainerContentsContent();
        //                            List<CNodeBE.PackingLine> contPackingLineColl = new List<PackingLine>();
        //                            containerContent.ChargeableWeight = packCont.Weight;
        //                            containerContent.Description = packCont.GoodsDescription;
        //                            containerContent.Volume = packCont.Volume;
        //                            switch (packCont.VolumeUnit.Code)
        //                            {
        //                                case "M3":
        //                                    containerContent.VolumeUnit = EMOTransGW.VolumeUnit.CBM;
        //                                    break;
        //                                case "CF":
        //                                    containerContent.VolumeUnit = EMOTransGW.VolumeUnit.CFT;
        //                                    break;
        //                            }
        //                            switch (packCont.WeightUnit.Code)
        //                            {
        //                                case "KG":
        //                                    containerContent.WeightUnit = EMOTransGW.WeightUnit.KG;
        //                                    break;
        //                                case "LB":
        //                                    containerContent.WeightUnit = EMOTransGW.WeightUnit.LB;
        //                                    break;

        //                            }
        //                            containerContent.QuantityShipped = packCont.PackQty;
        //                            containerContent.UnitOfMeasure = packCont.PackType.Code;
        //                            containerContents.Add(containerContent);
        //                            EMOTransGW.ContainerContents shpContents = new EMOTransGW.ContainerContents();
        //                            shpContents.Content = containerContents.ToArray();
        //                            container.Contents = shpContents;
        //                        }
        //                        container.SealNumber1 = cwContainer.Seal;
        //                        containerColl.Add(container);
        //                    }
        //                    EMOTransGW.Containers shpConts = new EMOTransGW.Containers();
        //                    shpConts.Container = containerColl.ToArray();
        //                    EmoShipment.Containers = shpConts;
        //                    EmoShipment.TEU = TEU;
        //                    break;
        //                case "AIR":
        //                    if (shipment.PortOfDestination.Code == orignCountry)
        //                    {
        //                        EmoShipment.TransportationMethod = EMOTransGW.TransportationMethod.AirImport;
        //                    }
        //                    else
        //                    {
        //                        EmoShipment.TransportationMethod = EMOTransGW.TransportationMethod.AirExport;
        //                    }
        //                    break;
        //            }
        //            EmoShipment.VesselName = consol.VesselName;
        //            EmoShipment.VoyageFlightNumber = consol.VoyageFlightNo;
        //            EmoShipment.BookingNumber = shipment.BookingConfirmationReference;
        //            EmoShipment.CustomerReference = shipment.OwnerRef;
        //            // Creating the unique filename

        //            String gwXML = Path.Combine(getCustPath(senderID), senderID + "-GWXML-" + EmoShipment.FileNumber + ".xml");
        //            int iFileCount = 0;
        //            while (File.Exists(gwXML))
        //            {
        //                iFileCount++;
        //                gwXML = Path.Combine(getCustPath(senderID), senderID + "-GWXML-" + EmoShipment.FileNumber + iFileCount + ".xml");
        //            }
        //            //Create the Stream of the New XML
        //            Stream outputGW = File.Open(gwXML, FileMode.Create);
        //            AddTransaction(cust.C_id, cust.P_id, gwXML, "S", true, trId, "");
        //            //Getting the system ready to serialize the EmoTrans Class to XML
        //            StringWriter writer = new StringWriter();
        //            XmlSerializer xSer = new XmlSerializer(typeof(EMOTransGW.Shipment));
        //            var cwNS = new XmlSerializerNamespaces();
        //            cwNS.Add(string.Empty, string.Empty);
        //            //Serialize and write data to XML file and Flush the memory stream
        //            xSer.Serialize(outputGW, EmoShipment, cwNS);
        //            outputGW.Flush();
        //            outputGW.Close();
        //            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Conversion Complete Saving file for transfer: " + gwXML + Environment.NewLine);
        //            CTCFtp ftp = new CTCFtp(cust.P_server, cust.P_username, cust.P_password);
        //            if (!String.IsNullOrEmpty(cust.P_path))
        //            {
        //                cust.P_path += "/";
        //            }
        //            //Send the Files 
        //            //TODO: Turn off Dummy Load.
        //            var ftpSend = ftp.upload(cust.P_path + Path.GetFileName(gwXML), gwXML);
        //            //FTP Dummy load
        //            //var ftpSend = "226";
        //            if (ftpSend.Contains("226"))
        //            {
        //                filesTx++;
        //                NodeResources.AddLabel(lblFilesTx, filesTx.ToString());
        //                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "File " + Path.GetFileName(gwXML) + " Sent. Response: " + ftpSend);
        //                AddTransactionLog((Guid)cust.C_id, cust.P_Senderid, Path.GetFileName(gwXML), ftpSend.ToString(), DateTime.Now, "Y", cust.P_Senderid, cust.P_Recipientid);
        //                result = "File Sent Ok";
        //                try
        //                {
        //                    System.GC.Collect();
        //                    System.GC.WaitForPendingFinalizers();
        //                    File.Delete(gwXML);
        //                }
        //                catch (Exception ex)
        //                {
        //                    result = "Failed: " + ex.Message;
        //                    AddTransactionLog((Guid)cust.C_id, cust.P_Senderid, Path.GetFileName(gwXML), ex.Message, DateTime.Now, "N", cust.P_Senderid, cust.P_Recipientid);
        //                }
        //            }
        //            else
        //            {
        //                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Warning: File " + Path.GetFileName(gwXML) + " not sent. Response: " + ftpSend);

        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Error Converting to GW file: " + file + ":" + ex.Message + Environment.NewLine);
        //    }


        //    //Shipment Loop


        //    try
        //    {
        //        System.GC.Collect();
        //        System.GC.WaitForPendingFinalizers();
        //        File.Delete(file);
        //    }
        //    catch (Exception ex)
        //    {
        //        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Unable to delete original file: " + file + ":" + ex.Message + Environment.NewLine);
        //    }
        //    DirectoryInfo gatewayXMLFolder = new DirectoryInfo(getCustPath(senderID));
        //}

        private void DTS(Guid profileID, String XMLfile)
        {
            SqlDataAdapter daDTS = new SqlDataAdapter("SELECT D_ID, D_INDEX, D_FINALPROCESSING, D_P, D_FILETYPE, D_DTSTYPE, D_DTS, D_SEARCHPATTERN, D_NEWVALUE, D_CURRENTVALUE, D_QUALIFIER, D_TARGET FROM DTS WHERE (D_P = @P_ID) ORDER BY D_INDEX", sqlConn);
            daDTS.SelectCommand.Parameters.AddWithValue("@P_ID", profileID);
            DataSet dsDTS = new DataSet();
            daDTS.Fill(dsDTS, "DTSList");
            //Loop through DTS
            foreach (DataRow drDTS in dsDTS.Tables["DTSList"].Rows)
            {
                //Load the XML File
                XmlDocument xmlDTS = new XmlDocument();
                var xdoc = XDocument.Load(XMLfile);
                //Ensure the Namespace is correct against qualifier
                XNamespace ns = xdoc.Root.GetDefaultNamespace();
                if (drDTS["D_QUALIFIER"].ToString() == ns.NamespaceName)
                {
                    var nsMgr = new XmlNamespaceManager(xmlDTS.NameTable);
                    nsMgr.AddNamespace("CW", ns.NamespaceName);
                    StreamReader sr = new StreamReader(XMLfile);
                    xmlDTS.Load(sr);
                    sr.Close();
                    sr.Dispose();
                    XmlNode xNode;
                    XmlNodeList xList;
                    try
                    {
                        if (drDTS["D_DTS"].ToString() == "R")//Modification to the Record
                        {
                            xList = xmlDTS.SelectNodes("//CW:" + drDTS["D_SEARCHPATTERN"].ToString(), nsMgr);
                            if (xList != null) //found the Search Pattern
                            {
                                String searchPattern = drDTS["D_SEARCHPATTERN"].ToString();
                                var fqNodeSearch = "//CW:" + searchPattern.Replace("/", "/CW:");
                                if (drDTS["D_TARGET"].ToString() != "")
                                {
                                    fqNodeSearch += "/CW:" + drDTS["D_TARGET"].ToString();
                                    if (drDTS["D_CURRENTVALUE"].ToString() != "")
                                    {
                                        fqNodeSearch += "[text() ='" + drDTS["D_CURRENTVALUE"].ToString() + "']";
                                    }
                                }
                                xNode = xmlDTS.SelectSingleNode(fqNodeSearch, nsMgr);
                                // xNode = FindNode(xList, drDTS["D_TARGET"].ToString());
                                if (xNode != null)  //Found the Target Key
                                {
                                    NodeResources.AddText(edLog, "DTS Service - Record Updated");
                                    switch (drDTS["D_DTS"].ToString())
                                    {
                                        case "R"://Replace
                                            {
                                                xNode.InnerText = drDTS["D_NEWVALUE"].ToString();
                                                break;
                                            }
                                        case "U"://Update
                                            {
                                                xNode.InnerText += drDTS["D_NEWVALUE"].ToString();
                                                break;
                                            }
                                        case "M"://Modify
                                            {
                                                break;
                                            }
                                        case "D"://Delete
                                            {
                                                break;
                                            }
                                    }
                                }
                            }
                            else
                            {
                                NodeResources.AddText(edLog, "DTS Service: No Value Found");
                            }
                        }
                        else
                        {
                            //Modify the Structure
                            switch (drDTS["D_DTSTYPE"].ToString())
                            {
                                case "R": //Replace
                                    {
                                        xList = xmlDTS.SelectNodes("//CW:" + drDTS["D_SEARCHPATTERN"].ToString(), nsMgr);
                                        if (xList != null)
                                        {
                                            xNode = FindNode(xList, drDTS["D_TARGET"].ToString());
                                            if (xNode != null)
                                            {
                                                XmlNode xNewNode = RenameNode(xNode, drDTS["D_NEWVALUE"].ToString(), ns.NamespaceName);
                                            }
                                        }
                                        break;
                                    }
                                case "D": //Delete
                                    {
                                        if (drDTS["D_SEARCHPATTERN"].ToString() == "")
                                        {
                                            XmlNodeList delNodes = xmlDTS.SelectNodes("//CW:" + drDTS["D_TARGET"].ToString(), nsMgr);
                                            foreach (XmlNode delNode in delNodes)
                                            {
                                                delNode.ParentNode.RemoveChild(delNode);
                                            }
                                        }
                                        else
                                        {
                                            xList = xmlDTS.SelectNodes("//CW:" + drDTS["D_SEARCHPATTERN"].ToString(), nsMgr);
                                            if (xList != null)
                                            {
                                                xNode = FindNode(xList, drDTS["D_TARGET"].ToString());
                                                if (xNode != null)
                                                {
                                                    xNode.ParentNode.RemoveChild(xNode);
                                                }
                                            }
                                        }
                                        break;
                                    }
                                case "M": //Modify
                                    {
                                        xList = xmlDTS.SelectNodes("//CW:" + drDTS["D_SEARCHPATTERN"].ToString(), nsMgr);
                                        if (xList != null)
                                        {
                                            xNode = FindNode(xList, drDTS["D_TARGET"].ToString());
                                            if (xNode != null)
                                            {
                                                XmlNode xNewNode = RenameNode(xNode, drDTS["D_NEWVALUE"].ToString(), ns.NamespaceName);
                                            }
                                        }
                                        break;
                                    }
                            }
                            NodeResources.AddText(edLog, "DTS Service - File Structure modified");
                        }
                        XmlNodeList blankNodes;
                        blankNodes = xmlDTS.SelectNodes("//*[not(string(.))]");
                        foreach (XmlNode blankNode in blankNodes)
                        {
                            blankNode.ParentNode.RemoveChild(blankNode);
                        }
                        xmlDTS.Save(XMLfile);
                        xmlDTS = null;
                    }
                    catch (Exception ex)
                    {
                        NodeResources.AddText(edLog, "Error found in DTS:" + ex.Message);
                        String elog = "";
                        foreach (DataColumn dc in drDTS.Table.Columns)
                        {
                            elog += dc.ColumnName + " :" + drDTS[dc].ToString() + Environment.NewLine;
                        }
                        String err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                        MailModule.sendMsg(XMLfile, Globals.AlertsTo, "CTC Node Exception Found: DTS Processing", err);
                        xmlDTS = null;
                    }
                }
                //Process the file as per the Profile and return to calling process. 
                xdoc = null;
            }
            //Clear all Blank/Empty Elements.
            XDocument xd = XDocument.Load(@XMLfile);
            xd.Descendants().
                Where(eg => (eg.Attributes().All(a => a.IsNamespaceDeclaration || string.IsNullOrWhiteSpace(a.Value))
            && string.IsNullOrWhiteSpace(eg.Value)
            && eg.Descendants().
            SelectMany(c => c.Attributes()).All(ca => ca.IsNamespaceDeclaration || string.IsNullOrWhiteSpace(ca.Value))))
        .Remove();
            xd.Save(@XMLfile);


        }

        private static void DeleteNode(XmlNode node, String xPath)
        {
            var nodes = node.SelectNodes(xPath);
            foreach (XmlNode nodetobedeleted in nodes)
            {
                nodetobedeleted.ParentNode.RemoveChild(nodetobedeleted);

            }
        }

        private XmlNode RenameNode(XmlNode e, string newName, String ns)
        {
            XmlNode renamedNode = null;
            try
            {
                XmlDocument doc = e.OwnerDocument;
                XmlNode newNode = doc.CreateNode(e.NodeType, newName, ns);
                while (e.HasChildNodes)
                {
                    newNode.AppendChild(e.FirstChild);
                }

                XmlAttributeCollection ac = e.Attributes;
                while (ac.Count > 0)
                {
                    newNode.Attributes.Append(ac[0]);
                }
                XmlNode parent = e.ParentNode;
                parent.ReplaceChild(newNode, e);
                renamedNode = newNode;
            }
            catch (Exception ex)
            {
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Error Renaming Node in DTS. Error was :" + ex.Message);
                String err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                MailModule.sendMsg(String.Empty, Globals.AlertsTo, "CTC Node Exception Found:Send CTC Adapter.", err);
            }
            return renamedNode;

        }

        public static XDocument DocumentToXDocumentReader(XmlDocument doc)
        {
            return XDocument.Load(new XmlNodeReader(doc));
        }

        public static XmlDocument RemoveNameSpace(XmlDocument doc)
        {
            XDocument xDoc;
            using (var nodeReader = new XmlNodeReader(doc))
            {
                nodeReader.MoveToContent();
                xDoc = XDocument.Load(nodeReader);
            }
            xDoc.Descendants().Attributes().Where(a => a.IsNamespaceDeclaration).Remove();
            foreach (var element in xDoc.Descendants())
            {
                element.Name = element.Name.LocalName;
            }
            doc.LoadXml(xDoc.ToString());
            return doc;

        }

        public ProcessResult ProcessXUE(string xmlfile)
        {
            ProcessResult thisResult = new ProcessResult();
            FileInfo processingFile = new FileInfo(xmlfile);
            thisResult.Processed = true;
            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            String eventCode = String.Empty;
            totfilesRx++;
            String msgDir = "";
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            cwRx++;
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());
            TransReference trRef = new TransReference();
            List<CustProfileRecord> custProfile = new List<CustProfileRecord>();
            XUE.UniversalInterchange ue = new XUE.UniversalInterchange();
            try
            {
                using (FileStream fStream = new FileStream(xmlfile, FileMode.Open))
                {
                    XmlSerializer cwConvert = new XmlSerializer(typeof(XUE.UniversalInterchange));
                    ue = (XUE.UniversalInterchange)cwConvert.Deserialize(fStream);
                    fStream.Close();
                }
                senderID = ue.Header.SenderID;
                recipientID = ue.Header.RecipientID;
                try
                {
                    reasonCode = ue.Body.BodyField.Event.DataContext.ActionPurpose.Code.ToString();
                }
                catch (Exception)
                {
                    reasonCode = string.Empty;

                }

                eventCode = ue.Body.BodyField.Event.DataContext.EventType.Code;
                SqlCommand sqlCustProfile = new SqlCommand("Select C_ID, P_ID, C_NAME, C_IS_ACTIVE, C_ON_HOLD, C_PATH, P_EVENTCODE, " +
                                                                "C_FTP_CLIENT, P_REASONCODE, P_SERVER, P_USERNAME, P_DELIVERY, " +
                                                                "P_PATH, P_PASSWORD, P_MSGTYPE, P_Recipientid, P_SENDERID, P_EMAILADDRESS, P_SUBJECT," +
                                                                "P_DESCRIPTION, P_BILLTO, P_PORT, P_DIRECTION, C_CODE, P_DTS, " +
                                                                "P_ACTIVE from vw_CustomerProfile WHERE P_SENDERID = @SENDERID " + //Removed the P_DIRECTION ='Y' not sure why it was there. 
                                                                "and P_Recipientid =@RECIPIENTID and (@REASONCODE IS NULL OR " +
                                                                "P_REASONCODE = @REASONCODE) and (@EVENTCODE is NULL or P_EVENTCODE = @EVENTCODE)" +
                                                                "and P_ACTIVE='Y'", sqlConn);
                sqlCustProfile.Parameters.AddWithValue("@SENDERID", senderID);
                sqlCustProfile.Parameters.AddWithValue("@RECIPIENTID", recipientID);
                sqlCustProfile.Parameters.AddWithValue("@REASONCODE", reasonCode);
                sqlCustProfile.Parameters.AddWithValue("@EVENTCODE", eventCode);
                if (sqlConn.State == ConnectionState.Open)
                {
                    sqlConn.Close();
                }
                sqlConn.ConnectionString = Globals.connString();
                sqlConn.Open();

                using (SqlDataReader drCustProfile = sqlCustProfile.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    if (drCustProfile.HasRows)
                    {
                        while (drCustProfile.Read())
                        {
                            CustProfileRecord result = new CustProfileRecord();
                            result.C_id = (Guid)drCustProfile["C_ID"];
                            result.P_id = (Guid)drCustProfile["P_ID"];
                            result.C_name = drCustProfile["C_NAME"].ToString();
                            result.C_is_active = drCustProfile["C_IS_ACTIVE"].ToString();
                            result.C_on_hold = drCustProfile["C_ON_HOLD"].ToString();
                            result.C_path = drCustProfile["C_PATH"].ToString();
                            result.C_ftp_client = drCustProfile["C_FTP_CLIENT"].ToString();
                            result.P_reasoncode = drCustProfile["P_REASONCODE"].ToString();
                            result.P_EventCode = drCustProfile["P_EVENTCODE"].ToString();
                            result.P_server = drCustProfile["P_SERVER"].ToString();
                            result.P_username = drCustProfile["P_USERNAME"].ToString();
                            result.P_delivery = drCustProfile["P_DELIVERY"].ToString();
                            result.P_password = drCustProfile["P_PASSWORD"].ToString();
                            result.P_Recipientid = drCustProfile["P_Recipientid"].ToString();
                            result.P_Senderid = drCustProfile["P_SENDERID"].ToString();
                            result.P_description = drCustProfile["P_DESCRIPTION"].ToString();
                            result.P_path = drCustProfile["P_PATH"].ToString();
                            result.P_port = drCustProfile["P_PORT"].ToString();
                            result.C_code = drCustProfile["C_CODE"].ToString();
                            result.P_BillTo = drCustProfile["P_BILLTO"].ToString();
                            result.P_Direction = drCustProfile["P_DIRECTION"].ToString();
                            result.P_MsgType = drCustProfile["P_MSGTYPE"].ToString();
                            result.P_Subject = drCustProfile["P_SUBJECT"].ToString();
                            result.P_EmailAddress = drCustProfile["P_EMAILADDRESS"].ToString();
                            if (drCustProfile["P_DTS"].ToString() == "Y")
                            {
                                result.P_DTS = true;
                            }
                            else
                            {
                                result.P_DTS = false;
                            }
                            custProfile.Add(result);
                        }
                    }
                    else
                    {
                        SqlCommand execAdd = new SqlCommand();
                        execAdd.Connection = sqlConn;
                        execAdd.CommandText = "Add_Profile";
                        execAdd.CommandType = CommandType.StoredProcedure;
                        SqlParameter PC = execAdd.Parameters.Add("@P_C", SqlDbType.UniqueIdentifier);
                        PC.Value = getCustID(senderID);
                        SqlParameter PReasonCode = execAdd.Parameters.Add("@P_REASONCODE", SqlDbType.Char, 3);
                        PReasonCode.Value = reasonCode.ToUpper();
                        SqlParameter PEventCode = execAdd.Parameters.Add("@P_EVENTCODE", SqlDbType.Char, 3);
                        PEventCode.Value = eventCode.ToUpper();
                        SqlParameter PServer = execAdd.Parameters.Add("@P_SERVER", SqlDbType.VarChar, 50);
                        PServer.Value = "";
                        SqlParameter PUsername = execAdd.Parameters.Add("@P_USERNAME", SqlDbType.VarChar, 50);
                        PUsername.Value = "";
                        SqlParameter PPath = execAdd.Parameters.Add("@P_PATH", SqlDbType.VarChar, 100);
                        PPath.Value = "";
                        SqlParameter PPassword = execAdd.Parameters.Add("@P_PASSWORD", SqlDbType.VarChar, 50);
                        PPassword.Value = "";
                        string sDelivery = String.Empty;
                        sDelivery = "R";
                        SqlParameter pDelivery = execAdd.Parameters.Add("@P_DELIVERY", SqlDbType.Char, 1);
                        pDelivery.Value = sDelivery;
                        SqlParameter pMSGType = execAdd.Parameters.Add("@MSGTYPE", SqlDbType.VarChar, 20);
                        pMSGType.Value = "Original";
                        SqlParameter pBillType = execAdd.Parameters.Add("@BILLTO", SqlDbType.VarChar, 15);
                        pBillType.Value = senderID;
                        SqlParameter pChargeable = execAdd.Parameters.Add("@CHARGEABLE", SqlDbType.Char, 1);
                        pChargeable.Value = "Y";
                        SqlParameter PPort = execAdd.Parameters.Add("@P_PORT", SqlDbType.Char, 10);
                        PPort.Value = "";
                        SqlParameter PDescription = execAdd.Parameters.Add("@P_DESCRIPTION", SqlDbType.VarChar, 100);
                        PDescription.Value = "No Transport Profile found. Store in Folder";
                        SqlParameter PRecipientId = execAdd.Parameters.Add("@P_Recipientid", SqlDbType.VarChar, 15);
                        PRecipientId.Value = recipientID;
                        SqlParameter PSenderID = execAdd.Parameters.Add("P_SENDERID", SqlDbType.VarChar, 15);
                        PSenderID.Value = senderID;
                        SqlParameter p_id = execAdd.Parameters.Add("@R_ID", SqlDbType.UniqueIdentifier);
                        SqlParameter PDirection = execAdd.Parameters.Add("@P_DIRECTION", SqlDbType.Char, 1);
                        PDirection.Value = "R";
                        p_id.Direction = ParameterDirection.Output;
                        SqlParameter r_Action = execAdd.Parameters.Add("@R_ACTION", SqlDbType.Char, 1);
                        r_Action.Direction = ParameterDirection.Output;
                        if (sqlConn.State == ConnectionState.Open)
                        {
                            sqlConn.Close();
                        }
                        sqlConn.Open();
                        execAdd.ExecuteNonQuery();
                        sqlConn.Close();
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Create New Profile Sender ID: " + senderID + " Recipient ID: " + recipientID + " Purpose Code: " + reasonCode + " Event Code:" + eventCode);
                        ProcessingErrors procerror = new ProcessingErrors();
                        CTCErrorCode error = new CTCErrorCode();
                        error.Code = NodeError.e100;
                        error.Description = "Customer Exists but no profile Found.";
                        error.Severity = "Warning";
                        procerror.ErrorCode = error;
                        procerror.SenderId = senderID;
                        procerror.RecipientId = recipientID;
                        procerror.FileName = xmlfile;
                        AddProcessingError(procerror);
                        thisResult.FolderLoc = Globals.FailLoc;
                        thisResult.Processed = false;
                        thisResult.FileName = xmlfile;
                        AddTransactionLog(Guid.Empty, senderID, xmlfile, "Customer Found but no Profile found. Move to Failed Location for retry.", DateTime.Now, "Y", senderID, recipientID);
                        return thisResult;
                    }
                    drCustProfile.Close();
                }
                SqlCommand remTodo = new SqlCommand("DELETE FROM TODO WHERE L_ID =@ID", sqlConn);
                SqlParameter remID = remTodo.Parameters.Add("@ID", SqlDbType.UniqueIdentifier);
                String archiveFile = String.Empty;
                if (custProfile.Count > 0)
                {
                    if (custProfile[0].C_is_active == "N" | custProfile[0].C_on_hold == "Y")
                    {
                        string cwxml = string.Empty;
                        string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfile[0].C_path, "Held"));
                        if (f.StartsWith("Warning"))
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                        }
                        else
                        { xmlfile = f; }

                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfile[0].C_name + " is not Active or is on Hold. Moving to Hold and skipping" + "");
                        thisResult.Processed = true;
                        thisResult.FileName = xmlfile;
                        return thisResult;
                    }
                    else
                    {
                        if (sqlConn.State == ConnectionState.Open)
                        {
                            sqlConn.Close();
                        }
                        CreateProfPath(custProfile[0].C_path + "\\Processing", recipientID + reasonCode);
                        //Send file to Archive folder
                        archiveFile = Globals.ArchiveFile(custProfile[0].C_path + "\\Archive", xmlfile);
                        string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfile[0].C_path, "Processing", recipientID + reasonCode));
                        if (f.StartsWith("Warning"))
                        {
                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                        }
                        else
                        { xmlfile = f; }


                        remID.Value = AddTodo(custProfile[0].P_id, xmlfile, "Add_Transaction", DateTime.Now);
                        AddTransaction(custProfile[0].C_id, custProfile[0].P_id, Path.GetFileName(xmlfile), msgDir, true, trRef, archiveFile);
                        if (custProfile[0].P_DTS)
                        {
                            remID.Value = AddTodo(custProfile[0].P_id, xmlfile, "DTS_Processing", DateTime.Now);
                            DTS(custProfile[0].P_id, xmlfile);
                        }
                        if (custProfile[0].P_MsgType.Trim() == "Conversion")
                        {
                            // Rename the Cargowise XML File to the new Global XML
                            ConvertCWFile(custProfile[0], xmlfile);
                            AddTransaction(custProfile[0].C_id, custProfile[0].P_id, Path.GetFileName(xmlfile), msgDir, true, trRef, archiveFile);
                        }
                        //Create the Delete from TODO list ready to be removed
                        String ftpSend = String.Empty;
                        String strSuccess = "N";
                        string result = "";
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Profile : " + custProfile[0].P_description);
                        switch (custProfile[0].P_delivery)
                        {
                            case ("C"):
                                {
                                    try
                                    {
                                        remID.Value = AddTodo(custProfile[0].P_id, xmlfile, "Added to todo List", DateTime.Now);
                                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfile[0].C_name + " Recipient found " + recipientID + ".Sending file via CTC Adapter" + "");
                                        String sendlog = CTCAdapter.SendMessage(custProfile[0].P_server, xmlfile, recipientID, custProfile[0].P_username, custProfile[0].P_password);
                                        // Test without sending

                                        //String sendlog = "Send OK";
                                        if (sendlog.Contains("SEND OK"))
                                        {
                                            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Transmitted Ok. ");
                                            if (sqlConn.State == ConnectionState.Open)
                                            {
                                                sqlConn.Close();
                                            }
                                            sqlConn.ConnectionString = Globals.connString();
                                            sqlConn.Open();
                                            remTodo.ExecuteNonQuery();
                                            sqlConn.Close();
                                            cwTx++;
                                            NodeResources.AddLabel(lblCWTx, cwTx.ToString());
                                            strSuccess = "Y";
                                            AddTransaction(custProfile[0].C_id, custProfile[0].P_id, Path.GetFileName(xmlfile), msgDir, false, trRef, archiveFile);
                                            System.GC.Collect();
                                            System.GC.WaitForPendingFinalizers();
                                            File.Delete(xmlfile);
                                            result = "CTC Adapter send Ok";
                                        }
                                        else
                                        {
                                            result = "CTC Adapter send Failed: " + sendlog;
                                            ProcessingErrors procerror = new ProcessingErrors();
                                            procerror.ErrorCode.Code = NodeError.e104;
                                            procerror.SenderId = senderID;
                                            procerror.RecipientId = recipientID;
                                            procerror.ErrorCode.Description = "CTC Adapter send Failed: " + sendlog;
                                            procerror.ProcId = custProfile[0].P_id;
                                            procerror.FileName = xmlfile;
                                            AddProcessingError(procerror);
                                            thisResult.FolderLoc = Globals.FailLoc;
                                            thisResult.Processed = false;
                                            thisResult.FileName = xmlfile;
                                        }
                                        AddTransactionLog(custProfile[0].P_id, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via CTC Adapter with the Result: " + sendlog, DateTime.Now, strSuccess, custProfile[0].P_BillTo, recipientID);
                                    }
                                    catch (Exception ex)
                                    {
                                        if (sqlConn.State == ConnectionState.Open)
                                        {
                                            sqlConn.Close();
                                        }
                                        ProcessingErrors procerror = new ProcessingErrors();
                                        procerror.ErrorCode.Code = NodeError.e104;
                                        procerror.SenderId = senderID;
                                        procerror.RecipientId = recipientID;
                                        procerror.ErrorCode.Description = "CTC Adapter send Failed: " + ex.Message;
                                        procerror.FileName = xmlfile;
                                        procerror.ProcId = custProfile[0].P_id;
                                        AddProcessingError(procerror);
                                        thisResult.FolderLoc = Globals.FailLoc;
                                        thisResult.Processed = false;
                                        thisResult.FileName = xmlfile;
                                        AddTransactionLog(custProfile[0].P_id, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via CTC Adapter Failed with the Result: " + ex.Message, DateTime.Now, strSuccess, custProfile[0].P_BillTo, recipientID);
                                        result = "CTC Adapter send failed: " + ex.Message;
                                    }
                                }
                                break;
                            case ("R"):
                                {
                                    NodeResources.MoveFile(xmlfile, Path.Combine(custProfile[0].C_path, "Processing"));
                                    break;

                                }


                        }
                        addSummRecord(DateTime.Now, recipientID, senderID, custProfile[0].P_MsgType, trRef, xmlfile, result);

                    }

                }
                else
                {
                    ProcessingErrors procerror = new ProcessingErrors();
                    // No customer Profile found so will move to Root Path for Pickup. 
                    CreateProfPath(getCustPath(senderID) + "\\Processing", recipientID + reasonCode);
                    string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfile[0].C_path, "Processing", recipientID + reasonCode));
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlfile = f; }

                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfile[0].C_name + " Does not have a Document Profile. Moving to Root Path" + "");
                    AddTransactionLog(custProfile[0].P_id, senderID, xmlfile, "Customer " + custProfile[0].C_name + " Recipient found " + recipientID + ". Moving to Folder to retrieval", DateTime.Now, "Y", custProfile[0].P_BillTo, recipientID);
                    AddTransaction(custProfile[0].C_id, custProfile[0].P_id, Path.GetFileName(xmlfile), msgDir, true, trRef, "N/A");
                    addSummRecord(DateTime.Now, recipientID, senderID, "Missing Profile", trRef, xmlfile, "Profile not found");
                    procerror = new ProcessingErrors();
                    procerror.ErrorCode.Code = NodeError.e100;
                    procerror.SenderId = senderID;
                    procerror.RecipientId = recipientID;
                    procerror.ErrorCode.Description = "Customer Profile Not Found. Move to Retry folder";
                    procerror.ProcId = custProfile[0].P_id;
                    procerror.FileName = xmlfile;
                    AddProcessingError(procerror);
                    thisResult.FolderLoc = Globals.FailLoc;
                    thisResult.Processed = false;
                    thisResult.FileName = xmlfile;

                    return thisResult;
                }
                return thisResult;
            }
            catch (Exception ex)
            {
                CreateProfPath(getCustPath(senderID) + "\\Processing", recipientID + reasonCode);
                string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfile[0].C_path, "Processing", recipientID + reasonCode));
                if (f.StartsWith("Warning"))
                {
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                }
                else
                { xmlfile = f; }

                string strEx = ex.GetType().Name;
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + strEx + " Error found. Process XUE failed with Error: " + ex.Message);
                //AddTransaction((Guid)custProfile[0].C_id, (Guid)custProfile[0].P_id, Path.GetFileName(xmlfile), msgDir, true, trRef, "N/A");
                //addSummRecord(DateTime.Now, recipientID, senderID, "Missing Profile", trRef, xmlfile, "Profile not found");
                ProcessingErrors procerror = new ProcessingErrors();
                procerror.ErrorCode.Code = NodeError.e112;
                procerror.SenderId = senderID;
                procerror.RecipientId = recipientID;
                procerror.ErrorCode.Description = "Customer Profile Not Found. Move to Retry folder";
                // procerror.ProcId = custProfile[0].P_id;
                procerror.FileName = xmlfile;
                AddProcessingError(procerror);
                thisResult.FolderLoc = Globals.FailLoc;
                thisResult.Processed = false;
                thisResult.FileName = xmlfile;

                return thisResult;

            }




        }
        private ProcessResult ProcessNDM(String xmlfile)
        {
            ProcessResult thisResult = new ProcessResult();
            FileInfo processingFile = new FileInfo(xmlfile);
            thisResult.Processed = true;
            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            String eventCode = String.Empty;
            totfilesRx++;
            NodeResources.AddLabel(lblTotFilesRx, totfilesRx.ToString());
            cwRx++;
            NodeResources.AddLabel(lblCwRx, cwRx.ToString());
            TransReference trRef = new TransReference();
            XDocument xDoc;
            String archiveFile;
            String msgDir = "";
            XElement datasource;
            XElement nativeType;
            String nativeString = String.Empty;
            XElement native;
            XNamespace ns = "http://www.cargowise.com/Schemas/Universal/2011/11";
            XNamespace nsa = "http://www.cargowise.com/Schemas/Native/2011/11";
            try
            {
                xDoc = XDocument.Load(xmlfile);
                var elSenderID = xDoc.Descendants().Where(n => n.Name == ns + "SenderID").FirstOrDefault();
                if (elSenderID != null)
                {
                    senderID = elSenderID.Value;
                }
                else
                {

                }
                var elRecipientID = xDoc.Descendants().Where(n => n.Name == ns + "RecipientID").FirstOrDefault();
                if (elRecipientID != null)
                {
                    recipientID = elRecipientID.Value;
                }
                else
                {

                }
                native = xDoc.Descendants().Where(n => n.Name == nsa + "Native").FirstOrDefault();
                if (native != null)
                {
                    datasource = xDoc.Descendants().Where(n => n.Name == ns + "DataSource").FirstOrDefault();
                    if (datasource != null)
                    {
                        nativeType = datasource.Element(ns + "Type");
                        nativeString = nativeType.Value;
                    }


                }
                try
                {
                    var pc = native.Descendants().Where(n => n.Name == ns + "ActionPurpose").FirstOrDefault();
                    if (pc != null)
                    {
                        reasonCode = pc.Element(ns + "Code").Value;
                    }

                }
                catch (Exception e)
                {
                    ProcessingErrors procerror = new ProcessingErrors();
                    CTCErrorCode error = new CTCErrorCode();
                    error.Code = NodeError.e110;
                    error.Description = "Error Processing NDM File. :" + e.Message;
                    error.Severity = "Warning";
                    procerror.ErrorCode = error;
                    procerror.SenderId = senderID;
                    procerror.RecipientId = recipientID;
                    procerror.FileName = xmlfile;
                    AddProcessingError(procerror);
                    thisResult.FolderLoc = Globals.FailLoc;
                    thisResult.Processed = false;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }

            }
            catch (Exception ex)
            {
                ProcessingErrors procerror = new ProcessingErrors();
                CTCErrorCode error = new CTCErrorCode();
                error.Code = NodeError.e110;
                error.Description = "Error Processing NDM File. :" + ex.Message;
                error.Severity = "Warning";
                procerror.ErrorCode = error;
                procerror.SenderId = senderID;
                procerror.RecipientId = recipientID;
                procerror.FileName = xmlfile;
                AddProcessingError(procerror);
                thisResult.FolderLoc = Globals.FailLoc;
                thisResult.Processed = false;
                thisResult.FileName = xmlfile;
                return thisResult;
            }

            CustProfileRecord custProfileRecord = new CustProfileRecord();
            custProfileRecord = GetCustomerProfile(senderID, recipientID, reasonCode, eventCode);

            SqlCommand remTodo = new SqlCommand("DELETE FROM TODO WHERE L_ID =@ID", sqlConn);
            SqlParameter remID = remTodo.Parameters.Add("@ID", SqlDbType.UniqueIdentifier);
            if (custProfileRecord.C_name != null)
            {
                //Is Client Active and not on hold
                if (custProfileRecord.C_is_active == "N" | custProfileRecord.C_on_hold == "Y")
                {
                    string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfileRecord.C_path, "Held"));
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlfile = f; }

                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_name + " is not Active or is on Hold. Moving to Hold and skipping" + "");
                    thisResult.Processed = true;
                    thisResult.FileName = xmlfile;
                    return thisResult;
                }
                else
                {
                    if (sqlConn.State == ConnectionState.Open)
                    {
                        sqlConn.Close();
                    }
                    CreateProfPath(custProfileRecord.C_path + "\\Processing", recipientID + reasonCode);
                    //Send file to Archive folder
                    archiveFile = Globals.ArchiveFile(custProfileRecord.C_path + "\\Archive", xmlfile);
                    string f = NodeResources.MoveFile(xmlfile, Path.Combine(custProfileRecord.C_path, "Processing", recipientID + reasonCode));
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlfile = f; }

                    //Add Initial file to TODO list. 
                    remID.Value = AddTodo(custProfileRecord.P_id, xmlfile, "Add_Transaction", DateTime.Now);
                    AddTransaction(custProfileRecord.C_id, custProfileRecord.P_id, Path.GetFileName(xmlfile), msgDir, true, trRef, archiveFile);
                    if (custProfileRecord.P_DTS)
                    {
                        remID.Value = AddTodo(custProfileRecord.P_id, xmlfile, "DTS_Processing", DateTime.Now);
                        DTS(custProfileRecord.P_id, xmlfile);
                    }
                    string result = "";
                    String strSuccess = "N";
                    //Message type will determine whether the message is sending to eAdapter (New Native file Imported/Converted) or sent to customer service. 
                    switch (custProfileRecord.P_MsgType.Trim())

                    {
                        case "Conversion":

                            thisResult = ConvertCWFile(custProfileRecord, xmlfile);


                            break;
                        case "eAdapter":
                            try
                            {
                                remID.Value = AddTodo(custProfileRecord.P_id, xmlfile, "Added to todo List", DateTime.Now);
                                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + custProfileRecord.C_name + " Recipient found " + recipientID + ".Sending file via CTC Adapter" + "");
                                String sendlog = CTCAdapter.SendMessage(custProfileRecord.P_server, xmlfile, recipientID, custProfileRecord.P_username, custProfileRecord.P_password);
                                // Test without sending

                                //String sendlog = "Send OK";
                                if (sendlog.Contains("SEND OK"))
                                {
                                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Transmitted Ok. ");
                                    if (sqlConn.State == ConnectionState.Open)
                                    {
                                        sqlConn.Close();
                                    }
                                    sqlConn.ConnectionString = Globals.connString();
                                    sqlConn.Open();
                                    remTodo.ExecuteNonQuery();
                                    sqlConn.Close();
                                    cwTx++;
                                    NodeResources.AddLabel(lblCWTx, cwTx.ToString());
                                    strSuccess = "Y";
                                    AddTransaction(custProfileRecord.C_id, custProfileRecord.P_id, Path.GetFileName(xmlfile), msgDir, false, trRef, archiveFile);
                                    System.GC.Collect();
                                    System.GC.WaitForPendingFinalizers();
                                    File.Delete(xmlfile);
                                    result = "CTC Adapter send Ok";
                                }
                                else
                                {
                                    result = "CTC Adapter send Failed: " + sendlog;
                                    ProcessingErrors procerror = new ProcessingErrors();
                                    procerror.ErrorCode.Code = NodeError.e104;
                                    procerror.SenderId = senderID;
                                    procerror.RecipientId = recipientID;
                                    procerror.ErrorCode.Description = "CTC Adapter send Failed: " + sendlog;
                                    procerror.ProcId = custProfileRecord.P_id;
                                    procerror.FileName = xmlfile;
                                    AddProcessingError(procerror);
                                    thisResult.FolderLoc = Globals.FailLoc;
                                    thisResult.Processed = false;
                                    thisResult.FileName = xmlfile;
                                }
                                thisResult.FileName = xmlfile;
                                AddTransactionLog(custProfileRecord.P_id, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via CTC Adapter with the Result: " + sendlog, DateTime.Now, strSuccess, custProfileRecord.P_BillTo, recipientID);
                            }
                            catch (Exception ex)
                            {
                                if (sqlConn.State == ConnectionState.Open)
                                {
                                    sqlConn.Close();
                                }
                                ProcessingErrors procerror = new ProcessingErrors();
                                procerror.ErrorCode.Code = NodeError.e104;
                                procerror.SenderId = senderID;
                                procerror.RecipientId = recipientID;
                                procerror.ErrorCode.Description = "CTC Adapter send Failed: " + ex.Message;
                                procerror.FileName = xmlfile;
                                procerror.ProcId = custProfileRecord.P_id;
                                AddProcessingError(procerror);
                                thisResult.FolderLoc = Globals.FailLoc;
                                thisResult.Processed = false;
                                thisResult.FileName = xmlfile;
                                AddTransactionLog(custProfileRecord.P_id, senderID, xmlfile, "File " + Path.GetFileName(xmlfile) + " sent via CTC Adapter Failed with the Result: " + ex.Message, DateTime.Now, strSuccess, custProfileRecord.P_BillTo, recipientID);
                                result = "CTC Adapter send failed: " + ex.Message;
                            }

                            break;

                        case "Forward":
                            break;

                        case "Original":
                            break;

                        case "Response":
                            switch (nativeString)
                            {
                                case "Product":


                                    break;
                            }

                            break;
                            // Rename the Cargowise XML File to the new Global XML


                    }
                    //Create the Delete from TODO list ready to be removed
                    String ftpSend = String.Empty;


                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Profile : " + custProfileRecord.P_description);
                }
            }
            System.GC.Collect();
            System.GC.WaitForPendingFinalizers();
            File.Delete(xmlfile);
            return thisResult;
        }

        private ProcessResult ProcessNon(String xmlfile)
        {
            ProcessResult thisResult = new ProcessResult();
            thisResult.Processed = false;
            return thisResult;
        }



        public bool CheckIfStopped()
        {
            if (btnStart.Text == "&Start")
            {
                return true;
            }
            else
            {
                return false;
            }

        }



        private void ProcessCargowiseXMLFile(String xmlfile)
        {
            FileInfo processingFile = new FileInfo(xmlfile);
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Found " + Path.GetFileName(xmlfile) + ". Checking for Cargowise File.");
            String ns = CTCAdapter.GetMessageNamespace(xmlfile);
            String appCode = CTCAdapter.GetApplicationCode(ns);

            String senderID = String.Empty;
            String recipientID = String.Empty;
            String reasonCode = String.Empty;
            ProcessResult canDelete = new ProcessResult();
            switch (appCode)
            {
                case "XMS":
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File is Cargowise (XMS) XML File :" + xmlfile + ". " + "");
                    canDelete = new ProcessResult { FileName = xmlfile, Processed = true };
                    MailModule.sendMsg(xmlfile, Globals.AlertsTo, "Processing XMS Message", "A new Legacy XMS Message has been found and the ProcessXMS function no longer exists");
                    break;
                case "UDM":
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File is Cargowise (UDM) XML File :" + xmlfile + ". " + "");
                    if (CTCAdapter.GetXMLType(xmlfile) == "UniversalEvent")
                    {
                        canDelete = ProcessXUE(xmlfile);
                    }
                    else
                    {
                        canDelete = ProcessUDM(xmlfile);
                    }
                    break;
                case "NDM":
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File is Cargowise (NDM) XML File :" + xmlfile + ". " + "");
                    canDelete = ProcessNDM(xmlfile);
                    break;
                default:
                    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File is not a valid Cargowise XML File :" + xmlfile + ". Checking Client Specific formats" + "");
                    canDelete = ProcessCustomFiles(xmlfile, string.Empty);
                    break;
            }
            if (canDelete.FileName != null)
            {
                if (canDelete.Processed)
                {
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    File.Delete(canDelete.FileName);
                }
                else
                {
                    string f = NodeResources.MoveFile(xmlfile, canDelete.FolderLoc);
                    if (f.StartsWith("Warning"))
                    {
                        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + f, Color.Red);
                    }
                    else
                    { xmlfile = f; }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void CreateProfPath(String path, String profile)
        {
            try
            {
                String profilePath = Path.Combine(path, profile);
                Directory.CreateDirectory(profilePath);
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("You do not have access to create a folder here.Please Check", "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (IOException exIO)
            {
                MessageBox.Show("Error Creating Directory: " + exIO.Message, "Folder Write Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void loadSettings()
        {
            var appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            var path = Path.Combine(appDataPath, "CTCNode");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            Globals.AppConfig = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name + ".XML";
            Globals.AppConfig = Path.Combine(path, Globals.AppConfig);
            FrmSettings settings = new FrmSettings();
            if (!File.Exists(Globals.AppConfig))
            {
                XDocument xmlConfig = new XDocument(
                            new XDeclaration("1.0", "UTF-8", "Yes"),
                            new XElement("CNode",
                            new XElement("Database"),
                            new XElement("Transmod"),
                            new XElement("HeartBeat"),
                            new XElement("Config")));
                xmlConfig.Save(Globals.AppConfig);
                settings.ShowDialog();
            }
            else
            {
                try
                {
                    Boolean loadconfig = false;
                    XmlDocument xmlConfig = new XmlDocument();
                    xmlConfig.Load(Globals.AppConfig);
                    XmlNodeList nodelist = xmlConfig.SelectNodes("/CNode/Database");
                    XmlNode xmlCatalog = nodelist[0].SelectSingleNode("Catalog");
                    XmlNode xnode;
                    if (xmlCatalog != null)
                    {
                        Globals.DataBase = xmlCatalog.InnerText;
                    }
                    else
                        loadconfig = true;
                    XmlNode xmlServer = nodelist[0].SelectSingleNode("Servername");
                    if (xmlServer != null)
                    {
                        Globals.ServerName = xmlServer.InnerText;
                    }
                    else
                        loadconfig = true;
                    XmlNode xmlUser = nodelist[0].SelectSingleNode("Username");
                    if (xmlUser != null)
                    {
                        Globals.UserName = xmlUser.InnerText;
                    }
                    else
                        loadconfig = true;
                    XmlNode xmlPassword = nodelist[0].SelectSingleNode("Password");
                    if (xmlUser != null)
                    {
                        Globals.Password = xmlPassword.InnerText;
                    }
                    else
                        loadconfig = true;
                    nodelist = xmlConfig.SelectNodes("/CNode/TransMod");
                    XmlNode xmlTransCatalog = nodelist[0].SelectSingleNode("Catalog");
                    if (xmlTransCatalog != null)
                    {
                        Globals.TransDatabase = xmlTransCatalog.InnerText;
                    }
                    else
                        loadconfig = true;
                    XmlNode xmlTransServer = nodelist[0].SelectSingleNode("Servername");
                    if (xmlTransServer != null)
                    {
                        Globals.TransServer = xmlTransServer.InnerText;
                    }
                    else
                        loadconfig = true;
                    XmlNode xmlTransUser = nodelist[0].SelectSingleNode("Username");
                    if (xmlTransUser != null)
                    {
                        Globals.TransUsername = xmlTransUser.InnerText;
                    }
                    else
                        loadconfig = true;
                    XmlNode xmlTransPassword = nodelist[0].SelectSingleNode("Password");
                    if (xmlTransPassword != null)
                    {
                        Globals.TransPassword = xmlTransPassword.InnerText;
                    }
                    else
                        loadconfig = true;
                    XmlNode xmlAlertsTo = nodelist[0].SelectSingleNode("AlertsTo");
                    if (xmlAlertsTo != null)
                    {
                        Globals.AlertsTo = xmlAlertsTo.InnerText;
                    }
                    else
                        loadconfig = true;
                    sqlConn = new SqlConnection();
                    sqlConn.ConnectionString = Globals.connString();
                    nodelist = null;
                    nodelist = xmlConfig.SelectNodes("/CNode/Config");
                    XmlNode xConfig = nodelist[0].SelectSingleNode("XmlLocation");
                    if (xConfig != null)
                    {
                        Globals.XMLLoc = xConfig.InnerText;

                    }
                    xConfig = nodelist[0].SelectSingleNode("ReportLocation");
                    if (xConfig != null)
                    {
                        Globals.ReportLoc = xConfig.InnerText;
                    }
                    xConfig = nodelist[0].SelectSingleNode("CustomerRootPath");
                    if (xConfig != null)
                    {
                        Globals.RootPath = xConfig.InnerText;
                    }
                    xConfig = nodelist[0].SelectSingleNode("CustomReceivePath");
                    if (xConfig != null)
                    {
                        Globals.CustomReceive = xConfig.InnerText;
                    }
                    xConfig = nodelist[0].SelectSingleNode("FailPath");
                    if (xConfig != null)
                    {
                        Globals.FailLoc = xConfig.InnerText;
                    }
                    xConfig = nodelist[0].SelectSingleNode("TestingLocation");
                    if (xConfig != null)
                    {
                        Globals.TestingLoc = xConfig.InnerText;
                    }
                    else
                        loadconfig = true;
                    XmlNodeList xSmtp = nodelist[0].SelectNodes("SMTP");
                    if (xSmtp[0] != null)
                    {
                        xnode = xSmtp[0].SelectSingleNode("SMTPServer");
                        if (xnode != null)
                        {
                            SMTPServer.Server = xnode.InnerText;
                            SMTPServer.Port = xnode.Attributes[0].InnerXml;
                        }
                        else
                            loadconfig = true;
                        xnode = xSmtp[0].SelectSingleNode("EmailAddress");
                        if (xnode != null)
                        { SMTPServer.Email = xnode.InnerText; }
                        else
                            loadconfig = true;
                        xnode = xSmtp[0].SelectSingleNode("SMTPUsername");
                        if (xnode != null)
                        {
                            SMTPServer.Username = xnode.InnerText;
                        }
                        else
                            loadconfig = true;
                        xnode = xSmtp[0].SelectSingleNode("SMTPPassword");
                        if (xnode != null)
                        {
                            SMTPServer.Password = xnode.InnerText;
                        }
                        else
                            loadconfig = true;
                    }
                    xConfig = nodelist[0].SelectSingleNode("XmlLocation");
                    nodelist = null;
                    //nodelist = xmlConfig.SelectNodes("/CNode/Config/Archive");
                    nodelist = xmlConfig.SelectNodes("/CNode/Config/Archive");
                    xnode = nodelist[0].SelectSingleNode("ArchiveLocation");
                    if (xnode != null)
                    {
                        Globals.ArchLoc = xnode.InnerText;
                    }
                    else
                        loadconfig = true;
                    xnode = nodelist[0].SelectSingleNode("ArchiveFrequency");
                    if (xnode != null)
                    {
                        Globals.ArcFrequency = xnode.InnerText;
                    }
                    else
                        loadconfig = true;
                    if (loadconfig)
                        throw new System.Exception("Mandatory Settings missing");
                }
                catch (Exception)
                {
                    MessageBox.Show("There was an error loading the Config files. Please check the settings", "Loading Settings", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FrmSettings FormSettings = new FrmSettings();
                    FormSettings.ShowDialog();
                    DialogResult dr = FormSettings.DialogResult;
                    if (dr == DialogResult.OK)
                    {
                        loadSettings();
                    }


                }

            }
            settings.Dispose();

        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (btnStart.Text == "&Stop")
                btnStart_Click(btnStart, null);
            FrmSettings FormSettings = new FrmSettings();
            FormSettings.ShowDialog();
            DialogResult dr = FormSettings.DialogResult;
            if (dr == DialogResult.OK)
            {
                loadSettings();
            }
        }

        private void customerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool started = false;
            if (btnStart.Text == "&Stop")
            {
                btnStart_Click(btnStart, null);
                started = true;
            }


            FrmCustomerProfile custProfile = new FrmCustomerProfile();
            custProfile.ShowDialog();
            if (started)
            {
                btnStart.BackColor = Color.LightCoral;
                btnStart.Text = "&Stop";
                RetrieveCustomerFiles();
                RetrieveEmails();
                ScanXMLFolders();
                tmrMain.Start();
                tslMain.Text = "Timed process running";
            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            loadSettings();
            tmrMain = new System.Timers.Timer();
            tmrMain.Interval = 30000;
            tmrMain.Elapsed += new ElapsedEventHandler(tmrMain_Elapsed);
            Globals.DbConn = ConfigurationManager.ConnectionStrings["CNodeBE.Properties.Settings.CNODEConnectionString"].ConnectionString;
            sqlConn = new SqlConnection(Globals.DbConn);
            tslMode.Text = "Production";
            productionToolStripMenuItem.Checked = true;
            // this.tslSpacer.Padding = new Padding((int)(this.Size.Width - 195), 0, 0, 0);
            // GetProcErrors();
            lblCwRx.Text = "0";
            lblFilesRx.Text = "0";
            lblCWTx.Text = "0";
            lblFilesTx.Text = "0";
            lblTotFilesRx.Text = "0";
            lblTotFilesTx.Text = "0";
            filesRx = 0;
            filesTx = 0;
            totfilesRx = 0;
            totfilesTx = 0;
            cwRx = 0;
            cwTx = 0;
            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            sqlConn.ConnectionString = Globals.connString();
            try
            {
                sqlConn.Open();
                GetProcErrors();
            }
            catch (Exception)
            {
                MessageBox.Show("Error opening Database. Please check settings and try again");
                FrmSettings settings = new FrmSettings();
                settings.ShowDialog();
            }


        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "&Start")
            {
                ((Button)sender).Text = "&Stop";
                ((Button)sender).BackColor = Color.LightCoral;

                //SetButtonText(btnStart, "&Stop");
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Timed processes running.");
                if (totfilesRx > 0 | totfilesTx > 0)
                {
                    DialogResult dr = new DialogResult();
                    dr = MessageBox.Show("Do you wish to reset the Counters as well?", "Start Process", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        lblCwRx.Text = "0";
                        lblFilesRx.Text = "0";
                        lblCWTx.Text = "0";
                        lblFilesTx.Text = "0";
                        lblTotFilesRx.Text = "0";
                        lblTotFilesTx.Text = "0";
                        filesRx = 0;
                        filesTx = 0;
                        totfilesRx = 0;
                        totfilesTx = 0;
                        cwRx = 0;
                        cwTx = 0;
                        dsMain.Tables["CurrentSummary"].Clear();
                    }
                }
                RetrieveEmails();
                RetrieveCustomerFiles();
                ScanXMLFolders();
                tmrMain.Start();
                tslMain.Text = "Timed process running";
                this.tslSpacer.Padding = new Padding(this.Size.Width - (195), 0, 0, 0);
            }
            else
            {
                ((Button)sender).Text = "&Start";
                ((Button)sender).BackColor = Color.LightGreen;
                NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Timed processes stopped.");
                tmrMain.Stop();
                tslMain.Text = "Timed process stopped";
            }
        }

        private void addSummRecord(DateTime datein, string recipient, string sender, string msgtype, TransReference tr, string filename, string result)
        {
            DataRow dr = dsMain.Tables["CurrentSummary"].NewRow();
            dr["DateIn"] = datein;
            dr["Recipient"] = recipient;
            dr["Sender"] = sender;
            dr["MsgType"] = msgtype;
            dr["Ref1"] = tr.Reference1;
            dr["Ref2"] = tr.Reference2;
            dr["Ref3"] = tr.Reference3;
            dr["Ref1Type"] = tr.Ref1Type;
            dr["Ref2Type"] = tr.Ref2Type;
            dr["Ref3Type"] = tr.Ref3Type;
            dr["filename"] = filename;
            dr["result"] = result;
            dsMain.Tables["CurrentSummary"].Rows.Add(dr);
        }


        private void xmlDebugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmXMLDebug XmlDebug = new frmXMLDebug();
            XmlDebug.Show();

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var systemPath = System.Environment.
                            GetFolderPath(
                                Environment.SpecialFolder.MyDocuments
                            );
            File.WriteAllText(Path.Combine(systemPath, "CTCNodeBE-Log.txt"), edLog.Text + Environment.NewLine);
            edLog.Text = "";

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (btnStart.Text == "&Stop")
                btnStart_Click(null, null);
            frmAbout About = new frmAbout();
            About.ShowDialog();
        }

        private void AddProcessingError(ProcessingErrors procError)
        {
            SqlCommand sqlProcErrors = new SqlCommand("Add_ProcessingError", sqlConn);
            sqlProcErrors.CommandType = CommandType.StoredProcedure;
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
            }
            sqlConn.Open();
            SqlParameter senderID = sqlProcErrors.Parameters.Add("@SENDERID", SqlDbType.VarChar, 15);
            SqlParameter recipientID = sqlProcErrors.Parameters.Add("@RECIPIENTID", SqlDbType.VarChar, 15);
            SqlParameter filename = sqlProcErrors.Parameters.Add("@FILENAME", SqlDbType.VarChar, -1);
            SqlParameter procDate = sqlProcErrors.Parameters.Add("@ProcDate", SqlDbType.DateTime);
            SqlParameter errorCode = sqlProcErrors.Parameters.Add("@ERRORCODE", SqlDbType.NChar, 10);
            SqlParameter errorDesc = sqlProcErrors.Parameters.Add("@ERRORDESC", SqlDbType.VarChar, 200);
            SqlParameter procId = sqlProcErrors.Parameters.Add("@PROCID", SqlDbType.UniqueIdentifier);
            senderID.Value = procError.SenderId;
            recipientID.Value = procError.RecipientId;
            filename.Value = Path.GetFileName(procError.FileName);
            errorCode.Value = procError.ErrorCode.Code;
            errorDesc.Value = procError.ErrorCode.Description;
            procDate.Value = DateTime.Now;
            procId.Value = procError.ProcId;
            sqlProcErrors.ExecuteNonQuery();
            GetProcErrors();
        }

        private void GetProcErrors()
        {

            if (sqlConn.State == ConnectionState.Open)
            {
                sqlConn.Close();
            }
            try
            {
                sqlConn.Open();
                SqlDataAdapter daProcErrors = new SqlDataAdapter("SELECT E_P, P_DESCRIPTION, E_SENDERID, E_RECIPIENTID, E_PROCDATE, E_FILENAME, E_ERRORDESC, E_ERRORCODE, " +
                                                                "E_PK, P_PATH FROM VW_ProcessngErrors ORDER BY E_PROCDATE", sqlConn);
                DataSet dsProcErrors = new DataSet();
                dgProcessingErrors.AutoGenerateColumns = false;
                daProcErrors.Fill(dsProcErrors, "PROCERRORS");
                dgProcessingErrors.DataSource = dsProcErrors;
                dgProcessingErrors.DataMember = "PROCERRORS";
                sqlConn.Close();
            }
            catch (Exception)
            {

            }
        }

        private void monthlyReportsToolStripMenuItem_Click(object sender, EventArgs e)
        {

            frmMonthly Monthly = new frmMonthly();
            Monthly.Show();
        }

        private void testXMLTransferToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CWTestModule testModule = new CWTestModule();
            testModule.Show();

        }

        private void deleteLineToolStripMenuItem_Click(object sender, EventArgs e)
        {

            int rowIndex = dgProcessingErrors.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            //DataGridViewRow row = dgProcessingErrors.Rows[rowIndex];
            DialogResult drDelete = MessageBox.Show("Are you sure you wish to delete Selected Event(s)?", "Delete from Error Processing", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (drDelete == DialogResult.Yes)
            {
                foreach (DataGridViewRow row in dgProcessingErrors.SelectedRows)
                {
                    SqlCommand execDelete = new SqlCommand("Delete from PROCESSING_ERRORS where E_PK = @E_ID", sqlConn);
                    Guid e_id = new Guid(row.Cells[0].Value.ToString());
                    execDelete.Parameters.Add("@E_ID", SqlDbType.UniqueIdentifier).Value = e_id;
                    try
                    {
                        if (sqlConn.State == ConnectionState.Open)
                        {
                            sqlConn.Close();
                        }
                        sqlConn.Open();
                        execDelete.ExecuteNonQuery();
                        sqlConn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("There was an error :" + ex.Message, "Error Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                MessageBox.Show("Error File(s) Deleted", "Error File(s) updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
                GetProcErrors();
            }
        }


        private void scratchPadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Scratchpad scratchpad = new Scratchpad();
            scratchpad.ShowDialog();

        }

        private void cargowiseFieldMappingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCargowiseValues cargowiseValues = new frmCargowiseValues();
            cargowiseValues.Show();
        }

        public void GenieMessage(XElement responseMsg)
        {
            filesTx++;
            NodeResources.AddLabel(lblFilesTx, filesTx.ToString());
            totfilesTx++;
            NodeResources.AddLabel(lblTotFilesTx, totfilesTx.ToString());
            //XElement genieESA = new XElement("ESA",
            //    new XElement("Activity", new XAttribute("id", activityId.Trim()),
            //    new XElement("Method", new XAttribute("name", messageName), new XAttribute("articleId", referenceField.Trim()), new XAttribute("result", msgResult), new XAttribute("message", msgDesc))));
            var result = CTCSoap.SendToGenie("WHOLESALEPAK", responseMsg.ToString(), responseMsg);
            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + result);
        }

        public XElement GenerateArticleXML(string id, string name, string idattribute, string articleid, bool msgResult, string message)
        {
            string result = string.Empty;
            XElement genieESA = new XElement("ESA",
                new XElement("Activity", new XAttribute("id", id.Trim()),
                new XElement("Method",
                        new XAttribute("name", name.Trim()),
                        new XAttribute(idattribute.Trim(), articleid.Trim()),
                        new XAttribute("action", "confirm"),
                        new XAttribute("result", msgResult),
                        new XAttribute("message", message)
                        )));
            return genieESA;
        }

        private void oneOffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //    FileInfo fi = new FileInfo(@"C:\TEMP\CTC NODE\Banksmeadow05-04.CSV");
            //    FileInfo fRef = new FileInfo(@"C:\TEMP\CTC NODE\REF-BNK.CSV");
            //    DataTable dt = NodeResources.ConvertCSVtoDataTable(fi.FullName);
            //    DataTable dtRef = NodeResources.ConvertCSVtoDataTable(fRef.FullName);

            //    DataSet dsPCFS = new DataSet();
            //    string SenderId = string.Empty;
            //    string RecipientID = string.Empty;
            //    DataTable tb = new DataTable();

            //    tb.Columns.Add(new DataColumn("PARTNUM", typeof(String)));
            //    tb.Columns.Add(new DataColumn("DESC", typeof(String)));
            //    tb.Columns.Add(new DataColumn("UOM", typeof(String)));
            //    tb.Columns.Add(new DataColumn("ARTICLE ID", typeof(string)));
            //    tb.Columns.Add(new DataColumn("Error", typeof(String)));

            //    DataTable tbPO = new DataTable();
            //    tbPO.Columns.Add(new DataColumn("CODE", typeof(String)));
            //    tbPO.Columns.Add(new DataColumn("DESC", typeof(String)));
            //    tbPO.Columns.Add(new DataColumn("QTY", typeof(int)));
            //    tbPO.Columns.Add(new DataColumn("UOM", typeof(String)));
            //    dsPCFS.Tables.Add(dt);
            //    tb.TableName = "Log";
            //    dsPCFS.Tables.Add(tb);

            //    SenderId = "WHOLESSYD2";
            //    RecipientID = "PCFSYDSYD";
            //    foreach (DataRow dr in dt.Rows)
            //    {
            //        string articleId = string.Empty;
            //        articleId = GetPCFSWSFArticleid(dr["SKU"].ToString());
            //        if (string.IsNullOrEmpty(articleId))
            //        {
            //            var refResult = (from r in dtRef.AsEnumerable() where r.Field<string>("PARTNUM")== dr["SKU"].ToString() select r.Field<string>("ARTICLE ID")).First<string>();
            //            DataRow drRef = dtRef.NewRow();                    
            //            DataRow drNew = tb.NewRow();
            //            drNew["PARTNUM"] = dr["SKU"].ToString();
            //            drNew["DESC"] = dr["NAME"].ToString();
            //            drNew["UOM"] = dr["OLDUOM"].ToString();
            //            drNew["ARTICLE ID"] = refResult;                   
            //            tb.Rows.Add(drNew);
            //        }
            //        else
            //        {
            //            DataRow drPO = tbPO.NewRow();
            //        drPO["CODE"] = dr["SKU"].ToString();
            //        if (dr["NAME"].ToString().Length > 80)
            //        {
            //            drPO["DESC"] = dr["NAME"].ToString().Substring(0,80);
            //        }
            //        else
            //        {
            //            drPO["DESC"] = dr["NAME"].ToString();
            //        }
            //       // drPO["DESC"] = dr["NAME"].ToString();
            //        drPO["QTY"] = dr["QTY"].ToString();
            //        drPO["UOM"] = dr["UOM"].ToString();
            //        tbPO.Rows.Add(drPO);

            //        }
            //    }
            //    StringBuilder sb = new StringBuilder();
            //    IEnumerable<string> columns = tb.Columns.Cast<DataColumn>().Select(col => col.ColumnName).ToArray();

            //    sb.AppendLine(string.Join(",", columns));
            //    foreach (DataRow dr in tb.Rows)
            //    {
            //        IEnumerable<string> fields = dr.ItemArray.Select(fld => fld.ToString()).ToArray();
            //        sb.AppendLine(string.Join(",", fields));
            //    }
            //    //File.WriteAllText(@"C:\TEMP\CTC NODE\Failed-Banksmeadow05-04.CSV", sb.ToString());

            //  //  ImportPO(tbPO, "Banksmeadow05-04-2018");
            //    WSPProduct(tb);
            //    //   File.Delete(fi.FullName);
            //    //    string xmlPath = Path.GetDirectoryName(Path.GetDirectoryName(fi.FullName));
            //    //   NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Converting CSV Data to Cargowise XML: " + SenderID + ": " + RecipientID + ": " + MsgType);
        }

        public void ImportPO(DataTable tb, string refPO, string senderCode, string recipCode)
        {
            //SqlDataAdapter daPo = new SqlDataAdapter("SELECT PO_ID, PO_POID, PO_ETA, PO_POCODE, PL_Code, PL_QTY, PL_UOM, PL_Article, PW_PARTNUM, PW_PRODUCTNAME, PW_CWUOM " +
            //                                    "FROM[PCFS-WSF-PurchaseOrder] AS po INNER JOIN " +
            //                                    "[PCFS-WSF-PurchaseLine] AS pol ON po.PO_ID = pol.PL_PO INNER JOIN " +
            //                                    "[PCFS-WSF-Products] AS prod ON pol.PL_Article = prod.PW_ARTICLEID " +
            //                                    "WHERE PO_ID = '" + poid + "'", sqlConn);
            //DataSet dsPo = new DataSet();
            //daPo.Fill(dsPo, "PO");
            //if (dsPo.Tables["PO"].Rows.Count > 0)
            {
                string filesPath = string.Empty;
                if (tslMode.Text == "Production")
                {
                    filesPath = Globals.XMLLoc;
                }
                else
                {
                    filesPath = Globals.TestingLoc;
                }
                UniversalInterchange uInt = new UniversalInterchange();
                UniversalInterchangeHeader uHeader = new UniversalInterchangeHeader();
                uHeader.RecipientID = recipCode;
                uHeader.SenderID = senderCode;
                uInt.Header = uHeader;
                UniversalInterchangeBody uBody = new UniversalInterchangeBody();
                UniversalShipmentData uShipData = new UniversalShipmentData();
                Shipment shipment = new Shipment();
                DataContext dc = new DataContext();
                List<DataTarget> dataTargetColl = new List<DataTarget>();
                DataTarget dataTarget = new DataTarget();
                dataTarget.Type = "WarehouseReceive";
                dataTargetColl.Add(dataTarget);
                dc.DataTargetCollection = dataTargetColl.ToArray();
                ShipmentOrder pOrder = new ShipmentOrder();
                pOrder.OrderNumber = refPO;
                pOrder.ClientReference = refPO;
                shipment.DataContext = dc;
                //TODO 
                //Create Variable to allow for Return Authority
                CodeDescriptionPair codePair = new CodeDescriptionPair();
                codePair.Code = "REC";
                codePair.Description = "GOODS RECEIPT";
                pOrder.Type = codePair;
                ShipmentOrderWarehouse warehouse = new ShipmentOrderWarehouse();
                warehouse.Code = "SYD";
                warehouse.Name = "PCFS SYDNEY";
                pOrder.Warehouse = warehouse;
                List<Date> dateColl = new List<Date>();
                Date orderDate = new Date();
                orderDate.Type = DateType.BookingConfirmed;
                orderDate.Value = DateTime.Now.ToString("s");
                orderDate.IsEstimate = false;
                orderDate.IsEstimateSpecified = true;
                dateColl.Add(orderDate);
                pOrder.DateCollection = dateColl.ToArray();
                List<ShipmentOrderOrderLineCollection> orderLineColl = new List<ShipmentOrderOrderLineCollection>();
                ShipmentOrderOrderLineCollection orderLine = new ShipmentOrderOrderLineCollection();
                int i = 0;
                orderLine.Content = CollectionContent.Complete;
                List<ShipmentOrderOrderLineCollectionOrderLine> olColl = new List<ShipmentOrderOrderLineCollectionOrderLine>();
                orderLine.ContentSpecified = true;
                foreach (DataRow dr in tb.Rows)
                {
                    i++;
                    ShipmentOrderOrderLineCollectionOrderLine oLine = new ShipmentOrderOrderLineCollectionOrderLine();
                    oLine.ExpectedQuantity = decimal.Parse(dr["QTY"].ToString());
                    oLine.ExpectedQuantitySpecified = true;

                    oLine.LineNumber = i;
                    oLine.LineNumberSpecified = true;
                    CodeDescriptionPair cdUnit = new CodeDescriptionPair();
                    cdUnit.Code = "UNT";
                    oLine.OrderedQtyUnit = cdUnit;
                    Product product = new Product();
                    product.Code = dr["CODE"].ToString(); ;
                    product.Description = dr["DESC"].ToString();
                    oLine.Product = product;
                    olColl.Add(oLine);
                }
                orderLine.OrderLine = olColl.ToArray();
                pOrder.OrderLineCollection = orderLine;
                List<OrganizationAddress> OrgAddColl = new List<OrganizationAddress>();
                OrganizationAddress oa = new OrganizationAddress();
                oa.AddressType = "ConsignorDocumentaryAddress";
                oa.OrganizationCode = senderCode;
                OrgAddColl.Add(oa);
                shipment.OrganizationAddressCollection = OrgAddColl.ToArray();
                shipment.Order = pOrder;
                uShipData.Shipment = shipment;
                uBody.BodyField = uShipData;
                uShipData.version = "1.1";
                uShipData.Shipment = shipment;
                String cwXML = Path.Combine(filesPath, senderCode + "PO" + pOrder.OrderNumber.Trim() + ".xml");
                int iFileCount = 0;
                while (File.Exists(cwXML))
                {
                    iFileCount++;
                    cwXML = Path.Combine(filesPath, senderCode + "PO" + pOrder.OrderNumber.Trim() + iFileCount + ".xml");
                }
                Stream outputCW = File.Open(cwXML, FileMode.Create);
                StringWriter writer = new StringWriter();
                uInt.Body = uBody;
                XmlSerializer xSer = new XmlSerializer(typeof(UniversalInterchange));
                var cwNSUniversal = new XmlSerializerNamespaces();
                cwNSUniversal.Add("", "http://www.cargowise.com/Schemas/Universal/2011/11");
                xSer.Serialize(outputCW, uInt, cwNSUniversal);
                outputCW.Flush();
                outputCW.Close();
            }
        }



        // public ProcessResult Cargowise2FreightTracker(string xmlFile, CustProfileRecord cust)
        //{
        //ProcessResult result = new ProcessResult();
        //UniversalInterchange cwUXML = new UniversalInterchange();
        ////using (FileStream fStream = new FileStream(xmlFile, FileMode.Open))
        ////{
        ////    XmlSerializer cwConvert = new XmlSerializer(typeof(UniversalInterchange));
        ////    cwUXML = (UniversalInterchange)cwConvert.Deserialize(fStream);
        ////    fStream.Close();
        ////}
        ////string custFolder = Path.Combine(cust.C_path, "Processing", cust.P_Recipientid);
        ////CWShipment cwConsol = new CWShipment();
        ////cwConsol = cwUXML.Body.BodyField.Shipment;


        ////foreach (RecipientRole role in cwConsol.DataContext.RecipientRoleCollection)
        ////{
        ////    if (role.Code == RecipientRoleCode.DCA)
        ////    {
        ////        if (cwConsol.DataContext.DataSourceCollection.Count() == 1 || cwConsol.DataContext.DataSourceCollection[0].Type == "CustomsDeclaration")
        ////        {
        ////            //Brokerage only
        ////            FreightTracker.Booking freightTrackerBooking = new FreightTracker.Booking();
        ////            freightTrackerBooking.EDIDateTime = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'sszzz");
        ////            freightTrackerBooking.CustomerCode = cwUXML.Header.SenderID;

        ////            if (cwConsol.PortOfDischarge.Code.Substring(0, 2) == "AU")
        ////                freightTrackerBooking.JobType = FreightTracker.BookingJobType.Import;
        ////            else
        ////                freightTrackerBooking.JobType = FreightTracker.BookingJobType.Export;
        ////            freightTrackerBooking.JobTypeSpecified = true;
        ////            freightTrackerBooking.JobTypeSpecified = true;
        ////            freightTrackerBooking.VesselName = cwConsol.VesselName;
        ////            freightTrackerBooking.VoyageNumber = cwConsol.VoyageFlightNo;
        ////            foreach (CustomizedField cf in cwConsol.CustomizedFieldCollection)
        ////            {
        ////                if (cf.Key == "KN Tracking")
        ////                {
        ////                    freightTrackerBooking.BookingReference = cf.Value.Trim();
        ////                }
        ////            }
        ////            if (!string.IsNullOrEmpty(cwConsol.AgentsReference))
        ////            {
        ////                freightTrackerBooking.CustomerReference = cwConsol.AgentsReference;
        ////            }

        ////            if (string.IsNullOrEmpty(freightTrackerBooking.BookingReference))
        ////            {
        ////                freightTrackerBooking.BookingReference = cwConsol.DataContext.DataSourceCollection[0].Key;
        ////            }

        ////            freightTrackerBooking.VesselName = cwConsol.VesselName;
        ////            freightTrackerBooking.VoyageNumber = cwConsol.VoyageFlightNo;
        ////            List<FreightTracker.BookingAddressCollectionAddress> bookingAddressColl = new List<FreightTracker.BookingAddressCollectionAddress>();
        ////            foreach (OrganizationAddress orgAdd in cwConsol.OrganizationAddressCollection)
        ////            {
        ////                FreightTracker.BookingAddressCollectionAddress bookingAddress = new FreightTracker.BookingAddressCollectionAddress();
        ////                switch (orgAdd.AddressType)
        ////                {
        ////                    case "ConsigneePickupDeliveryAddress":
        ////                        bookingAddress.AddressType = FreightTracker.BookingAddressCollectionAddressAddressType.Delivery;
        ////                        bookingAddress.AddressTypeSpecified = true;
        ////                        bookingAddress.AddressLine1 = orgAdd.Address1;
        ////                        bookingAddress.AddressLine2 = orgAdd.Address2;
        ////                        bookingAddress.AddressName = orgAdd.CompanyName;
        ////                        bookingAddress.AddressCode = orgAdd.OrganizationCode;
        ////                        bookingAddress.AddressPostCode = orgAdd.Postcode;
        ////                        bookingAddress.AddressState = orgAdd.State;
        ////                        bookingAddress.AddressSuburb = orgAdd.City;
        ////                        bookingAddressColl.Add(bookingAddress);
        ////                        break;

        ////                    case "ConsigneeDocumentaryAddress":
        ////                        bookingAddress.AddressType = FreightTracker.BookingAddressCollectionAddressAddressType.Consignee;
        ////                        bookingAddress.AddressTypeSpecified = true;
        ////                        bookingAddress.AddressLine1 = orgAdd.Address1;
        ////                        bookingAddress.AddressLine2 = orgAdd.Address2;
        ////                        bookingAddress.AddressName = orgAdd.CompanyName;
        ////                        bookingAddress.AddressCode = orgAdd.OrganizationCode;
        ////                        bookingAddress.AddressPostCode = orgAdd.Postcode;
        ////                        bookingAddress.AddressState = orgAdd.State;
        ////                        bookingAddress.AddressSuburb = orgAdd.City;
        ////                        bookingAddressColl.Add(bookingAddress);
        ////                        break;

        ////                    case "ImporterPickupDeliveryAddress":
        ////                        bookingAddress.AddressType = FreightTracker.BookingAddressCollectionAddressAddressType.Delivery;
        ////                        bookingAddress.AddressTypeSpecified = true;
        ////                        bookingAddress.AddressLine1 = orgAdd.Address1;
        ////                        bookingAddress.AddressLine2 = orgAdd.Address2;
        ////                        bookingAddress.AddressName = orgAdd.CompanyName;
        ////                        bookingAddress.AddressCode = orgAdd.OrganizationCode;
        ////                        bookingAddress.AddressPostCode = orgAdd.Postcode;
        ////                        bookingAddress.AddressState = orgAdd.State;
        ////                        bookingAddress.AddressSuburb = orgAdd.City;
        ////                        bookingAddressColl.Add(bookingAddress);
        ////                        break;
        ////                    case "ImporterDocumentaryAddress":
        ////                        bookingAddress.AddressType = FreightTracker.BookingAddressCollectionAddressAddressType.Consignee;
        ////                        bookingAddress.AddressTypeSpecified = true;
        ////                        bookingAddress.AddressLine1 = orgAdd.Address1;
        ////                        bookingAddress.AddressLine2 = orgAdd.Address2;
        ////                        bookingAddress.AddressName = orgAdd.CompanyName;
        ////                        bookingAddress.AddressCode = orgAdd.OrganizationCode;
        ////                        bookingAddress.AddressPostCode = orgAdd.Postcode;
        ////                        bookingAddress.AddressState = orgAdd.State;
        ////                        bookingAddress.AddressSuburb = orgAdd.City;
        ////                        bookingAddressColl.Add(bookingAddress);
        ////                        break;
        ////                }

        ////            }
        ////            freightTrackerBooking.AddressCollection = bookingAddressColl.ToArray();
        ////            List<FreightTracker.BookingContainerCollectionContainer> bookingContainerCol = new List<FreightTracker.BookingContainerCollectionContainer>();
        ////            foreach (Container container in cwConsol.ContainerCollection.Container)
        ////            {
        ////                FreightTracker.BookingContainerCollectionContainer bookingContainer = new FreightTracker.BookingContainerCollectionContainer();
        ////                bookingContainer.ContainerNumber = container.ContainerNumber;
        ////                bookingContainer.ISO = container.ContainerType.ISOCode;
        ////                bookingContainer.GrossWeight = container.GrossWeight.ToString();
        ////                bookingContainer.SealNumber = container.Seal;
        ////                if (freightTrackerBooking.JobType == FreightTracker.BookingJobType.Import)
        ////                {
        ////                    if (!string.IsNullOrEmpty(cwConsol.LocalProcessing.FCLDeliveryEquipmentNeeded.Code))
        ////                    {
        ////                        bookingContainer.DropMode = (FreightTracker.BookingContainerCollectionContainerDropMode)Enum.Parse(typeof(FreightTracker.BookingContainerCollectionContainerDropMode), cwConsol.LocalProcessing.FCLDeliveryEquipmentNeeded.Code);

        ////                    }
        ////                    else
        ////                        bookingContainer.DropMode = FreightTracker.BookingContainerCollectionContainerDropMode.ASK;
        ////                }
        ////                else
        ////                {
        ////                    if (!string.IsNullOrEmpty(cwConsol.LocalProcessing.FCLPickupEquipmentNeeded.Code))
        ////                    {
        ////                        bookingContainer.DropMode = (FreightTracker.BookingContainerCollectionContainerDropMode)Enum.Parse(typeof(FreightTracker.BookingContainerCollectionContainerDropMode), cwConsol.LocalProcessing.FCLPickupEquipmentNeeded.Code);

        ////                    }
        ////                    else
        ////                        bookingContainer.DropMode = FreightTracker.BookingContainerCollectionContainerDropMode.ASK;
        ////                }
        ////                bookingContainer.DropModeSpecified = true;
        ////                if (string.IsNullOrEmpty(cwConsol.LocalProcessing.EstimatedDelivery))
        ////                {
        ////                    bookingContainer.RequiredDate = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'sszzz");
        ////                }
        ////                else
        ////                {
        ////                    bookingContainer.RequiredDate = cwConsol.LocalProcessing.EstimatedDelivery;
        ////                }
        ////                bookingContainerCol.Add(bookingContainer);

        ////            }
        ////            freightTrackerBooking.ContainerCollection = bookingContainerCol.ToArray();
        ////            String cwXML = Path.Combine(custFolder, cwUXML.Header.SenderID + freightTrackerBooking.BookingReference + ".xml");
        ////            int iFileCount = 0;
        ////            while (File.Exists(cwXML))
        ////            {
        ////                iFileCount++;
        ////                cwXML = Path.Combine(custFolder, cwUXML.Header.SenderID + freightTrackerBooking.BookingReference + iFileCount + ".xml");
        ////            }
        ////            Stream outputFreightTracker = File.Open(cwXML, FileMode.Create);
        ////            StringWriter writer = new StringWriter();
        ////            XmlSerializer xSer = new XmlSerializer(typeof(FreightTracker.Booking));
        ////            var freightTrackerNS = new XmlSerializerNamespaces();
        ////            freightTrackerNS.Add("", "http://freighttracker.com.au/Schemas/ImportBooking");
        ////            xSer.Serialize(outputFreightTracker, freightTrackerBooking, freightTrackerNS);
        ////            outputFreightTracker.Flush();
        ////            outputFreightTracker.Close();
        ////            string fResult = NodeResources.FtpSend(cwXML, cust);
        ////            if (!fResult.Contains("Ok"))
        ////            {
        ////                result.FileName = NodeResources.MoveFile(xmlFile, Path.Combine(cust.C_path, "Failed"));
        ////                result.Processed = false;
        ////                result.FolderLoc = Path.GetDirectoryName(result.FileName);

        ////            }
        ////            else
        ////            {
        ////                NodeResources.AddRTBText(edLog, string.Format("{0:g} ", DateTime.Now) + " FreightTracker Booking File created and sent. Ok.", Color.Green);
        ////                result.Processed = true;
        ////            }


        ////        }
        ////        else
        ////        {
        ////            //Consol and Shipment
        ////            if (cwConsol.SubShipmentCollection != null)
        ////            {
        ////                foreach (CWShipment shipment in cwConsol.SubShipmentCollection)
        ////                {
        ////                    FreightTracker.Booking freightTrackerBooking = new FreightTracker.Booking();
        ////                    freightTrackerBooking.CustomerCode = cwUXML.Header.SenderID;
        ////                    freightTrackerBooking.EDIDateTime = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'sszzz");
        ////                    foreach (CustomizedField cf in cwConsol.CustomizedFieldCollection)
        ////                    {
        ////                        if (cf.Key == "KN Tracking")
        ////                        {
        ////                            freightTrackerBooking.BookingReference = cf.Value.Trim();
        ////                        }
        ////                    }
        ////                    if (!string.IsNullOrEmpty(cwConsol.AgentsReference))
        ////                    {
        ////                        freightTrackerBooking.CustomerReference = cwConsol.AgentsReference;
        ////                    }

        ////                    if (string.IsNullOrEmpty(freightTrackerBooking.BookingReference))
        ////                    {
        ////                        freightTrackerBooking.BookingReference = cwConsol.DataContext.DataSourceCollection[0].Key;
        ////                    }


        ////                    if (string.IsNullOrEmpty(freightTrackerBooking.BookingReference))
        ////                    {
        ////                        foreach (DataSource ds in shipment.DataContext.DataSourceCollection)
        ////                        {
        ////                            switch (ds.Type)
        ////                            {
        ////                                case "ForwardingShipment":
        ////                                    freightTrackerBooking.BookingReference = ds.Key;
        ////                                    break;
        ////                            }
        ////                        }
        ////                    }


        ////                    if (shipment.PortOfDischarge.Code.Substring(0, 2) == "AU")
        ////                        freightTrackerBooking.JobType = FreightTracker.BookingJobType.Import;
        ////                    else
        ////                        freightTrackerBooking.JobType = FreightTracker.BookingJobType.Export;
        ////                    freightTrackerBooking.JobTypeSpecified = true;
        ////                    freightTrackerBooking.VesselName = shipment.VesselName;
        ////                    freightTrackerBooking.VoyageNumber = shipment.VoyageFlightNo;
        ////                    List<FreightTracker.BookingAddressCollectionAddress> bookingAddressColl = new List<FreightTracker.BookingAddressCollectionAddress>();
        ////                    foreach (OrganizationAddress orgAdd in shipment.OrganizationAddressCollection)
        ////                    {
        ////                        FreightTracker.BookingAddressCollectionAddress bookingAddress = new FreightTracker.BookingAddressCollectionAddress();
        ////                        switch (orgAdd.AddressType)
        ////                        {
        ////                            case "ConsigneePickupDeliveryAddress":
        ////                                bookingAddress.AddressType = FreightTracker.BookingAddressCollectionAddressAddressType.Consignee;
        ////                                bookingAddress.AddressTypeSpecified = true;
        ////                                bookingAddress.AddressLine1 = orgAdd.Address1;
        ////                                bookingAddress.AddressLine2 = orgAdd.Address2;
        ////                                bookingAddress.AddressName = orgAdd.CompanyName;
        ////                                bookingAddress.AddressCode = orgAdd.OrganizationCode;
        ////                                bookingAddress.AddressPostCode = orgAdd.Postcode;
        ////                                bookingAddress.AddressState = orgAdd.State;
        ////                                bookingAddress.AddressSuburb = orgAdd.City;
        ////                                bookingAddressColl.Add(bookingAddress);
        ////                                break;

        ////                            case "ImporterPickupDeliveryAddress":
        ////                                break;
        ////                        }

        ////                    }
        ////                    freightTrackerBooking.AddressCollection = bookingAddressColl.ToArray();
        ////                    List<FreightTracker.BookingContainerCollectionContainer> bookingContainerCol = new List<FreightTracker.BookingContainerCollectionContainer>();
        ////                    foreach (Container container in shipment.ContainerCollection.Container)
        ////                    {
        ////                        FreightTracker.BookingContainerCollectionContainer bookingContainer = new FreightTracker.BookingContainerCollectionContainer();
        ////                        bookingContainer.ContainerNumber = container.ContainerNumber;
        ////                        bookingContainer.ISO = container.ContainerType.ISOCode;
        ////                        bookingContainer.GrossWeight = container.GrossWeight.ToString();
        ////                        bookingContainer.SealNumber = container.Seal;
        ////                        if (freightTrackerBooking.JobType == FreightTracker.BookingJobType.Import)
        ////                        {
        ////                            if (!string.IsNullOrEmpty(shipment.LocalProcessing.FCLDeliveryEquipmentNeeded.Code))
        ////                            {
        ////                                bookingContainer.DropMode = (FreightTracker.BookingContainerCollectionContainerDropMode)Enum.Parse(typeof(FreightTracker.BookingContainerCollectionContainerDropMode), shipment.LocalProcessing.FCLDeliveryEquipmentNeeded.Code);

        ////                            }
        ////                            else
        ////                                bookingContainer.DropMode = FreightTracker.BookingContainerCollectionContainerDropMode.ASK;
        ////                        }
        ////                        else
        ////                        {
        ////                            if (!string.IsNullOrEmpty(shipment.LocalProcessing.FCLPickupEquipmentNeeded.Code))
        ////                            {
        ////                                bookingContainer.DropMode = (FreightTracker.BookingContainerCollectionContainerDropMode)Enum.Parse(typeof(FreightTracker.BookingContainerCollectionContainerDropMode), shipment.LocalProcessing.FCLPickupEquipmentNeeded.Code);

        ////                            }
        ////                            else
        ////                                bookingContainer.DropMode = FreightTracker.BookingContainerCollectionContainerDropMode.ASK;
        ////                        }
        ////                        bookingContainer.DropModeSpecified = true;
        ////                        if (string.IsNullOrEmpty(shipment.LocalProcessing.EstimatedDelivery))
        ////                        {
        ////                            bookingContainer.RequiredDate = DateTime.Now.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'sszzz");
        ////                        }
        ////                        else
        ////                        {
        ////                            bookingContainer.RequiredDate = shipment.LocalProcessing.EstimatedDelivery;
        ////                        }

        ////                        bookingContainerCol.Add(bookingContainer);
        ////                    }
        ////                    freightTrackerBooking.ContainerCollection = bookingContainerCol.ToArray();
        ////                    String cwXML = Path.Combine(custFolder, cwUXML.Header.SenderID + freightTrackerBooking.BookingReference + ".xml");
        ////                    int iFileCount = 0;
        ////                    while (File.Exists(cwXML))
        ////                    {
        ////                        iFileCount++;
        ////                        cwXML = Path.Combine(custFolder, cwUXML.Header.SenderID + freightTrackerBooking.BookingReference + iFileCount + ".xml");
        ////                    }
        ////                    Stream outputFreightTracker = File.Open(cwXML, FileMode.Create);
        ////                    StringWriter writer = new StringWriter();
        ////                    XmlSerializer xSer = new XmlSerializer(typeof(FreightTracker.Booking));
        ////                    var freightTrackerNS = new XmlSerializerNamespaces();
        ////                    freightTrackerNS.Add("", "http://freighttracker.com.au/Schemas/ImportBooking");
        ////                    xSer.Serialize(outputFreightTracker, freightTrackerBooking, freightTrackerNS);
        ////                    outputFreightTracker.Flush();
        ////                    outputFreightTracker.Close();
        //// NodeResources.FtpSend(cwXML, cust);
        //string fResult = NodeResources.FtpSend(cwXML, cust);
        //if (!fResult.Contains("Ok"))
        //{
        //    result.FileName = NodeResources.MoveFile(xmlFile, Path.Combine(cust.C_path, "Failed"));
        //    result.Processed = false;
        //    result.FolderLoc = Path.GetDirectoryName(result.FileName);

        //}
        //else
        //{
        //    NodeResources.AddRTBText(edLog, string.Format("{0:g} ", DateTime.Now) + " FreightTracker Booking File created and sent. Ok.", Color.Green);
        //    result.Processed = true;
        //}


        // }
        //}

        // }
        //        }
        //    }

        //    return result;
        //}

        private void testingToolStringMenuItem_Click(object sender, EventArgs e)
        {
            if (testingToolStripMenuItem.Checked)
            {
                productionToolStripMenuItem.Checked = false;
                tslMode.Text = "Testing";
            }
            else
            {
                productionToolStripMenuItem.Checked = true;
                tslMode.Text = "Production";
            }
        }

        private void productionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (productionToolStripMenuItem.Checked)
            {
                testingToolStripMenuItem.Checked = false;
                tslMode.Text = "Production";
            }
            else
            {
                testingToolStripMenuItem.Checked = true;
                tslMode.Text = "Testing";
            }
        }

        private void rpttestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMonthlyReport monthlyreport = new frmMonthlyReport();
            monthlyreport.Show();
        }
    }
}








