﻿using System;

namespace CNodeBE
{
    public class DataItem
    {
        public Guid ValueItem { get; set; }

        public string StringItem { get; set; }
    }
}
