﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using NodeResources;
using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Schema;

namespace CNodeBE
{
    public class ProfileOperations
    {
        #region members
        private string connString;
        #endregion

        #region properties
        public string ConnString
        {
            get { return connString; }
            set
            {
                connString = value;
            }
        }
        #endregion

        #region Constructors
        public ProfileOperations(string connString)
        {
            ConnString = connString;
        }
        #endregion

        #region methods

        public vw_CustomerProfile IdentifyXML(TaskList task)
        {
            string lo = Globals.AppLogPath;
            string cp = string.Empty;
            string de = string.Empty;
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;
            ConnectionManager connMgr = new ConnectionManager(Globals.ServerName, Globals.DataBase, Globals.UserName, Globals.Password);
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
            {
                var profList = uow.CustProfiles.Find(x => x.P_ACTIVE == "Y" && !string.IsNullOrEmpty(x.P_XSD)).ToList();
                if (profList.Count > 0)
                {
                    foreach (var p in profList)
                    {
                        var schemaFile = Path.Combine(p.C_PATH, "Lib", p.P_XSD);
                        if (File.Exists(schemaFile))
                        {
                            try
                            {
                                var schema = GetSchema(schemaFile);
                                XmlReaderSettings settings = new XmlReaderSettings();
                                settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                                settings.ValidationType = ValidationType.Schema;
                                settings.Schemas.Add(schema);
                                settings.ValidationEventHandler += (o, ea) =>
                                {
                                    throw new XmlSchemaValidationException(
                                        string.Format("Schema Not Found: {0}",
                                                      ea.Message),
                                        ea.Exception);
                                };
                                if (CheckValidXSD(Path.Combine(task.TL_Path, task.TL_FileName), settings))
                                {

                                    return p;
                                }



                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        else
                        {
                            cp = p.P_DESCRIPTION;
                            de = "Schema library file not found (" + schemaFile + ").";
                            di = Path.Combine(p.C_PATH, "Lib");


                        }
                    }
                }

                return null;
            }

        }

        public vw_CustomerProfile IdentifyXML(vw_CustomerProfile prof, string file)
        {
            string lo = Globals.AppLogPath;
            string cp = string.Empty;
            string de = string.Empty;
            DateTime ti = DateTime.Now;
            string er = string.Empty;
            string di = string.Empty;

            var schemaFile = Path.Combine(prof.C_PATH, "Lib", prof.P_XSD);
            if (File.Exists(schemaFile))
            {
                try
                {
                    var schema = GetSchema(schemaFile);
                    XmlReaderSettings settings = new XmlReaderSettings();
                    settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                    settings.ValidationType = ValidationType.Schema;
                    settings.Schemas.Add(schema);
                    settings.ValidationEventHandler += (o, ea) =>
                    {
                        throw new XmlSchemaValidationException(
                            string.Format("Schema Not Found: {0}",
                                          ea.Message),
                            ea.Exception);
                    };
                    if (CheckValidXSD(file, settings))
                    {

                        return prof;
                    }



                }
                catch (Exception ex)
                {

                }
            }

            return null;


        }
        private bool CheckValidXSD(string file, XmlReaderSettings settings)
        {
            using (var stream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                try
                {
                    using (var cusXML = XmlReader.Create(stream, settings))
                    {
                        while (cusXML.Read())
                        {

                        }
                        stream.Close();


                        return true;

                    }
                }
                catch (Exception ex)
                {

                    return false;
                }

            }
        }

        public vw_CustomerProfile GetProfile(TaskList task)
        {
            if (task != null)
            {


                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
                {
                    var profile = uow.CustProfiles.Find(x => x.P_ID == task.TL_P && x.P_ACTIVE == "Y").FirstOrDefault();
                    return profile ?? null;

                }
            }
            return null;

        }

        public vw_CustomerProfile GetInactiveProfile(TaskList task)
        {
            if (task != null)
            {


                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
                {
                    var profile = uow.CustProfiles.Find(x => x.P_ID == task.TL_P && x.P_ACTIVE == "N").FirstOrDefault();
                    return profile ?? null;

                }
            }
            return null;
        }

        public string GetParamValue(string paramArray, string paramToFind)
        {
            var result = string.Empty;
            var paramlist = paramArray.Split('|').ToList();

            foreach (var p in paramlist)
            {
                if (!string.IsNullOrEmpty(p))
                {
                    var pList = p.Split(',').ToList();
                    var profParma = pList.Where(toFind => toFind.Contains(paramToFind)).FirstOrDefault();
                    if (!string.IsNullOrEmpty(profParma))
                    {
                        var value = profParma.Split(':');
                        if (value.Count() == 2)
                        {
                            return value[1];
                        }
                    }


                }
            }




            return result;
        }

        public vw_CustomerProfile GetProfile(string tL_SenderID, string tL_RecipientID, string actionPurpose, string eventCode, string subject, string appCode, string cwFileName)
        {
            if (!string.IsNullOrEmpty(tL_SenderID) || !string.IsNullOrEmpty(tL_RecipientID))
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(connString)))
                {
                    var profList = uow.CustProfiles.Find(x => x.P_SENDERID == tL_SenderID
                                                        && x.P_RECIPIENTID == tL_RecipientID
                                                        ).ToList();
                    if (profList.Count > 0)
                    {
                        foreach (var p in profList)
                        {
                            if (CheckValidProfile(p, actionPurpose, eventCode, subject, appCode, cwFileName))
                            {
                                return p;
                            }
                        }
                    }
                }

            }

            return null;
        }

        public vw_CustomerProfile GetProfileFromAPiLog(ApiLog apiLog, string originalFile)
        {

            using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.NodeDataContext(connString)))
            {
                var proflist = uow.CustProfiles.Find(x => x.P_SENDERID == apiLog.F_SENDERID &&
                                                x.P_RECIPIENTID == apiLog.F_CLIENTID &&
                                                x.P_CWAPPCODE == apiLog.F_APPCODE &&
                                                x.P_ACTIVE == "Y").ToList();
                if (proflist.Count > 0)
                {
                    var profile = (from x in proflist
                                   where x.P_CWSUBJECT == apiLog.F_SUBJECT
                                   select x).FirstOrDefault();
                    if (profile == null)
                    {
                        profile = (from x in proflist
                                   where x.P_CWSUBJECT == "*"
                                   select x).FirstOrDefault();
                    }
                    return profile;
                }

            }
            return null;

        }
        #endregion

        #region helpers
        private XmlSchema GetSchema(string schemaFile)
        {
            var sXsd = new XmlTextReader(schemaFile);
            return XmlSchema.Read(sXsd, CustomValidateHandler.HandlerErrors);

        }

        private bool CheckValidProfile(vw_CustomerProfile cp, string reasonCode, string eventCode, string subject, string appCode, string cwFileName)
        {
            bool eventFound = false;
            bool purposeFound = false;
            if (!string.IsNullOrEmpty(cp.P_CWSUBJECT))
            {
                if (cp.P_CWSUBJECT.Trim() != "*")
                {
                    if (subject != cp.P_CWSUBJECT)
                    {
                        return false;
                    }
                }
            }
            if (!string.IsNullOrEmpty(cp.P_CWFILENAME))
            {
                if (cp.P_CWFILENAME.Trim() != "*")
                {
                    if (cwFileName != cp.P_CWFILENAME)
                    {
                        return false;
                    }
                }
            }
            if (string.IsNullOrEmpty(cp.P_REASONCODE))
            {
                return false;
            }
            if (string.IsNullOrEmpty(cp.P_EVENTCODE))
            {
                return false;

            }
            if ((cp.P_REASONCODE.Trim() == "*") && (cp.P_EVENTCODE.Trim() == "*"))
            {

            }
            if (cp.P_REASONCODE.Trim() == reasonCode)
            {
                purposeFound = true;
                if (cp.P_EVENTCODE.Trim() == eventCode)
                {
                    eventFound = true;
                }
                else if (cp.P_EVENTCODE.Trim() == "*")
                {
                    eventFound = true;
                }
            }
            else if (cp.P_REASONCODE.Trim() == "*")
            {
                purposeFound = true;
                if (cp.P_EVENTCODE.Trim() == eventCode)
                {
                    eventFound = true;
                }
                else if (cp.P_EVENTCODE.Trim() == "*")
                {
                    eventFound = true;
                }
            }

            if (eventFound && purposeFound)
            {
                return true;
            }
            return false;
        }
        #endregion




        //public static vw_CustomerProfile GetProfile(TaskList task)
        //{
        //    ConnectionManager connMgr = new ConnectionManager(Globals.ServerName, Globals.DataBase, Globals.UserName, Globals.Password);
        //    using (CTCNODEEntities _context = new CTCNODEEntities(connMgr.ConnString))
        //    {
        //        var profile = (from p in _context.vw_CustomerProfile
        //                       where (p.P_ID == task.TL_P)
        //                       && p.P_ACTIVE.Equals("Y")
        //                       select p).SingleOrDefault();
        //        if (profile != null)
        //        {
        //            return profile;
        //        }
        //        return null;
        //    }
        //}


        //public static Guid CustProfileID(string tL_SenderID, string tL_RecipientID, string eventCode, string actionPurpose, string subject, string cwFileName)
        //{
        //    if (!string.IsNullOrEmpty(tL_SenderID) || !string.IsNullOrEmpty(tL_RecipientID))
        //    {
        //        ConnectionManager connMgr = new ConnectionManager(Globals.ServerName, Globals.DataBase, Globals.UserName, Globals.Password);
        //        using (CTCNODEEntities _context = new CTCNODEEntities(connMgr.ConnString))
        //        {
        //            var profList = from p in _context.vw_CustomerProfile
        //                           where p.P_ACTIVE == "Y"
        //                           && p.P_SENDERID == tL_SenderID
        //                           && p.P_RECIPIENTID == tL_RecipientID
        //                           select p;
        //            if (profList != null)
        //            {
        //                foreach (vw_CustomerProfile p in profList)
        //                {
        //                    if (CheckValidProfile(p, actionPurpose, eventCode, subject, cwFileName) != null)
        //                    {
        //                        return p.P_ID;

        //                    }


        //                }
        //            }
        //            return Guid.Empty;
        //        }

        //    }
        //    else
        //    {
        //        return Guid.Empty;
        //    }
        //}

        //    public static vw_CustomerProfile CheckValidProfile(vw_CustomerProfile cp, string reasonCode, string eventCode, string subject, string cwFileName)
        //{
        //    bool eventFound = false;
        //    bool purposeFound = false;
        //    if ((cp.P_REASONCODE.Trim() == "*") && (cp.P_EVENTCODE.Trim() == "*"))
        //    {

        //    }
        //    if (cp.P_REASONCODE.Trim() == reasonCode)
        //    {
        //        purposeFound = true;
        //        if (cp.P_EVENTCODE.Trim() == eventCode)
        //        {
        //            eventFound = true;
        //        }
        //        else if (cp.P_EVENTCODE.Trim() == "*")
        //        {
        //            eventFound = true;
        //        }
        //    }
        //    else if (cp.P_REASONCODE.Trim() == "*")
        //    {
        //        purposeFound = true;
        //        if (cp.P_EVENTCODE.Trim() == eventCode)
        //        {
        //            eventFound = true;
        //        }
        //        else if (cp.P_EVENTCODE.Trim() == "*")
        //        {
        //            eventFound = true;
        //        }
        //    }
        //    //if (!string.IsNullOrEmpty(cp.P_CWSUBJECT))
        //    //{
        //    //    if (cp.P_CWSUBJECT != "*")
        //    //    {
        //    //        if (subject.Contains("*"))
        //    //        {
        //    //            matchFound = Regex.IsMatch(subject, cp.P_CWSUBJECT);
        //    //        }
        //    //        else
        //    //        {
        //    //            matchFound = false;
        //    //        }
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    matchFound = true;
        //    //}

        //    //if (!string.IsNullOrEmpty(cp.P_CWFILENAME))
        //    //{
        //    //    if (cp.P_CWFILENAME != "*")
        //    //    {
        //    //        if (fileName.Contains("*"))
        //    //        {
        //    //            matchFound = Regex.IsMatch(fileName, cp.P_CWFileName);
        //    //        }
        //    //        else
        //    //        {
        //    //            matchFound = false;
        //    //        }
        //    //    }
        //    //}
        //    //if (!string.IsNullOrEmpty(cp.P_CWAppCode))
        //    //{
        //    //    if (cp.P_CWAppCode != "UDM")
        //    //    {
        //    //        if (appCode == cp.P_CWAppCode)
        //    //        {
        //    //            matchFound = true;
        //    //        }
        //    //    }
        //    //}
        //    if (eventFound && purposeFound)
        //    {
        //        return cp;
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}




    }
}
