﻿
using NodeData;
using NodeData.DTO;
using NodeData.Models;
using NodeResources;
using System;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace CNodeBE.Classes
{
    public class FileProcessor
    {
        #region members
        string _processingFile;
        string _connString;
        FileInfo _file;
        string _errLog;
        string _log;

        #endregion

        #region properties
        public ProcessResult ProcResult
        {
            get; set;
        }
        public string Log
        {
            get
            { return _log; }
            set
            {
                _log = value;
            }
        }
        public string ErrorLog
        {
            get
            { return _errLog; }
            set
            { _errLog = value; }
        }
        public string ProcessingFile
        {
            get
            { return _processingFile; }
            set
            { _processingFile = value; }
        }

        public string ConnString
        {
            get
            { return _connString; }
            set
            {
                _connString = value;
            }
        }
        #endregion

        #region constructors
        public FileProcessor(string processingFile, string connString)
        {
            ProcessingFile = processingFile;
            ConnString = connString;
        }
        #endregion

        #region methods

        public ApiLog CheckApiLog(string originalFileName)
        {
            if (string.IsNullOrEmpty(originalFileName))
            {
                return new ApiLog();
            }
            string fileName = Path.GetFileName(originalFileName);
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                ApiLog _apiLog;
                _apiLog = uow.ApiLogs.Find(x => x.F_FILENAME == fileName).FirstOrDefault();
                if (_apiLog == null)
                {
                    _apiLog = CheckApiLog(CheckTasksByFileName(originalFileName));
                }
                return _apiLog != null ? _apiLog : new ApiLog();

            }
        }
        public ApiLog CheckApiLog(string originalFileName, string xmlFile)
        {
            string fileName = !string.IsNullOrEmpty(originalFileName) ? Path.GetFileName(originalFileName) : Path.GetFileName(xmlFile);
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                ApiLog _apiLog;
                _apiLog = uow.ApiLogs.Find(x => x.F_FILENAME == fileName).FirstOrDefault();
                if (_apiLog == null)
                {
                    _apiLog = CheckApiLog(CheckTasksByFileName(fileName));
                }
                return _apiLog != null ? _apiLog : new ApiLog();

            }
        }

        public string CheckTasksByFileName(string fileName)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                var task = uow.TaskLists.Find(x => x.TL_FileName == fileName).FirstOrDefault();
                return task != null ? task.TL_OrginalFileName : string.Empty;
            }


        }

        public string CheckFileName(NodeData.Models.TaskList task)
        {

            _file = new FileInfo(_processingFile);
            if (task != null)
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.NodeDataContext(_connString)))
                {
                    if (task.TL_P != null || task.TL_P != Guid.Empty)
                    {
                        var prof = uow.Profiles.Get((Guid)task.TL_P);
                        if (prof == null)
                        {
                            return _file.FullName;
                        }
                        if (prof.P_RENAMEFILE)
                        {
                            var newFile = Path.Combine(_file.Directory.FullName, task.TL_OrginalFileName);
                        }
                    }

                }
            }
            return _file.FullName;

        }

        private vw_CustomerProfile GetXMSDetails(string xmlFile)
        {
            XDocument xDoc = XDocument.Load(xmlFile);
            var xmsXML = xDoc.Descendants();
            var sourceNode = xmsXML.Elements().Where(e => e.Name.LocalName == "Source").FirstOrDefault();
            if (sourceNode != null)
            {

                var senderID = GetValueFromElement(sourceNode, "EnterpriseCode") +
                                GetValueFromElement(sourceNode, "CompanyCode") +
                                GetValueFromElement(sourceNode, "OriginServer");
                var purpose = GetValueFromElement(sourceNode, "Purpose");
                var ediOrg = xmsXML.Elements().Where(e => e.Name.LocalName == "EDIOrganisation").FirstOrDefault();
                if (ediOrg != null)
                {
                    var recipientid = ediOrg.Attributes("EDICode").FirstOrDefault().Value;
                    if (!string.IsNullOrEmpty(recipientid))
                    {
                        ProfileOperations profOps = new ProfileOperations(_connString);

                        return profOps.GetProfile(senderID, recipientid, purpose, "*", "*", "XMS", "*");

                    }
                }

            }
            return null;
        }

        private string GetValueFromElement(XElement element, string valToFind)
        {
            return element.Elements().Where(e => e.Name.LocalName == valToFind).FirstOrDefault().Value;
        }

        public bool ProcessXMS(string xmlFile, string originalFile)
        {
            var apiLog = CheckApiLog(originalFile, xmlFile);
            if (!File.Exists(xmlFile))
            {
                return false;
            }
            try
            {
                XDocument xdoc = XDocument.Load(xmlFile);

                vw_CustomerProfile profile;
                if (string.IsNullOrEmpty(apiLog.F_CLIENTID))
                {
                    profile = GetXMSDetails(xmlFile);
                    if (profile == null)
                    {
                        _errLog = "File is missing Profile";
                        return false;
                    }

                }
                else
                {
                    ProfileOperations profOps = new ProfileOperations(_connString);
                    profile = profOps.GetProfileFromAPiLog(apiLog, originalFile);
                }

                if (profile != null)
                {
                    if (profile.C_IS_ACTIVE == "Y")
                    {
                        var task = UpdateTask(apiLog.F_FILENAME, profile.P_ID);
                    }


                }
                ProcResult = ProcessFileDelivery(profile, apiLog);
            }
            catch (IOException ex)
            {
                _errLog = "File no longer exists";
            }

            return ProcResult != null;

        }

        private NodeData.Models.TaskList UpdateTask(string fileName, Guid profileId)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.NodeDataContext(_connString)))
            {
                var t = (uow.TaskLists.Find(x => x.TL_OrginalFileName == fileName)).FirstOrDefault();
                if (t != null)
                {
                    t.TL_P = profileId;
                    uow.Complete();
                }
                return t;

            }
        }

        private NodeData.Models.Profile GetProfile(ApiLog apiLog, string originalFile)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.NodeDataContext(_connString)))
            {
                var proflist = uow.Profiles.Find(x => x.P_SENDERID == apiLog.F_SENDERID &&
                                                x.P_RECIPIENTID == apiLog.F_CLIENTID &&
                                                x.P_CWAPPCODE == apiLog.F_APPCODE &&
                                                x.P_ACTIVE == "Y").ToList();
                if (proflist.Count > 0)
                {
                    var profile = (from x in proflist
                                   where x.P_CWSUBJECT == apiLog.F_SUBJECT
                                   select x).FirstOrDefault();
                    if (profile == null)
                    {
                        profile = (from x in proflist
                                   where x.P_CWSUBJECT == "*"
                                   select x).FirstOrDefault();
                    }
                    return profile;
                }

            }
            return null;
        }

        private ProcessResult ProcessFileDelivery(vw_CustomerProfile prof, ApiLog cNodeLog)
        {
            if (prof == null)
            {
                _errLog += "ERRROR: Profile missing for Legacy File " + cNodeLog.F_FILENAME;
                _errLog += "Incoming Details: " + Environment.NewLine +
                    "Sender ID : " + cNodeLog.F_SENDERID + Environment.NewLine +
                    "Recipient ID : " + cNodeLog.F_CLIENTID + Environment.NewLine +
                    "Subject : " + cNodeLog.F_SUBJECT + Environment.NewLine +
                    "App Code : " + cNodeLog.F_APPCODE + Environment.NewLine +
                    "File Name :" + cNodeLog.F_FILENAME;
                return null;
            }
            ProcessResult thisResult = new ProcessResult();
            FileInfo fi = new FileInfo(_processingFile);
            string result = string.Empty;
            switch (prof.P_DELIVERY)
            {
                case ("R"):
                    //{
                    //  // NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + prof.C_NAME + " Recipient found " + prof.P_RECIPIENTID + ". Moving to Folder", Color.Green);
                    //    result = "File Saved";
                    //    if (string.IsNullOrEmpty(prof.P_SERVER))
                    //    {
                    //        NodeResources.MoveFile(_processingFile, Path.Combine(prof.C_PATH, "Processing"));
                    //    }
                    //    else
                    //    {
                    //        if (!Directory.Exists(prof.P_SERVER))
                    //        {
                    //            try
                    //            {
                    //                Directory.CreateDirectory(prof.P_SERVER);
                    //            }
                    //            catch (Exception ex)
                    //            {
                    //              //  NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "Moving file failed Error was : " + ex.Message + ". ", Color.Red);
                    //                string err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                    //                using (MailModule mail = new MailModule(_mailServer))
                    //                {
                    //                    mail.SendMsg(_processingFile, Globals.AlertsTo, "Unable to move File to Folder " + prof.P_SERVER, "Error Message: " + err);
                    //                }
                    //                thisResult.Processed = true;
                    //                thisResult.FileName = _processingFile;
                    //                break;
                    //            }


                    //        }
                    //        NodeResources.MoveFile(_processingFile, prof.P_SERVER);
                    //        thisResult.Processed = true;
                    //        thisResult.FileName = _processingFile;

                    //    }
                    //}
                    break;
                case ("D"):
                    {
                        //string ftpSend = string.Empty;
                        //if (File.Exists(_processingFile))
                        {
                            //try
                            //{

                            //    NodeData.Models.Ivw_CustomerProfile v = new NodeData.Models.vw_CustomerProfile();
                            //    IFtpHelper ftp = new FtpHelper(_processingFile, Globals.WinSCPLocation);


                            //    string server = prof.P_SERVER;
                            //    string username = prof.P_USERNAME;
                            //    string password = prof.P_PASSWORD;
                            //    string folder = prof.P_PATH;
                            //    bool ssl = prof.P_SSL == "Y" ? true : false;
                            //    int port = int.Parse(prof.P_PORT);


                            //    string ftpResult = string.Empty;
                            //    if (prof.P_SSL == "Y" ? true : false)
                            //    {
                            //        ftp.sshfingerPrint = prof.P_EMAILADDRESS;
                            //        ftp.PrivateKeyPath = prof.P_SUBJECT;
                            //        ftp.FileName = _processingFile;
                            //        var o = ftp.BuildSessionOptions(server, port, username, password, ssl);
                            //        ftp.FTPsessionOptions = o;
                            //        ftpResult = ftp.FtpSend(prof.P_PATH);
                            //    }
                            //    else
                            //    {
                            //        ftpResult = ftp.FtpSend(_processingFile, server, folder, username, password);

                            //    }

                            //    // string ftpResult = NodeResources.FtpSend(_processingFile, prof);
                            //    if (ftpResult.Contains("File Sent Ok"))
                            //    {

                            //        //    
                            //        //   NodeResources.AddLabel(lblFilesTx, filesTx.ToString());
                            //        //   NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + "File " + Path.GetFileName(_processingFile) + " Sent. Response: " + ftpResult, Color.Green);
                            //        result = ftpResult;
                            //        try
                            //        {
                            //            System.GC.Collect();
                            //            System.GC.WaitForPendingFinalizers();
                            //            File.Delete(_processingFile);
                            //            thisResult.Processed = true;
                            //            thisResult.FileName = _processingFile;
                            //            thisResult.FolderLoc = Path.GetDirectoryName(_processingFile);
                            //        }
                            //        catch (Exception ex)
                            //        {
                            //            ProcessingErrors procerror = new ProcessingErrors
                            //            {
                            //                ErrorCode = new CTCErrorCode
                            //                {
                            //                    Code = NodeError.e104,
                            //                    Description = "Failed to Delete: " + ex.Message + ". "
                            //                },
                            //                FileName = _processingFile,
                            //                ProcId = prof.P_ID,
                            //                RecipientId = prof.P_RECIPIENTID,
                            //                SenderId = prof.P_SENDERID
                            //            };
                            //            result = "Failed: " + ex.Message;
                            //        }

                            //    }
                            //    else
                            //    {
                            //        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Transfer Result is invalid: ", Color.Red);
                            //        using (MailModule mail = new MailModule(_mailServer))
                            //        {
                            //            mail.SendMsg(_processingFile, Globals.AlertsTo, "FTP Failed to send (" + prof.P_DESCRIPTION + ")", "The FTP Proces failed to send the file." + Environment.NewLine +
                            //                "Error Message was :" + result);
                            //        }
                            //    }
                            //}
                            //catch (Exception ex)
                            //{
                            //    AddTodo(prof.P_ID, _processingFile, ftpSend, DateTime.Now);
                            //    ProcessingErrors procerror = new ProcessingErrors
                            //    {
                            //        ErrorCode = new CTCErrorCode
                            //        {
                            //            Code = NodeError.e104,
                            //            Description = "Cargowise FTP Send Error Found: " + ex.Message + ". "
                            //        },
                            //        FileName = _processingFile,
                            //        ProcId = prof.P_ID,
                            //        RecipientId = prof.P_RECIPIENTID,
                            //        SenderId = prof.P_SENDERID
                            //    };

                            //    AddProcessingError(procerror);
                            //    thisResult.FolderLoc = prof.C_PATH + "\\Processing\\";
                            //    thisResult.Processed = false;
                            //    thisResult.FileName = _processingFile;
                            //    NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Cargowise FTP Send Error Found: " + ex.Message + ". " + "", Color.Red);
                            //    string err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                            //    using (MailModule mail = new MailModule(_mailServer))
                            //    {
                            //        mail.SendMsg(_processingFile, Globals.AlertsTo, "FTP Failed to send (" + prof.P_DESCRIPTION + ")", "The FTP Proces failed to send the file." + Environment.NewLine +
                            //            "Error Message was :" + result);
                            //    }

                            //}

                        }
                        break;
                    }
                case ("C"):
                    {
                        try
                        {
                            // NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + prof.C_NAME + " Recipient found " + prof.P_RECIPIENTID + ".Sending file via CTC Adapter" + "", Color.Green);
                            string sendlog = string.Empty;
                            if (prof.P_MSGTYPE != null)
                            {
                                if (prof.P_MSGTYPE.Trim() == "Forward")
                                {
                                    if (cNodeLog != null)
                                    {
                                        sendlog = CTCAdapter.SendMessage(prof.P_SERVER, _processingFile, prof.P_RECIPIENTID, prof.P_USERNAME, prof.P_PASSWORD, cNodeLog.F_TRACKINGID, cNodeLog.F_SUBJECT, cNodeLog.F_CWFILENAME);
                                    }
                                }
                                else
                                {
                                    sendlog = CTCAdapter.SendMessage(prof.P_SERVER, _processingFile, prof.P_RECIPIENTID, prof.P_USERNAME, prof.P_PASSWORD, null);
                                }
                            }


                            // Test without sending. UnComment the following
                            //String sendlog = "Send OK";
                            if (sendlog.Contains("SEND OK"))
                            {
                                _log += string.Format("{0:g} ", DateTime.Now) + fi.Name + " File Transmitted Ok. " + Environment.NewLine;

                                File.Delete(_processingFile);
                                result = "CTC Adapter send Ok";
                                thisResult.Processed = true;
                                thisResult.FileName = _processingFile;
                                AddTransaction(prof, Path.GetFileName(_processingFile), "S", true);
                            }
                            else
                            {
                                result = "CTC Adapter send Failed: " + sendlog;
                                ProcessingErrors procerror = new ProcessingErrors
                                {
                                    ErrorCode = new CTCErrorCode
                                    {
                                        Code = NodeError.e104,
                                        Description = "CTC Adapter send Failed: " + sendlog
                                    },
                                    FileName = _processingFile,
                                    ProcId = prof.P_ID,
                                    RecipientId = prof.P_RECIPIENTID,
                                    SenderId = prof.P_SENDERID
                                };
                                _errLog += "CTC Adapter send failed: " + sendlog;
                                thisResult.FolderLoc = Globals.FailLoc;
                                thisResult.Processed = false;
                                thisResult.FileName = _processingFile;
                            }
                        }
                        catch (Exception ex)
                        {
                            ProcessingErrors procerror = new ProcessingErrors
                            {
                                ErrorCode = new CTCErrorCode
                                {
                                    Code = NodeError.e104,
                                    Description = "CTC Adapter send Failed: " + ex.Message
                                },
                                FileName = _processingFile,
                                ProcId = prof.P_ID,
                                RecipientId = prof.P_RECIPIENTID,
                                SenderId = prof.P_SENDERID
                            };

                            thisResult.FolderLoc = Globals.FailLoc;
                            thisResult.Processed = false;
                            thisResult.FileName = _processingFile;
                            result = "CTC Adapter send failed: " + ex.Message;
                        }
                    }
                    break;
                    //case "E":
                    //    {
                    //        Guid remID = AddTodo(prof.P_ID, _processingFile, "Added to todo List", DateTime.Now);
                    //        NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Customer " + prof.C_NAME + " Recipient found " + prof.P_RECIPIENTID + ".Sending file via Email" + "");
                    //        string mailResult = string.Empty;
                    //        using (MailModule mail = new MailModule(_mailServer))
                    //        {
                    //            mailResult = mail.SendMsg(_processingFile, prof.P_EMAILADDRESS, prof.P_SUBJECT, "Cargowise File Attached");
                    //        }
                    //        if (mailResult == "Message Sent OK")
                    //        {
                    //            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " File Transmitted Ok. ", Color.Green);
                    //            RemoveFromTodo(remID);
                    //            cwTx++;
                    //            NodeResources.AddLabel(lblCWTx, cwTx.ToString());
                    //            result = mailResult;
                    //            thisResult.Processed = true;
                    //            thisResult.FileName = _processingFile;
                    //        }
                    //        else
                    //        {
                    //            remID = AddTodo(prof.P_ID, _processingFile, "Message send failed. Error message was: " + mailResult, DateTime.Now);
                    //            NodeResources.AddText(edLog, string.Format("{0:g} ", DateTime.Now) + " Message send failed. Error message was: " + mailResult, Color.Red);
                    //            result = "Email send failed: " + mailResult;
                    //            ProcessingErrors procerror = new ProcessingErrors
                    //            {
                    //                ErrorCode = new CTCErrorCode
                    //                {
                    //                    Code = NodeError.e105,
                    //                    Description = "Email message failed to send " + mailResult
                    //                },
                    //                FileName = _processingFile,
                    //                ProcId = prof.P_ID,
                    //                RecipientId = prof.P_RECIPIENTID,
                    //                SenderId = prof.P_SENDERID
                    //            };
                    //            AddProcessingError(procerror);
                    //            thisResult.FolderLoc = prof.C_PATH + "\\Processing\\";
                    //            thisResult.Processed = false;
                    //            thisResult.FileName = _processingFile;

                    //        }
                    //        AddTransactionLog(prof.P_ID, prof.P_SENDERID, _processingFile, "File " + Path.GetFileName(_processingFile) + " sent via Email with the Result: " + mailResult, DateTime.Now, thisResult.Processed ? "Y" : "N", prof.P_BILLTO, prof.P_RECIPIENTID);
                    //        break;
                    //    }
            }
            return thisResult;
        }


        private void AddTransaction(vw_CustomerProfile profile, string fileName, string direction, bool original)
        {
            string msgType = original ? "Original" : "Duplicate";
            try
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
                {
                    var trans = (uow.Transactions.Find(x => x.T_FILENAME == fileName && x.T_MSGTYPE == msgType)).FirstOrDefault();
                    if (trans == null)
                    {
                        trans = new NodeData.Models.Transaction
                        {
                            T_ID = Guid.NewGuid(),
                            T_C = profile.P_C,
                            T_P = profile.P_ID,
                            T_FILENAME = Path.GetFileName(fileName),
                            T_DATETIME = DateTime.Now,
                            T_TRIAL = "N",
                            T_DIRECTION = direction,
                            T_MSGTYPE = msgType,
                            T_INVOICED = "N",
                            T_CHARGEABLE = "Y",
                            T_BILLTO = profile.P_BILLTO
                        };
                        uow.Transactions.Add(trans);
                    }
                    uow.Complete();
                }
            }
            catch (DbEntityValidationException ex)
            {
                var errorMessages = ex.EntityValidationErrors
                   .SelectMany(x => x.ValidationErrors)
                   .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                string strEx = ex.GetType().Name;

                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;

            }
        }

        #endregion

        #region helpers
        #endregion
    }
}
