﻿using NodeData.Models;
using System.IO;
using System.Xml.Serialization;

namespace CNodeBE.Classes
{
    public class FileBuilder
    {
        #region members
        string _outPath;
        #endregion

        #region properties
        string OutPath
        {
            get
            {
                return _outPath;
            }
            set
            {
                _outPath = value;
            }
        }
        #endregion

        #region constructor
        public FileBuilder(string outPath)
        {
            OutPath = outPath;
        }
        #endregion

        #region methods

        public string BuildXmlFile<TypeOfXml>(XmlSerializerNamespaces xmlNS, string ns, TypeOfXml xmlObject, vw_CustomerProfile cust, string filename, string fileType)
        {
            string cwXML = Path.Combine(_outPath, filename + fileType);
            int iFileCount = 0;
            while (File.Exists(cwXML))
            {
                iFileCount++;
                cwXML = Path.Combine(_outPath, cust.P_SENDERID + filename + "-" + iFileCount + fileType);
            }
            using (Stream outputFreightTracker = File.Open(cwXML, FileMode.Create))
            {
                StringWriter writer = new StringWriter();
                XmlSerializer xSer = new XmlSerializer(typeof(TypeOfXml));
                xmlNS.Add("", ns);
                xSer.Serialize(outputFreightTracker, xmlObject, xmlNS);
                outputFreightTracker.Flush();
                outputFreightTracker.Close();
            }


            return cwXML;
        }
        #endregion



    }
}
