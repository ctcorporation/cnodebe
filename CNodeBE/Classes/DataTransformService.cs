﻿using NodeData;
using NodeData.DTO;
using NodeResources;
using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

namespace CNodeBE.Classes
{
    public class DataTransformService
    {
        #region members
        string _connString;
        string _log = string.Empty;
        #endregion

        #region properties
        public string ConnString
        {
            get
            { return _connString; }
            set
            { _connString = value; }
        }
        public MailServerSettings MailServer
        {
            get;
            set;
        }
        #endregion

        #region constructors
        public DataTransformService(string connString)
        {
            ConnString = connString;
        }
        #endregion


        #region methods
        public void Transform(Guid profileID, string XMLfile, IMailServerSettings ms)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.NodeDataContext(_connString)))
            {
                var dts = uow.DTServices.Find(x => x.D_P == profileID).ToList();
                if (dts.Count > 0)
                {
                    foreach (var d in dts)
                    {
                        XmlDocument xmlDTS = new XmlDocument();
                        var xdoc = XDocument.Load(XMLfile);
                        //Ensure the Namespace is correct against qualifier
                        XNamespace ns = xdoc.Root.GetDefaultNamespace();
                        if (d.D_QUALIFIER == ns.NamespaceName)
                        {
                            var nsMgr = new XmlNamespaceManager(xmlDTS.NameTable);
                            nsMgr.AddNamespace("CW", ns.NamespaceName);
                            using (StreamReader sr = new StreamReader(XMLfile))
                            {
                                xmlDTS.Load(sr);
                            };

                            XmlNode xNode;
                            XmlNodeList xList;
                            try
                            {
                                if (d.D_DTS == "R")//Modification to the Record
                                {
                                    xList = xmlDTS.SelectNodes("//CW:" + d.D_SEARCHPATTERN, nsMgr);
                                    if (xList != null) //found the Search Pattern
                                    {
                                        string searchPattern = d.D_SEARCHPATTERN;
                                        var fqNodeSearch = "//CW:" + searchPattern.Replace("/", "/CW:");
                                        if (d.D_TARGET != "")
                                        {
                                            fqNodeSearch += "/CW:" + d.D_TARGET;
                                            if (d.D_CURRENTVALUE != "")
                                            {
                                                fqNodeSearch += "[text() ='" + d.D_CURRENTVALUE + "']";
                                            }
                                        }
                                        xNode = xmlDTS.SelectSingleNode(fqNodeSearch, nsMgr);
                                        // xNode = FindNode(xList, drDTS["D_TARGET"].ToString());
                                        if (xNode != null)  //Found the Target Key
                                        {
                                            _log += "DTS Service - Record Updated" + Environment.NewLine;
                                            switch (d.D_DTS)
                                            {
                                                case "R"://Replace
                                                    {
                                                        xNode.InnerText = d.D_NEWVALUE;
                                                        break;
                                                    }
                                                case "U"://Update
                                                    {
                                                        xNode.InnerText += d.D_NEWVALUE;
                                                        break;
                                                    }
                                                case "M"://Modify
                                                    {
                                                        break;
                                                    }
                                                case "D"://Delete
                                                    {
                                                        break;
                                                    }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        _log += "DTS Service: No Value Found" + Environment.NewLine;
                                    }
                                }
                                else
                                {
                                    //Modify the Structure
                                    switch (d.D_DTSTYPE)
                                    {
                                        case "R": //Replace
                                            {
                                                xList = xmlDTS.SelectNodes("//CW:" + d.D_SEARCHPATTERN, nsMgr);
                                                if (xList != null)
                                                {
                                                    xNode = FindNode(xList, d.D_TARGET);
                                                    if (xNode != null)
                                                    {
                                                        XmlNode xNewNode = RenameNode(xNode, d.D_NEWVALUE, ns.NamespaceName);
                                                    }
                                                }
                                                break;
                                            }
                                        case "D": //Delete
                                            {
                                                if (d.D_SEARCHPATTERN == "")
                                                {
                                                    XmlNodeList delNodes = xmlDTS.SelectNodes("//CW:" + d.D_TARGET, nsMgr);
                                                    foreach (XmlNode delNode in delNodes)
                                                    {
                                                        delNode.ParentNode.RemoveChild(delNode);
                                                    }
                                                }
                                                else
                                                {
                                                    xList = xmlDTS.SelectNodes("//CW:" + d.D_SEARCHPATTERN, nsMgr);
                                                    if (xList != null)
                                                    {
                                                        xNode = FindNode(xList, d.D_TARGET);
                                                        if (xNode != null)
                                                        {
                                                            xNode.ParentNode.RemoveChild(xNode);
                                                        }
                                                    }
                                                }
                                                break;
                                            }
                                        case "M": //Modify
                                            {
                                                xList = xmlDTS.SelectNodes("//CW:" + d.D_SEARCHPATTERN, nsMgr);
                                                if (xList != null)
                                                {
                                                    xNode = FindNode(xList, d.D_TARGET);
                                                    if (xNode != null)
                                                    {
                                                        XmlNode xNewNode = RenameNode(xNode, d.D_NEWVALUE, ns.NamespaceName);
                                                    }
                                                }
                                                break;
                                            }
                                    }
                                    _log += "DTS Service - File Structure modified" + Environment.NewLine;
                                }
                                XmlNodeList blankNodes;
                                blankNodes = xmlDTS.SelectNodes("//*[not(string(.))]");
                                foreach (XmlNode blankNode in blankNodes)
                                {
                                    blankNode.ParentNode.RemoveChild(blankNode);
                                }
                                xmlDTS.Save(XMLfile);
                                xmlDTS = null;
                            }
                            catch (Exception ex)
                            {
                                _log += "Error found in DTS:" + ex.Message + Environment.NewLine;
                                //foreach (DataColumn dc in drDTS.Table.Columns)
                                //{
                                //    elog += dc.ColumnName + " :" + drDTS[dc].ToString() + Environment.NewLine;
                                //}
                                string err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;
                                using (MailModule mail = new MailModule(ms))
                                {
                                    mail.SendMsg(XMLfile, Globals.AlertsTo, "CTC Node Exception Found: DTS Processing", err);
                                }

                                xmlDTS = null;
                            }
                        }
                        //Process the file as per the Profile and return to calling process. 
                        xdoc = null;
                    }
                    RemoveBlankFromXML(XMLfile);
                }
            }

        }
        #endregion

        #region helpers

        private void RemoveBlankFromXML(string xmlFile)
        {
            //Clear all Blank/Empty Elements.
            XDocument xd = XDocument.Load(xmlFile);
            xd.Descendants().
                Where(eg => (eg.Attributes().All(a => a.IsNamespaceDeclaration || string.IsNullOrWhiteSpace(a.Value))
            && string.IsNullOrWhiteSpace(eg.Value)
            && eg.Descendants().
            SelectMany(c => c.Attributes()).All(ca => ca.IsNamespaceDeclaration || string.IsNullOrWhiteSpace(ca.Value))))
        .Remove();
            xd.Save(xmlFile);
        }
        private XmlNode FindNode(XmlNodeList list, string nodeName)
        {
            if (list.Count > 0)
            {
                foreach (XmlNode node in list)
                {
                    if (node.Name.Equals(nodeName))
                    {
                        return node;
                    }

                    if (node.HasChildNodes)
                    {
                        XmlNode nodeFound = FindNode(node.ChildNodes, nodeName);
                        if (nodeFound != null)
                        {
                            return nodeFound;
                        }
                    }
                }
            }
            return null;
        }
        private static void DeleteNode(XmlNode node, string xPath)
        {
            var nodes = node.SelectNodes(xPath);
            foreach (XmlNode nodetobedeleted in nodes)
            {
                nodetobedeleted.ParentNode.RemoveChild(nodetobedeleted);

            }
        }

        private XmlNode RenameNode(XmlNode e, string newName, string ns)
        {
            XmlNode renamedNode = null;
            try
            {
                XmlDocument doc = e.OwnerDocument;
                XmlNode newNode = doc.CreateNode(e.NodeType, newName, ns);
                while (e.HasChildNodes)
                {
                    newNode.AppendChild(e.FirstChild);
                }

                XmlAttributeCollection ac = e.Attributes;
                while (ac.Count > 0)
                {
                    newNode.Attributes.Append(ac[0]);
                }
                XmlNode parent = e.ParentNode;
                parent.ReplaceChild(newNode, e);
                renamedNode = newNode;
            }
            catch (Exception ex)
            {
                _log += " Error Renaming Node in DTS. Error was :" + ex.Message + Environment.NewLine;
                string err = ex.Message + Environment.NewLine + ex.Source + ex.StackTrace;

            }
            return renamedNode;

        }

        public static XDocument DocumentToXDocumentReader(XmlDocument doc)
        {
            return XDocument.Load(new XmlNodeReader(doc));
        }

        public static XmlDocument RemoveNameSpace(XmlDocument doc)
        {
            XDocument xDoc;
            using (var nodeReader = new XmlNodeReader(doc))
            {
                nodeReader.MoveToContent();
                xDoc = XDocument.Load(nodeReader);
            }
            xDoc.Descendants().Attributes().Where(a => a.IsNamespaceDeclaration).Remove();
            foreach (var element in xDoc.Descendants())
            {
                element.Name = element.Name.LocalName;
            }
            doc.LoadXml(xDoc.ToString());
            return doc;

        }
        #endregion
    }
}
