﻿using System.IO;

namespace CNodeBE
{
    public interface IMsgAtt
    {
        FileInfo AttFilename { get; set; }
    }
}