﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using XMLLocker.Cargowise.Native;

namespace CNodeBE.Classes
{
    public class CWRatesBuilder
    {
        #region members
        const string cwDateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'sszzz";
        #endregion

        #region properties  

        #endregion

        #region constructor
        public CWRatesBuilder()
        {

        }
        #endregion

        #region methods
        public void RatesFile()
        {
            var nativeNS = new XmlSerializerNamespaces();
            nativeNS.Add("", "http://www.cargowise.com/Schemas/Native/2011/11");
            var nativeHeader = new Native();
            var nativeBody = new NativeBody();
            var nativeRate = CreateRate();


            List<RateData> rate = new List<RateData>();
            var rateData = new RateData
            {
                version = "2.0",
                RatingHeader = nativeRate
            };
            rate.Add(rateData);
            nativeBody.Any = new[] { nativeRate.AsXmlElement(nativeNS) };
            nativeHeader.Body = nativeBody;
            nativeHeader.Header = new NativeHeader
            {
                OwnerCode = "TEST",
                EnableCodeMapping = true,
                EnableCodeMappingSpecified = true
            };
        }

        private NativeRate CreateRate()
        {
            NativeRate rate = new NativeRate();
            rate.Action = XMLLocker.Cargowise.Native.Action.INSERT;
            rate.ActionSpecified = true;
            rate.RateType = "COS";
            rate.QuoteNumber = "";

            rate.GlobalRateDescription = "TEST Imported Rate";
            rate.AirCFX = 0.00M;
            rate.SeaCFX = 0.00M;
            rate.ExportAirCFX = 0.00M;
            rate.ExportSeaCFX = 0.00M;

            rate.IsCancelled = false;
            rate.OneTimeQuote = false;
            rate.IsLocked = false;
            rate.IsOneOffQuoteConsumed = false;
            rate.PrintRateLevelOriginCharges = true;
            rate.PrintRateLevelDestinationCharges = true;
            rate.PrintInheritedOriginCharges = true;
            rate.PrintInheritedDestinationCharges = true;
            rate.SystemCreateTimeUtc = DateTime.Now.ToString(cwDateFormat);
            rate.RateEntryCollection = GetRateEntries().ToArray();

            return rate;
        }

        private List<NativeRateRateEntry> GetRateEntries()
        {
            List<NativeRateRateEntry> rateEntry = new List<NativeRateRateEntry>();
            NativeRateRateEntry rate = new NativeRateRateEntry();
            rate.Action = XMLLocker.Cargowise.Native.Action.MERGE;
            rate.LineOrder = 1;

            rate.OriginLRC = "COU";
            ;
            rate.SystemCreateTimeUtc = DateTime.Now.ToString(cwDateFormat);
            rate.SystemLastEditTimeUtc = DateTime.Now.ToString(cwDateFormat);

            rate.IsCrossTrade = false;
            rate.DataChecked = false;
            rate.MatchContainerRateClass = false;
            rate.Mode = "FCL";
            rate.RateCategory = "ORG";
            rate.RateLinesCollection = GetRateLines().ToArray();


            return rateEntry;
        }

        private List<NativeRateRateEntryRateLines> GetRateLines()
        {
            List<NativeRateRateEntryRateLines> rateLines = new List<NativeRateRateEntryRateLines>();
            NativeRateRateEntryRateLines rateLine = new NativeRateRateEntryRateLines();
            rateLine.Action = XMLLocker.Cargowise.Native.Action.MERGE;


            return rateLines;
        }
        #endregion


    }
}
