﻿using NodeData;
using NodeData.DTO;
using System.Collections.Generic;
using System.Linq;

namespace CNodeBE.Classes
{
    public class TaskListSearch
    {
        #region members
        string _connstring;
        string _errorLog;
        #endregion

        #region properties
        public string ConnString
        {
            get
            { return _connstring; }
            set
            {
                _connstring = value;
            }
        }

        #endregion

        #region constructors
        public TaskListSearch(string connString)
        {
            ConnString = connString;
        }
        #endregion

        #region methods

        public List<string> ReturnOperationType()
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.NodeDataContext(_connstring)))
            {
                var opsType = (from y in uow.TaskLists.GetAll()
                               orderby y.TL_ReceiverProcess
                               select y.TL_ReceiverProcess ).Distinct().ToList();
                
                if (opsType.Count > 0)
                {

                    return opsType;
                }

            }
            return null;
        }
        #endregion

        #region helpers
        #endregion
    }
}
