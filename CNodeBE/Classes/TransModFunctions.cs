﻿using NodeData;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Data;
using System.Linq;
using XUS;

namespace CNodeBE.Classes
{
    // This class is used to Create or Update Transmod Jobs. 
    // Input is a UniversalInterchange Object and the Transmod Connection string. 
    // Ouput is a Boolean Skipped flag and Error messages if it fails

    public class TransModFunctions
    {
        #region members
        private string _tConnString;
        private string _errMsg;
        #endregion

        #region properties
        public string TConnString
        {
            get
            {
                return _tConnString;

            }
            set
            {
                _tConnString = value;
            }

        }

        public string ErrMsg
        {
            get
            {
                return _errMsg;
            }
            set
            {
                _errMsg = value;
            }
        }
        public bool Skipped
        {
            get; set;
        }
        #endregion

        #region constructors
        public TransModFunctions(string connString)
        {
            TConnString = connString;

        }
        #endregion

        #region methods
        private NodeData.Models.Address GetOrgAddress(XUS.OrganizationAddress legOrg, Guid custid)
        {
            if (legOrg != null)
            {
                using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.TransModContext(_tConnString)))
                {
                    var org = uow.Organisations.Find(x => x.O_Name == legOrg.CompanyName &&
                                                    (string.IsNullOrEmpty(legOrg.OrganizationCode) || x.O_Code.Trim() == legOrg.OrganizationCode.Trim())
                                                    && x.O_O_PARENT == custid).FirstOrDefault();
                    if (org == null)
                    {
                        org = new NodeData.Models.Organisation
                        {
                            O_Name = legOrg.CompanyName,
                            O_Code = legOrg.OrganizationCode,
                            O_Isactive = true,
                            O_O_PARENT = custid,
                            O_IsConsignee = true,
                            O_Email = legOrg.Email,
                            O_ID = Guid.NewGuid(),
                            O_OnHold = false,
                           //flag is cleint false as leg Org
                            O_IsClient = false

                        };
                        uow.Organisations.Add(org);
                    }
                    var address = uow.Addresses.Find(x => x.A_O == org.O_ID
                                                     && x.A_AddressName.Trim() == legOrg.CompanyName
                                                     && x.A_AddressType.Trim() == legOrg.AddressType
                                                     && x.A_Suburb.Trim() == legOrg.City
                                                     && x.A_State.Trim() == legOrg.State).FirstOrDefault();
                    if (address == null)
                    {
                        address = new NodeData.Models.Address
                        {
                            A_AddressName = legOrg.CompanyName,
                            A_Address1 = legOrg.Address1,
                            A_Address2 = legOrg.Address2,
                            A_Address3 = legOrg.Country != null ? legOrg.Country.Code : string.Empty,
                            A_Suburb = legOrg.City,
                            A_PostCode = legOrg.Postcode,
                            A_State = legOrg.State,
                            A_ID = Guid.NewGuid(),
                            A_O = org.O_ID,
                            A_AddressType = legOrg.AddressType,
                            //set Address as active
                            A_IsActive = true
                        };
                        uow.Addresses.Add(address);
                        uow.Complete();
                    }
                    return address;
                }
            }
            return null;
        }

        public ProcessResult ProcessImportLocalTransport(XUS.UniversalInterchange cwXml)
        {
            ProcessResult result = new ProcessResult();
            result.Processed = true;
            string senderID = cwXml.Header.SenderID;
            string recipientID = cwXml.Header.RecipientID;
            var custid = ValidTransModCustomer(senderID);
            if (custid == Guid.Empty)
            {
                Skipped = true;
                ErrMsg = "No Valid Transmod Customer found. Skipping";
                return result;
            }
            int legNo = 0;
            NodeData.Models.TransReference trRef = new NodeData.Models.TransReference();
            var shipment = cwXml.Body.BodyField.Shipment;

            string reasonCode = shipment.DataContext.ActionPurpose != null ? shipment.DataContext.ActionPurpose.Code : string.Empty;
            string eventCode = shipment.DataContext.EventType != null ? shipment.DataContext.EventType.Code : string.Empty;

            if (shipment.ContainerMode.Code == "CNT")
            {
                // Transmod is for LCL deliveries only. FCL jobs to be rejected.
                Skipped = true;
                ErrMsg = "FCL Delivery Job Found. Skipping";
                return result;

            }
            try
            {
                var newJob = CreateJob(shipment, custid);
                if (newJob == null)
                {
                    return result;
                }
                GetTransportLegs(shipment, newJob);
            }
            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                _errMsg += strEx + " Exception found in Transport XML:" + ex.Message;
            }
            return result;

        }

        private void GetTransportLegs(XUS.Shipment shipment, JobHeader newJob)
        {

            using (IUnitOfWork uow = new UnitOfWork(new TransModContext(_tConnString)))
            {
                //var pickCustomer = Guid.Empty;
                Guid pickCustomer = Guid.Empty;
                Guid getTransportLegADD = Guid.Empty;
                int transmodLegno = 0;
                int getTransportLegLegno = 0;

                if (shipment.InstructionCollection != null)
                {
                    foreach (var leginstruction in shipment.InstructionCollection)
                    {

                        var address = GetOrgAddress(leginstruction.Address, (Guid)newJob.J_O_Customer);
                        if (address == null)
                        {
                            continue;
                        }
                        if (address.A_O == Guid.Empty)
                        {
                            _errMsg += "Pick Address missing A_O";
                        }
                        //If PIC or first leg then save Pick Customer add and go back
                        if (leginstruction.Type.Code == "PIC")
                        {
                            pickCustomer = address.A_ID;
                            continue;
                        }
                        if (leginstruction.Type.Code == "DLV" && pickCustomer == Guid.Empty)
                        {
                            //go back up as no Pickup address found. Need a saved pickup address to create the terransport leg
                            continue;
                        }
                        if (leginstruction.Type.Code == "DLV" && address.A_O == Guid.Empty)
                        {
                            //go back up as no delivery address found. Need a Delivery address to lookup the transport leg
                            continue;
                        }
                        //if MLT leg and origional Pickup empty - set pickup and continue
                        if (leginstruction.Type.Code == "MLT" && pickCustomer == Guid.Empty)
                        {
                            pickCustomer = address.A_ID;
                            continue;
                        }
                        //if (leginstruction.Type.Code == "PIC" || leginstruction.Sequence == 1 || pickCustomer == null )
                        //{
                        //    pickCustomer = address.A_ID;
                        //    transmodLegno = leginstruction.Sequence;
                        //    getTransportLegADD = address.A_ID;
                        //   getTransportLegLegno = leginstruction.Sequence;
                        //}
                        //else
                        //{
                        //    getTransportLegADD = pickCustomer;
                        //    getTransportLegLegno = transmodLegno;
                        //}

                        var leg = uow.TransportLegs.Find(x => x.T_J == newJob.J_JOBID
                       && x.T_A_Delivery == address.A_ID
                       && x.T_LegNo == leginstruction.Sequence).FirstOrDefault();
                        //orig selection
                        //var leg = uow.TransportLegs.Find(x => x.T_J == newJob.J_JOBID
                        //&& x.T_A_Pick == address.A_ID
                        //&& x.T_JobType.Trim() == leginstruction.Type.Code
                        //&& x.T_LegNo == pickLegno).FirstOrDefault();

                        //use routing of 1st leg is PIC and next leg is DLV
                        if (leg == null)
                        {
                            leg = new NodeData.Models.TransportLeg();
                            leg.T_LegNo = leginstruction.Sequence;
                            leg.T_JobType = leginstruction.Type != null ? leginstruction.Type.Code?.Trim() : string.Empty;
                            leg.T_J = newJob.J_JOBID;
                            leg.T_A = address.A_ID;
                            leg.T_A_Pick = pickCustomer;
                            leg.T_A_Delivery = address.A_ID;
                            leg.T_ItemDescription = shipment.GoodsDescription;
                        }

                            if (shipment.PackingLineCollection != null)
                            {
                                var packlink = GetPackLineForLeg(leginstruction, shipment.PackingLineCollection.PackingLine);
                                if (packlink != null)
                                {
                                    leg.T_Pieces = packlink.PackQty;
                                    leg.T_UOM = packlink.PackType.Code;
                                    leg.T_Weight = packlink.Weight;
                                    leg.T_Volume = packlink.Volume;
                                    leg.T_ContainerNo = packlink.ContainerNumber;
                                    leg.T_Reference = packlink.ContainerNumber;
                                    leg.T_ItemQuantity = packlink.PackQty;
                                }
                            }
                            
                            leg.T_DropMode = leginstruction.DropMode != null ? leginstruction.DropMode.Code?.Trim() : string.Empty;

                            //Check mode to Populate the Leg Type - Air Sea Warehouse
                            switch (shipment.TransportMode.Code)
                            {
                                case "ROA":
                                    leg.T_Type = 0;
                                    break;
                                case "AIR":
                                    leg.T_Type = 1;
                                    break;
                                case "SEA":
                                    leg.T_Type = 2;
                                    break;
                                case "RAI":
                                    leg.T_Type = 2;
                                    break;
                                default:
                                    leg.T_Type = 0;
                                    break;
                            }

                            //if MLT leg set PIC address - ie going back to warehouse
                            //MTL = PIC - Back to WHOUSE - DLV
                        if (leginstruction.Type.Code == "MLT")
                        {
                                pickCustomer = address.A_ID;
                        } 

                        if (leg.T_ID == Guid.Empty)
                        {
                            leg.T_ID = Guid.NewGuid();
                            uow.TransportLegs.Add(leg);
                        }


                        var conf = GetConfirmationFromLeg(leginstruction);
                        if (conf != null)
                        {
                            DateTime dt;
                            if (DateTime.TryParse(conf.RequiredFromDate, out dt))
                            {
                                leg.T_RequiredDate = dt;

                            }
                            if (!string.IsNullOrEmpty(conf.ServiceInstruction))
                            {
                                var jobNote = uow.JobNotes.Find(x => x.N_T == leg.T_ID && x.N_Note == conf.ServiceInstruction).FirstOrDefault();
                                if (jobNote == null)
                                {
                                    jobNote = new JobNote();
                                    jobNote.N_Note = conf.ServiceInstruction;
                                    jobNote.N_Date = DateTime.Now;
                                    jobNote.N_JobNoteType = GetEnum("Instruction", "Job Note Type");
                                    jobNote.N_T = leg.T_ID;
                                    uow.JobNotes.Add(jobNote);
                                }


                            }
                        }
                        uow.Complete();
                        //}

                    }
                }

            }
        }

        private NodeData.Models.TransportLeg GetTransportLeg(Guid j_JOBID, Guid t_A, string t_JobType, int t_LegNo)
        {
            using (IUnitOfWork uow = new UnitOfWork(new TransModContext(_tConnString)))
            {
                return uow.TransportLegs.Find(x => x.T_J == j_JOBID
                                               && x.T_A == t_A
                                               && x.T_JobType == t_JobType
                                               && x.T_LegNo == t_LegNo).FirstOrDefault();

            }
        }
        private JobHeader CreateJob(XUS.Shipment shipment, Guid customerId)
        {
            string jobNo = GetJobNo(shipment.DataContext, "LocalTransport");
            if (shipment.OrganizationAddressCollection == null)
            {
                _errMsg += "No Orgs found in Shipment.";
                Skipped = true;
                return null;
            }
            // Get the Client code from OrganisationAddressCollection.OrganizationAddress.AddressType = SendersLocalClient
            var client = GetOrgFromOrgs(shipment.OrganizationAddressCollection, "SendersLocalClient", customerId);

            using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.TransModContext(_tConnString)))
            {
                var jobHeader = uow.JobHeaders.Find(x => x.J_TransportJobNo.Trim() == jobNo && x.J_O_Customer == customerId).FirstOrDefault();
                if (jobHeader == null)
                {
                    jobHeader = new JobHeader();
                    jobHeader.J_CreateDate = DateTime.Now;
                    jobHeader.J_PlannedDeliveryTime = null;
                    jobHeader.J_TransportJobNo = jobNo.Trim();
                    jobHeader.J_Status = GetEnum("Pending", "Job Status");

                }
                jobHeader.J_O_Client = client.O_ID;
                jobHeader.J_LastUpdated = DateTime.Now;
                jobHeader.J_Priority = 0;
                jobHeader.J_TotalPieces = shipment.TotalNoOfPacksSpecified ? shipment.TotalNoOfPacks : 0;
                jobHeader.J_TotalWeight = shipment.TotalWeightSpecified ? shipment.TotalWeight : 0;
                //get weightunit
                switch (shipment.TotalWeightUnit.Code)
                {
                    case "KG":
                        jobHeader.J_WeightUnitType = 0;
                        break;
                    case "T":
                        jobHeader.J_WeightUnitType = 1;
                        break;
                    default:
                        jobHeader.J_WeightUnitType = 0;
                        break;
                }
                //add validation to see if JobType Code is null
                var shiptype = shipment.LocalTransportJobType.Code != null ? shipment.LocalTransportJobType.Code : "DRLC";
                shipment.LocalTransportJobType.Code = shiptype;
                jobHeader.J_JobType = GetJobType(shipment.LocalTransportJobType)?.Trim();
                jobHeader.J_TotalVolume = shipment.TotalVolumeSpecified ? shipment.TotalVolume : 0;
                jobHeader.J_ContainerMode = shipment.ContainerMode != null ? shipment.ContainerMode.Code?.Trim() : string.Empty;
                jobHeader.J_DropMode = shipment.LocalTransportEquipmentNeeded != null ? shipment.LocalTransportEquipmentNeeded.Code?.Trim() : string.Empty;
                jobHeader.J_Description = shipment.GoodsDescription;
                jobHeader.J_UOM = shipment.OuterPacksSpecified ? shipment.OuterPacksPackageType.Code?.Trim() : string.Empty;
                jobHeader.J_ORDERNO = shipment.Order != null ? shipment.Order.OrderNumber?.Trim() : string.Empty;
                jobHeader.J_O_Customer = customerId;
                jobHeader.J_EmailCode = GetEmailCode(jobNo);
                jobHeader.J_Hazardous = shipment.IsHazardous ? "Y" : "N";
                if (jobHeader.J_JOBID == Guid.Empty)
                {
                    uow.JobHeaders.Add(jobHeader);

                }
                uow.Complete();
                return jobHeader;

            }
        }

        private string GetEmailCode(string jobNo)
        {
            if (string.IsNullOrEmpty(jobNo))
            {
                return string.Empty;
            }
            string jobcode = string.Empty;

            //For cargowise import based on Job number
            //found all codes need to be TRN. was S0 - SHP , W0 - WOD, B0 - DEC
            switch (jobNo.Substring(0, 2))
            {
                case "S0":
                    jobcode = "TRN";
                    break;
                case "W0":
                    jobcode = "TRN";
                    break;
                case "B0":
                    jobcode = "TRN";
                    break;
                case "T0":
                    jobcode = "TRN";
                    break;
                case "TB":
                    jobcode = "TRN";
                    break;
            }
            return jobcode;
        }



        private Organisation GetOrgFromOrgs(OrganizationAddress[] organizationAddressCollection, string orgType, Guid custId)
        {
            var client = (from o in organizationAddressCollection
                          where o.AddressType == orgType
                          select o).FirstOrDefault();
            if (client == null)
            {
                return null;
            }
            using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.TransModContext(_tConnString)))
            {
                //search for customer based on OrgCode - Transmod Client and If is Customer Client yes
                //If not customer client tends to had all DLV clients as same code
                var org = uow.Organisations.Find(x => x.O_Code == client.OrganizationCode
                                        && x.O_O_PARENT == custId && x.O_IsClient == true).FirstOrDefault();
                if (org == null)
                {
                    org = new NodeData.Models.Organisation
                    {
                        O_Name = client.CompanyName,
                        O_Code = client.OrganizationCode,
                        O_Isactive = true,
                        O_O_PARENT = custId,
                        O_IsConsignee = true,
                        O_Email = client.Email,
                        O_ID = Guid.NewGuid(),
                        O_OnHold = false,
                        //check if Customer - flag client
                        O_IsClient = orgType == "SendersLocalClient" ? true : false,
                        O_EmailPOD = false
                    };

                    uow.Organisations.Add(org);
                    uow.Complete();


                }
                return org;

            }


        }

        private int GetEnum(string status, string eeType)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.TransModContext(_tConnString)))
            {
                var eType = uow.TransEnumTypes.Find(x => x.Z_Name == eeType).FirstOrDefault();
                if (eType != null)
                {
                    var e = uow.TransEnums.Find(x => x.E_Name == status && x.E_EnumType == eType.Z_Id).FirstOrDefault();
                    return e.E_Id;
                }
                return -1;
            }
        }
        private Guid ValidTransModCustomer(string senderID)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeData.Models.TransModContext(_tConnString)))
            {
                var cust = uow.Organisations.Find(x => x.O_Code == senderID && x.O_Isactive == true).FirstOrDefault();
                if (cust == null)
                    return Guid.Empty;
                bool valid = true;
                if (cust.O_TrialStart != null)
                {
                    if (cust.O_TrialStart <= DateTime.Now)
                    {
                        if (cust.O_TrialEnd != null)
                        {
                            if (cust.O_TrialEnd >= DateTime.Now)
                            {
                                valid = true;
                            }
                            else
                                valid = false;
                        }
                    }
                    else
                    {
                        valid = false;
                    }
                    if (!valid)
                    {
                        return Guid.Empty;
                    }
                }
                return cust != null ? cust.O_ID : Guid.Empty;

            }
        }


        #endregion

        #region helpers
        private XUS.Confirmation GetConfirmationFromLeg(XUS.ShipmentInstruction leginstruction)
        {
            if (leginstruction.InstructionPackingLineLinkCollection != null)
            {
                var conf = (from ip in leginstruction.InstructionPackingLineLinkCollection
                            select ip.ConfirmationCollection).FirstOrDefault();
                if (conf != null)
                {
                    return (from c in conf
                            select c).First();
                }
            }

            return null;
        }
        private XUS.PackingLine GetPackLineForLeg(XUS.ShipmentInstruction leginstruction, XUS.PackingLine[] packingLineCollection)
        {

            if (leginstruction.InstructionPackingLineLinkCollection != null)
            {
                foreach (var pl in leginstruction.InstructionPackingLineLinkCollection)
                {
                    return (from p in packingLineCollection
                            where p.Link == pl.PackingLineLink
                            select p).FirstOrDefault();

                }
            }
            return null;

        }
        private string GetJobType(XUS.CodeDescriptionPair4Char localTransportJobType)
        {
            if (localTransportJobType != null)
            {

                switch (localTransportJobType.Code.Substring(0, 2))
                {
                    case "DR": //Destination Delivery 
                        return "DELIVERY";
                        break;
                    case "OR": //Origin Pickup
                        return "PICKUP";
                        break;
                    case "ES": //Export Sea Pickup
                        return "PICKUP";
                        break;
                    case "EA": //Export Air Pickup
                        return "PICKUP";
                        break;
                    case "IS": //Import Sea Delivery
                        return "DELIVERY";
                        break;
                    case "IA": //Import Air Delivery
                        return "DELIVERY";
                        break;

                }

            }
            return string.Empty;
        }
        private string GetJobNo(XUS.DataContext dc, string jobType)
        {
            return (from job in dc.DataSourceCollection
                    where job.Type == jobType
                    select job.Key).FirstOrDefault();
        }
        #endregion



    }
}
