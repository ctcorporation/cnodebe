﻿namespace CNodeBE
{
    public interface IMailServerSettings
    {
        string UserName { get; set; }
        string Password { get; set; }
        int Port { get; set; }
        string Server { get; set; }
        string Email { get; set; }
        bool IsSecure { get; set; }

        void GetMailServer();


    }
}