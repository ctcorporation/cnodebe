﻿using RestSharp;
using System;
using System.IO;

namespace CNodeBE.Classes
{
    public class RestSendAPI
    {
        #region members
        private string _url;
        private string _endPoint;
        private string _err;
        private bool _sendResult;

        #endregion
        #region contructors
        public RestSendAPI(string url, string endPoint)
        {
            _url = url;
            _endPoint = endPoint;

        }
        #endregion
        #region properties
        public string ErrString
        {
            get
            {
                return _err;
            }
            set
            {
                _err = value;
            }
        }
        public bool APISendResult
        {
            get
            {
                return _sendResult;
            }
            set
            {
                _sendResult = value;
            }

        }

        #endregion
        #region methods
        public void SendFile(string filetoSend)
        {
            try
            {
                IRestResponse restResponse = SendviaRest(filetoSend);
                if (restResponse.StatusCode == System.Net.HttpStatusCode.OK)
                    _sendResult = true;
                else
                {
                    _sendResult = false;
                    if (restResponse != null)
                    {
                        _err = restResponse.StatusCode.ToString() + " : " + restResponse.StatusDescription + Environment.NewLine +
                       restResponse.ErrorMessage;
                    }
                }
            }
            catch (Exception ex)
            {
                _err = ex.GetType().Name + ": " + ex.Message + Environment.NewLine + ex.InnerException;
            }

        }

        private IRestResponse SendviaRest(string filetoSend)
        {
            try
            {
                var client = new RestClient(_url);
                string endPoint = _endPoint;
                if (!endPoint.StartsWith("?"))
                {
                    endPoint = "?" + endPoint;
                }
                var req = new RestRequest(endPoint, Method.POST);
                var bytes = File.ReadAllBytes(filetoSend);

                req.AddFile("file", bytes, filetoSend);
                req.AlwaysMultipartFormData = true;
                var response = client.Execute(req);
                return response;
            }
            catch (IOException ex)
            {
                _err = ex.GetType().Name + ": " + ex.Message + Environment.NewLine + ex.InnerException;
                return null;
            }
            catch (Exception ex)
            {
                _err = ex.GetType().Name + ": " + ex.Message + Environment.NewLine + ex.InnerException;
                return null;
            }

        }


        #endregion
        #region helpers

        #endregion
    }
}
