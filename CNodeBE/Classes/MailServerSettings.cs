﻿namespace CNodeBE
{
    public class MailServerSettings : IMailServerSettings
    {
        public string Server
        {
            get;
            set;
        }
        public int Port
        {
            get;
            set;
        }
        public string UserName
        {
            get;
            set;
        }
        public string Username
        {
            get;
            set;
        }
        public string Password
        {
            get;
            set;
        }
        public bool IsSecure { get; set; }

        public string Email { get; set; }

        public void GetMailServer()
        {
            throw new System.NotImplementedException();
        }
    }
}
