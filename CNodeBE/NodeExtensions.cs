﻿namespace CNodeBE
{
    class NodeExtensions
    {

    }
    public static class StringEx
    {
        public static string Truncate(this string value, int maxLength)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }

            return value.Length <= maxLength ? value : value.Substring(0, maxLength);
        }

        public static string RemoveChars(this string value, string stringToFind)
        {
            if (value.Contains(stringToFind))
            {
                var idx = value.IndexOf(stringToFind);
                value = value.Remove(idx, stringToFind.Length);

            }
            return value;
        }
    }
}
