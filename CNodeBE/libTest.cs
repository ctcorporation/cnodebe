﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;
using XUE;

namespace CNodeBE
{
    class libTest
    {
        public static string DoConvert(String fileName, String senderid, String recipientid, String outputpath)
        {
            ActivityUpdate swiftActivity = new ActivityUpdate();
            string result = string.Empty;
            string xmlPath = Path.GetDirectoryName(fileName);
            if (Directory.Exists(xmlPath))
            {
                XmlSerializer xmlSwift = new XmlSerializer(typeof(ActivityUpdate));
                //ContainerEvents silkFile;
                StreamReader sr = new StreamReader(fileName);
                String xmlSource = sr.ReadToEnd();
                sr.Close();
                sr.Dispose();
                try
                {
                    using (StringReader reader = new StringReader(xmlSource))
                    {
                        swiftActivity = (ActivityUpdate)xmlSwift.Deserialize(reader);
                        reader.Close();
                        reader.Dispose();
                    }

                }
                catch (Exception ex)
                {
                    return "Invalid XSD. Checking Next";
                }

                for (int i = 0; i < swiftActivity.ContainerActivity.Count(); i++)
                {
                    try
                    {
                        XUE.UniversalInterchange interchange = new XUE.UniversalInterchange();
                        XUE.UniversalInterchangeHeader header = new XUE.UniversalInterchangeHeader();
                        header.SenderID = senderid;
                        header.RecipientID = recipientid;
                        interchange.Header = header;
                        interchange.version = "1.1";
                        XUE.UniversalInterchangeBody body = new XUE.UniversalInterchangeBody();
                        XUE.UniversalEventData bodydata = new XUE.UniversalEventData();
                        List<XUE.UniversalEventData> silkEventColl = new List<XUE.UniversalEventData>();
                        XUE.UniversalEventData silkEvent = new XUE.UniversalEventData();
                        silkEvent.version = "1.1";
                        silkEvent.xmlnsa = "http://www.cargowise.com/Schemas/Universal/2011/11";
                        XUE.Event silkevent = new XUE.Event();
                        XUE.DataContext dc = new XUE.DataContext();
                        List<XUE.DataTarget> dtColl = new List<XUE.DataTarget>();
                        XUE.DataTarget dt = new XUE.DataTarget();
                        dt.Type = "ForwardingShipment";
                        List<ActivityUpdateContainerActivity> updateContainerActivityColl = new List<ActivityUpdateContainerActivity>();
                        ActivityUpdateContainerActivity updateContainerActivity = new ActivityUpdateContainerActivity();
                        updateContainerActivity.ContainerNo = swiftActivity.ContainerActivity[i].ContainerNo;
                        dt.Key = swiftActivity.ContainerActivity[i].CustomerRef;
                        dtColl.Add(dt);
                        dc.DataTargetCollection = dtColl.ToArray();
                        silkevent.DataContext = dc;

                        silkevent.EventTime = swiftActivity.ContainerActivity[i].RequiredDate + "T" + swiftActivity.ContainerActivity[i].RequiredTime;
                        silkevent.IsEstimate = false;
                        switch (swiftActivity.ContainerActivity[i].ActivityCode)
                        {
                            case "3310":
                                silkevent.EventType = "ESC";

                                break;
                            case "HL013":
                                silkevent.EventType = "DCF";
                                break;
                            case "HL009":
                                silkevent.EventType = "GIY";
                                break;
                            case "HL002":
                                silkevent.EventType = "GOY";
                                break;
                            case "HL011":
                                silkevent.EventType = "CPI";
                                break;
                            case "HL012":
                                silkevent.EventType = "CPO";
                                break;
                            default:
                                // throw new ConversionException("Event Type not mapped");
                                break;




                        }
                        List<Context> ctColl = new List<Context>();
                        Context ct = new Context();
                        ContextType ctype = new ContextType();
                        ctype.Value = "ContainerNumber";
                        ct.Type = ctype;
                        //ct.Type = (ContextType)"ContainerNumber";
                        ct.Value = swiftActivity.ContainerActivity[i].ContainerNo;
                        ctColl.Add(ct);
                        silkevent.ContextCollection = ctColl.ToArray();
                        silkEvent.Event = silkevent;
                        silkEventColl.Add(silkEvent);
                        bodydata.Event = silkevent;
                        body.BodyField = bodydata;
                        bodydata.version = "1.1";
                        interchange.Body = body;
                        String cwXML = Path.Combine(outputpath, "SWIFT" + swiftActivity.ContainerActivity[i].ContainerNo + swiftActivity.ContainerActivity[i].CustomerRef + ".xml");
                        int iFileCount = 0;
                        while (File.Exists(cwXML))
                        {
                            iFileCount++;
                            cwXML = Path.Combine(outputpath, "SWIFT" + swiftActivity.ContainerActivity[i].ContainerNo + swiftActivity.ContainerActivity[i].CustomerRef + iFileCount + ".xml");
                        }
                        Stream outputSilk = File.Open(cwXML, FileMode.Create);
                        StringWriter writer = new StringWriter();
                        XmlSerializer xSer = new XmlSerializer(typeof(XUE.UniversalInterchange));
                        xSer.Serialize(outputSilk, interchange);
                        outputSilk.Flush();
                        outputSilk.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        //throw new ConversionException(ex.Message);
                        return (ex.Message);
                    }
                    sr.Close();
                }
                return "Conversion Complete";
            }
            else
            {

                MessageBox.Show("XML Path not found");
                return ("XML Path not found");

                // throw new ConversionException("XML Path not found");
            }

        }
    }
}
