﻿using Ionic.Zip;
using NodeData.Models;
using System;
using System.IO;
using System.Linq;

namespace CNodeBE
{

    public class Globals
    {
        private static string heartbeat;

        public static string AppPath { get; set; }
        public static string Pdf2Xml { get; set; }

        public static string AppLogPath { get; set; }
        public static string AlertsTo { get; set; }
        public static string ServerName { get; set; }
        public static string CustomReceive { get; set; }
        public static string DbConn { get; set; }

        public static string TestingLoc { get; set; }
        public static string DataBase { get; set; }

        public static int PollTime { get; set; }
        public static string UserName { get; set; }
        public static string Password { get; set; }

        public static string TransServer { get; set; }
        public static string TransDatabase { get; set; }

        public static string TransUsername { get; set; }

        public static string TransPassword { get; set; }
        public static int CycleCount { get; set; }
        public static int MainTimerInt { get; set; }

        public static string connString()
        {
            string sConn = "Data Source=" + ServerName + ";Initial Catalog=" + DataBase + ";User ID = " + UserName + ";Password=" + Password + ";";
            return sConn;
        }

        public static string connTransString()
        {
            string sConn = "Data Source=" + TransServer + ";Initial Catalog=" + TransDatabase + ";User ID = " + TransUsername + ";Password=" + TransPassword + ";";
            return sConn;
        }
        public static string AppConfig { get; set; }

        public static string CustomerFolder { get; set; }

        public static string XMLLoc { get; set; }

        public static string RootPath { get; set; }

        public static string ArchLoc { get; set; }
        public static string ArcFrequency { get; set; }

        public static string ReportLoc { get; set; }

        public static string ArchiveOptions { get; set; }

        public static string FailLoc { get; set; }

        public static int GuardianPollTime { get; set; }
        public static string WinSCPLocation { get; internal set; }

        static int GetWeekNumberOfMonth(DateTime date)
        {
            date = date.Date;
            DateTime firstMonthDay = new DateTime(date.Year, date.Month, 1);
            DateTime firstMonthMonday = firstMonthDay.AddDays((DayOfWeek.Monday + 7 - firstMonthDay.DayOfWeek) % 7);
            if (firstMonthMonday > date)
            {
                firstMonthDay = firstMonthDay.AddMonths(-1);
                firstMonthMonday = firstMonthDay.AddDays((DayOfWeek.Monday + 7 - firstMonthDay.DayOfWeek) % 7);
            }
            return (date - firstMonthMonday).Days / 7 + 1;
        }


        public static String ArchiveFile(vw_CustomerProfile cust, string fileName)
        {
            String archive = "";
            String archiveName = cust.P_SENDERID + DateTime.Now.ToString("MMM-yyyy");
            string arcPath = Path.Combine(cust.C_PATH, "Archive");
            try
            {

                if (!File.Exists(Path.Combine(arcPath, archiveName + ".ZIP")))
                {
                    using (ZipFile ZipNew = new ZipFile())
                    {
                        String newArchive = Path.Combine(arcPath, archiveName + ".ZIP");
                        ZipNew.AddFile(fileName, "");
                        ZipNew.Save(Path.Combine(newArchive));
                        archive = newArchive;
                        ZipNew.Dispose();
                    }
                }
                else
                {
                    var zipFile = ZipFile.Read(@Path.Combine(arcPath, archiveName + ".ZIP"));
                    var result = zipFile.Any(entry => entry.FileName.EndsWith(Path.GetFileName(fileName)));
                    zipFile.Dispose();
                    if (!result)
                    {
                        using (ZipFile Zip1 = ZipFile.Read(@Path.Combine(arcPath, archiveName + ".ZIP")))
                        {

                            try
                            {
                                Zip1.AddFile(fileName, "");
                                Zip1.Save(@Path.Combine(arcPath, archiveName + ".ZIP"));
                                Zip1.Save();
                                archive = Path.Combine(arcPath, archiveName + ".ZIP");
                            }
                            catch (Exception)
                            {

                            }
                            Zip1.Dispose();
                        }
                    }
                }
                //   File.Delete(fileName);
            }
            catch (Exception)
            {
            }

            return Path.GetFileName(archive);
        }
        public static string ArchiveFile(string arcPath, string fileName)
        {
            string archive = "";
            string archiveName = "CTCNODE";
            switch (Globals.ArcFrequency)
            {
                case "Daily":
                    archiveName = DateTime.Now.ToString("ddd-dd-MM-yy");
                    break;
                case "Weekly":
                    archiveName = "Week-" + GetWeekNumberOfMonth(DateTime.Now).ToString() + string.Format("{0:MMMyyyy}", DateTime.Now);
                    break;
                case "Monthly":
                    archiveName = DateTime.Now.ToString("MMM-yyyy");
                    break;
            }

            try
            {

                if (!File.Exists(Path.Combine(arcPath, archiveName + ".ZIP")))
                {
                    using (ZipFile ZipNew = new ZipFile())
                    {
                        string newArchive = Path.Combine(arcPath, archiveName + ".ZIP");
                        ZipNew.AddFile(fileName, "");
                        ZipNew.Save(Path.Combine(newArchive));
                        archive = newArchive;
                        ZipNew.Dispose();

                    }
                }
                else
                {
                    var zipFile = ZipFile.Read(@Path.Combine(arcPath, archiveName + ".ZIP"));
                    var result = zipFile.Any(entry => entry.FileName.EndsWith(Path.GetFileName(fileName)));
                    zipFile.Dispose();
                    if (!result)
                    {
                        using (ZipFile Zip1 = ZipFile.Read(@Path.Combine(arcPath, archiveName + ".ZIP")))
                        {

                            try
                            {
                                Zip1.AddFile(fileName, "");
                                Zip1.Save(@Path.Combine(arcPath, archiveName + ".ZIP"));
                                Zip1.Save();
                                archive = Path.Combine(arcPath, archiveName + ".ZIP");
                            }
                            catch (Exception)
                            {

                            }
                            Zip1.Dispose();

                        }
                    }
                }
                //   File.Delete(fileName);
            }
            catch (Exception)
            {
            }

            return Path.GetFileName(archive);
        }
    }
}
